/*
 * This file is part of tango-plugin.
 * 
 * tango-plugin is free software: you can redistribute it and/or modify it under the terms of the GNU General Public
 * License as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later
 * version.
 * 
 * tango-plugin is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the
 * implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for more
 * details.
 * 
 * You should have received a copy of the GNU General Public License along with tango-plugin. If not, see
 * <https://www.gnu.org/licenses/>.
 */
package org.cdma.gui.databrowser.impl.tango;

import java.awt.BorderLayout;
import java.awt.Color;
import java.util.List;

import javax.swing.BoxLayout;
import javax.swing.JComboBox;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JTextField;
import javax.swing.WindowConstants;
import javax.swing.event.DocumentEvent;
import javax.swing.event.DocumentListener;
import javax.swing.plaf.basic.BasicComboBoxEditor;

import org.jdesktop.swingx.autocomplete.AutoCompleteDecorator;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.tango.utils.DevFailedUtils;
import org.tango.utils.TangoUtil;

import fr.esrf.Tango.DevFailed;
import fr.esrf.TangoApi.Database;
import fr.soleil.comete.bean.tangotree.IDeviceSelector;
import fr.soleil.comete.bean.tangotree.datasource.DatabaseTangoSourceDevice;
import fr.soleil.comete.bean.tangotree.datasource.ISourceDevice;
import fr.soleil.comete.bean.tangotree.swing.SwingDeviceSelector;
import fr.soleil.comete.definition.widget.IComponent;
import fr.soleil.comete.swing.Panel;
import fr.soleil.comete.tango.data.service.helper.TangoDeviceHelper;
import fr.soleil.lib.project.swing.text.ErrorTextField;

public class DevicePanel extends Panel implements DocumentListener {

    private static final long serialVersionUID = -2772498705478353256L;

    private static final Logger LOGGER = LoggerFactory.getLogger(DevicePanel.class);

    private static final String JOKER = "*";

    private Database database = null;

    private String typedDeviceName = null;

    private JComboBox<String> deviceCombo = null;
    private IDeviceSelector tangoSelector = null;
    protected ErrorTextField deviceComboEditor = null;

    public DevicePanel() {
        super();

        initComponents();
        layoutComponents();
    }

    private void initComponents() {
        database = TangoDeviceHelper.getDatabase();

        deviceCombo = new JComboBox<>();
        fillAliases();
        deviceCombo.setSelectedItem(null);
        deviceCombo.setEditable(true);// must be set before decorator
        AutoCompleteDecorator.decorate(deviceCombo);

        deviceComboEditor = new ErrorTextField();
        deviceComboEditor.setErrorColor(Color.PINK);
        deviceComboEditor.setErrorTooltip(TangoDataSourceSeeker.MESSAGES.getString("Device.Name.Tooltip.Error"));
        deviceCombo.setEditor(new BasicComboBoxEditor() {
            @Override
            protected JTextField createEditorComponent() {
                return deviceComboEditor;
            }
        });
        deviceComboEditor.getDocument().addDocumentListener(this);
    }

    private void layoutComponents() {

        JLabel deviceLabel = new JLabel(TangoDataSourceSeeker.MESSAGES.getString("Device.Name"));
        setLayout(new BorderLayout());

        try {
            ISourceDevice tangoSource = new DatabaseTangoSourceDevice(true, true);
            tangoSelector = new SwingDeviceSelector(tangoSource, true, true);

            IComponent selectorComponent = tangoSelector.getSelectorComponent();
            add(selectorComponent, BorderLayout.CENTER);

        } catch (Exception e) {
            LOGGER.error("Source creation error {}", e.getMessage());
            LOGGER.debug("Source creation error {}", e);
        }
        JPanel panelDevice = new JPanel();
        panelDevice.setLayout(new BoxLayout(panelDevice, BoxLayout.PAGE_AXIS));
        panelDevice.add(deviceLabel);
        panelDevice.add(deviceCombo);

        add(panelDevice, BorderLayout.SOUTH);

    }

    @Override
    public void removeUpdate(final DocumentEvent e) {
        onTextChange();
    }

    @Override
    public void insertUpdate(final DocumentEvent e) {
        onTextChange();
    }

    @Override
    public void changedUpdate(final DocumentEvent e) {
        onTextChange();
    }

    private void onTextChange() {
        typedDeviceName = null;
        String deviceName = deviceComboEditor.getText();
        if (!deviceName.isEmpty()) {
            String completeDeviceName = deviceName;
            if (deviceName.indexOf("/") < 0) {// It is an alias
                try {
                    completeDeviceName = TangoUtil.getfullNameForDevice(deviceName);
                } catch (DevFailed e) {
                    completeDeviceName = deviceName;
                    LOGGER.error("Cannot get full name for device {} because {}", deviceName,
                            DevFailedUtils.toString(e));
                    LOGGER.debug("Stack trace", e);
                }
            }
            if (TangoDeviceHelper.isDeviceRunning(completeDeviceName)) {
                deviceComboEditor.setInError(false);
                typedDeviceName = deviceName;
            } else {
                deviceComboEditor.setInError(true);
            }
        } else {
            deviceComboEditor.setInError(false);
        }
    }

    public List<String> getSelectedDevices() {
        return tangoSelector.getSelectedDevices();
    }

    public String getTypedDeviceName() {
        return typedDeviceName;
    }

    public void clearPanel() {
        // TODO
        // Empty Tree selection
        tangoSelector.clearSelectedDevices();

        // TODO update list of aliases?

        deviceCombo.setSelectedItem(null);
        deviceComboEditor.setInError(false);
        typedDeviceName = null;
    }

    private void fillAliases() {
        if (database != null) {
            deviceCombo.removeAllItems();
            try {
                String[] devicesAliases = database.get_device_alias_list(JOKER);
                if (devicesAliases != null) {
                    for (String alias : devicesAliases) {
                        deviceCombo.addItem(alias);
                    }
                }
            } catch (DevFailed e) {
                LOGGER.error("Cannot get all device alias list because {}", DevFailedUtils.toString(e));
                LOGGER.debug("Stack trace", e);
            } catch (Exception e) {
                LOGGER.error("Cannot get all device alias list because {}", e.getMessage());
                LOGGER.debug("Stack trace", e);
            }
        }
    }

    public static void main(final String[] args) {
        DevicePanel devicePanel = new DevicePanel();

        JFrame frame = new JFrame("Test device panel");
        frame.setDefaultCloseOperation(WindowConstants.EXIT_ON_CLOSE);

        frame.setContentPane(devicePanel);

        frame.pack();
        frame.setLocationRelativeTo(null);
        frame.setVisible(true);
    }

}
