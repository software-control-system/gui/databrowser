/*
 * This file is part of tango-plugin.
 * 
 * tango-plugin is free software: you can redistribute it and/or modify it under the terms of the GNU General Public
 * License as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later
 * version.
 * 
 * tango-plugin is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the
 * implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for more
 * details.
 * 
 * You should have received a copy of the GNU General Public License along with tango-plugin. If not, see
 * <https://www.gnu.org/licenses/>.
 */
package org.cdma.gui.databrowser.impl.tango;

import java.io.File;
import java.util.ArrayList;
import java.util.List;
import java.util.ResourceBundle;

import javax.swing.JComponent;
import javax.swing.JTabbedPane;

import org.cdma.gui.databrowser.impl.AbstractFileDataSourceSeeker;

import fr.soleil.comete.tango.data.service.TangoDataSourceFactory;
import fr.soleil.comete.tango.data.service.TangoKey;
import fr.soleil.comete.tango.data.service.TangoKeyTool;
import fr.soleil.data.service.BasicKey;
import fr.soleil.data.service.IKey;
import fr.soleil.lib.project.file.FileUtils;

public class TangoDataSourceSeeker extends AbstractFileDataSourceSeeker {

    public static final ResourceBundle MESSAGES = ResourceBundle
            .getBundle("org.cdma.gui.databrowser.impl.tango.messages");

    private static final String SUPPORTED_EXTENSION = "txt";
    private static final String FILE_DESCRIPTION = MESSAGES.getString("ATKTuningFiles.filterDescription");

    private JTabbedPane tabbedPane;
    private DevicePanel devicePanel;

    public TangoDataSourceSeeker() {
        super(null);
    }

    @Override
    public String getDataSourceBrowserId() {
        return null;
    }

    private void initPanel() {
        // File Chooser creation
        initFileChooser();

        // Device panel creation
        devicePanel = new DevicePanel();

        tabbedPane = new JTabbedPane();
        tabbedPane.add(MESSAGES.getString("Device.TabName"), devicePanel);
        tabbedPane.add(MESSAGES.getString("ATKTuningFiles.TabName"), fileChooser);
    }

    @Override
    public void cleanComponent() {
        if (tabbedPane != null) {
            devicePanel.clearPanel();
            cleanFileChooser();
        }
    }

    @Override
    public JComponent getComponent() {
        if (tabbedPane == null) {
            initPanel();
        }
        return tabbedPane;
    }

    @Override
    public List<IKey> getResult() {
        List<IKey> result = new ArrayList<>();

        if (tabbedPane != null) {
            File[] selectedFiles = fileChooser.getSelectedFiles();
            for (File selectedFile : selectedFiles) {
                IKey key = createKeyFromFile(selectedFile);
                result.add(key);
            }

            List<String> devices = devicePanel.getSelectedDevices();
            TangoKey tangoKey = null;
            if (devices != null) {
                for (String selectedDeviceName : devices) {
                    tangoKey = new TangoKey();
                    TangoKeyTool.registerDeviceName(tangoKey, selectedDeviceName);
                    result.add(tangoKey);
                }
            }

            String typedDeviceName = devicePanel.getTypedDeviceName();
            if (typedDeviceName != null) {
                tangoKey = new TangoKey();
                TangoKeyTool.registerDeviceName(tangoKey, typedDeviceName);
                result.add(tangoKey);
            }
        }
        if (result.isEmpty()) {
            result = null;
        }
        return result;
    }

    public static IKey createKeyFromFile(final File file) {
        BasicKey key = new BasicKey(TangoDataSourceFactory.SOURCE_PRODUCER_ID);
        key.setInformationKey(file.getAbsolutePath());
        return key;
    }

    public static boolean canRead(final File file) {
        return (file != null) && file.canRead()
                && (file.isDirectory() || SUPPORTED_EXTENSION.equalsIgnoreCase(FileUtils.getExtension(file)));
    }

    @Override
    public boolean acceptFile(File file) {
        return canRead(file);
    }

    @Override
    protected String getFileDescription() {
        return FILE_DESCRIPTION;
    }

}
