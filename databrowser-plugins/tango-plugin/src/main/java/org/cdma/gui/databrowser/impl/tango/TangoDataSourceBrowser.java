/*
 * This file is part of tango-plugin.
 * 
 * tango-plugin is free software: you can redistribute it and/or modify it under the terms of the GNU General Public
 * License as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later
 * version.
 * 
 * tango-plugin is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the
 * implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for more
 * details.
 * 
 * You should have received a copy of the GNU General Public License along with tango-plugin. If not, see
 * <https://www.gnu.org/licenses/>.
 */
package org.cdma.gui.databrowser.impl.tango;

import java.io.File;
import java.io.IOException;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;

import org.cdma.gui.databrowser.exception.BadFileException;
import org.cdma.gui.databrowser.exception.DataBrowserException;
import org.cdma.gui.databrowser.impl.AbstractDataSourceProducerBrowser;
import org.cdma.gui.databrowser.interfaces.AxisType;
import org.cdma.gui.databrowser.interfaces.DataFormat;
import org.cdma.gui.databrowser.interfaces.DataType;
import org.cdma.gui.databrowser.interfaces.IDataSourceSeeker;
import org.cdma.gui.databrowser.view.tree.GroupTreeNode;
import org.cdma.gui.databrowser.view.tree.ItemTreeNode;
import org.cdma.gui.databrowser.view.tree.SourceTreeNode;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.tango.utils.TangoUtil;

import fr.esrf.Tango.AttrDataFormat;
import fr.esrf.Tango.DevFailed;
import fr.esrf.Tango.DevInfo;
import fr.esrf.TangoApi.AttributeInfo;
import fr.esrf.TangoApi.Database;
import fr.esrf.TangoApi.DeviceProxy;
import fr.soleil.comete.definition.widget.IChartViewer;
import fr.soleil.comete.definition.widget.properties.ChartProperties;
import fr.soleil.comete.definition.widget.properties.PlotProperties;
import fr.soleil.comete.definition.widget.util.ITreeNode;
import fr.soleil.comete.tango.data.service.TangoDataSourceFactory;
import fr.soleil.comete.tango.data.service.TangoKey;
import fr.soleil.comete.tango.data.service.TangoKeyTool;
import fr.soleil.comete.tango.data.service.helper.TangoAttributeHelper;
import fr.soleil.comete.tango.data.service.helper.TangoConstHelper;
import fr.soleil.comete.tango.data.service.helper.TangoDeviceHelper;
import fr.soleil.comete.tango.data.service.helper.TangoExceptionHelper;
import fr.soleil.data.service.BasicKey;
import fr.soleil.data.service.DataSourceProducerProvider;
import fr.soleil.data.service.HistoryKey;
import fr.soleil.data.service.IKey;
import fr.soleil.data.service.PolledRefreshingStrategy;
import fr.soleil.data.source.HistoryDataSourceProducer;

public class TangoDataSourceBrowser extends AbstractDataSourceProducerBrowser {

    private static final Logger LOGGER = LoggerFactory.getLogger(TangoDataSourceBrowser.class);
    public static final String ID = TangoDataSourceFactory.SOURCE_PRODUCER_ID;
    private final TangoDataSourceSeeker seeker = new TangoDataSourceSeeker();
    private static final Map<String, Boolean> DEVICE_RUNNING_MAP = new ConcurrentHashMap<>();
    private static final Map<IKey, TangoFile> TANGO_FILE_MAP = new ConcurrentHashMap<>();
    private static final String TANGO_HOST = "TANGO_HOST";

    public TangoDataSourceBrowser() {
        super(TangoDataSourceFactory.class);
        DataSourceProducerProvider.pushNewProducer(HistoryDataSourceProducer.class);
        HistoryDataSourceProducer historicProducer = (HistoryDataSourceProducer) DataSourceProducerProvider
                .getProducer(HistoryDataSourceProducer.ID);
        if (historicProducer != null) {
            historicProducer.setDefaultRefreshingStrategy(new PolledRefreshingStrategy(1000));
        }
    }

    @Override
    public boolean isSourceBrowserEnabled() {
        boolean enable = false;
        if (super.isSourceBrowserEnabled()) {
            enable = isTangoEnabled();
        }
        return enable;
    }

    public static boolean isTangoEnabled() {
        boolean enable = false;
        String tangoHost = System.getProperty(TANGO_HOST);
        if ((tangoHost == null) || tangoHost.isEmpty()) {
            tangoHost = System.getenv(TANGO_HOST);
        }

        if ((tangoHost != null) && !tangoHost.isEmpty()) {
            enable = TangoDeviceHelper.getDatabase() != null;
        }
        return enable;
    }

    @Override
    public String getId() {
        return ID;
    }

    @Override
    public String getDescription() {
        return "This browser can read Tango devices and attributes or atktunning files";
    }

    @Override
    public boolean canRead(final String sourcePath) {
        // first tests for atk tuning file
        boolean canRead = TangoDataSourceSeeker.canRead(new File(sourcePath));

        // then tests for a device name
        if (!canRead) {
            if ((sourcePath != null) && !sourcePath.isEmpty()) {
                String completeDeviceName = sourcePath;
                if (sourcePath.indexOf("/") < 0) {// It is an alias
                    try {
                        completeDeviceName = TangoUtil.getfullNameForDevice(sourcePath);
                    } catch (DevFailed e) {
                        LOGGER.debug(String.format("Cannot read %s because %s", sourcePath,
                                TangoExceptionHelper.getErrorMessage(e)), e);
                        completeDeviceName = sourcePath;
                    }
                }
                canRead = TangoDeviceHelper.isDeviceRunning(completeDeviceName);
            }
        }
        return canRead;
    }

    @Override
    public boolean canRead(final IKey key) {
        boolean canRead;
        IKey currentKey = getAdaptedKey(key);
        canRead = currentKey instanceof TangoKey;
        if ((!canRead) && (currentKey instanceof HistoryKey) && (currentKey.getInformationKey() != null)) {
            canRead = canRead(currentKey.getInformationKey());
        } else if (key instanceof BasicKey) {
            try {
                File tmp = new File(key.getInformationKey());
                canRead = tmp.canRead() && !tmp.isDirectory();
            } catch (Exception e) {
                canRead = false;
            }
        }
        return canRead;
    }

    @Override
    public IKey createKeyFromSource(final String sourcePath) {
        IKey result = null;
        File file = new File(sourcePath);
        if (file.exists()) {
            result = TangoDataSourceSeeker.createKeyFromFile(file);
        } else {
            result = new TangoKey();
            TangoKeyTool.registerDeviceName(result, sourcePath);
        }
        return result;
    }

    @Override
    public String getSourceFromKey(final IKey key) {
        String sourceId = null;
        if (key != null) {
            if (key instanceof TangoKey) {
                String deviceName = TangoKeyTool.getDeviceName(key);
                if (TangoDeviceHelper.isDeviceRunning(deviceName)) {
                    sourceId = deviceName;
                    DeviceProxy proxy = TangoDeviceHelper.getDeviceProxy(deviceName);
                    if (proxy != null) {
                        try {
                            String alias = proxy.get_alias();
                            // If the name is an alias
                            if ((alias != null) && alias.equalsIgnoreCase(deviceName)) {
                                sourceId = proxy.get_db_obj().get_device_from_alias(alias);
                            }
                        } catch (Exception e) {
                            LOGGER.debug(String.format("Cannot get Source From Key %s because %s", deviceName,
                                    TangoExceptionHelper.getErrorMessage(e)), e);
                        }
                    }
                }
            } else {
                sourceId = key.getInformationKey();
            }
        }
        return sourceId;
    }

    @Override
    public Map<String, String> getInformations(final IKey key) {
        Map<String, String> infoMap = null;
        IKey currentKey = getAdaptedKey(key);
        if (currentKey instanceof TangoKey) {
            String alias = getAlias(currentKey);
            String deviceName = TangoKeyTool.getDeviceName(currentKey);
            if (TangoKeyTool.isAnAttribute(currentKey)) {
                infoMap = super.getInformations(key);
                infoMap.put(LABEL_ATTRIBUTE + "Label", getLabel(currentKey));
                infoMap.put(LABEL_ATTRIBUTE + "Description", getTangoDescription(currentKey));
                infoMap.put(LABEL_ATTRIBUTE + "Writable", Boolean.toString(generateSettableKey(currentKey) != null));
                infoMap.put(LABEL_ATTRIBUTE + "Unit", getUnit(currentKey));
                infoMap.put(LABEL_ATTRIBUTE + "Format", getNumericalFormat(currentKey));
                infoMap.put(LABEL_ATTRIBUTE + "Max alarm", getMaxAlarm(currentKey));
                infoMap.put(LABEL_ATTRIBUTE + "Min value", getMinValue(currentKey));
                infoMap.put(LABEL_ATTRIBUTE + "Max value", getMaxValue(currentKey));
                infoMap.put(LABEL_ATTRIBUTE + "Min alarm", getMinAlarm(currentKey));
                infoMap.put(LABEL_ATTRIBUTE + "Shape", Arrays.toString(getShape(currentKey)));
                if (alias != null) {
                    infoMap.put(LABEL_ATTRIBUTE + "Alias", alias);
                }
            } else if (TangoDeviceHelper.isDeviceRunning(deviceName)) {
                // It is a device Node
                infoMap = new HashMap<>();
                DeviceProxy proxy = TangoDeviceHelper.getDeviceProxy(deviceName);
                if (proxy != null) {
                    try {
                        infoMap.put("Name", TangoUtil.getfullNameForDevice(deviceName));
                    } catch (Exception e) {
                        LOGGER.debug(String.format("Cannot get alias for device %s because %s", deviceName,
                                TangoExceptionHelper.getErrorMessage(e)), e);
                    }

                    if (alias != null) {
                        infoMap.put("Alias", alias);
                    }

                    try {
                        infoMap.put("host_name", proxy.get_host_name());
                        infoMap.put("idl_version", String.valueOf(proxy.get_idl_version()));
                    } catch (DevFailed e) {
                        LOGGER.error(String.format("Cannot get informations for device %s because %s", deviceName,
                                TangoExceptionHelper.getErrorMessage(e)), e);
                    }

                    try {
                        DevInfo devInfo = proxy.info();
                        infoMap.put("dev_class", devInfo.dev_class);
                        infoMap.put("doc_url", devInfo.doc_url);
                        infoMap.put("server_id", devInfo.server_id);
                    } catch (DevFailed e) {
                        LOGGER.error(String.format("Cannot get informations for device %s because %s", deviceName,
                                TangoExceptionHelper.getErrorMessage(e)), e);
                    }
                }
            }
        }
        return infoMap;
    }

    private static String getLabel(final IKey key) {
        String label = getTangoLabel(key);
        if ((label == null) && (key != null) && (key instanceof TangoKey)) {
            String deviceName = TangoKeyTool.getDeviceName(key);
            if (TangoKeyTool.isAnAttribute(key)) {
                String attributeName = TangoKeyTool.getAttributeName(key);
                label = deviceName + "/" + attributeName;
            }
        }
        return label;
    }

    private static String getTangoLabel(final IKey key) {
        String label = null;
        if ((key != null) && (key instanceof TangoKey)) {
            String deviceName = TangoKeyTool.getDeviceName(key);
            if (TangoKeyTool.isAnAttribute(key)) {
                String attributeName = TangoKeyTool.getAttributeName(key);
                AttributeInfo attributeInfo = TangoAttributeHelper.getAttributeInfo(deviceName, attributeName);
                if (attributeInfo != null) {
                    label = attributeInfo.label;
                }
            }
        }

        return label;
    }

    private static String getTangoDescription(final IKey key) {
        String description = "None";
        if (key instanceof TangoKey) {
            String deviceName = TangoKeyTool.getDeviceName(key);
            if (TangoKeyTool.isAnAttribute(key)) {
                String attributeName = TangoKeyTool.getAttributeName(key);
                AttributeInfo attributeInfo = TangoAttributeHelper.getAttributeInfo(deviceName, attributeName);

                if (attributeInfo != null) {
                    description = attributeInfo.description;
                }
            }
        }

        return description;
    }

    private static String getUnit(final IKey key) {
        String unit = "None";
        if ((key != null) && (key instanceof TangoKey)) {
            String deviceName = TangoKeyTool.getDeviceName(key);
            if (TangoKeyTool.isAnAttribute(key)) {
                String attributeName = TangoKeyTool.getAttributeName(key);
                AttributeInfo attributeInfo = TangoAttributeHelper.getAttributeInfo(deviceName, attributeName);

                if (attributeInfo != null) {
                    unit = attributeInfo.unit;
                }
            }
        }

        return unit;
    }

    private static String getMinValue(final IKey key) {
        String minValue = "None";
        if ((key != null) && (key instanceof TangoKey)) {
            String deviceName = TangoKeyTool.getDeviceName(key);
            if (TangoKeyTool.isAnAttribute(key)) {
                String attributeName = TangoKeyTool.getAttributeName(key);
                AttributeInfo attributeInfo = TangoAttributeHelper.getAttributeInfo(deviceName, attributeName);

                if (attributeInfo != null) {
                    minValue = attributeInfo.min_value;
                }
            }
        }

        return minValue;
    }

    private static String getMaxValue(final IKey key) {
        String maxValue = "None";
        if ((key != null) && (key instanceof TangoKey)) {
            String deviceName = TangoKeyTool.getDeviceName(key);
            if (TangoKeyTool.isAnAttribute(key)) {
                String attributeName = TangoKeyTool.getAttributeName(key);
                AttributeInfo attributeInfo = TangoAttributeHelper.getAttributeInfo(deviceName, attributeName);

                if (attributeInfo != null) {
                    maxValue = attributeInfo.max_value;
                }
            }
        }

        return maxValue;
    }

    private static String getMinAlarm(final IKey key) {
        String minValue = "None";
        if ((key != null) && (key instanceof TangoKey)) {
            String deviceName = TangoKeyTool.getDeviceName(key);
            if (TangoKeyTool.isAnAttribute(key)) {
                String attributeName = TangoKeyTool.getAttributeName(key);
                AttributeInfo attributeInfo = TangoAttributeHelper.getAttributeInfo(deviceName, attributeName);

                if (attributeInfo != null) {
                    minValue = attributeInfo.min_alarm;
                }
            }
        }

        return minValue;
    }

    private static String getMaxAlarm(final IKey key) {
        String maxValue = "None";
        if ((key != null) && (key instanceof TangoKey)) {
            String deviceName = TangoKeyTool.getDeviceName(key);
            if (TangoKeyTool.isAnAttribute(key)) {
                String attributeName = TangoKeyTool.getAttributeName(key);
                AttributeInfo attributeInfo = TangoAttributeHelper.getAttributeInfo(deviceName, attributeName);

                if (attributeInfo != null) {
                    maxValue = attributeInfo.max_alarm;
                }
            }
        }

        return maxValue;
    }

    @Override
    public DataType getKeyType(final IKey key) {
        DataType type = DataType.TREE;
        IKey currentKey = getAdaptedKey(key);
        if (currentKey instanceof TangoKey) {
            String deviceName = TangoKeyTool.getDeviceName(currentKey);
            if (TangoKeyTool.isAnAttribute(currentKey)) {
                String attributeName = TangoKeyTool.getAttributeName(currentKey);
                int tangoType = TangoAttributeHelper.getAttributeType(deviceName, attributeName);
                switch (tangoType) {
                    case AttrDataFormat._SCALAR:
                        if (key instanceof HistoryKey) {
                            type = DataType.SPECTRUM;

                        } else {
                            type = DataType.SCALAR;
                        }
                        break;

                    case AttrDataFormat._SPECTRUM:
                        type = DataType.SPECTRUM;
                        break;

                    case AttrDataFormat._IMAGE:
                        type = DataType.IMAGE;
                        break;
                }

            } else if ((deviceName != null) && !deviceName.isEmpty() && TangoDeviceHelper.isDeviceRunning(deviceName)) {
                type = DataType.TREE;
            }
        }
        return type;
    }

    private IKey getAdaptedKey(IKey key) {
        IKey currentKey = key;
        if (currentKey instanceof HistoryKey) {
            currentKey = ((HistoryKey) currentKey).getHistory();
        }
        return currentKey;
    }

    @Override
    public DataFormat getFormat(final IKey key) {
        DataFormat format = DataFormat.VOID;
        IKey currentKey = getAdaptedKey(key);

        if (currentKey instanceof TangoKey) {
            String deviceName = TangoKeyTool.getDeviceName(currentKey);
            if (TangoKeyTool.isAnAttribute(currentKey)) {
                String attributeName = TangoKeyTool.getAttributeName(currentKey);
                if (TangoAttributeHelper.isAttributeRunning(deviceName, attributeName)) {
                    int tangoFormat = TangoAttributeHelper.getAttributeFormat(deviceName, attributeName);
                    format = getDataFormat(tangoFormat);
                }
            }
        }

        return format;
    }

    public static DataFormat getDataFormat(int tangoFormat) {
        DataFormat format = DataFormat.VOID;
        switch (tangoFormat) {
            case TangoConstHelper.BOOLEAN_FORMAT:
                format = DataFormat.BOOLEAN;
                break;

            case TangoConstHelper.STRING_FORMAT:
                format = DataFormat.TEXT;
                break;

            case TangoConstHelper.NUMERICAL_FORMAT:
                format = DataFormat.NUMERICAL;
                break;
        }
        return format;
    }

    private String getNumericalFormat(final IKey key) {
        String format = null;
        IKey currentKey = getAdaptedKey(key);

        if (currentKey instanceof TangoKey) {
            String deviceName = TangoKeyTool.getDeviceName(currentKey);
            if (TangoKeyTool.isAnAttribute(currentKey)) {
                String attributeName = TangoKeyTool.getAttributeName(currentKey);
                if (TangoAttributeHelper.isAttributeRunning(deviceName, attributeName)) {
                    AttributeInfo attributeInfo = TangoAttributeHelper.getAttributeInfo(deviceName, attributeName);
                    if (attributeInfo != null) {
                        format = attributeInfo.format;
                    }

                }
            }
        }
        return format;
    }

    @Override
    public String getDescription(final IKey key) {
        String description = null;
        IKey currentKey = getAdaptedKey(key);
        if (currentKey != null) {
            // First check alias
            description = getAlias(currentKey);
            if ((description == null) || description.trim().isEmpty()) {
                description = getTangoDescription(currentKey);
                if ((description == null) || description.trim().isEmpty()
                        || description.trim().equalsIgnoreCase("no description")) {
                    description = getTangoLabel(currentKey);
                    if ((description != null) && description.trim().isEmpty()) {
                        description = null;
                    }
                }
            }
        }
        return description;
    }

    @Override
    public ITreeNode createNode(final IKey key) throws DataBrowserException {
        ITreeNode sourceNode = null;
        String file = null;
        if (key != null) {
            boolean sourceOk = false;
            try {
                if (key instanceof TangoKey) {
                    String deviceName = TangoKeyTool.getDeviceName(key);
                    DeviceProxy proxy = TangoDeviceHelper.getDeviceProxy(deviceName, false);
                    if (proxy != null) {
                        String[] attributeList = null;
                        try {
                            attributeList = proxy.get_attribute_list();
                        } catch (DevFailed e) {
                            String errorMessage = "Cannot read the attribut list of " + deviceName + " because "
                                    + TangoExceptionHelper.getErrorMessage(e);
                            throw new DataBrowserException(errorMessage, e);
                        }

                        if ((attributeList != null) && (attributeList.length > 0)) {
                            sourceNode = new SourceTreeNode();
                            sourceNode.setName(deviceName);
                            sourceNode.setData(key);
                            ITreeNode attributeNode = new GroupTreeNode();
                            attributeNode.setName("attributes");
                            ITreeNode trendNode = new GroupTreeNode();
                            trendNode.setName("trend");
                            sourceNode.addNodes(attributeNode, trendNode);

                            ItemTreeNode child = null;
                            TangoKey attributeKey = null;
                            IKey settableKey = null;
                            HistoryKey historyKey = null;
                            DataType dataType = null;
                            for (String attributeName : attributeList) {
                                attributeKey = generateReadableKey(deviceName, attributeName);
                                dataType = getType(attributeKey);
                                child = new ItemTreeNode();
                                child.setName(attributeName);
                                child.setData(attributeKey);
                                attributeNode.addNodes(child);

                                // Create a write node for a non scalar attribute
                                settableKey = generateSettableKey(attributeKey);
                                if ((settableKey != null) && (dataType != DataType.SCALAR)) {
                                    child.setName(child.getName() + ":read");
                                    child = new ItemTreeNode();
                                    child.setName(attributeName + ":write");
                                    child.setData(settableKey);
                                    attributeNode.addNodes(child);
                                }

                                if ((dataType != DataType.IMAGE) && (getFormat(attributeKey) != DataFormat.TEXT)) {
                                    child = new ItemTreeNode();
                                    historyKey = new HistoryKey(attributeKey);
                                    child.setName(attributeName);
                                    child.setData(historyKey);
                                    trendNode.addNodes(child);

                                    if (settableKey != null) {
                                        child.setName(child.getName() + ":read");
                                        child = new ItemTreeNode();
                                        attributeKey = new TangoKey();
                                        historyKey = new HistoryKey(settableKey);
                                        child.setName(attributeName + ":write");
                                        child.setData(historyKey);
                                        trendNode.addNodes(child);
                                    }
                                }
                            }
                            addSourceEntry(key);
                            sourceOk = true;
                        }
                    }

                } else if (key.getInformationKey() != null) {
                    file = key.getInformationKey();
                    sourceNode = new SourceTreeNode();
                    sourceNode.setName(file);
                    sourceNode.setData(key);
                    TangoFile tangoFile = null;
                    try {
                        tangoFile = new TangoFile(file);
                        TANGO_FILE_MAP.put(key, tangoFile);
                        ITreeNode attributesNode = null;
                        ITreeNode trendNode = null;
                        ItemTreeNode itemNode = null;
                        String attributeName = null;
                        String deviceName = null;
                        List<IKey> simpleKeyList = tangoFile.getSimpleKeyList();

                        if ((simpleKeyList != null) && !simpleKeyList.isEmpty()) {
                            if (attributesNode == null) {
                                attributesNode = new GroupTreeNode();
                                attributesNode.setName("attributes");
                            }
                            for (IKey simpleKey : simpleKeyList) {
                                attributeName = TangoKeyTool.getAttributeName(simpleKey);
                                deviceName = TangoKeyTool.getDeviceName(simpleKey);
                                itemNode = new ItemTreeNode();
                                itemNode.setName(deviceName + "/" + attributeName);
                                itemNode.setData(simpleKey);
                                TANGO_FILE_MAP.put(simpleKey, tangoFile);
                                getPlotProperties(simpleKey);
                                getChartProperties(simpleKey);
                                attributesNode.addNodes(itemNode);
                            }
                        }

                        List<HistoryKey> trendKeyList = tangoFile.getTrendKeyList();
                        if ((trendKeyList != null) && !trendKeyList.isEmpty()) {
                            if (trendNode == null) {
                                trendNode = new GroupTreeNode();
                                trendNode.setName("trend");
                            }
                            IKey simpleKey = null;
                            for (HistoryKey historyKey : trendKeyList) {
                                simpleKey = historyKey.getHistory();
                                attributeName = TangoKeyTool.getAttributeName(simpleKey);
                                deviceName = TangoKeyTool.getDeviceName(simpleKey);
                                itemNode = new ItemTreeNode();
                                itemNode.setName(deviceName + "/" + attributeName);
                                itemNode.setData(historyKey);
                                TANGO_FILE_MAP.put(historyKey, tangoFile);
                                getPlotProperties(historyKey);
                                getChartProperties(historyKey);
                                trendNode.addNodes(itemNode);
                            }
                        }

                        if (attributesNode != null) {
                            sourceNode.addNodes(attributesNode);
                        }
                        if (trendNode != null) {
                            sourceNode.addNodes(trendNode);
                        }

                        addSourceEntry(key);
                        sourceOk = true;

                    } catch (IOException exp) {
                        throw new BadFileException("Cannot read file " + file);
                    } catch (Exception exp) {
                        throw new BadFileException("Cannot read file " + file);
                    } finally {
                        if (tangoFile != null) {
                            tangoFile.close();
                        }
                    }
                }
            } catch (Exception e) {
                LOGGER.error(String.format("Cannot create node for %s because %s", key, e.getMessage()), e);
                if (e instanceof DataBrowserException) {
                    throw (DataBrowserException) e;
                } else {
                    String message = "Cannot parse key ";
                    if (file != null) {
                        message = message + file;
                    } else if (key.getInformationKey() != null) {
                        message = message + key.getInformationKey();
                    }
                    throw new DataBrowserException(message, e);
                }
            } finally {
                if (!sourceOk) {
                    closeSource(key);
                }
            }
        }
        return sourceNode;
    }

    /**
     * Get a read TangoKey according to a attributeName
     * 
     */
    public TangoKey generateReadableKey(String deviceName, String attributeName) {
        TangoKey attributeKey = null;
        if (deviceName != null && !deviceName.isEmpty() && attributeName != null && !attributeName.isEmpty()) {
            attributeKey = new TangoKey();
            TangoKeyTool.registerAttribute(attributeKey, deviceName, attributeName);
        }
        return attributeKey;
    }

    @Override
    public IKey generateSettableKey(final IKey key) {
        IKey writeKey = null;
        if ((producer != null) && producer.isSourceSettable(key) && (key instanceof TangoKey)
                && !(key instanceof HistoryKey)) {
            String deviceName = TangoKeyTool.getDeviceName(key);
            String attributeName = TangoKeyTool.getAttributeName(key);
            writeKey = new TangoKey();
            TangoKeyTool.registerWriteAttribute(writeKey, deviceName, attributeName);
        }
        return writeKey;
    }

    @Override
    public IKey generateRegionKey(final IKey key, final int[] shape, final int[] origine) {
        return key;
    }

    @Override
    public boolean isTypeModifiable() {
        return false;
    }

    public static boolean isDeviceRunning(String deviceName) {
        boolean running = false;
        Boolean deviceRunning = DEVICE_RUNNING_MAP.get(deviceName);
        if (deviceRunning == null) {
            if (isTangoEnabled()) {
                deviceRunning = TangoDeviceHelper.isDeviceRunning(deviceName);
                DEVICE_RUNNING_MAP.put(deviceName, deviceRunning);
            }
        } else {
            running = deviceRunning.booleanValue();
        }

        return running;
    }

    public static String getFullDeviceName(String deviceName) {
        String fullDeviceName = deviceName;
        try {
            fullDeviceName = TangoUtil.getfullNameForDevice(deviceName);
        } catch (DevFailed e) {
            fullDeviceName = deviceName;
        }
        return fullDeviceName;
    }

    @Override
    public String getDisplayName(final IKey key) {
        String displayName = null;
        if (key != null) {
            displayName = key.getInformationKey();
        }
        String alias = getAlias(key);
        if ((alias != null) && !alias.trim().isEmpty()) {
            displayName = alias;
            if (isSettable(key)) {
                displayName = displayName + ":write";
            }
        }

        return displayName;
    }

    @Override
    public IDataSourceSeeker getSeeker() {
        return seeker;
    }

    @Override
    public String getAlias(IKey key) {
        String alias = null;
        IKey simpleKey = getSimpleKey(key);
        String deviceName = TangoKeyTool.getDeviceName(simpleKey);

        if (deviceName != null) {
            String deviceAlias = null;
            try {
                String fullNameForDevice = TangoUtil.getfullNameForDevice(deviceName);
                Database database = TangoDeviceHelper.getDatabase();
                if (database != null) {
                    deviceAlias = database.get_alias_from_device(fullNameForDevice);
                }
            } catch (DevFailed e) {
                LOGGER.debug(String.format("Cannot get alias for device %s because %s", deviceName,
                        TangoExceptionHelper.getErrorMessage(e)), e);
                deviceAlias = null;
            }
            if ((deviceAlias != null) && deviceAlias.isEmpty()) {
                deviceAlias = null;
            }

            if (TangoKeyTool.isAnAttribute(simpleKey)) {
                String attributeName = TangoKeyTool.getAttributeName(simpleKey);
                Database database = TangoDeviceHelper.getDatabase();
                if ((database != null) && (attributeName != null)) {
                    try {
                        String fullNameForDevice = TangoUtil.getfullNameForDevice(deviceName);
                        alias = database.get_alias_from_attribute(fullNameForDevice + "/" + attributeName);
                    } catch (DevFailed e) {
                        LOGGER.debug(String.format("Cannot get alias for attribute %s/%s because %s", deviceName,
                                attributeName, TangoExceptionHelper.getErrorMessage(e)), e);
                        alias = null;
                    }
                }
                if ((alias == null) && (deviceAlias != null)) {
                    alias = deviceAlias + "/" + attributeName;
                }
            } else if (deviceAlias != null) {
                alias = deviceAlias;
            }
        }
        return alias;
    }

    @Override
    public void setType(IKey key, DataType type) {
        // The type is not modifiable in Tango
    }

    @Override
    public PlotProperties getPlotProperties(IKey key) {
        PlotProperties plotProperties = null;
        if (key != null) {
            TangoFile tangoFile = TANGO_FILE_MAP.get(key);
            if (tangoFile != null) {
                plotProperties = tangoFile.getPlotProperties(key);
            }
        }
        return plotProperties;
    }

    @Override
    public ChartProperties getChartProperties(IKey key) {
        ChartProperties chartProperties = null;
        if (key != null) {
            TangoFile tangoFile = TANGO_FILE_MAP.get(key);
            if (tangoFile != null) {
                chartProperties = tangoFile.getChartProperties(key);
            }
        }
        return chartProperties;
    }

    @Override
    public AxisType getAxis(IKey key) {

        PlotProperties plotProperties = getPlotProperties(key);
        AxisType axisSelected = null;
        if (plotProperties != null) {
            int axisChoice = plotProperties.getAxisChoice();
            switch (axisChoice) {
                case IChartViewer.X:
                    axisSelected = AxisType.X;
                    break;
                case IChartViewer.Y1:
                    axisSelected = AxisType.Y1;
                    break;
                case IChartViewer.Y2:
                    axisSelected = AxisType.Y2;
                    break;
            }
        }

        return axisSelected;
    }

    @Override
    public boolean isSettable(IKey key) {
        Boolean settable = Boolean.FALSE;
        if (key != null) {
            settable = Boolean.valueOf(TangoKeyTool.isMonitorWritePartOnly(key));
        }
        return settable;
    }

    @Override
    public boolean isNodeReachable(final ITreeNode node) {
        return TangoDataSourceBrowser.isDeviceRunning(node.getName());
    }

    @Override
    public String getCompletePath(String simplePath) {
        return TangoDataSourceBrowser.getFullDeviceName(simplePath);
    }

}
