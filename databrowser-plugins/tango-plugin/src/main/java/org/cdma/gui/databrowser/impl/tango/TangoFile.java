/*
 * This file is part of tango-plugin.
 * 
 * tango-plugin is free software: you can redistribute it and/or modify it under the terms of the GNU General Public
 * License as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later
 * version.
 * 
 * tango-plugin is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the
 * implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for more
 * details.
 * 
 * You should have received a copy of the GNU General Public License along with tango-plugin. If not, see
 * <https://www.gnu.org/licenses/>.
 */
package org.cdma.gui.databrowser.impl.tango;

import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;

import org.cdma.gui.databrowser.interfaces.AxisType;

import fr.soleil.comete.bean.trend.model.TrendFile;
import fr.soleil.comete.definition.widget.properties.ChartProperties;
import fr.soleil.comete.definition.widget.properties.PlotProperties;
import fr.soleil.comete.tango.data.service.TangoKey;
import fr.soleil.comete.tango.data.service.TangoKeyTool;
import fr.soleil.comete.tango.data.service.helper.TangoAttributeHelper;
import fr.soleil.comete.tango.data.service.helper.TangoConstHelper;
import fr.soleil.comete.tango.data.service.helper.TangoDeviceHelper;
import fr.soleil.data.service.HistoryKey;
import fr.soleil.data.service.IKey;

public class TangoFile {

    private final List<IKey> simpleKeyList = new ArrayList<>();
    private final List<HistoryKey> trendKeyList = new ArrayList<>();
    private BufferedReader br = null;

    private final Map<IKey, PlotProperties> plotPropertiesMap = new HashMap<>();

    private final ChartProperties chartProperties;

    private AxisType axisType;

    public TangoFile(String fileName) throws IOException {

        // First test with ATKTrend
        TrendFile trendFile = new TrendFile(fileName);

        chartProperties = trendFile.getChartProperties();
        Collection<Entry<String, PlotProperties>> knownPlotProperties = trendFile.getKnownPlotProperties();

        if ((knownPlotProperties != null) && !knownPlotProperties.isEmpty()) {
            PlotProperties plotProperties = null;
            for (Entry<String, PlotProperties> entry : knownPlotProperties) {
                String key = entry.getKey();
                plotProperties = entry.getValue();
                IKey hkey = trendFile.convertToKey(key);
                if ((hkey instanceof HistoryKey) && (plotProperties.getAxisChoice() != TrendFile.AXIS_NONE)) {
                    trendKeyList.add((HistoryKey) hkey);
                    plotPropertiesMap.put(hkey, plotProperties);
                    int index = key.lastIndexOf('/');
                    if (index > 0) {
                        String attributeName = key.substring(index + 1);
                        String deviceName = key.substring(0, index);
                        TangoKey tangoKey = new TangoKey();
                        TangoKeyTool.registerAttribute(tangoKey, deviceName, attributeName);
                        simpleKeyList.add(tangoKey);
                        plotPropertiesMap.put(tangoKey, plotProperties);
                    }
                }
            }
        } else {// It is a atktunning file
            br = new BufferedReader(new FileReader(fileName));
            String s = null;
            String completeAttributeName = null;
            String attributeName = null;
            String deviceName = null;
            TangoKey tangoKey = null;
            HistoryKey historyKey = null;

            while (((s = br.readLine()) != null) && !s.trim().isEmpty()) {
                if (!s.startsWith("#")) {
                    completeAttributeName = s.trim();
                    deviceName = TangoDeviceHelper.getDeviceName(completeAttributeName);
                    attributeName = TangoDeviceHelper.getEntityName(completeAttributeName);

                    if (TangoAttributeHelper.isAttributeRunning(deviceName, attributeName)) {
                        tangoKey = new TangoKey();
                        TangoKeyTool.registerAttribute(tangoKey, deviceName, attributeName);
                        simpleKeyList.add(tangoKey);

                        int tangoFormat = TangoAttributeHelper.getAttributeFormat(deviceName, attributeName);
                        int tangoType = TangoAttributeHelper.getAttributeType(deviceName, attributeName);
                        if ((tangoFormat != TangoConstHelper.STRING_FORMAT)
                                && (tangoType != TangoConstHelper.IMAGE_TYPE)) {
                            tangoKey = new TangoKey();
                            TangoKeyTool.registerAttribute(tangoKey, deviceName, attributeName);
                            TangoKeyTool.registerRefreshed(tangoKey, false);
                            historyKey = new HistoryKey(tangoKey);
                            trendKeyList.add(historyKey);
                        }
                    }
                }
            }
        }

    }

    public void close() {
        if (br != null) {
            try {
                br.close();
            } catch (IOException e) {
            }
        }
    }

    public List<IKey> getSimpleKeyList() {
        return simpleKeyList;
    }

    public List<HistoryKey> getTrendKeyList() {
        return trendKeyList;
    }

    public PlotProperties getPlotProperties(IKey key) {
        return key == null ? null : plotPropertiesMap.get(key);
    }

    public ChartProperties getChartProperties(IKey key) {
        return chartProperties;
    }

    public AxisType getAxisProperties(IKey key) {
        return axisType;

    }

    public IKey getXScale(IKey key) {
        return key;
    }

    public IKey getYScale(IKey key) {
        return key;
    }

    public IKey getSliderScale(IKey key) {
        return key;
    }
}
