/*
 * This file is part of cdma-plugin.
 * 
 * cdma-plugin is free software: you can redistribute it and/or modify it under the terms of the GNU General Public
 * License as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later
 * version.
 * 
 * cdma-plugin is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the
 * implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for more
 * details.
 * 
 * You should have received a copy of the GNU General Public License along with cdma-plugin. If not, see
 * <https://www.gnu.org/licenses/>.
 */
package org.cdma.gui.databrowser.impl.cdma.db;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;
import java.util.Date;
import java.util.Timer;
import java.util.TimerTask;

import javax.swing.GroupLayout;
import javax.swing.GroupLayout.Alignment;
import javax.swing.JComboBox;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JSpinner;
import javax.swing.LayoutStyle.ComponentPlacement;
import javax.swing.SpinnerDateModel;
import javax.swing.SpinnerNumberModel;
import javax.swing.SwingConstants;
import javax.swing.WindowConstants;

import org.cdma.gui.databrowser.util.ApplicationUtil;

import fr.soleil.lib.project.swing.ConstrainedCheckBox;

public class OptionsPanel extends JPanel implements ActionListener {

    private static final long serialVersionUID = -6076963091951258987L;

    private static final boolean USE_TIMER = true;
    private static final boolean OVERRIDE_DEFAULT_VALUE = false;
    private static final boolean DYNAMIC_DEFAULT_VALUE = false;

    private static final long HOUR_IN_SECONDS = 3600000;// in seconds
    private static final long DAY_IN_SECONDS = 24 * HOUR_IN_SECONDS;// in seconds

    private static final String NONE_VALUE = "NONE";
    private static final String SECOND_VALUE = "SECOND";
    private static final String MINUTE_VALUE = "MINUTE";
    private static final String HOUR_VALUE = "HOUR";
    private static final String DAY_VALUE = "DAY";
    private static final String MONTH_VALUE = "MONTH";

    private static enum SamplingPeriod {
        NONE(DBDataSourceSeeker.MESSAGES.getString("Sampling.SamplingStep.None"), NONE_VALUE),
        SECOND(DBDataSourceSeeker.MESSAGES.getString("Sampling.SamplingStep.Second"), SECOND_VALUE),
        MINUTE(DBDataSourceSeeker.MESSAGES.getString("Sampling.SamplingStep.Minute"), MINUTE_VALUE),
        HOUR(DBDataSourceSeeker.MESSAGES.getString("Sampling.SamplingStep.Hour"), HOUR_VALUE),
        DAY(DBDataSourceSeeker.MESSAGES.getString("Sampling.SamplingStep.Day"), DAY_VALUE),
        MONTH(DBDataSourceSeeker.MESSAGES.getString("Sampling.SamplingStep.Month"), MONTH_VALUE);

        private String name;
        private String value;

        private SamplingPeriod(final String name, final String value) {
            this.name = name;
            this.value = value;
        }

        @Override
        public String toString() {
            return name;
        }

        public String getValue() {
            return value;
        }
    }

    private static enum TimeRange {
        LAST_1H(DBDataSourceSeeker.MESSAGES.getString("TimeRange.DynamicRange.Last1H"), 1 * HOUR_IN_SECONDS),
        LAST_4H(DBDataSourceSeeker.MESSAGES.getString("TimeRange.DynamicRange.Last4H"), 4 * HOUR_IN_SECONDS),
        LAST_8H(DBDataSourceSeeker.MESSAGES.getString("TimeRange.DynamicRange.Last8H"), 8 * HOUR_IN_SECONDS),
        LAST_1D(DBDataSourceSeeker.MESSAGES.getString("TimeRange.DynamicRange.Last1D"), 1 * DAY_IN_SECONDS),
        LAST_3D(DBDataSourceSeeker.MESSAGES.getString("TimeRange.DynamicRange.Last3D"), 3 * DAY_IN_SECONDS),
        LAST_7D(DBDataSourceSeeker.MESSAGES.getString("TimeRange.DynamicRange.Last7D"), 7 * DAY_IN_SECONDS),
        LAST_30D(DBDataSourceSeeker.MESSAGES.getString("TimeRange.DynamicRange.Last30D"), 30 * DAY_IN_SECONDS);

        private String name;
        private Long delta;

        TimeRange(final String name, final long delta) {
            this.name = name;
            this.delta = Long.valueOf(delta);
        }

        @Override
        public String toString() {
            return name;
        }

        public Long getDelta() {
            return delta;
        }
    }

    private static final String HUMAN_TIME_PATTERN = "yyyy-MM-dd HH:mm:ss";

    private ConstrainedCheckBox overrideCheckbox;
    private ConstrainedCheckBox dynamicCheckbox;
    private JComboBox<TimeRange> rangeCombo;
    private JComboBox<SamplingPeriod> samplingCombo;
    private JSpinner startField;
    private JSpinner endField;
    private SpinnerNumberModel samplingFactorModel;
    private JSpinner samplingFactorField;

    private JLabel startLabel;
    private JLabel endLabel;
    private JLabel samplingPeriodLabel;
    private JLabel samplingFactorLabel;

    // auto update fields on dynamic range mode
    private Timer timer;
    private TimerTask updateTask;

    public OptionsPanel() {
        initComponents();
        layoutComponents();
    }

    private void initComponents() {
        overrideCheckbox = new ConstrainedCheckBox(DBDataSourceSeeker.MESSAGES.getString("TimeRange.Override"),
                OVERRIDE_DEFAULT_VALUE);

        dynamicCheckbox = new ConstrainedCheckBox(DBDataSourceSeeker.MESSAGES.getString("TimeRange.Dynamic"),
                DYNAMIC_DEFAULT_VALUE);

        rangeCombo = new JComboBox<>();
        for (TimeRange item : TimeRange.values()) {
            rangeCombo.addItem(item);
        }
        rangeCombo.setEnabled(dynamicCheckbox.isSelected());

        startField = new JSpinner(new SpinnerDateModel());
        JSpinner.DateEditor editor = new JSpinner.DateEditor(startField, HUMAN_TIME_PATTERN);
        startField.setEditor(editor);
        editor.getTextField().setHorizontalAlignment(SwingConstants.LEADING);

        endField = new JSpinner(new SpinnerDateModel());
        editor = new JSpinner.DateEditor(endField, HUMAN_TIME_PATTERN);
        endField.setEditor(editor);
        editor.getTextField().setHorizontalAlignment(SwingConstants.LEADING);

        startLabel = new JLabel(DBDataSourceSeeker.MESSAGES.getString("TimeRange.StartDate"));
        endLabel = new JLabel(DBDataSourceSeeker.MESSAGES.getString("TimeRange.EndDate"));

        samplingCombo = new JComboBox<>();
        for (SamplingPeriod sampling : SamplingPeriod.values()) {
            samplingCombo.addItem(sampling);
        }
        samplingCombo.setEnabled(overrideCheckbox.isSelected());
        samplingFactorModel = new SpinnerNumberModel(1, 1, Integer.MAX_VALUE, 1);
        samplingFactorField = new JSpinner(samplingFactorModel);

        samplingPeriodLabel = new JLabel(DBDataSourceSeeker.MESSAGES.getString("Sampling.Sampling"));
        samplingFactorLabel = new JLabel(DBDataSourceSeeker.MESSAGES.getString("Sampling.SamplingFactor"));

        // initialize fields using selected range
        updateRangeFields();
        updateSamplingFactor();

        overrideCheckbox.addActionListener(this);
        dynamicCheckbox.addActionListener(this);
        rangeCombo.addActionListener(this);
        samplingCombo.addActionListener(this);

        setPanelEnabled();
    }

    public void enableOverridePart() {
        overrideCheckbox.setSelected(true);
        setPanelEnabled();
        overrideCheckbox.setVisible(false);
    }

    private void layoutComponents() {
        GroupLayout groupLayout = new GroupLayout(this);
        setLayout(groupLayout);

        groupLayout.setAutoCreateGaps(true);
        groupLayout.setAutoCreateContainerGaps(true);

        GroupLayout.SequentialGroup dynamicGroup = groupLayout.createSequentialGroup();
        dynamicGroup.addComponent(dynamicCheckbox).addComponent(rangeCombo);

        GroupLayout.SequentialGroup rangeGroup = groupLayout.createSequentialGroup();
        rangeGroup.addGroup(
                groupLayout.createParallelGroup(Alignment.TRAILING).addComponent(startLabel).addComponent(endLabel));
        rangeGroup.addGroup(groupLayout.createParallelGroup().addComponent(startField).addComponent(endField));

        GroupLayout.SequentialGroup samplingGroup = groupLayout.createSequentialGroup();
        samplingGroup.addGroup(groupLayout.createParallelGroup(Alignment.TRAILING).addComponent(samplingPeriodLabel)
                .addComponent(samplingFactorLabel));
        samplingGroup.addGroup(
                groupLayout.createParallelGroup().addComponent(samplingCombo).addComponent(samplingFactorField));

        GroupLayout.ParallelGroup hGroup = groupLayout.createParallelGroup();
        hGroup.addComponent(overrideCheckbox).addGroup(dynamicGroup).addGroup(rangeGroup).addGroup(samplingGroup);

        GroupLayout.SequentialGroup vGroup = groupLayout.createSequentialGroup();
        vGroup.addComponent(overrideCheckbox);
        vGroup.addPreferredGap(ComponentPlacement.UNRELATED, 25, 25);
        vGroup.addGroup(groupLayout.createParallelGroup(Alignment.BASELINE).addComponent(dynamicCheckbox)
                .addComponent(rangeCombo));
        vGroup.addPreferredGap(ComponentPlacement.UNRELATED, 15, 15);
        vGroup.addGroup(
                groupLayout.createParallelGroup(Alignment.BASELINE).addComponent(startLabel).addComponent(startField));
        vGroup.addGroup(
                groupLayout.createParallelGroup(Alignment.BASELINE).addComponent(endLabel).addComponent(endField));
        vGroup.addPreferredGap(ComponentPlacement.UNRELATED, 25, 25);
        vGroup.addGroup(groupLayout.createParallelGroup(Alignment.BASELINE).addComponent(samplingPeriodLabel)
                .addComponent(samplingCombo));
        vGroup.addGroup(groupLayout.createParallelGroup(Alignment.BASELINE).addComponent(samplingFactorLabel)
                .addComponent(samplingFactorField));

        groupLayout.setHorizontalGroup(hGroup);
        groupLayout.setVerticalGroup(vGroup);
    }

    @Override
    public void actionPerformed(final ActionEvent e) {
        Object source = e.getSource();

        if ((source == overrideCheckbox) || (source == dynamicCheckbox)) {
            setPanelEnabled();
            manageRangeFieldsUpdate();
        } else if (source == rangeCombo) {
            updateRangeFields();
        } else if (source == samplingCombo) {
            setPanelEnabled();
            updateSamplingFactor();
        }
    }

    private void setPanelEnabled() {
        boolean override = overrideCheckbox.isSelected();
        boolean isDynamicRange = dynamicCheckbox.isSelected();
        boolean isSampled = (samplingCombo.getSelectedItem() != SamplingPeriod.NONE);
        samplingPeriodLabel.setEnabled(override);
        samplingFactorLabel.setEnabled(override);
        samplingCombo.setEnabled(override);
        samplingFactorField.setEnabled(override && isSampled);
        dynamicCheckbox.setEnabled(override);
        rangeCombo.setEnabled(override && isDynamicRange);
        startLabel.setEnabled(override && !isDynamicRange);
        endLabel.setEnabled(override && !isDynamicRange);
        startField.setEnabled(override && !isDynamicRange);
        endField.setEnabled(override && !isDynamicRange);
    }

    private void manageRangeFieldsUpdate() {
        if (updateTask != null) {
            updateTask.cancel();
        }
        if (overrideCheckbox.isSelected() && dynamicCheckbox.isSelected()) {
            if (USE_TIMER) {
                updateTask = new TimerTask() {
                    @Override
                    public void run() {
                        ApplicationUtil.runInEDT(new Runnable() {
                            @Override
                            public void run() {
                                updateRangeFields();
                            }
                        });
                    }
                };
                // update fields every second
                startSchedule();
            } else {
                updateRangeFields();
            }
        }
    }

    private void startSchedule() {
        Timer timer = getTimer();
        if (timer != null) {
            timer.schedule(updateTask, 0, 1000);
        }
    }

    private void updateRangeFields() {
        long endDate = System.currentTimeMillis();
        long delta = ((TimeRange) rangeCombo.getSelectedItem()).getDelta();
        long startDate = endDate - delta;

        startField.setValue(new Date(startDate));
        endField.setValue(new Date(endDate));
    }

    private void updateSamplingFactor() {
        SamplingPeriod period = (SamplingPeriod) samplingCombo.getSelectedItem();

        int maximum = 1;

        switch (period) {
            case SECOND:
            case MINUTE:
                maximum = 60;
                break;
            case HOUR:
                maximum = 24;
                break;
            case DAY:
                maximum = 30;
                break;
            case MONTH:
                maximum = 12;
                break;
            default:
                // nop
                break;
        }
        if (period != SamplingPeriod.NONE) {
            if (samplingFactorModel.getNumber().intValue() > maximum) {
                samplingFactorField.setValue(maximum);
            }
            samplingFactorModel.setMaximum(maximum);
        }
    }

    public void clearPanel() {
        overrideCheckbox.setSelected(OVERRIDE_DEFAULT_VALUE);
        dynamicCheckbox.setSelected(DYNAMIC_DEFAULT_VALUE);
        setPanelEnabled();
        manageRangeFieldsUpdate();
    }

    private Timer getTimer() {
        if (timer == null) {
            timer = new Timer();
        }
        return timer;
    }

    public void startRefreshing() {
        if (USE_TIMER) {
            getTimer();
            manageRangeFieldsUpdate();
        }
    }

    public void stopRefreshing() {
        if (USE_TIMER) {
            if (timer != null) {
                timer.cancel();
                timer = null;
            }
        }
    }

    public boolean isPanelEnabled() {
        return overrideCheckbox.isSelected();
    }

    public boolean isDynamicRange() {
        return dynamicCheckbox.isSelected();
    }

    public long getDelta() {
        return ((TimeRange) rangeCombo.getSelectedItem()).getDelta();
    }

    public long getStartDate() {
        return ((Date) startField.getValue()).getTime();
    }

    public long getEndDate() {
        return ((Date) endField.getValue()).getTime();
    }

    public org.cdma.engine.sql.utils.SamplingType.SamplingPeriod getSamplingPeriod() {
        org.cdma.engine.sql.utils.SamplingType.SamplingPeriod cdmaPeriod = org.cdma.engine.sql.utils.SamplingType.SamplingPeriod.NONE;
        try {
            String stringValue = ((SamplingPeriod) samplingCombo.getSelectedItem()).getValue();
            cdmaPeriod = org.cdma.engine.sql.utils.SamplingType.SamplingPeriod.valueOf(stringValue);
        } catch (Exception e) {
            // TODO: handle exception
        }
        return cdmaPeriod;
    }

    public int getSamplingFactor() {
        return samplingFactorModel.getNumber().intValue();
    }

    public static void main(final String[] args) {
        final OptionsPanel optionsPanel = new OptionsPanel();

        JFrame frame = new JFrame("Test options panel");
        frame.setDefaultCloseOperation(WindowConstants.EXIT_ON_CLOSE);

        frame.setContentPane(optionsPanel);

        frame.addWindowListener(new WindowAdapter() {
            @Override
            public void windowOpened(final WindowEvent e) {
                optionsPanel.startRefreshing();
                super.windowOpened(e);
            }

            @Override
            public void windowClosing(final WindowEvent e) {
                optionsPanel.stopRefreshing();
                super.windowClosing(e);
            }
        });

        frame.pack();
        frame.setLocationRelativeTo(null);
        frame.setVisible(true);
    }

}
