/*
 * This file is part of cdma-plugin.
 * 
 * cdma-plugin is free software: you can redistribute it and/or modify it under the terms of the GNU General Public
 * License as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later
 * version.
 * 
 * cdma-plugin is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the
 * implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for more
 * details.
 * 
 * You should have received a copy of the GNU General Public License along with cdma-plugin. If not, see
 * <https://www.gnu.org/licenses/>.
 */
package org.cdma.gui.databrowser.impl.cdma;

import java.io.File;
import java.io.FileReader;
import java.net.URI;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;

import org.cdma.Factory;
import org.cdma.IFactory;
import org.cdma.gui.databrowser.exception.BadFileException;
import org.cdma.gui.databrowser.exception.DataBrowserException;
import org.cdma.gui.databrowser.exception.LoadException;
import org.cdma.gui.databrowser.impl.AbstractDataSourceProducerBrowser;
import org.cdma.gui.databrowser.impl.cdma.db.ArchivingUtil;
import org.cdma.gui.databrowser.impl.cdma.db.DBDataSourceSeeker;
import org.cdma.gui.databrowser.interfaces.DataFormat;
import org.cdma.gui.databrowser.interfaces.DataType;
import org.cdma.gui.databrowser.interfaces.IDataSourceBrowser;
import org.cdma.gui.databrowser.interfaces.IDataSourceSeeker;
import org.cdma.gui.databrowser.view.tree.GroupTreeNode;
import org.cdma.gui.databrowser.view.tree.ItemTreeNode;
import org.cdma.gui.databrowser.view.tree.SourceTreeNode;
import org.reflections.Reflections;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import fr.soleil.comete.cdma.data.keys.CDMAKey;
import fr.soleil.comete.cdma.data.service.CDMADataSourceFactory;
import fr.soleil.comete.cdma.data.service.CDMAKeyFactory;
import fr.soleil.comete.cdma.data.service.CDMAKeyFactory.BrowsingMode;
import fr.soleil.comete.definition.widget.util.ITreeNode;
import fr.soleil.data.service.AbstractRefreshingManager;
import fr.soleil.data.service.GenericDescriptor;
import fr.soleil.data.service.IKey;
import fr.soleil.data.service.PolledRefreshingStrategy;
import fr.soleil.data.source.AbstractDataSource;
import fr.soleil.lib.project.ObjectUtils;

/**
 * 
 * @author SAINTIN
 * 
 *         This class provide a specific IDataSourceBrowser based on
 *         CDMADataSourceFactory It can manage all the detected plugin in the
 *         CDMA and will encapsulate it in a IDataSourceBrowser
 * 
 * @see CDMADataSourceFactory
 * @see Factory
 */

public class CDMADataSourceBrowser extends AbstractDataSourceProducerBrowser {

    private static final Logger LOGGER = LoggerFactory.getLogger(CDMADataSourceBrowser.class);

    public static final String FLAT_VIEW = "FLAT_VIEW";
    public static final String PHYSICAL_VIEW = "PHYSICAL_VIEW";
    private static final String MAMBO_VALUE = "VALUE";
    private static final String MAMBO_TIME = "TIME";

    public static final String DB_START_PROPERTY = "DB_START_PROPERTY";
    public static final String DB_END_PROPERTY = "DB_END_PROPERTY";
    public static final String DB_SAMPLING_PROPERTY = "DB_SAMPLING_PROPERTY";
    public static final String DB_FACTOR_PROPERTY = "DB_FACTOR_PROPERTY";
    private static final String DB_REFRESHING_ENABLE = "DB_REFRESHING_ENABLE";

    private static final String FALSE = "false";

    private static final String INVALID_FILE_STRUCTURE = "Invalid file structure";
    private static final String FILE_IS_NOT_READABLE = "File is not readable (please check your user rights)";
    private static final String INVALID_URI = "Invalid URI";
    private static final String DEFAULT_DESCRIPTION = "This browser can read URIs supported by avalaible CDMA plugins";

    private final Map<String, CDMAKeyFactory> keyFactoryMap;
    protected final Map<ITreeNode, List<IKey>> dimensionMap;

    private IFactory factory;
    private IDataSourceSeeker seeker;

    private final List<IDataSourceBrowser> dataBrowserList;

    private boolean refreshingEnable;
    private boolean keyFactoryMapInitialized;

    public CDMADataSourceBrowser() {
        this(null);
        initFactoryKeyMap();
        String refreshingProp = System.getProperty(DB_REFRESHING_ENABLE, FALSE);
        refreshingEnable = Boolean.parseBoolean(refreshingProp);
        if (refreshingEnable && (producer instanceof AbstractRefreshingManager<?>)) {
            ((AbstractRefreshingManager<?>) producer).setDefaultRefreshingStrategy(new PolledRefreshingStrategy(1000));
        }
        initDataSourceBrowserList();
    }

    private CDMADataSourceBrowser(final IFactory factory) {
        super(CDMADataSourceFactory.class);
        this.factory = factory;
        dimensionMap = new HashMap<>();
        keyFactoryMap = new HashMap<>();
        dataBrowserList = new ArrayList<>();
        keyFactoryMapInitialized = false;
    }

    private void initFactoryKeyMap() {
        if (!keyFactoryMapInitialized) {
            keyFactoryMapInitialized = true;

            CDMAKeyFactory physicalKeyFactory = new CDMAKeyFactory();
            physicalKeyFactory.enableMode(BrowsingMode.PHYSICAL);
            physicalKeyFactory.setPluginRestriction(getId());
            keyFactoryMap.put(PHYSICAL_VIEW, physicalKeyFactory);

            CDMAKeyFactory flatKeyFactory = new CDMAKeyFactory();
            flatKeyFactory.enableMode(BrowsingMode.LOGICAL);
            flatKeyFactory.setPluginRestriction(getId());
            flatKeyFactory.setLogicalView(ObjectUtils.EMPTY_STRING);
            keyFactoryMap.put(FLAT_VIEW, flatKeyFactory);
            List<String> availableViews = getAvailableViews();
            for (String view : availableViews) {
                CDMAKeyFactory newKeyFactory = new CDMAKeyFactory();
                newKeyFactory.setPluginRestriction(getId());
                newKeyFactory.enableMode(BrowsingMode.LOGICAL);
                newKeyFactory.setLogicalView(view);
                keyFactoryMap.put(view, newKeyFactory);
            }
        }
    }

    public CDMAKeyFactory getKeyFactory(final String view) {
        CDMAKeyFactory keyFactory = null;
        if (view != null) {
            initFactoryKeyMap();
            keyFactory = keyFactoryMap.get(view);
        }
        return keyFactory;
    }

    public CDMAKeyFactory getKeyFactory(final CDMAKey key) {
        CDMAKeyFactory keyFactory = getKeyFactory(PHYSICAL_VIEW);
        if (key != null) {
            BrowsingMode mode = key.getMode();
            if (mode == BrowsingMode.LOGICAL) {
                String view = key.getView();
                if ((view == null) || view.isEmpty() || !keyFactoryMap.containsKey(view)) {
                    view = FLAT_VIEW;
                }
                keyFactory = getKeyFactory(view);
            }
        }
        return keyFactory;
    }

    private void initDataSourceBrowserList() {
        Set<Class<? extends IDataSourceSeeker>> implementationSeekerSet = getDataSourceSeekerImplementations();
        Map<String, IDataSourceSeeker> tmpSeekerMap = new HashMap<>();

        IDataSourceSeeker tmpSeeker = null;
        for (Class<? extends IDataSourceSeeker> implementation : implementationSeekerSet) {
            try {
                tmpSeeker = implementation.newInstance();
                if ((tmpSeeker != null) && (tmpSeeker.getDataSourceBrowserId() != null)) {
                    tmpSeekerMap.put(tmpSeeker.getDataSourceBrowserId(), tmpSeeker);
                }
            } catch (Exception ignore) {
                LOGGER.debug(String.format("Impossible to create a corresponding seeker %s", ignore.getMessage()),
                        ignore);
            }
        }

        // Setup environment before recovering factories
        ArchivingUtil.initArchivingEnvironnement();
        // Here, we recover available CDMA factories
        CDMAKey availablePluginsKey = getKeyFactory(PHYSICAL_VIEW).generateKeyListAvailablePlugins();
        if ((availablePluginsKey != null) && (producer != null)) {
            // Close the source after creation
            String[] pluginList = null;
            try {
                pluginList = getStringArrayValue(availablePluginsKey);
                LOGGER.trace("pluginList=" + Arrays.toString(pluginList));
            } catch (Exception e) {
                LOGGER.debug(String.format("Impossible to read plugin list %s", e.getMessage()), e);
            }
            removeDataSource(availablePluginsKey);
            if (pluginList != null) {
                LOGGER.info("detected plugins {}", Arrays.toString(pluginList));
                IFactory aFactory = null;
                CDMADataSourceBrowser browser = null;
                for (String id : pluginList) {
                    aFactory = Factory.getFactory(id);
                    browser = new CDMADataSourceBrowser(aFactory);
                    LOGGER.info("Factory class={}", aFactory.getClass().getName());
                    seeker = tmpSeekerMap.get(browser.getId());
                    if (seeker != null) {
                        if (seeker instanceof AbstractCDMADataSourceSeeker) {
                            ((AbstractCDMADataSourceSeeker) seeker).setCDMADataSourceSeeker(browser);
                        }
                        browser.setSeeker(tmpSeeker);
                    } else {
                        new CDMADataSourceSeeker(browser);
                    }

                    dataBrowserList.add(browser);
                }
            }
        }
    }

    public boolean isLogical() {
        boolean result = false;
        if (factory != null) {
            result = factory.isLogicalModeAvailable();
        }
        return result;
    }

    public List<String> getAvailableViews() {
        // TODO voir quand on pourra filtrer selon le plugin
        return Factory.getAvailableViews();
    }

    @Override
    public List<IDataSourceBrowser> getDataSourceBrowserList() {
        return dataBrowserList;
    }

    @Override
    public boolean isSourceBrowserEnabled() {
        return true;
    }

    @Override
    public String getId() {
        String id = CDMADataSourceFactory.SOURCE_PRODUCER_ID;
        if (factory != null) {
            id = factory.getName();
        }
        return id;
    }

    @Override
    public String getVersion() {
        String version = super.getVersion();
        if (factory != null) {
            version = factory.getPluginVersion();
        }
        return version;
    }

    @Override
    public String getLabel() {
        String label = super.getLabel();
        if (factory != null) {
            label = factory.getPluginLabel();
        }
        return label;
    }

    @Override
    public String getDescription() {
        String description = DEFAULT_DESCRIPTION;
        if (factory != null) {
            description = factory.getPluginDescription();
        }
        return description;
    }

    @Override
    public String getName() {
        String displayName = userName;
        if (((userName == null) || userName.isEmpty()) && (factory != null)) {
            displayName = factory.getPluginLabel();
        } else {
            displayName = super.getName();
        }
        return displayName;
    }

    @Override
    public boolean canRead(final String sourcePath) {
        return canRead(new File(sourcePath));
    }

    public boolean canRead(final File file) {
        boolean canRead = false;
        if (file != null) {
            canRead = canRead(file.toURI());
        }
        return canRead;
    }

    @Override
    public boolean canRead(final IKey key) {
        boolean canRead = false;
        if (key instanceof CDMAKey) {
            URI uri = ((CDMAKey) key).getSource();
            canRead = canRead(uri);
        }
        return canRead;
    }

    private boolean canRead(final URI uri) {
        boolean canRead = false;
        if (factory != null) {
            canRead = factory.getPluginURIDetector().isReadable(uri);
        }
        return canRead;
    }

    protected boolean isExperiment(final URI uri) {
        boolean experiment = false;
        if (factory != null) {
            experiment = factory.getPluginURIDetector().isExperiment(uri);
        }
        return experiment;
    }

    private IDataSourceBrowser getDataSourceBrowser(final URI uri) {
        IFactory factory2 = Factory.getFactory(uri);
        IDataSourceBrowser browser = null;
        if (factory2 != null) {
            for (IDataSourceBrowser browser2 : dataBrowserList) {
                if (factory2.getName().equals(browser2.getId())) {
                    browser = browser2;
                    break;
                }
            }
        }
        return browser;
    }

    @Override
    public IDataSourceBrowser getDataSourceBrowser(final String sourcePath) {
        return getDataSourceBrowser(new File(sourcePath).toURI());
    }

    @Override
    public IDataSourceBrowser getDataSourceBrowser(final IKey key) {
        IDataSourceBrowser browser = null;
        if (key instanceof CDMAKey) {
            CDMAKey cdmaKey = (CDMAKey) key;
            // Get the selected plugin
            String pluginId = cdmaKey.getPlugin();
            if ((pluginId != null) && !pluginId.isEmpty()) {
                for (IDataSourceBrowser browser2 : dataBrowserList) {
                    if (pluginId.equals(browser2.getId())) {
                        browser = browser2;
                        break;
                    }
                }
            }
            // If the plugin is not found get the best plugin
            if (browser == null) {
                cdmaKey.setPlugin(null);
                URI uri = cdmaKey.getSource();
                browser = getDataSourceBrowser(uri);
            }
        }
        return browser;
    }

    @Override
    public IKey createKeyFromSource(final String sourcePath) {
        IKey key = null;
        if (sourcePath != null) {
            try {
                key = createKeyFromURI(new File(sourcePath).toURI());
            } catch (Exception e) {
                LOGGER.error(
                        String.format("Impossible to create Key from source %s because %s", sourcePath, e.getMessage()),
                        e);
            }
        }
        return key;
    }

    private IKey createKeyFromURI(final URI uri) {
        CDMAKey key = getKeyFactory(PHYSICAL_VIEW).generateKeySourceTitle(uri);
        // setPlugin(key);
        return key;
    }

    @Override
    public String getSourceFromKey(final IKey key) {
        String result = null;
        if (key instanceof CDMAKey) {
            URI uri = ((CDMAKey) key).getSource();
            try {
                result = getStringFromURI(uri, false);
            } catch (Exception e) {
                LOGGER.error(String.format("Impossible to get source from Key %s because %s", uri, e.getMessage()), e);
            }
        }
        LOGGER.trace("get Source From Key {} = {}", key, result);

        return result;
    }

    private String getStringFromURI(final URI uri, final boolean withFragment) throws DataBrowserException {
        String result = null;
        if ((uri != null) && (uri.getPath() != null) && !uri.getPath().isEmpty()) {
            File file = new File(uri.getPath());
            if (!file.exists()) {
                throw new BadFileException("File not found " + file.getPath());
            }
            if (!file.canRead()) {
                throw new BadFileException("Reading permission denied on File " + file.getPath());
            }
            result = file.getPath();
            if ((uri.getFragment() != null) && withFragment) {
                result = result + uri.getFragment();
            }
        }
        return result;
    }

    @Override
    public Map<String, String> getInformations(final IKey key) {
        Map<String, String> infoMap = null;

        if (key instanceof CDMAKey) {
            CDMAKey cdmaKey = (CDMAKey) key;
            BrowsingMode mode = cdmaKey.getMode();
            LOGGER.trace("cdmaKey.isGroup()={}", cdmaKey.isGroup());
            if (mode == BrowsingMode.PHYSICAL) {
                {
                    infoMap = super.getInformations(key);
                    if (infoMap == null) {
                        infoMap = new HashMap<>();
                    }
                    infoMap.put(LABEL_INFO + "Rank", String.valueOf(getRank(key)));
                    infoMap.put(LABEL_INFO + "Shape", Arrays.toString(getShape(key)));
                }
            }

            if (cdmaKey.getMode() == BrowsingMode.LOGICAL) {
                String view = cdmaKey.getView();
                if ((view == null) || view.isEmpty() || !keyFactoryMap.containsKey(view)) {
                    view = FLAT_VIEW;
                }
                if (infoMap == null) {
                    infoMap = new HashMap<>();
                }
                infoMap.put(LABEL_INFO + "Logical view", view);
            }
        }

        if (infoMap == null) {
            infoMap = new HashMap<>();
        }

        Map<String, String> attributeMap = getAttributes(key);
        if (attributeMap != null) {
            infoMap.putAll(attributeMap);
        }

        return infoMap;
    }

    private Map<String, String> getAttributes(final IKey key) {
        Map<String, String> infoMap = new HashMap<>();
        if ((key instanceof CDMAKey) && (producer != null)) {
            CDMAKey cdmaKey = (CDMAKey) key;
            CDMAKeyFactory keyFactory = getKeyFactory(cdmaKey);
            if (keyFactory != null) {
                IKey attributeListKey = keyFactory.generateKeyAttributeList(cdmaKey.getSource(), cdmaKey.getPath());
                String[] attributeList = null;
                try {
                    attributeList = getStringArrayValue(attributeListKey);
                } catch (Exception e) {
                    LOGGER.error(String.format("Impossible to read attributelist Key %s because %s", attributeListKey,
                            e.getMessage()), e);
                }
                if (attributeList != null) {
                    IKey attributeValue = null;
                    for (String attributeName : attributeList) {
                        attributeValue = keyFactory.generateKeyAttributeValue(cdmaKey.getSource(), cdmaKey.getPath(),
                                attributeName);
                        try {
                            infoMap.put(LABEL_ATTRIBUTE + attributeName, getStringValue(attributeValue));
                        } catch (Exception e) {
                            LOGGER.error(String.format("Impossible to read value Key %s because %s", attributeValue,
                                    e.getMessage()), e);
                        }

                    }
                }
            }
        }
        return infoMap;
    }

    @Override
    public DataType getKeyType(final IKey key) {
        DataType type = DataType.IMAGE;

        int rank = getRank(key);

        Map<String, String> map = getAttributes(key);
        if (map.containsKey(LABEL_ATTRIBUTE + "interpretation")) {
            String interpretation = map.get(LABEL_ATTRIBUTE + "interpretation").trim();
            if (interpretation.toLowerCase().startsWith("ima")) {
                type = DataType.IMAGE;
            } else if (interpretation.toLowerCase().startsWith("spect")) {
                type = DataType.SPECTRUM;
            } else {
                if (rank > 0) {
                    type = DataType.SPECTRUM;
                } else {
                    type = DataType.SCALAR;
                }
            }
        } else {
            switch (rank) {
                case 0:
                    type = DataType.SCALAR;
                    break;

                case 1:
                    type = DataType.SPECTRUM;
                    break;

                case 2:
                    DataFormat format = getFormat(key);
                    if (format == DataFormat.TEXT) {
                        type = DataType.SPECTRUM;
                    } else {
                        type = DataType.IMAGE;
                    }
                    break;
            }
        }
        return type;
    }

    @Override
    public DataFormat getFormat(final IKey key) {
        DataFormat format = DataFormat.VOID;

        if ((key instanceof CDMAKey) && (producer != null)) {
            AbstractDataSource<?> source = null;
            try {
                source = createDataSource(key);
            } catch (Exception e) {
                LOGGER.error(String.format("createDataSource for Key %s = %s", key, e.getMessage()), e);
            }
            // LOGGER.trace("AbstractDataSource class for Key {} = {} ", key, source.getClass().getName());
            if (source != null) {
                GenericDescriptor descriptor = source.getDataType();
                Class<?> classType = descriptor.getPrimitiveType();
                // LOGGER.trace("Source type = {} ", classType);
                LOGGER.trace("get class type for Key {} = {}", key, classType);
                if (classType != null) {
                    if (classType.isAssignableFrom(String.class) || classType.isAssignableFrom(char.class)) {
                        format = DataFormat.TEXT;
                    }
                    if (classType.isAssignableFrom(boolean.class) || classType.isAssignableFrom(Boolean.class)) {
                        format = DataFormat.BOOLEAN;
                    }
                    if (classType.isAssignableFrom(Number.class) || classType.isAssignableFrom(Double.class)
                            || classType.isAssignableFrom(Float.class) || classType.isAssignableFrom(Short.class)
                            || classType.isAssignableFrom(Integer.class) || classType.isAssignableFrom(Long.class)
                            || classType.isAssignableFrom(Byte.class) || classType.isAssignableFrom(double.class)
                            || classType.isAssignableFrom(float.class) || classType.isAssignableFrom(short.class)
                            || classType.isAssignableFrom(int.class) || classType.isAssignableFrom(long.class)
                            || classType.isAssignableFrom(byte.class)) {
                        format = DataFormat.NUMERICAL;
                    }
                }
            } else {
                LOGGER.debug("Cannot create source for key {}", key);
            }
        }
        // new Exception("Format=" + format).printStackTrace();
        return format;
    }

    @Override
    public String getDescription(final IKey key) {
        String description = null;
        if (key != null) {
            description = getAlias(key);
            if ((description == null) || description.trim().isEmpty()) {
                description = getLabel(key);
                if ((description == null) || description.trim().isEmpty()) {
                    Map<String, String> attributeMap = getAttributes(key);
                    if ((attributeMap != null) && attributeMap.containsKey(LABEL_ATTRIBUTE + "description")) {
                        description = attributeMap.get(LABEL_ATTRIBUTE + "description");
                    }
                    if ((description != null) && (description.trim().equalsIgnoreCase("no description")
                            || description.trim().isEmpty())) {
                        description = null;
                    }
                }
            }
        }
        return description;
    }

    @Override
    public ITreeNode createNode(final IKey key) throws DataBrowserException {
        ITreeNode fileNode = null;
        if ((key instanceof CDMAKey) && (producer != null) && (((CDMAKey) key).getSource() != null)) {
            CDMAKey cdmaKey = (CDMAKey) key;
            IKey startDate = null;
            if (key.containProperty(DB_START_PROPERTY)) {
                startDate = (IKey) key.getPropertyValue(DB_START_PROPERTY);
                // To execute the command
                try {
                    getDataSource(startDate);
                } catch (Exception e) {
                    LOGGER.error(String.format("Impossible to read Key %s because %s", startDate, e.getMessage()), e);
                }
            }
            IKey endDate = null;
            if (key.containProperty(DB_END_PROPERTY)) {
                endDate = (IKey) key.getPropertyValue(DB_END_PROPERTY);
                try {
                    // To execute the command
                    getDataSource(endDate);
                } catch (Exception e) {
                    LOGGER.error(String.format("Impossible to read Key %s because %s", endDate, e.getMessage()), e);
                }
            }
            IKey sampling = null;
            if (key.containProperty(DB_SAMPLING_PROPERTY)) {
                sampling = (IKey) key.getPropertyValue(DB_SAMPLING_PROPERTY);
                try {
                    // To execute the command
                    getDataSource(sampling);
                } catch (Exception e) {
                    LOGGER.error(String.format("Impossible to read Key %s because %s", sampling, e.getMessage()), e);
                }
            }

            IKey samplingFactor = null;
            if (key.containProperty(DB_FACTOR_PROPERTY)) {
                samplingFactor = (IKey) key.getPropertyValue(DB_FACTOR_PROPERTY);
                try {
                    // To execute the command
                    getDataSource(samplingFactor);
                } catch (Exception e) {
                    LOGGER.error(String.format("Impossible to read Key %s because %s", samplingFactor, e.getMessage()),
                            e);
                }

            }

            boolean sourceOk = false;

            URI uri = cdmaKey.getSource();
            if (uri != null) {
                try {
                    String sourceNodeName = getStringFromURI(uri, true);
                    if (sourceNodeName != null) {
                        fileNode = new SourceTreeNode();
                        fileNode.setName(sourceNodeName);

                        fileNode.setData(key);
                        CDMAKeyFactory keyFactory = getKeyFactory(cdmaKey);
                        createGroupNode(keyFactory, fileNode, uri, new String[0]);
                        createItemNode(keyFactory, fileNode, uri, new String[0]);

                        addSourceEntry(key);
                        sourceOk = true;
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                    LOGGER.error(String.format("Impossible to createNode for key %s because %s", key, e.getMessage()),
                            e);
                    if (e instanceof DataBrowserException) {
                        throw (DataBrowserException) e;
                    } else {
                        throw new DataBrowserException("Impossible to open source " + e.getMessage());
                    }
                } finally {
                    if (!sourceOk) {
                        closeSource(key);
                    }
                }
            }

        } else {
            String keyInformation = "null key";
            if (key != null) {
                keyInformation = key.getInformationKey();
            }
            LOGGER.error("Impossible to createNode for key {} is not supported", key);
            throw new LoadException("Cannot load source", keyInformation + " is not supported");

        }
        return fileNode;
    }

    private void createGroupNode(final CDMAKeyFactory keyFactory, final ITreeNode parent, final URI uri,
            final String[] path) throws DataBrowserException {
        if ((parent != null) && (uri != null) && (producer != null) && (path != null) && (keyFactory != null)) {
            CDMAKey cdmaKey = keyFactory.generateKeyGroupList(uri, path);
            String[] groupList = getStringArrayValue(cdmaKey);
            if (groupList == null) {
                // a problem occurred
                String errorMessage;
                try {
                    File file = new File(uri);
                    if (file.exists()) {
                        if (file.canRead()) {
                            errorMessage = INVALID_FILE_STRUCTURE;
                            try (FileReader reader = new FileReader(file)) {
                                reader.read();
                            } catch (Exception e) {
                                errorMessage = FILE_IS_NOT_READABLE;
                            }
                        } else {
                            errorMessage = FILE_IS_NOT_READABLE;
                        }
                    } else {
                        errorMessage = INVALID_URI;
                    }
                } catch (Exception e) {
                    errorMessage = INVALID_FILE_STRUCTURE;
                }
                LOGGER.error("Group list for uri {} is null", cdmaKey);
                throw new BadFileException(errorMessage);
            } else {
                ITreeNode child = null;
                String[] newPath = null;
                CDMAKey groupKey = null;
                for (String groupName : groupList) {
                    newPath = Arrays.copyOf(path, path.length + 1);
                    newPath[newPath.length - 1] = groupName;
                    groupKey = keyFactory.generateKeyAttributeList(uri, newPath);
                    child = new GroupTreeNode();
                    child.setData(groupKey);
                    child.setName(groupName);
                    parent.addNodes(child);
                    createGroupNode(keyFactory, child, uri, newPath);
                    createItemNode(keyFactory, child, uri, newPath);
                    createDimensionNode(keyFactory, child, uri, newPath);
                }
            }
        }
    }

    private void createItemNode(final CDMAKeyFactory keyFactory, final ITreeNode parent, final URI uri,
            final String[] path) throws DataBrowserException {
        if ((parent != null) && (uri != null) && (path != null) && (keyFactory != null)) {
            CDMAKey cdmaKey = keyFactory.generateKeyItemList(uri, path);
            String[] itemList = getStringArrayValue(cdmaKey);
            String pluginId = keyFactory.getPluginRestriction();
            boolean isMamboPlugin = DBDataSourceSeeker.MAMBO_SOLEIL_PLUGIN_ID.equals(pluginId);
            if (itemList != null) {
                ItemTreeNode child = null;
                CDMAKey keyNode = null;
                String[] newPath = null;
                List<IKey> tmpKeyList = null;
                if (tmpKeyList == null) {
                    tmpKeyList = new ArrayList<IKey>();
                }
                for (String itemName : itemList) {
                    newPath = Arrays.copyOf(path, path.length + 1);
                    newPath[newPath.length - 1] = itemName;
                    keyNode = keyFactory.generateKeyArrayValue(uri, newPath);
                    child = new ItemTreeNode();
                    child.setName(itemName);
                    child.setData(keyNode);
                    parent.addNodes(child);

                    // fill scale map if is Mambo
                    if (isMamboPlugin) {
                        tmpKeyList = dimensionMap.get(parent);
                        if (tmpKeyList == null) {
                            tmpKeyList = new ArrayList<IKey>();
                            dimensionMap.put(parent, tmpKeyList);
                        }
                        if (itemName.endsWith(MAMBO_VALUE)) {
                            tmpKeyList.add(keyNode);
                        }
                    }
                }
            }
        }
    }

    private void createDimensionNode(final CDMAKeyFactory keyFactory, final ITreeNode parent, final URI uri,
            final String[] path) throws DataBrowserException {
        if ((parent != null) && (uri != null) && (path != null) && (keyFactory != null)) {
            CDMAKey cdmaKey = keyFactory.generateKeyDimensionList(uri, path);
            String[] dimensionList = getStringArrayValue(cdmaKey);
            if (dimensionList != null) {
                ItemTreeNode child = null;
                CDMAKey keyNode = null;
                List<IKey> tmpKeyList = null;
                int rank = -1;
                for (String dimensionName : dimensionList) {
                    keyNode = keyFactory.generateKeyDimensionValue(uri, path, dimensionName);
                    child = new ItemTreeNode();
                    child.setName(dimensionName);
                    child.setData(keyNode);
                    parent.addNodes(child);

                    if (MAMBO_TIME.equalsIgnoreCase(dimensionName)) {
                        tmpKeyList = dimensionMap.get(parent);
                        if ((tmpKeyList != null) && !tmpKeyList.isEmpty()) {
                            for (IKey tmpKey : tmpKeyList) {
                                rank = getRank(tmpKey);
                                if (rank == 1) {
                                    xScaleMap.put(tmpKey, keyNode);
                                } else if (rank > 1) {
                                    sliderScaleMap.put(tmpKey, keyNode);
                                }
                            }
                        }
                    }
                }
            }
        }
    }

    @Override
    public IKey generateSettableKey(final IKey key) {
        // nothing to do because databrowser is provided in read only mode
        return null;
    }

    @Override
    public IKey generateRegionKey(final IKey key, final int[] shape, final int[] origine) {
        IKey newKey = null;
        if ((key instanceof CDMAKey) && (shape != null) && (origine != null) && (shape.length == origine.length)) {
            CDMAKey cdmaKey = (CDMAKey) key;
            CDMAKeyFactory keyFactory = getKeyFactory(cdmaKey);
            if (keyFactory != null) {
                newKey = keyFactory.generateKeyArrayRegion(cdmaKey.getSource(), cdmaKey.getPath(), shape, origine);
            }
        }
        return newKey;
    }

    @Override
    public String getDisplayName(final IKey key) {
        String displayName;
        if (key instanceof CDMAKey) {
            CDMAKey cdmaKey = ((CDMAKey) key);
            // XXX Don't show file name in display name (too long string)
//            // extract source filename from the uri
//            URI uri = cdmaKey.getSource();
//            String sourcePath = uri.getPath();
//            String filename = sourcePath;
//            int lastIndexOf = sourcePath.lastIndexOf("/");
//            if (lastIndexOf > -1) {
//                filename = sourcePath.substring(lastIndexOf + 1);
//            }
//
//            StringBuilder sb = new StringBuilder(filename);
//            sb.append(" - ");
            StringBuilder sb = new StringBuilder();
            String[] pathParts = cdmaKey.getPath();
            StringBuilder keyPathBuilder = new StringBuilder();
            for (String part : pathParts) {
                keyPathBuilder.append('/');
                keyPathBuilder.append(part);
            }
            String keyPath = keyPathBuilder.toString();

            String alias = null;

            if ((pathParts != null) && (pathParts.length > 0)) {
                alias = getAlias(key);
            }

            if ((alias != null) && !alias.trim().isEmpty()) {
                sb.append(alias);
            } else {
                sb.append(keyPath);
            }
            displayName = sb.toString();
        } else {
            displayName = null;
        }

        return displayName;
    }

    private String getLabel(final IKey key) {
        String long_name = null;
        Map<String, String> attributeMap = getAttributes(key);
        if ((attributeMap != null) && attributeMap.containsKey(LABEL_ATTRIBUTE + "long_name")) {
            long_name = attributeMap.get(LABEL_ATTRIBUTE + "long_name");
        }
        return long_name;
    }

    public String getFileFilterDescription() {
        String description = null;
        if (factory != null) {
            description = factory.getPluginURIDetector().getURITypeDescription();
        }
        return description;
    }

    private static Set<Class<? extends IDataSourceSeeker>> getDataSourceSeekerImplementations() {
        Reflections reflections = new Reflections("org.cdma.gui.databrowser");
        Set<Class<? extends IDataSourceSeeker>> dataSourceSeekerSet = reflections
                .getSubTypesOf(IDataSourceSeeker.class);

        return dataSourceSeekerSet;
    }

    public void setSeeker(final IDataSourceSeeker seeker) {
        this.seeker = seeker;
    }

    @Override
    public IDataSourceSeeker getSeeker() {
        return seeker;
    }

    @Override
    public boolean isSourceOpened(final IKey sourceKey) {
        // Manage the case of a Logical URI that corresponding to a directory
        boolean isOpened = super.isSourceOpened(sourceKey);
        if (!isOpened && (sourceKey instanceof CDMAKey)) {
            URI uri = ((CDMAKey) sourceKey).getSource();
            isOpened = CDMAKeyFactory.isOpenedDataset(uri);
            // System.out.println(sourceKey.getInformationKey() + " is already open " + isOpened);
        }
        return isOpened;
    }

    @Override
    public void closeSource(final IKey sourceKey) {
        if (sourceKey instanceof CDMAKey) {
            CDMAKeyFactory.unregisterDataset((CDMAKey) sourceKey);
        }
        super.closeSource(sourceKey);
    }

    @Override
    public String getAlias(IKey key) {
        String alias = null;
        Map<String, String> attributeMap = getAttributes(key);
        if ((attributeMap != null) && attributeMap.containsKey(LABEL_ATTRIBUTE + "alias")) {
            alias = attributeMap.get(LABEL_ATTRIBUTE + "alias");
        }
        return alias;
    }

    @Override
    public boolean isTypeModifiable() {
        return true;
    }

}
