/*
 * This file is part of cdma-plugin.
 * 
 * cdma-plugin is free software: you can redistribute it and/or modify it under the terms of the GNU General Public
 * License as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later
 * version.
 * 
 * cdma-plugin is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the
 * implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for more
 * details.
 * 
 * You should have received a copy of the GNU General Public License along with cdma-plugin. If not, see
 * <https://www.gnu.org/licenses/>.
 */
package org.cdma.gui.databrowser.impl.cdma;

import java.awt.BorderLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.File;
import java.net.URI;
import java.util.ArrayList;
import java.util.List;

import javax.swing.BorderFactory;
import javax.swing.Box;
import javax.swing.JButton;
import javax.swing.JComboBox;
import javax.swing.JFileChooser;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTextField;
import javax.swing.SwingUtilities;
import javax.swing.SwingWorker;
import javax.swing.tree.DefaultTreeModel;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import fr.soleil.cdma.box.view.component.ExperimentTree;
import fr.soleil.comete.cdma.data.service.CDMAKeyFactory;
import fr.soleil.comete.definition.widget.util.BasicTreeNode;
import fr.soleil.comete.definition.widget.util.ITreeNode;
import fr.soleil.lib.project.ObjectUtils;
import fr.soleil.lib.project.swing.panel.LoadingPanel;

public class LogicalPanel extends JPanel implements ActionListener {

    private static final long serialVersionUID = 3500580349351513195L;

    private static final Logger LOGGER = LoggerFactory.getLogger(LogicalPanel.class);
    protected CDMADataSourceBrowser dataSourceBrowser;
    protected JTextField topUriField;
    protected JComboBox<String> viewCombo;

    protected ExperimentTree experimentTree;
    protected LoadingPanel<JScrollPane> experimentLoadingPanel;

    private JButton reloadTreeButton;

    private URI topUri;
    private String topUriText;
    private JButton topUriButton;
    private JFileChooser topUriChooser;

    public LogicalPanel(final CDMADataSourceBrowser browser) {
        dataSourceBrowser = browser;

        initPanel();
        layoutPanel();
    }

    private void initPanel() {
        CDMAKeyFactory cdmaKeyFactory = dataSourceBrowser.getKeyFactory(CDMADataSourceBrowser.FLAT_VIEW);

        // fill the view combo
        viewCombo = new JComboBox<>();
        List<String> availableViews = dataSourceBrowser.getAvailableViews();
        viewCombo.addItem(CDMADataSourceBrowser.FLAT_VIEW);
        for (String view : availableViews) {
            viewCombo.addItem(view);
        }
        viewCombo.addActionListener(this);

        experimentTree = new ExperimentTree("DataBrowser", null, null, null, cdmaKeyFactory);

        JScrollPane experimentScrollPane = new JScrollPane(experimentTree);
        experimentLoadingPanel = new LoadingPanel<JScrollPane>(experimentScrollPane);
        experimentLoadingPanel.setOpaque(false);
        experimentLoadingPanel.setLoading(false);

        topUriField = new JTextField();
        topUriField.addActionListener(this);

        topUriButton = new JButton(CDMADataSourceSeeker.MESSAGES.getString("Logical.WorkingDirectory.Button"));
        topUriButton.addActionListener(this);

        topUriChooser = new JFileChooser();
        topUriChooser.setMultiSelectionEnabled(false);
        topUriChooser.setFileSelectionMode(JFileChooser.DIRECTORIES_ONLY);

        reloadTreeButton = new JButton(CDMADataSourceSeeker.MESSAGES.getString("Logical.ReloadTree"));
        reloadTreeButton.addActionListener(this);
    }

    private void layoutPanel() {
        JLabel topUriTitle = new JLabel(CDMADataSourceSeeker.MESSAGES.getString("Logical.WorkingDirectory"));
        Box topUriPanel = Box.createHorizontalBox();
        topUriPanel.add(topUriTitle);
        topUriPanel.add(Box.createHorizontalStrut(5));
        topUriPanel.add(topUriField);
        topUriPanel.add(Box.createHorizontalStrut(3));
        topUriPanel.add(topUriButton);

        JLabel availableViewsLabel = new JLabel(CDMADataSourceSeeker.MESSAGES.getString("Logical.AvailableViews"));
        Box comboPanel = Box.createHorizontalBox();
        comboPanel.add(availableViewsLabel);
        comboPanel.add(Box.createHorizontalStrut(5));
        comboPanel.add(viewCombo);

        JPanel treePanel = new JPanel(new BorderLayout(0, 5));

        treePanel.add(topUriPanel, BorderLayout.NORTH);
        treePanel.add(experimentLoadingPanel, BorderLayout.CENTER);
        treePanel.add(reloadTreeButton, BorderLayout.SOUTH);

        setBorder(BorderFactory.createEmptyBorder(5, 5, 5, 5));
        setLayout(new BorderLayout(5, 5));
        add(comboPanel, BorderLayout.NORTH);
        add(treePanel, BorderLayout.CENTER);
    }

    public void setTopUri(final URI topUri) {
        setTopUri(topUri == null ? null : topUri.toString(), topUri);
    }

    public void setTopUri(final String topUriPath) {
        String uriText = topUriPath;
        URI uri = null;
        if (uriText != null) {
            boolean uriSet = false;
            uriText = uriText.replace(File.separator, "/");
            if (uriText.indexOf("//") < 0) {
                // No protocol defined: check for file path
                try {
                    File tempFile = new File(topUriPath);
                    if (tempFile.exists()) {
                        uri = tempFile.toURI();
                        uriSet = true;
                    }
                } catch (Exception e) {
                    LOGGER.error("Impossible to set top uri {}", e.getMessage());
                    LOGGER.debug("Stack trace", e);
                    uriSet = false;
                }
            }
            if (!uriSet) {
                try {
                    uri = URI.create(uriText);
                } catch (Exception e) {
                    LOGGER.error("Impossible to set top uri {}", e.getMessage());
                    LOGGER.debug("Stack trace", e);
                    uri = null;
                }
            }
        }
        setTopUri(topUriPath, uri);
    }

    protected void setTopUri(final String text, final URI topUri) {
        if (!ObjectUtils.sameObject(topUri, this.topUri)) {
            this.topUri = topUri;

            String nodeText = text;
            if (nodeText == null) {
                nodeText = "";
            }
            topUriText = nodeText;
            SwingUtilities.invokeLater(new Runnable() {
                @Override
                public void run() {
                    topUriField.setText(topUriText);
                }
            });
            updateTopURI();
        }
    }

    protected void updateTopURI() {
        ITreeNode rootNode = new BasicTreeNode();
        rootNode.setName(topUriText);
        rootNode.setData(topUri);
        if (experimentTree != null) {
            experimentTree.setRootNode(rootNode);
            SwingUtilities.invokeLater(new Runnable() {
                @Override
                public void run() {
                    experimentTree.setSelectionRow(0);
                    experimentTree.connectSelectedNodes();
                    experimentTree.expandAll(true);
                    experimentTree.clearSelection();
                }
            });
        }
    }

    @Override
    public void actionPerformed(final ActionEvent e) {
        if (e.getSource() == viewCombo) {
            String selectedView = (String) viewCombo.getSelectedItem();
            LOGGER.info("Selected view {}", selectedView);
            CDMAKeyFactory keyFactory = dataSourceBrowser.getKeyFactory(selectedView);
            experimentTree.setCdmaKeyFactory(keyFactory);
        } else if (e.getSource() == reloadTreeButton) {
            ((DefaultTreeModel) experimentTree.getModel()).reload();
        } else if (e.getSource() == topUriField) {
            topUriField.setEnabled(false);
            topUriButton.setEnabled(false);
            experimentLoadingPanel.setLoading(true);
            SwingWorker<Void, Void> worker = new SwingWorker<Void, Void>() {
                @Override
                protected Void doInBackground() {
                    setTopUri(topUriField.getText());
                    return null;
                }

                @Override
                protected void done() {
                    experimentLoadingPanel.setLoading(false);
                    topUriField.setEnabled(true);
                    topUriButton.setEnabled(true);
                }
            };
            worker.execute();
        } else if (e.getSource() == topUriButton) {
            File file = new File(topUriText);
            topUriChooser.setSelectedFile(file);
            int choice = topUriChooser.showOpenDialog(this);
            if (choice == JFileChooser.APPROVE_OPTION) {
                File selectedFile = topUriChooser.getSelectedFile();
                setTopUri(selectedFile.getPath());
            }
        }
    }

    public String getSelectedView() {
        String view = null;
        if (viewCombo != null) {
            view = (String) viewCombo.getSelectedItem();
        }
        return view;
    }

    public List<URI> getSelectedURIs() {
        List<URI> result = new ArrayList<URI>();

        if (!experimentLoadingPanel.isLoading()) {
            result = experimentTree.getSelectedURIs();
        }

        return result;
    }

    public void cleanPanel() {
        experimentTree.clearSelection();
    }

}
