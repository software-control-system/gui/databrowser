/*
 * This file is part of cdma-plugin.
 * 
 * cdma-plugin is free software: you can redistribute it and/or modify it under the terms of the GNU General Public
 * License as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later
 * version.
 * 
 * cdma-plugin is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the
 * implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for more
 * details.
 * 
 * You should have received a copy of the GNU General Public License along with cdma-plugin. If not, see
 * <https://www.gnu.org/licenses/>.
 */
package org.cdma.gui.databrowser.impl.cdma;

import java.io.File;
import java.util.ArrayList;
import java.util.List;

import org.cdma.gui.databrowser.impl.AbstractFileDataSourceSeeker;

import fr.soleil.data.service.IKey;

public abstract class AbstractCDMADataSourceSeeker extends AbstractFileDataSourceSeeker {

    public AbstractCDMADataSourceSeeker(final CDMADataSourceBrowser browser) {
        super(browser);
        setCDMADataSourceSeeker(browser);
    }

    public void setCDMADataSourceSeeker(final CDMADataSourceBrowser browser) {
        if (browser != null) {
            browser.setSeeker(this);
            if (dataSourceBrowser == null) {
                dataSourceBrowser = browser;
            }
        }
    }

    protected CDMADataSourceBrowser getCDMADataSourceBrowser() {
        return dataSourceBrowser instanceof CDMADataSourceBrowser ? (CDMADataSourceBrowser) dataSourceBrowser : null;
    }

    @Override
    protected String getFileDescription() {
        CDMADataSourceBrowser dataSourceBrowser = getCDMADataSourceBrowser();
        return dataSourceBrowser == null ? null : dataSourceBrowser.getFileFilterDescription();
    }

    @Override
    public boolean acceptFile(final File file) {
        CDMADataSourceBrowser dataSourceBrowser = getCDMADataSourceBrowser();
        return dataSourceBrowser == null ? false : dataSourceBrowser.canRead(file);
    }

    protected List<IKey> getFileChooserResult() {
        List<IKey> result = null;
        CDMADataSourceBrowser dataSourceBrowser = getCDMADataSourceBrowser();
        if ((fileChooser != null) && (dataSourceBrowser != null)) {
            File[] selectedFiles = fileChooser.getSelectedFiles();
            result = new ArrayList<>();
            for (File selectedFile : selectedFiles) {
                IKey key = dataSourceBrowser.getKeyFactory(CDMADataSourceBrowser.PHYSICAL_VIEW)
                        .generateKeySourceTitle(selectedFile.toURI());
                result.add(key);
            }
        }
        return result;
    }

}
