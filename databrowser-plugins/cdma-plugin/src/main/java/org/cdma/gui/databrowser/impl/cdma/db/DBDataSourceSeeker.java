/*
 * This file is part of cdma-plugin.
 * 
 * cdma-plugin is free software: you can redistribute it and/or modify it under the terms of the GNU General Public
 * License as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later
 * version.
 * 
 * cdma-plugin is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the
 * implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for more
 * details.
 * 
 * You should have received a copy of the GNU General Public License along with cdma-plugin. If not, see
 * <https://www.gnu.org/licenses/>.
 */
package org.cdma.gui.databrowser.impl.cdma.db;

import java.awt.BorderLayout;
import java.io.File;
import java.net.URI;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.ResourceBundle;

import javax.swing.BorderFactory;
import javax.swing.JComponent;
import javax.swing.JPanel;
import javax.swing.JTabbedPane;

import org.cdma.gui.databrowser.impl.cdma.AbstractCDMADataSourceSeeker;
import org.cdma.gui.databrowser.impl.cdma.CDMADataSourceBrowser;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import fr.soleil.comete.cdma.data.keys.CDMAKey;
import fr.soleil.comete.cdma.data.service.CDMAKeyFactory;
import fr.soleil.data.service.IKey;

public class DBDataSourceSeeker extends AbstractCDMADataSourceSeeker {

    private static final Logger LOGGER = LoggerFactory.getLogger(DBDataSourceSeeker.class);

    public static final ResourceBundle MESSAGES = ResourceBundle
            .getBundle("org.cdma.gui.databrowser.impl.cdma.db.messages");

    private JPanel mamboPanel;
    private ArchivingPanel hdbPanel;
    private ArchivingPanel tdbPanel;
    private JTabbedPane tabbedPane;
    private OptionsPanel optionsPanel;
    private static final String START_ATTRIBUTE = "startDate";
    private static final String END_ATTRIBUTE = "endDate";
    private static final String SAMPLING_ATTRIBUTE = "samplingType";
    private static final String FACTOR_ATTRIBUTE = "samplingFactor";
    public static final String MAMBO_SOLEIL_PLUGIN_ID = "MamboSoleil";

    public DBDataSourceSeeker() {
        super(null);
    }

    @Override
    public String getDataSourceBrowserId() {
        return MAMBO_SOLEIL_PLUGIN_ID;
    }

    private void initPanel() {

        initMamboVCPanel();
        initArchivingPanel(true);
        initArchivingPanel(false);

        tabbedPane = new JTabbedPane();
        tabbedPane.add(MESSAGES.getString("Mambo.TabName"), mamboPanel);
        if (hdbPanel != null) {
            tabbedPane.add(MESSAGES.getString("Mambo.HDB.TabName"), hdbPanel);
        }
        if (tdbPanel != null) {
            tabbedPane.add(MESSAGES.getString("Mambo.TDB.TabName"), tdbPanel);
        }

    }

    private void initMamboVCPanel() {
        if (mamboPanel == null) {
            // options panel creation
            optionsPanel = new OptionsPanel();

            optionsPanel.setBorder(BorderFactory.createCompoundBorder(BorderFactory.createEmptyBorder(6, 0, 6, 6),
                    BorderFactory.createTitledBorder(BorderFactory.createEtchedBorder(),
                            MESSAGES.getString("Options.Title"))));

            mamboPanel = new JPanel();
            mamboPanel.setLayout(new BorderLayout());
            mamboPanel.add(optionsPanel, BorderLayout.EAST);

            // File Chooser creation
            JComponent fileChooser = getFileChooser();
            if (fileChooser != null) {
                mamboPanel.add(fileChooser, BorderLayout.WEST);
            }
        }

    }

    private void initArchivingPanel(boolean hdb) {
        ArchivingPanel initPanel = hdbPanel;
        if (!hdb) {
            initPanel = tdbPanel;
        }
        if ((initPanel == null) && ArchivingUtil.isArchivingIsActive(hdb)) {
            initPanel = new ArchivingPanel(hdb);
            if (hdb) {
                hdbPanel = initPanel;
            } else {
                tdbPanel = initPanel;
            }
        }
    }

    @Override
    public JComponent getComponent() {
        if (mamboPanel == null) {
            initPanel();
        }
        return tabbedPane;
    }

    @Override
    public void cleanComponent() {
        if (mamboPanel != null) {
            optionsPanel.clearPanel();
            cleanFileChooser();
        }
    }

    @Override
    public List<IKey> getResult() {
        List<IKey> result = new ArrayList<IKey>();
        // mamboPanel
        List<IKey> mamboResult = getFileChooserResult();
        getFillArchivingProperties(mamboResult, optionsPanel);
        if ((mamboResult != null) && !mamboResult.isEmpty()) {
            result.addAll(mamboResult);
        }

        // hdbPanel
        List<IKey> hdbResult = getArchivingResult(hdbPanel);
        if ((hdbResult != null) && !hdbResult.isEmpty()) {
            result.addAll(hdbResult);
        }

        // tdbPanel
        List<IKey> tdbResult = getArchivingResult(tdbPanel);
        if ((tdbResult != null) && !tdbResult.isEmpty()) {
            result.addAll(tdbResult);
        }

        if ((result != null) && result.isEmpty()) {
            result = null;
        }

        return result;
    }

    private List<IKey> getArchivingResult(ArchivingPanel archivingPanel) {
        List<IKey> result = new ArrayList<IKey>();
        if (archivingPanel != null) {
            File generatedFile = archivingPanel.getGeneratedFile();
            if (generatedFile != null) {
                IKey key = getCDMADataSourceBrowser().getKeyFactory(CDMADataSourceBrowser.PHYSICAL_VIEW)
                        .generateKeySourceTitle(generatedFile.toURI());
                if (key != null) {
                    result.add(key);
                }
            }
            getFillArchivingProperties(result, archivingPanel.getOptionsPanel());
        }
        return result;
    }

    private void getFillArchivingProperties(List<IKey> origineListKey, OptionsPanel panel) {
        if ((panel != null) && (origineListKey != null) && panel.isPanelEnabled() && (dataSourceBrowser != null)) {
            CDMAKeyFactory keyFactory = getCDMADataSourceBrowser().getKeyFactory(CDMADataSourceBrowser.PHYSICAL_VIEW);
            for (IKey key : origineListKey) {
                if ((key != null) && (key instanceof CDMAKey)) {
                    URI uri = ((CDMAKey) key).getSource();
                    String[] path = ((CDMAKey) key).getPath();
                    LOGGER.trace("Selected path {}", Arrays.toString(path));

                    LOGGER.trace("{} = {}", START_ATTRIBUTE, panel.getStartDate());
                    IKey startKey = keyFactory.generateKeySetAttribute(uri, path, START_ATTRIBUTE,
                            panel.getStartDate());
                    key.registerProperty(CDMADataSourceBrowser.DB_START_PROPERTY, startKey);

                    LOGGER.trace("{} = {}", END_ATTRIBUTE, panel.getEndDate());
                    IKey endKey = keyFactory.generateKeySetAttribute(uri, path, END_ATTRIBUTE, panel.getEndDate());
                    key.registerProperty(CDMADataSourceBrowser.DB_END_PROPERTY, endKey);

                    LOGGER.trace("{} = {}", SAMPLING_ATTRIBUTE, panel.getSamplingPeriod());
                    IKey samplingType = keyFactory.generateKeySetAttribute(uri, path, SAMPLING_ATTRIBUTE,
                            panel.getSamplingPeriod().toString());
                    key.registerProperty(CDMADataSourceBrowser.DB_SAMPLING_PROPERTY, samplingType);

                    LOGGER.trace("{} = {}", FACTOR_ATTRIBUTE, panel.getSamplingFactor());
                    IKey samplingFactor = keyFactory.generateKeySetAttribute(uri, path, FACTOR_ATTRIBUTE,
                            panel.getSamplingFactor());
                    key.registerProperty(CDMADataSourceBrowser.DB_FACTOR_PROPERTY, samplingFactor);
                }
            }
        }
    }

    @Override
    public void startSeeking() {
        if (mamboPanel != null) {
            optionsPanel.startRefreshing();
        }
    }

    @Override
    public void stopSeeking() {
        if (mamboPanel != null) {
            optionsPanel.stopRefreshing();
        }
    }

}
