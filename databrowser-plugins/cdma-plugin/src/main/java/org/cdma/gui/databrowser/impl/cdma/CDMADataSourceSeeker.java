/*
 * This file is part of cdma-plugin.
 * 
 * cdma-plugin is free software: you can redistribute it and/or modify it under the terms of the GNU General Public
 * License as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later
 * version.
 * 
 * cdma-plugin is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the
 * implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for more
 * details.
 * 
 * You should have received a copy of the GNU General Public License along with cdma-plugin. If not, see
 * <https://www.gnu.org/licenses/>.
 */
package org.cdma.gui.databrowser.impl.cdma;

import java.net.URI;
import java.util.ArrayList;
import java.util.List;
import java.util.ResourceBundle;

import javax.swing.JComponent;
import javax.swing.JTabbedPane;
import javax.swing.event.ChangeEvent;
import javax.swing.event.ChangeListener;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import fr.soleil.comete.cdma.data.service.CDMAKeyFactory;
import fr.soleil.data.service.IKey;

public class CDMADataSourceSeeker extends AbstractCDMADataSourceSeeker implements ChangeListener {

    private static final Logger LOGGER = LoggerFactory.getLogger(CDMADataSourceSeeker.class);
    public static final ResourceBundle MESSAGES = ResourceBundle
            .getBundle("org.cdma.gui.databrowser.impl.cdma.messages");

    private JTabbedPane tabbedPane;
    private LogicalPanel logicalPanel;
    private boolean firstTime = true;

    public CDMADataSourceSeeker(final CDMADataSourceBrowser browser) {
        super(browser);
    }

    @Override
    public String getDataSourceBrowserId() {
        return null;
    }

    private JTabbedPane getFullPanel() {
        if (tabbedPane == null) {
            logicalPanel = new LogicalPanel(getCDMADataSourceBrowser());

            tabbedPane = new JTabbedPane();
            tabbedPane.addTab(MESSAGES.getString("Tab.Physical"), getFileChooser());
            tabbedPane.addTab(MESSAGES.getString("Tab.Logical"), logicalPanel);
            tabbedPane.addChangeListener(this);
        }

        return tabbedPane;
    }

    @Override
    public void cleanComponent() {
        if ((dataSourceBrowser != null) && getCDMADataSourceBrowser().isLogical()) {
            if (logicalPanel != null) {
                logicalPanel.cleanPanel();
            }
        }
        cleanFileChooser();
    }

    @Override
    public JComponent getComponent() {
        JComponent result = null;

        if ((dataSourceBrowser != null) && getCDMADataSourceBrowser().isLogical()) {
            result = getFullPanel();
        } else {
            result = getFileChooser();
        }

        return result;
    }

    @Override
    public List<IKey> getResult() {
        List<IKey> result = new ArrayList<>();

        // get the logical results
        if ((dataSourceBrowser != null) && getCDMADataSourceBrowser().isLogical()) {
            List<IKey> logicalResult = getLogicalResult();
            if (logicalResult != null) {
                result.addAll(logicalResult);
            }
        }

        // get the filechooser results
        List<IKey> fileChooserResult = getFileChooserResult();
        if (fileChooserResult != null) {
            result.addAll(fileChooserResult);
        }

        if (result.isEmpty()) {
            result = null;
        }
        return result;
    }

    protected List<IKey> getLogicalResult() {
        List<IKey> result = new ArrayList<>();
        if ((logicalPanel != null) && (logicalPanel.getSelectedView() != null)) {
            CDMAKeyFactory keyFactory = getCDMADataSourceBrowser().getKeyFactory(logicalPanel.getSelectedView());
            List<URI> selectedURIs = logicalPanel.getSelectedURIs();
            IKey key = null;
            LOGGER.trace("SelectedURIs");
            for (URI uri : selectedURIs) {
                if (getCDMADataSourceBrowser().isExperiment(uri)) {
                    LOGGER.trace("- {}", uri);
                    key = keyFactory.generateKeySourceTitle(uri);
                    result.add(key);
                }
            }
        }
        return result;
    }

    @Override
    public void stateChanged(final ChangeEvent e) {
        if ((tabbedPane.getSelectedComponent() == logicalPanel) && firstTime) {
            if (directory != null) {
                LOGGER.trace("directory ={}", directory);
                logicalPanel.setTopUri(directory);
            }
            firstTime = false;
        }
    }

}
