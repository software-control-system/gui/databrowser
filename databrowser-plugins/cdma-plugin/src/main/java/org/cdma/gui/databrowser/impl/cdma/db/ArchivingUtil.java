/*
 * This file is part of cdma-plugin.
 * 
 * cdma-plugin is free software: you can redistribute it and/or modify it under the terms of the GNU General Public
 * License as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later
 * version.
 * 
 * cdma-plugin is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the
 * implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for more
 * details.
 * 
 * You should have received a copy of the GNU General Public License along with cdma-plugin. If not, see
 * <https://www.gnu.org/licenses/>.
 */
package org.cdma.gui.databrowser.impl.cdma.db;

import org.cdma.gui.databrowser.impl.tango.TangoDataSourceBrowser;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import fr.esrf.TangoApi.Database;
import fr.esrf.TangoApi.DbDatum;
import fr.soleil.comete.tango.data.service.helper.TangoDeviceHelper;
import fr.soleil.comete.tango.data.service.helper.TangoExceptionHelper;
import fr.soleil.lib.project.SystemUtils;

public class ArchivingUtil {
    private static final Logger LOGGER = LoggerFactory.getLogger(ArchivingUtil.class);

    private static final String DB_USER = "DB_USER";
    private static final String DB_USER_DEVICE_PROPERTY = "DbUser";

    private static final String DB_PASSWORD = "DB_PASSWORD";
    private static final String DB_PASSWORD_DEVICE_PROPERTY = "DbPassword";

    private static final String DB_DRIVER = "DB_DRIVER";
    private static final String DB_TYPE_DEVICE_PROPERTY = "DbType";

    private static final String JDBC_MYSQL = "jdbc:mysql";
    private static final String ORACLE = "oracle";
    private static final String JDBC_ORACLE_THIN = "jdbc:oracle:thin";

    private static final String DB_RAC = "DB_RAC";
    private static final String IS_RAC_DEVICE_PROPERTY = "isRac";

    private static final String DB_SCHEMA = "DB_SCHEMA";
    private static final String DB_SCHEMA_DEVICE_PROPERTY = "DbSchema";

    private static final String DB_NAME = "DB_NAME";
    private static final String DB_NAME_DEVICE_PROPERTY = "DbName";
    private static final String DB_ONS_CONF_DEVICE_PROPERTY = "DbONSConf";

    private static final String DB_HOST = "DB_HOST";
    private static final String DB_HOST_DEVICE_PROPERTY = "DbHost";
    private static final String DB_TNS_NAMES_DEVICE_PROPERTY = "DbTnsNames";

    private static final String HDB_CLASS_NAME = "HdbExtractor";
    private static final String TDB_CLASS_NAME = "TdbExtractor";

    private static final String HDB_ARCHIVER_CLASS_NAME = "HdbArchiver";
    private static final String TDB_ARCHIVER_CLASS_NAME = "TdbArchiver";

    private static final String HDB_PREFIX = "H";
    private static final String TDB_PREFIX = "T";

    public static final String ALL_ARCHIVED_CMD = "GetAttNameAll";
    public static final String CURRENT_ARCHIVED_CMD = "GetCurrentArchivedAtt";

    private static String hdbExtractorDevice;
    private static String tdbExtractorDevice;

    private static boolean variablesInitialized = false;

    static {
        // Init the environment for Archiving to activate the Mambo plugin
        initArchivingEnvironnement();
    }

    public static boolean isArchivingIsActive(boolean hdb) {
        boolean isActive = false;
        String extractorDevice = getExtractorDevice(hdb);
        String userPropertyName = getProperty(hdb, DB_USER);
        String userProperty = SystemUtils.getSystemProperty(userPropertyName);
        isActive = ((extractorDevice != null) && (!extractorDevice.isEmpty()))
                || ((userProperty != null) && (!userProperty.isEmpty()));
        return isActive;
    }

    public static String getExtractorDevice(boolean hdb) {
        initArchivingEnvironnement();
        String deviceName = hdbExtractorDevice;
        if (!hdb) {
            deviceName = tdbExtractorDevice;
        }
        return deviceName;
    }

    private static void initExtractorDevice(boolean hdb) {
        String extractorDevice = hdbExtractorDevice;
        if (!hdb) {
            extractorDevice = tdbExtractorDevice;
        }
        if (extractorDevice == null) {
            try {
                String extractorClassName = HDB_CLASS_NAME;
                if (!hdb) {
                    extractorClassName = TDB_CLASS_NAME;
                }

                if (TangoDataSourceBrowser.isTangoEnabled()) {
                    Database database = TangoDeviceHelper.getDatabase();
                    if (database != null) {
                        String[] extractorList = database.get_device_exported_for_class(extractorClassName);
                        if ((extractorList != null) && (extractorList.length > 0)) {
                            extractorDevice = extractorList[0];
                            if (hdb) {
                                hdbExtractorDevice = extractorDevice;
                            } else {
                                tdbExtractorDevice = extractorDevice;
                            }
                        }
                    }
                }
            } catch (Exception e) {
                LOGGER.error("Error while recovering extractor device: " + TangoExceptionHelper.getErrorMessage(e), e);
            }
        }
    }

    public static void initArchivingEnvironnement() {
        if ((!variablesInitialized) && TangoDataSourceBrowser.isTangoEnabled()) {
            initExtractorDevice(true);
            initExtractorDevice(false);
            initArchivingEnvironnement(true);
            initArchivingEnvironnement(false);
            variablesInitialized = true;
        }
    }

    private static void initArchivingEnvironnement(boolean hdb) {
        final String extractorDevice;
        final String archiverDevice;
        if (hdb) {
            extractorDevice = hdbExtractorDevice;
            archiverDevice = HDB_ARCHIVER_CLASS_NAME;
        } else {
            extractorDevice = tdbExtractorDevice;
            archiverDevice = TDB_ARCHIVER_CLASS_NAME;
        }

        if ((extractorDevice != null) && !extractorDevice.isEmpty()) {
            String userPropertyName = getProperty(hdb, DB_USER);
            String userProperty = SystemUtils.getSystemProperty(userPropertyName);
            if ((userProperty == null) || userProperty.isEmpty()) {
                userProperty = getDeviceProperty(extractorDevice, DB_USER_DEVICE_PROPERTY);
                if ((userProperty != null) && !userProperty.isEmpty()) {
                    System.setProperty(userPropertyName, userProperty);
                }
            }

            String passPropertyName = getProperty(hdb, DB_PASSWORD);
            String passProperty = SystemUtils.getSystemProperty(passPropertyName);
            if ((passProperty == null) || passProperty.isEmpty()) {
                passProperty = getDeviceProperty(extractorDevice, DB_PASSWORD_DEVICE_PROPERTY);
                if ((passProperty != null) && !passProperty.isEmpty()) {
                    System.setProperty(passPropertyName, passProperty);
                }
            }
        }

        if ((archiverDevice != null) && !archiverDevice.isEmpty()) {
            String dbDriverPropertyName = getProperty(hdb, DB_DRIVER);
            String driverProperty = SystemUtils.getSystemProperty(dbDriverPropertyName);
            if ((driverProperty == null) || driverProperty.isEmpty()) {
                driverProperty = getClassProperty(archiverDevice, DB_TYPE_DEVICE_PROPERTY);
                if ((driverProperty != null) && !driverProperty.isEmpty()) {
                    String driverType = JDBC_MYSQL;
                    if (driverProperty.toLowerCase().contains(ORACLE)) {
                        driverType = JDBC_ORACLE_THIN;
                    }
                    System.setProperty(dbDriverPropertyName, driverType);
                }
            }

            String racPropertyName = getProperty(hdb, DB_RAC);
            String racProperty = SystemUtils.getSystemProperty(racPropertyName);
            if ((racProperty == null) || racProperty.isEmpty()) {
                racProperty = getClassProperty(archiverDevice, IS_RAC_DEVICE_PROPERTY);
                if ((racProperty != null) && !racProperty.isEmpty()) {
                    System.setProperty(racPropertyName, racProperty);
                }
            }

            boolean isRac = Boolean.parseBoolean(racProperty);

            String schemaPropertyName = getProperty(hdb, DB_SCHEMA);
            String schemaProperty = SystemUtils.getSystemProperty(schemaPropertyName);
            if ((schemaProperty == null) || schemaProperty.isEmpty()) {
                schemaProperty = getClassProperty(archiverDevice, DB_SCHEMA_DEVICE_PROPERTY);
                if ((schemaProperty != null) && !schemaProperty.isEmpty()) {
                    System.setProperty(schemaPropertyName, schemaProperty);
                }
            }

            String namePropertyName = getProperty(hdb, DB_NAME);
            String nameProperty = SystemUtils.getSystemProperty(namePropertyName);
            if ((nameProperty == null) || nameProperty.isEmpty()) {
                if (isRac) {
                    nameProperty = getClassProperty(archiverDevice, DB_ONS_CONF_DEVICE_PROPERTY);
                } else {
                    nameProperty = getClassProperty(archiverDevice, DB_NAME_DEVICE_PROPERTY);
                }
                if ((nameProperty != null) && !nameProperty.isEmpty()) {
                    System.setProperty(namePropertyName, nameProperty);
                }
            }

            String hostPropertyName = getProperty(hdb, DB_HOST);
            String hostProperty = SystemUtils.getSystemProperty(hostPropertyName);
            if ((hostProperty == null) || hostProperty.isEmpty()) {
                if (isRac) {
                    hostProperty = getClassProperty(archiverDevice, DB_TNS_NAMES_DEVICE_PROPERTY);
                } else {
                    hostProperty = getClassProperty(archiverDevice, DB_HOST_DEVICE_PROPERTY);
                }
                if ((hostProperty != null) && !hostProperty.isEmpty()) {
                    System.setProperty(hostPropertyName, hostProperty);
                }
            }
        }
    }

    private static String getDeviceProperty(final String deviceName, final String propertyName) {
        String value = null;
        try {
            Database database = TangoDeviceHelper.getDatabase();
            if (database != null) {
                DbDatum db_property = database.get_device_property(deviceName, propertyName);
                if ((db_property != null) && !db_property.is_empty()) {
                    value = db_property.extractString();
                }
            }
        } catch (Exception e) {
            LOGGER.error("Error while reading device property: " + TangoExceptionHelper.getErrorMessage(e), e);
        }
        return value;
    }

    private static String getClassProperty(final String className, final String propertyName) {
        String value = null;
        try {
            Database database = TangoDeviceHelper.getDatabase();
            if (database != null) {
                DbDatum db_property = database.get_class_property(className, propertyName);
                if ((db_property != null) && !db_property.is_empty()) {
                    value = db_property.extractString();
                }
            }
        } catch (Exception e) {
            LOGGER.error("Error while reading device class property: " + TangoExceptionHelper.getErrorMessage(e), e);
        }
        return value;
    }

    private static String getProperty(boolean hdb, final String propName) {
        final String suffixe = hdb ? HDB_PREFIX : TDB_PREFIX;
        return suffixe + propName;
    }

}
