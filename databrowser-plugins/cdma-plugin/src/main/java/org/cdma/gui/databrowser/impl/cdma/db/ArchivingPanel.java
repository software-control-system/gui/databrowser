/*
 * This file is part of cdma-plugin.
 * 
 * cdma-plugin is free software: you can redistribute it and/or modify it under the terms of the GNU General Public
 * License as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later
 * version.
 * 
 * cdma-plugin is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the
 * implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for more
 * details.
 * 
 * You should have received a copy of the GNU General Public License along with cdma-plugin. If not, see
 * <https://www.gnu.org/licenses/>.
 */
package org.cdma.gui.databrowser.impl.cdma.db;

import java.awt.BorderLayout;
import java.awt.FlowLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.File;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Date;
import java.util.List;
import java.util.ResourceBundle;

import javax.swing.JPanel;

import org.cdma.plugin.mambo.utils.MamboVCGenerator;

import fr.soleil.comete.bean.tangotree.IDeviceSelector;
import fr.soleil.comete.bean.tangotree.datasource.GenericSourceDevice;
import fr.soleil.comete.bean.tangotree.datasource.ISourceDevice;
import fr.soleil.comete.bean.tangotree.swing.SwingDeviceSelector;
import fr.soleil.comete.definition.widget.IComponent;
import fr.soleil.comete.definition.widget.IFileBrowser;
import fr.soleil.comete.swing.FileBrowserFieldButton;
import fr.soleil.comete.swing.Panel;
import fr.soleil.comete.swing.ScrollPane;
import fr.soleil.comete.swing.TextArea;
import fr.soleil.lib.project.ObjectUtils;
import fr.soleil.lib.project.swing.ConstrainedCheckBox;

public class ArchivingPanel extends Panel {

    private static final long serialVersionUID = 4669817673447062259L;

    private static final ResourceBundle MESSAGES = ResourceBundle
            .getBundle("org.cdma.gui.databrowser.impl.cdma.db.messages");
    private boolean hdb;
    private OptionsPanel optionsPanel;
    private final String extractorDevice;
    private ISourceDevice currentArchivedSource;
    private ISourceDevice allArchivedSource;
    private IDeviceSelector archivingSelector;
    private IComponent archivingComponent;
    private ConstrainedCheckBox longTerm;
    private FileBrowserFieldButton fileBrowser;
    private TextArea deviceArea;

    public ArchivingPanel(boolean hdb) {
        this.hdb = hdb;
        extractorDevice = ArchivingUtil.getExtractorDevice(hdb);
        initSource();
        initPanel();
    }

    private void initSource() {
        if (extractorDevice != null) {
            currentArchivedSource = new GenericSourceDevice(extractorDevice + "/" + ArchivingUtil.CURRENT_ARCHIVED_CMD);
            allArchivedSource = new GenericSourceDevice(extractorDevice + "/" + ArchivingUtil.ALL_ARCHIVED_CMD);
            archivingSelector = new SwingDeviceSelector(currentArchivedSource, true, true);
        }
    }

    private void initPanel() {
        setLayout(new BorderLayout());

        if (archivingSelector != null) {
            final ConstrainedCheckBox currentAttributeCB = new ConstrainedCheckBox(
                    MESSAGES.getString("Mambo.CurrentArchived"));
            currentAttributeCB.setSelected(true);
            currentAttributeCB.addActionListener(new ActionListener() {

                @Override
                public void actionPerformed(ActionEvent e) {
                    boolean isSelected = currentAttributeCB.isSelected();
                    ISourceDevice currentSelected = archivingSelector.getSourceDevice();
                    if (isSelected && (currentSelected != currentArchivedSource)) {
                        archivingSelector.setSourceDevice(currentArchivedSource);
                    } else {
                        archivingSelector.setSourceDevice(allArchivedSource);
                    }
                }
            });
            archivingComponent = archivingSelector.getSelectorComponent();
            add(currentAttributeCB, BorderLayout.NORTH);
        } else {
            ScrollPane scrollPane = new ScrollPane();
            deviceArea = new TextArea();
            scrollPane.setViewportView(deviceArea);
            archivingComponent = scrollPane;

        }
        optionsPanel = new OptionsPanel();
        optionsPanel.enableOverridePart();

        add(archivingComponent, BorderLayout.CENTER);
        add(optionsPanel, BorderLayout.EAST);

        JPanel southPanel = new JPanel(new FlowLayout(FlowLayout.LEFT));
        final ConstrainedCheckBox userFileCB = new ConstrainedCheckBox("Save VC configuration");
        southPanel.add(userFileCB);
        userFileCB.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                boolean selected = userFileCB.isSelected();
                fileBrowser.setEnabled(selected);
                fileBrowser.setEditable(selected);
                if (!selected) {
                    fileBrowser.setText(ObjectUtils.EMPTY_STRING);
                    fileBrowser.setFile(null);
                }
            }
        });
        fileBrowser = new FileBrowserFieldButton();
        fileBrowser.setSize(300, 20);
        fileBrowser.setBrowserType(IFileBrowser.PATH);
        fileBrowser.setSendButtonVisible(false);
        fileBrowser.setEnabled(false);
        fileBrowser.setEditable(false);
        southPanel.add(fileBrowser);

        if (!hdb) {
            longTerm = new ConstrainedCheckBox("Long term");
            southPanel.add(longTerm);
        }

        add(southPanel, BorderLayout.SOUTH);
    }

    public OptionsPanel getOptionsPanel() {
        return optionsPanel;
    }

    private List<String> getSelectedDevices() {
        List<String> selectedDevices = null;
        if (ArchivingUtil.isArchivingIsActive(hdb)) {
            if (archivingSelector != null) {
                selectedDevices = archivingSelector.getSelectedDevices();
            } else if (deviceArea != null) {
                String text = deviceArea.getText();
                if (text != null) {
                    String[] split = text.split("\n");
                    if ((split != null) && (split.length > 0)) {
                        selectedDevices = new ArrayList<String>();
                        Collections.addAll(selectedDevices, split);
                    }
                }
            }
        }
        return selectedDevices;
    }

    public File getGeneratedFile() {
        File vcFile = null;
        List<String> selectedDevices = getSelectedDevices();
        if ((selectedDevices != null) && !selectedDevices.isEmpty()) {
            MamboVCGenerator mamboGenerator = new MamboVCGenerator("databrowserVC");
            mamboGenerator.setHistoric(hdb);
            mamboGenerator.setAttributeList(selectedDevices);
            mamboGenerator.setStartDate(new Date(optionsPanel.getStartDate()));
            mamboGenerator.setEndDate(new Date(optionsPanel.getEndDate()));
            mamboGenerator.setSamplingFactor(optionsPanel.getSamplingFactor());
            mamboGenerator.setSamplingPeriod(optionsPanel.getSamplingPeriod());
            if (fileBrowser.isEditable()) {
                String userFile = fileBrowser.getText();
                mamboGenerator.setUserPath(userFile);
            }
            if (longTerm != null) {
                mamboGenerator.setLongTerm(longTerm.isSelected());
            }
            try {
                vcFile = mamboGenerator.getVcFile();
            } catch (Exception e) {
                // TODO: handle exception
            }
        }
        return vcFile;
    }

}
