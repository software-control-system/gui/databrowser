<p style="text-align: center"><a href="https://www.synchrotron-soleil.fr/en"><img src="https://www.synchrotron-soleil.fr/sites/default/files/logo_0.png" alt="SOLEIL Synchrotron"/></a></p>

In order to visualize the content of the downloaded HDF, Nexus files (and Tango Control System data), you have to get the DataBrowser application.

<p style="text-align: center"><img src="https://support.hdfgroup.org/images/hdf_logo.jpg" alt="HDF" height=55 />&nbsp;&nbsp;<img src="https://manual.nexusformat.org/_images/NeXus.png" alt="NeXus" height=55 />&nbsp;&nbsp;<img src="https://www.tango-controls.org/static/tango/img/logo_tangocontrols.png" alt="Tango" height=55 />&nbsp;&nbsp;<img src="https://epics-controls.org/wp-content/uploads/2018/08/EPICS_black_blue_logo_rgb_1000p_v03.png" alt="EPICS" height=55 /></p>

---
# What is DataBrowser?
_(see [Tango Meeting presentation](doc/pdf/Tango_Meeting-SOLEIL_DataBrowser.pdf "Tango Meeting presentation of DataBrowser"))_

The DataBrowser is an Open Source Scientific Application developped in collaboration by [SOLEIL](http://www.synchrotron-soleil.fr/portal/page/portal/Accueil) and [ANSTO](http://www.ansto.gov.au/) institutes. It is pubished under [GPL license](http://www.gnu.org/licenses/gpl.html).
This Application allows the visualisation of scientific data files based on [HDF format](http://www.hdfgroup.org/) (like HDF and [Nexus](http://www.nexusformat.org/) files), and [Tango](http://www.tango-controls.org/) Control System data.
Supports plugins:
* **<span style="color: blue">HDF 5, Nexus</span>** files (.nxs, .h5)
* **<span style="color: blue">TANGO</span>** attibutes (atktrend atkpanel atktunning configuration files)
* **<span style="color: blue">HDB</span>** (Java) Tango Historical Database (Mambo VC .vc files)
* **<span style="color: blue">EDF</span>** ESRF data format (.edf files)
* **<span style="color: blue">EPICS</span>** Process Variables (Channel access implementation EPICS .db files) and Archiver Appliance (Google protobuf .pb files)
![DataBrowser-Screenshot](doc/images/DataBrowser-ScreenShot-26-05-2014.png)

---
# User Manual
_(For SOLEIL users, a detailed user manual is available in [confluence](http://confluence.synchrotron-soleil.fr/pages/viewpage.action?pageId=7045437))_

1. Lauch the DataBrowser
2. Open a File with Open Button
3. Navigate on the Tree on the Left
4. Click on an Item to visualize the value on the Right.

You can get the user manual inside the application by clicking on "Help/About..." menu and then "User Manual" link.

---
# Download links
**<span style="color: red"><u>Caution! DataBrowser software works with java 8 only.</u></span>**

* [version 1.2.0](http://svn.code.sf.net/p/cometeapps/code/DataBrowser/delivery/DataBrowser-1.2.0-6800-2018-01-05-141058.zip) last update January 2018 technical contact [Raphaël GIRARDOT](mailto:raphael.girardot@synchrotron-soleil.fr)
* [version 1.2.1](http://svn.code.sf.net/p/cometeapps/code/DataBrowser/delivery/DataBrowser-1.2.1-7036-2019-02-25-150920.zip) last update February 2019 technical contact [Raphaël GIRARDOT](mailto:raphael.girardot@synchrotron-soleil.fr)
* [version 1.3.0-7521](https://svn.code.sf.net/p/cometeapps/code/DataBrowser/delivery/DataBrowser-1.3.0-7521-2019-11-05-105650.zip) last update November 2019 technical contact [Raphaël GIRARDOT](mailto:raphael.girardot@synchrotron-soleil.fr) _(Uses latest versions of HDF5 libraries)_
* [version 1.3.0-7615](https://svn.code.sf.net/p/cometeapps/code/DataBrowser/delivery/DataBrowser-1.3.0-7615-2020-01-24-150708.zip) last update January 2020 technical contact [Raphaël GIRARDOT](mailto:raphael.girardot@synchrotron-soleil.fr) _(Compatibility with standalone openjdk and 1st help in case application is not working)_
* [version 1.3.1](http://katy.saintin.free.fr/soleil/databrowser/DataBrowserCEA-1.3.1-2021-07.zip) last update July 2021 technical contact [Katy SAINTIN](mailto:katy.saintin@cea.fr) _(include EPICS CS and Archive Appliance EPICS plugins, HDF improvment, Muscade plugin)_
* [version 1.3.3-7933](https://svn.code.sf.net/p/cometeapps/code/DataBrowser/delivery/DataBrowser-1.3.3-7933-2022-01-05-162749.zip) last update January 2022 technical contact [Raphaël GIRARDOT](mailto:raphael.girardot@synchrotron-soleil.fr) _(Less CPU consumption)_
* [version 1.3.4-8024](https://svn.code.sf.net/p/cometeapps/code/DataBrowser/delivery/DataBrowser-1.3.4-8024-2022-05-12-103314.zip) last update May 2022 technical contact [Raphaël GIRARDOT](mailto:raphael.girardot@synchrotron-soleil.fr) _(Better scalars UI)_
* [version 1.3.5-8050](https://svn.code.sf.net/p/cometeapps/code/DataBrowser/delivery/DataBrowser-1.3.5-8050-2022-08-23-115503.zip) last update August 2022 technical contact [Raphaël GIRARDOT](mailto:raphael.girardot@synchrotron-soleil.fr) _(ATK trend files opening bug fix)_
* [version 1.3.9-251](https://svn.code.sf.net/p/cometeapps/code/DataBrowser/delivery/DataBrowser-1.3.9-251-2023-12-19-120743.zip) last update December 2023 technical contact [Raphaël GIRARDOT](mailto:raphael.girardot@synchrotron-soleil.fr) _(Added the possibility to set ImageViewer default axis format. Various bug fixes)_

---
# Installation
1. Download the archive.
2. Unzip the archive.
   * <span style="color: red"><b><u>Warning for Windows OS:</u></b></span> if you unzipped on an SSD, put the folder at the root folder of your SSD and rename it with a shorter name. Otherwise, DataBrowser won't launch.
   * Example: If you unzipped it in **<span style="font-family: monospace">D:\Your Name\Your Programs\My Experiments\DataBrowser a.b.c-index-full build date with hour</span>** and **<span style="font-family: monospace">D:</span>** is on an SSD, move it to **<span style="font-family: monospace">D:\DataBrowser a.b.c-index</span>**.
3. Launch the executable file according to your Operating System:
   * <span style="font-family: monospace"><b>databrowser.bat</b></span> for windows
   * <span style="font-family: monospace"><b>databrowser.sh</b></span> for linux system
   * <span style="font-family: monospace"><b>databrowser.command</b></span> for mac

---
Technical contacts: [Raphaël GIRARDOT](mailto:raphael.girardot@synchrotron-soleil.fr) and [Katy SAINTIN](mailto:katy.saintin@cea.fr)
