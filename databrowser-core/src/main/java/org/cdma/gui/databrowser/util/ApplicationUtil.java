/*
 * This file is part of databrowser-core.
 * 
 * databrowser-core is free software: you can redistribute it and/or modify it under the terms of the GNU General Public
 * License as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later
 * version.
 * 
 * databrowser-core is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the
 * implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for more
 * details.
 * 
 * You should have received a copy of the GNU General Public License along with databrowser-core. If not, see
 * <https://www.gnu.org/licenses/>.
 */
package org.cdma.gui.databrowser.util;

import java.io.File;
import java.io.UnsupportedEncodingException;
import java.net.URL;
import java.net.URLDecoder;
import java.security.CodeSource;
import java.util.logging.Logger;

import javax.swing.SwingUtilities;

public class ApplicationUtil {

    private static final Logger LOGGER = Logger.getLogger(ApplicationUtil.class.getName());

    public static String getFileJarName(final Class<?> clazz) {
        String jarFileName = getCompleteFileJarName(clazz);
        if ((jarFileName != null) && !jarFileName.isEmpty()) {
            int indexEnd = jarFileName.indexOf("-");
            if (indexEnd > -1) {
                jarFileName = jarFileName.substring(0, indexEnd);
            }
        }
        return jarFileName;
    }

    private static String getCompleteFileJarName(Class<?> clazz) {
        String completeJarFilename = null;
        if (clazz != null) {
            CodeSource source = clazz.getProtectionDomain().getCodeSource();
            URL location = source.getLocation();
            String path = location.getPath();

            try {
                String decodedPath = URLDecoder.decode(path, "UTF-8");
                File file = new File(decodedPath);
                String name = file.getName();

                if (name.endsWith(".jar")) {
                    completeJarFilename = name;
                }
            } catch (UnsupportedEncodingException e) {
                LOGGER.severe("Impossible to read jar file from class " + clazz + " because " + e.getMessage());
            }
        }
        return completeJarFilename;
    }

    public static String getFileJarVersion(Class<?> clazz) {
        String version = null;
        if (clazz != null) {
            String jarFileName = getCompleteFileJarName(clazz);
            if ((jarFileName != null) && !jarFileName.isEmpty()) {
                int indexEnd = jarFileName.lastIndexOf(".jar");
                int indexBegin = jarFileName.lastIndexOf("-");
                if ((indexEnd > -1) && (indexBegin > -1)) {
                    version = jarFileName.substring(indexBegin + 1, indexEnd);
                }
            }
        }
        return version;
    }

    public static void runInEDT(final Runnable runnable) {
        if (SwingUtilities.isEventDispatchThread()) {
            runnable.run();
        } else {
            SwingUtilities.invokeLater(runnable);
        }
    }

}
