/*
 * This file is part of databrowser-core.
 * 
 * databrowser-core is free software: you can redistribute it and/or modify it under the terms of the GNU General Public
 * License as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later
 * version.
 * 
 * databrowser-core is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the
 * implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for more
 * details.
 * 
 * You should have received a copy of the GNU General Public License along with databrowser-core. If not, see
 * <https://www.gnu.org/licenses/>.
 */
package org.cdma.gui.databrowser.impl;

import java.io.File;
import java.util.Timer;
import java.util.TimerTask;

import javax.swing.JFileChooser;
import javax.swing.JOptionPane;
import javax.swing.UIManager;
import javax.swing.filechooser.FileFilter;

import org.cdma.gui.databrowser.interfaces.IDataSourceBrowser;
import org.cdma.gui.databrowser.interfaces.IDataSourceSeeker;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import fr.soleil.lib.project.ObjectUtils;

public abstract class AbstractFileDataSourceSeeker implements IDataSourceSeeker {

    private static final Logger LOGGER = LoggerFactory.getLogger(AbstractFileDataSourceSeeker.class);

    private static final String SELECTED_DIRECTORY = "selectedDirectory={}";
    private static final String DIRECTORY_IS_NOT_READABLE = "{} directory is not readable ";
    private static final String PERMISSION_DENIED_ON = "Permission denied on ";
    private static final String DIRECTORTY_PERMISSION = "Directorty permission";

    protected static final long DIRECTORY_SCAN_PERIOD = 1000;// 1s

    protected Timer timer;
    protected IDataSourceBrowser dataSourceBrowser;

    protected JFileChooser fileChooser;
    protected String directory;
    protected final String timerName;
    protected File currentDirectory;
    protected volatile long lastModified;
    protected volatile long lastDirectoryUpdateDate;

    public AbstractFileDataSourceSeeker(final IDataSourceBrowser browser) {
        dataSourceBrowser = browser;
        timerName = getClass().getSimpleName() + ": Refresh current directory";
    }

    @Override
    public void setDirectory(final String path) {
        directory = path;
        if ((fileChooser != null) && (directory != null)) {
            fileChooser.setCurrentDirectory(new File(directory));
        }
    }

    @Override
    public String getDirectory() {
        if (fileChooser != null) {
            File file = fileChooser.getCurrentDirectory();
            if (file != null) {
                directory = file.getAbsolutePath();
            }
        }
        return directory;
    }

    /**
     * 
     * @param file
     * @return true if the file is readable
     */
    public abstract boolean acceptFile(final File file);

    /**
     * @return the description of file
     */
    protected abstract String getFileDescription();

    protected void cleanFileChooser() {
        if (fileChooser != null) {
            fileChooser.setSelectedFiles(null);
        }
    }

    protected void initFileChooser() {
        fileChooser = new JFileChooser();
        fileChooser.setOpaque(true);
        fileChooser.setBackground(UIManager.getColor("Panel.background"));
        fileChooser.setFileSelectionMode(JFileChooser.FILES_ONLY);
        fileChooser.setMultiSelectionEnabled(true);
        fileChooser.setControlButtonsAreShown(false);
        fileChooser.addPropertyChangeListener(JFileChooser.DIRECTORY_CHANGED_PROPERTY, (evt) -> {
            Object newValue = evt.getNewValue();
            if (newValue instanceof File) {
                File newFile = (File) newValue;
                if (!ObjectUtils.sameObject(newFile, currentDirectory)) {
                    currentDirectory = (File) newValue;
                    LOGGER.trace(SELECTED_DIRECTORY, currentDirectory.getAbsolutePath());
                    if (currentDirectory.canRead()) {
                        lastDirectoryUpdateDate = System.currentTimeMillis();
                        lastModified = currentDirectory.lastModified();
                    } else {
                        LOGGER.error(DIRECTORY_IS_NOT_READABLE, currentDirectory.getAbsolutePath());
                        JOptionPane.showMessageDialog(fileChooser,
                                PERMISSION_DENIED_ON + currentDirectory.getAbsolutePath(), DIRECTORTY_PERMISSION,
                                JOptionPane.ERROR_MESSAGE);
                    }
                }
            }
        });
        if (directory != null) {
            fileChooser.setCurrentDirectory(new File(directory));
        }
        fileChooser.setFileFilter(new FileFilter() {
            @Override
            public boolean accept(final File f) {
                boolean result = false;
                if ((f != null) && ((currentDirectory == null) || currentDirectory.canRead())) {
                    if (f.isDirectory()) {
                        // always accept directories
                        result = true;
                    } else if (f.canRead()) {
                        // For file check the read property before call plugin method
                        result = acceptFile(f);
                    }
                }
                return result;
            }

            @Override
            public String getDescription() {
                return getFileDescription();
            }
        });
    }

    protected JFileChooser getFileChooser() {
        if ((fileChooser == null) && (dataSourceBrowser != null)) {
            initFileChooser();
        }
        return fileChooser;
    }

    @Override
    public void startSeeking() {
        // EXPDATA-642: Refresh directory at opening and then regularly
        if (fileChooser != null) {
            lastDirectoryUpdateDate = System.currentTimeMillis();
            lastModified = fileChooser.getCurrentDirectory().lastModified();
            fileChooser.rescanCurrentDirectory();
        }
        timer = new Timer(timerName);
        timer.scheduleAtFixedRate(new RefreshDirectoryTask(), DIRECTORY_SCAN_PERIOD, DIRECTORY_SCAN_PERIOD);
    }

    @Override
    public void stopSeeking() {
        // EXPDATA-642: Stop directory refreshing when file choosing dialog is not visible
        if (timer != null) {
            timer.cancel();
            timer = null;
        }
    }

    // ///////////// //
    // Inner classes //
    // ///////////// //

    /**
     * {@link TimerTask} that refreshes {@link JFileChooser}'s current directory if necessary.
     */
    protected class RefreshDirectoryTask extends TimerTask {
        @Override
        public void run() {
            if ((fileChooser != null) && fileChooser.isShowing()) {
                long now = System.currentTimeMillis();
                if (now - lastDirectoryUpdateDate >= DIRECTORY_SCAN_PERIOD) {
                    lastDirectoryUpdateDate = now;
                    if (fileChooser.getCurrentDirectory().lastModified() != lastModified) {
                        fileChooser.rescanCurrentDirectory();
                    }
                }
            }
        }
    }
}
