/*
 * This file is part of databrowser-core.
 * 
 * databrowser-core is free software: you can redistribute it and/or modify it under the terms of the GNU General Public
 * License as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later
 * version.
 * 
 * databrowser-core is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the
 * implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for more
 * details.
 * 
 * You should have received a copy of the GNU General Public License along with databrowser-core. If not, see
 * <https://www.gnu.org/licenses/>.
 */
package org.cdma.gui.databrowser.impl;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.cdma.gui.databrowser.exception.DataBrowserException;
import org.cdma.gui.databrowser.util.ApplicationUtil;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import fr.soleil.data.container.matrix.StringMatrix;
import fr.soleil.data.service.DataSourceProducerProvider;
import fr.soleil.data.service.HistoryKey;
import fr.soleil.data.service.IConstantSourceProducer;
import fr.soleil.data.service.IDataSourceProducer;
import fr.soleil.data.service.IKey;
import fr.soleil.data.source.AbstractDataSource;

/**
 * This abstract class provide basic method for a IDataSourceBrowser based on IDataSourceProducer from
 * DataConnectionManagement
 * 
 * @see AbstractDataSourceBrowser
 * @see IDataSourceProducer
 *
 * @author SAINTIN
 */
public abstract class AbstractDataSourceProducerBrowser extends AbstractDataSourceBrowser {

    private static final Logger LOGGER = LoggerFactory.getLogger(AbstractDataSourceProducerBrowser.class);

    protected IDataSourceProducer producer;
    protected Map<String, List<IKey>> openedKeysForSource;

    protected AbstractDataSourceProducerBrowser(final Class<? extends IDataSourceProducer> producerClass) {
        DataSourceProducerProvider.pushNewProducer(producerClass);
        producer = DataSourceProducerProvider.getProducerByClassName(producerClass.getName());
        openedKeysForSource = new HashMap<>();
    }

    @Override
    public int getRank(final IKey key) {
        int rank = -1;
        if ((key != null) && (producer != null)) {
            try {
                rank = producer.getRank(key);
            } catch (Exception e) {
                LOGGER.error(String.format("Impossible to get rank for key %s because %s", key.getInformationKey(),
                        e.getMessage()), e);
            }
        }
        return rank;
    }

    @Override
    public int[] getShape(final IKey key) {
        int[] shape = null;
        try {
            if ((key != null) && (producer != null)) {
                shape = producer.getShape(key);
            }
        } catch (Exception e) {
            LOGGER.error(String.format("Impossible to get shape for key %s because %s", key.getInformationKey(),
                    e.getMessage()), e);
        }
        return shape;
    }

    @Override
    public String getLabel() {
        String name = this.getClass().getSimpleName();
        if (producer != null) {
            name = ApplicationUtil.getFileJarName(producer.getClass());
        }
        return name;
    }

    @Override
    public String getName() {
        String name = userName;
        if ((name == null) || name.isEmpty()) {
            if (producer != null) {
                name = producer.getName();
            } else {
                name = getLabel();
            }
        }
        return name;
    }

    @Override
    public String getVersion() {
        return getFileJarVersion();
    }

    private String getFileJarVersion() {
        String version = null;
        if (producer != null) {
            ApplicationUtil.getFileJarName(producer.getClass());
        }
        return version;
    }

    protected AbstractDataSource<?> createDataSource(final IKey key) throws DataBrowserException {
        AbstractDataSource<?> source = null;
        if ((key != null) && (producer != null)) {
            LOGGER.trace("producer class for Key {} = {} ", key, producer.getClass().getName());
            try {
                source = producer.createDataSource(key);
            } catch (Exception e) {
                String message = String.format("Impossible to create data source for %s because %s",
                        key.getInformationKey(), e.getMessage());
                LOGGER.error(message, e);
                throw new DataBrowserException(message, e);
            }
        }
        return source;
    }

    protected Object getDataSource(final IKey key) throws DataBrowserException {
        return getDataSource(key, false);
    }

    protected Object getDataSource(final IKey key, final boolean closeSource) throws DataBrowserException {
        Object data = null;
        try {
            AbstractDataSource<?> source = createDataSource(key);
            if (source != null) {
                data = source.getData();
                if (!closeSource) {
                    addSourceEntry(key);
                }
            }
        } catch (Exception e) {
            String message = String.format("Impossible to read data for source %s because %s", key.getInformationKey(),
                    e.getMessage());
            LOGGER.error(message, e);
            throw new DataBrowserException(message, e);
        }
        if (data != null) {
            if (closeSource) {
                removeDataSource(key);
            }
        }
        return data;
    }

    protected String[] getStringArrayValue(final IKey key) throws DataBrowserException {
        String[] values = null;
        Object data = getDataSource(key);
        if ((data != null) && (data instanceof StringMatrix)) {
            values = ((StringMatrix) data).getFlatValue();
        }
        return values;
    }

    protected String getStringValue(final IKey key) throws DataBrowserException {
        String value = null;
        Object data = getDataSource(key);
        if (data != null) {
            value = data.toString();
        }
        return value;
    }

    protected void removeDataSource(final IKey key) {
        if ((key != null) && (producer instanceof IConstantSourceProducer)) {
            ((IConstantSourceProducer) producer).removeDataSource(key);
        }
    }

    @Override
    public boolean isSourceOpened(final IKey sourceKey) {
        String sourceId = getSourceFromKey(sourceKey);
        boolean sourceOpened = openedKeysForSource.containsKey(sourceId);
        return sourceOpened;
    }

    protected void addSourceEntry(final IKey key) {
        if (key != null) {
            String sourceId = getSourceFromKey(key);
            List<IKey> keyList = openedKeysForSource.get(sourceId);
            if (keyList == null) {
                keyList = new ArrayList<>();
                openedKeysForSource.put(sourceId, keyList);
            }
            keyList.add(key);
        }
    }

    protected abstract String getSourceFromKey(final IKey sourceKey);

    @Override
    public void closeSource(final IKey sourceKey) {
        String sourceId = getSourceFromKey(sourceKey);
        if (sourceId != null) {
            List<IKey> removedKeys = openedKeysForSource.remove(sourceId);
            if (removedKeys != null) {
                for (IKey key : removedKeys) {
                    removeDataSource(key);
                }
                removedKeys.clear();
                removedKeys = null;
            }
        }
    }

    protected IKey getSimpleKey(IKey key) {
        IKey simpleKey = key;
        if (key instanceof HistoryKey) {
            simpleKey = ((HistoryKey) key).getHistory();
        }
        return simpleKey;
    }

}
