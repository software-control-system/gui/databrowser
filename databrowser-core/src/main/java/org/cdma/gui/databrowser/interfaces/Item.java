/*
 * This file is part of databrowser-core.
 * 
 * databrowser-core is free software: you can redistribute it and/or modify it under the terms of the GNU General Public
 * License as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later
 * version.
 * 
 * databrowser-core is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the
 * implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for more
 * details.
 * 
 * You should have received a copy of the GNU General Public License along with databrowser-core. If not, see
 * <https://www.gnu.org/licenses/>.
 */
package org.cdma.gui.databrowser.interfaces;

import fr.soleil.data.service.IKey;

public class Item {

    protected DataType userType;
    protected final IKey key, sourceKey;
    protected final IDataSourceBrowser browser;

    protected final DataFormat format;
    protected AxisType axis;
    protected Item xItem, yItem;

    public Item(final IKey key, final IKey sourceKey, final IDataSourceBrowser browser, final DataType type,
            final DataFormat format) {
        this.key = key;
        this.sourceKey = sourceKey;
        this.browser = browser;
        this.userType = type;
        this.format = format;
        axis = getDefaultAxis();
        xItem = null;
        yItem = null;
    }

    public Item(final Item item) {
        key = item.getKey();
        sourceKey = item.getSourceKey();
        browser = item.getBrowser();
        format = item.getFormat();
        userType = item.getUserType();
        axis = item.getAxis();
        xItem = item.getXItem();
        yItem = item.getYItem();
    }

    public void setXKey(IKey key, IKey sourceKey) {
        if (key != null) {
            Item xItem = new Item(key, sourceKey, browser, userType, format);
            setXItem(xItem);
        }
    }

    public void setXKey(IKey key, IKey sourceKey, final IDataSourceBrowser browser, final DataType type,
            final DataFormat format) {
        if (key != null) {
            Item xItem = new Item(key, sourceKey, browser, userType, format);
            setXItem(xItem);
        }
    }

    public void setYKey(IKey key, IKey sourceKey, final IDataSourceBrowser browser, final DataType type,
            final DataFormat format) {
        if (key != null) {
            Item yItem = new Item(key, sourceKey, browser, userType, format);
            setYItem(yItem);
        }
    }

    public Item getXItem() {
        return xItem;
    }

    public void setXItem(Item xItem) {
        this.xItem = xItem;
    }

    public Item getYItem() {
        return yItem;
    }

    public void setYItem(Item yItem) {
        this.yItem = yItem;
    }

    public AxisType getDefaultAxis() {
        AxisType result = AxisType.SCALAR;
        switch (getUserType()) {
            case SCALAR:
                result = AxisType.SCALAR;
                break;
            case SPECTRUM:
                switch (format) {
                    case BOOLEAN:
                    case NUMERICAL:
                        result = AxisType.Y1;
                        break;
                    case TEXT:
                        result = AxisType.SPECTRUM_STRING;
                        break;

                    default:
                        // should not happen
                        break;
                }
                break;
            case IMAGE:
                switch (format) {
                    case NUMERICAL:
                    default:
                        result = AxisType.IMAGE;
                        break;
                }
                break;

            default:
                // should not happen
                break;
        }
        return result;
    }

    public AxisType getAxis() {
        return axis;
    }

    public void setAxis(final AxisType axis) {
        this.axis = axis;
    }

    public void setDefaultAxis() {
        axis = getDefaultAxis();
    }

    public IKey getKey() {
        return key;
    }

    public IKey getSourceKey() {
        return sourceKey;
    }

    public DataType getType() {
        return browser.getType(key);
    }

    public DataType getUserType() {
        DataType tmpType = userType;
        if (tmpType == null) {
            tmpType = getType();
        }
        return tmpType;
    }

    public void setUserType(DataType userType) {
        this.userType = userType;
    }

    public DataFormat getFormat() {
        return format;
    }

    public IDataSourceBrowser getBrowser() {
        return browser;
    }

    @Override
    public boolean equals(final Object obj) {
        boolean result = false;
        if (obj != null && obj instanceof Item) {
            result = key.equals(((Item) obj).key);
        }
        return result;
    }

    @Override
    public String toString() {
        StringBuilder sb = new StringBuilder();
        sb.append("{");
        sb.append(browser.getName());
        sb.append(", ").append(browser.getDisplayName(key));
        sb.append(", ").append(getType());
        sb.append(", ").append(format);
        sb.append(", ").append(axis);
        sb.append("}");

        return sb.toString();
    }
}
