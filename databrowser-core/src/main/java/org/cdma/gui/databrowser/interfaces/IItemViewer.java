/*
 * This file is part of databrowser-core.
 * 
 * databrowser-core is free software: you can redistribute it and/or modify it under the terms of the GNU General Public
 * License as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later
 * version.
 * 
 * databrowser-core is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the
 * implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for more
 * details.
 * 
 * You should have received a copy of the GNU General Public License along with databrowser-core. If not, see
 * <https://www.gnu.org/licenses/>.
 */
package org.cdma.gui.databrowser.interfaces;

import java.util.Collection;

public interface IItemViewer {

    public Collection<Runnable> addItem(final Item item);

    public Collection<Runnable> removeItem(final Item item);

    public Collection<Runnable> selectItem(final Item item);

    public Collection<Runnable> deselectItem(final Item item);

    public Collection<Runnable> clearSelection();

}
