/*
 * This file is part of databrowser-core.
 * 
 * databrowser-core is free software: you can redistribute it and/or modify it under the terms of the GNU General Public
 * License as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later
 * version.
 * 
 * databrowser-core is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the
 * implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for more
 * details.
 * 
 * You should have received a copy of the GNU General Public License along with databrowser-core. If not, see
 * <https://www.gnu.org/licenses/>.
 */
package org.cdma.gui.databrowser.interfaces;

import java.util.List;
import java.util.Map;

import org.cdma.gui.databrowser.exception.DataBrowserException;

import fr.soleil.comete.definition.widget.properties.ChartProperties;
import fr.soleil.comete.definition.widget.properties.PlotProperties;
import fr.soleil.comete.definition.widget.util.ITreeNode;
import fr.soleil.data.service.IKey;

/**
 * 
 * @author SAINTIN
 * 
 *         This interface provides methods used by the data browser to display a
 *         data
 * @see IKey
 * 
 * 
 */

public interface IDataSourceBrowser {

    /**
     * return a unique identifier for the IDataSourceBrowser
     * 
     * @return a unique identifier for the IDataSourceBrowser
     */
    public String getId();

    /**
     * Indicate if the IDataSourceBrowser is enable on the environment If not
     * enable, the IDataSourceBrowser is not display in the DataBrowser Tree
     * 
     * @return true if the activation conditions for the IDataSourceBrowser are
     *         satisfied
     */
    public boolean isSourceBrowserEnabled();

    /**
     * Activate or Deactivate the IDataSourceBrowser If not
     * enable, the IDataSourceBrowser is not display in the DataBrowser Tree
     * 
     * @param true if the activation conditions for the IDataSourceBrowser are
     *            satisfied
     */
    public void setSourceBrowserEnabled(boolean enable);

    /**
     * return a version number for the IDataSourceBrowser, or null. This version
     * is displayed in the plug-in panel information
     * 
     * @return a version number for the IDataSourceBrowser, or null
     */
    public String getVersion();

    /**
     * return a short definition of the IDataSourceBrowser, or null. This label
     * is displayed in the plug-in panel information
     * 
     * @return a short definition of the IDataSourceBrowser, or null
     */
    public String getLabel();

    /**
     * return a description for the IDataSourceBrowser, or null. This
     * description is displayed in the plug-in panel information and in tooltip
     * in the DataBrowser tree
     * 
     * @return a description for the IDataSourceBrowser, or null
     */
    public String getDescription();

    /**
     * return the name displayed in the data browser tree.
     * 
     * @return the name displayed in the data browser tree
     */
    public String getName();

    /**
     * Override the name displayed in the data browser tree
     */
    public void setName(String name);

    /**
     * Get the associated seeker displayed when opening a source
     * 
     * @return the associated seeker
     * @see IDataSourceSeeker
     */
    public IDataSourceSeeker getSeeker();

    /**
     * 
     * Get the internal IDataSourceBrowser that can handle the path if any, or
     * null
     * 
     * The return browser is used when a source is load from a drag and drop
     * action or from a command line argument
     * 
     * @param sourcePath
     *            the path for a source (file path, device name, ...)
     * @return the internal IDataSourceBrowser that can handle the path if any,
     *         or null
     */
    public IDataSourceBrowser getDataSourceBrowser(String sourcePath);

    /**
     * the internal IDataSourceBrowser that can handle the key if any, or null
     * 
     * The return browser is used when a source is load from a configuration
     * file or to get the informations displayed in the information panel
     * 
     * @param key
     *            the key for a source
     * @return the internal IDataSourceBrowser that can handle the key if any,
     *         or null
     */
    public IDataSourceBrowser getDataSourceBrowser(IKey key);

    /**
     * Creates a key for a valid source used for drag and drop action or from a
     * command line argument
     * 
     * @param sourcePath
     *            path for the source to read (filename, device name, ...).
     *            sourcePath is expected to be not null and valid
     * @return a new key for the source
     */
    public IKey createKeyFromSource(String sourcePath);

    /**
     * a Comete tree node that is the root for the subtree corresponding to the
     * key. This node will be added in the DataBrowser tree under this plug-in
     * node
     * 
     * @param key
     *            the key we want to create a node for. Usually a source key
     * @return a Comete tree node that is the root for the subtree corresponding
     *         to the key
     * @throws DataBrowserException
     * @see ITreeNode
     */
    public ITreeNode createNode(IKey key) throws DataBrowserException;

    /**
     * Tests if it can handle the given source
     * 
     * @param sourcePath
     *            path for the source to read (filename, device name, ...)
     * @return true if the browser or one of its plug-ins can read the file,
     *         false otherwise
     */
    public boolean canRead(String sourcePath);

    /**
     * Tests if it can handle the given key
     * 
     * @param key
     * @return true if the browser or one of its plug-ins can read the key,
     *         false otherwise
     */
    public boolean canRead(IKey key);

    /**
     * Compute a writable key from the original one
     * 
     * @param key
     * @return a writable key generated from the original one
     */
    public IKey generateSettableKey(IKey key);

    /**
     * Compute a key for a slice of data from the original one
     * 
     * @param key
     *            the original key
     * @param shape
     *            the shape of the slice
     * @param origine
     *            the origins in the different dimensions
     * @return a new key for the slice of data
     */
    public IKey generateRegionKey(IKey key, int[] shape, int[] origine);

    /**
     * Compute the name to display in the DisplayManager for the key
     * 
     * @param key
     * @return the name to display
     */
    public String getDisplayName(IKey key);

    /**
     * return the shape of the data pointed by this key
     * 
     * @param key
     * @return the shape of the data pointed by this key
     */
    public int[] getShape(IKey key);

    /**
     * 
     * return the rank of the data pointed by this key
     * 
     * @param key
     * @return the rank of the data pointed by this key
     */
    public int getRank(IKey key);

    /**
     * 
     * return the type of data pointed by this key, as one of scalar, spectrum
     * or image
     * 
     * @param key
     * @return the type of data pointed by this key, as one of scalar, spectrum
     *         or image
     * @see DataType
     */
    public DataType getType(IKey key);

    public boolean isTypeModifiable();

    /**
     * 
     * Force the type of data (Image or spectrum or scalar)
     * 
     * @param key
     * @return the type of data pointed by this key, as one of scalar, spectrum
     *         or image
     * @see DataType
     */
    public void setType(IKey key, DataType type);

    /**
     * return the format of data pointed by this key, as one of number, boolean
     * or text
     * 
     * @param key
     * @return the format of data pointed by this key, as one of number, boolean
     *         or text
     * @see DataFormat
     */
    public DataFormat getFormat(IKey key);

    /**
     * return a description of a given IKey. This description is displayed in
     * tooltip in the databrowser tree and in the display manager
     * 
     * @param key
     * @return
     */
    public String getDescription(IKey key);

    /**
     * return an alias if it exist. This alias is displayed in
     * tooltip in the databrowser tree and in the display manager
     * 
     * @param key
     * @return
     */
    public String getAlias(IKey key);

    /**
     * return a map with specific informations about the key This informations
     * are displayed in the information panel
     * 
     * @param key
     * @return a map with specific informations about the key
     */
    public Map<String, String> getInformations(IKey key);

    /**
     * return the list of activated plug-ins if any, or null
     * 
     * @return the list of activated plug-ins if any, or null
     */
    public List<IDataSourceBrowser> getDataSourceBrowserList();

    /**
     * test if the source is already opened in the databrowser. prevent from
     * opening a source twice
     * 
     * @param sourceKey
     * @return true if the source is opened, false otherwise
     */
    public boolean isSourceOpened(IKey sourceKey);

    /**
     * close a given source
     * 
     * @param sourceKey
     * 
     */
    public void closeSource(IKey sourceKey);

    public PlotProperties getPlotProperties(IKey key);

    public ChartProperties getChartProperties(IKey key);

    // public ImageProperties getImageProperties(IKey key);

    public IKey getXScale(IKey key);

    // public IKey getYScale(IKey key);

    public IKey getSliderScale(IKey key);

    public AxisType getAxis(IKey key);

    public boolean isSettable(IKey key);

    /**
     * Add this method in order to remove Tango reference in DataBrowserGUI Code
     * 
     * @param the tested node
     * @return true if the node is reachable
     */
    public boolean isNodeReachable(ITreeNode node);

    public String getCompletePath(String simplePath);

}
