/*
 * This file is part of databrowser-core.
 * 
 * databrowser-core is free software: you can redistribute it and/or modify it under the terms of the GNU General Public
 * License as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later
 * version.
 * 
 * databrowser-core is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the
 * implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for more
 * details.
 * 
 * You should have received a copy of the GNU General Public License along with databrowser-core. If not, see
 * <https://www.gnu.org/licenses/>.
 */
package org.cdma.gui.databrowser.interfaces;

import java.util.List;

import javax.swing.JComponent;

import fr.soleil.data.service.IKey;

public interface IDataSourceSeeker {

    /**
     * Get the id associated with this seeker. This is used to pair a browser
     * and a specific seeker.
     * 
     * @return the id of the browser it is associated with
     */
    public String getDataSourceBrowserId();

    /**
     * Get the panel that will be embedded in the seeker frame of the
     * DataBrowser.
     * 
     * @return the panel that contains the seeker
     */
    public JComponent getComponent();

    /**
     * Clean the component. Empty fields, clear filechooser, and so on.
     */
    public void cleanComponent();

    /**
     * Get the list of keys for the selected sources
     * 
     * @return the list of keys, or null if no key has been selected
     */
    public List<IKey> getResult();

    /**
     * Sets the default directory for the filechooser, if any.
     * 
     * @param path
     *            the directory to use with the filechooser, if any
     */
    public void setDirectory(final String path);

    /**
     * Get the last selected directory in the filechooser, if any.
     * 
     * @return the last directory used in the filechooser if any, or null
     */
    public String getDirectory();

    /**
     * Notify the seeker that it is displayed
     */
    public void startSeeking();

    /**
     * Notify the seeker that it is hidden
     */
    public void stopSeeking();
}
