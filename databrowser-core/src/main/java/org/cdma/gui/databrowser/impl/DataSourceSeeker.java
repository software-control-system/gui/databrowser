/*
 * This file is part of databrowser-core.
 * 
 * databrowser-core is free software: you can redistribute it and/or modify it under the terms of the GNU General Public
 * License as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later
 * version.
 * 
 * databrowser-core is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the
 * implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for more
 * details.
 * 
 * You should have received a copy of the GNU General Public License along with databrowser-core. If not, see
 * <https://www.gnu.org/licenses/>.
 */
package org.cdma.gui.databrowser.impl;

import java.util.Set;

import org.cdma.gui.databrowser.interfaces.IDataSourceBrowser;
import org.reflections.Reflections;

public class DataSourceSeeker {

    public static Set<Class<? extends IDataSourceBrowser>> getDataSourceBrowserImplementations() {
        Reflections reflections = new Reflections(DataSourceSeeker.class.getPackage().getName());
        Set<Class<? extends IDataSourceBrowser>> dataSourceBrowserSet = reflections
                .getSubTypesOf(IDataSourceBrowser.class);
        return dataSourceBrowserSet;
    }

}
