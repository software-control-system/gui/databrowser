/*
 * This file is part of databrowser-core.
 * 
 * databrowser-core is free software: you can redistribute it and/or modify it under the terms of the GNU General Public
 * License as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later
 * version.
 * 
 * databrowser-core is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the
 * implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for more
 * details.
 * 
 * You should have received a copy of the GNU General Public License along with databrowser-core. If not, see
 * <https://www.gnu.org/licenses/>.
 */
package org.cdma.gui.databrowser.impl;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.cdma.gui.databrowser.interfaces.AxisType;
import org.cdma.gui.databrowser.interfaces.DataType;
import org.cdma.gui.databrowser.interfaces.IDataSourceBrowser;

import fr.soleil.comete.definition.widget.properties.ChartProperties;
import fr.soleil.comete.definition.widget.properties.PlotProperties;
import fr.soleil.comete.definition.widget.util.ITreeNode;
import fr.soleil.data.service.IKey;

/**
 * 
 * @author SAINTIN
 * 
 *         This abstract class provide basic method for a IDataSourceBrowser
 *         based on
 * 
 * @see IDataSourceBrowser
 */

public abstract class AbstractDataSourceBrowser implements IDataSourceBrowser {

    protected final String LABEL_ATTRIBUTE = "ATTR : ";
    protected final String LABEL_INFO = "INFO : ";
    protected String userName = null;
    private boolean isUserEnable = true;
    private final Map<IKey, DataType> userDataType = new HashMap<>();

    protected final Map<IKey, IKey> xScaleMap = new HashMap<>();
    protected final Map<IKey, IKey> sliderScaleMap = new HashMap<>();

    @Override
    public Map<String, String> getInformations(IKey key) {
        Map<String, String> infoMap = new HashMap<>();
        // infoMap.put("Description", getDescription(key));
        infoMap.put(LABEL_INFO + "Format", getFormat(key).toString());
        infoMap.put(LABEL_INFO + "Type", getType(key).toString());
        // infoMap.put("NumericalFormat", getNumericalFormat(key));
        return infoMap;
    }

    @Override
    public boolean isSourceBrowserEnabled() {
        return isUserEnable;
    }

    @Override
    public void setSourceBrowserEnabled(boolean enable) {
        isUserEnable = enable;
    }

    @Override
    public String getName() {
        return userName;
    }

    @Override
    public void setName(final String name) {
        userName = name;
    }

    @Override
    public List<IDataSourceBrowser> getDataSourceBrowserList() {
        return null;
    }

    @Override
    public IDataSourceBrowser getDataSourceBrowser(final String sourcePath) {
        IDataSourceBrowser browser = null;
        if (canRead(sourcePath)) {
            browser = this;
        }
        return browser;
    }

    @Override
    public IDataSourceBrowser getDataSourceBrowser(final IKey key) {
        IDataSourceBrowser browser = null;
        if (canRead(key)) {
            browser = this;
        }
        return browser;
    }

    @Override
    public void setType(IKey key, DataType type) {
        if (key != null) {
            userDataType.put(key, type);
        }
    }

    public abstract DataType getKeyType(IKey key);

    @Override
    public DataType getType(IKey key) {
        DataType type = userDataType.get(key);
        if (type == null) {
            type = getKeyType(key);
        }
        return type;
    }

    @Override
    public PlotProperties getPlotProperties(IKey key) {
        return null;
    }

    @Override
    public ChartProperties getChartProperties(IKey key) {
        return null;
    }

    @Override
    public IKey getXScale(IKey key) {
        return xScaleMap.get(key);
    }

    @Override
    public IKey getSliderScale(IKey key) {
        return sliderScaleMap.get(key);
    }

    @Override
    public AxisType getAxis(IKey key) {
        return null;
    }

    @Override
    public boolean isSettable(IKey key) {
        return false;
    }

    @Override
    public boolean isNodeReachable(ITreeNode node) {
        return false;
    }

    @Override
    public String getCompletePath(String simplePath) {
        return simplePath;
    }

}
