/*
 * This file is part of databrowser-gui.
 * 
 * databrowser-gui is free software: you can redistribute it and/or modify it under the terms of the GNU General Public
 * License as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later
 * version.
 * 
 * databrowser-gui is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the
 * implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for more
 * details.
 * 
 * You should have received a copy of the GNU General Public License along with databrowser-gui. If not, see
 * <https://www.gnu.org/licenses/>.
 */
package org.cdma.gui.databrowser.interfaces;

import java.util.EventListener;

import org.cdma.gui.databrowser.DataBrowserItemEvent;

public interface IDataBrowserItemListener extends EventListener {

    public void onItemOpened(final DataBrowserItemEvent event);

    public void onItemClosed(final DataBrowserItemEvent event);

    public void onItemSelected(final DataBrowserItemEvent event);

    public void onItemDeselected(final DataBrowserItemEvent event);

    public void onItemAxisChanged(final DataBrowserItemEvent event);
}
