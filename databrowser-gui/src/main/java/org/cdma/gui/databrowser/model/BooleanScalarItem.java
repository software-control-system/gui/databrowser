/*
 * This file is part of databrowser-gui.
 * 
 * databrowser-gui is free software: you can redistribute it and/or modify it under the terms of the GNU General Public
 * License as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later
 * version.
 * 
 * databrowser-gui is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the
 * implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for more
 * details.
 * 
 * You should have received a copy of the GNU General Public License along with databrowser-gui. If not, see
 * <https://www.gnu.org/licenses/>.
 */
package org.cdma.gui.databrowser.model;

import org.cdma.gui.databrowser.DataBrowserController;
import org.cdma.gui.databrowser.interfaces.Item;
import org.cdma.gui.databrowser.view.item.CheckBoxPanel;
import org.cdma.gui.databrowser.view.item.ScalarViewer;

import fr.soleil.comete.box.scalarbox.BooleanScalarBox;
import fr.soleil.comete.swing.BooleanComboBox;
import fr.soleil.data.target.scalar.IBooleanTarget;

public class BooleanScalarItem extends ScalarItem<CheckBoxPanel, BooleanComboBox> {

    public BooleanScalarItem(final ScalarViewer scalarViewer, final Item item, DataBrowserController controller) {
        super(scalarViewer, item, controller);
    }

    @Override
    public CheckBoxPanel createViewer() {
        // JIRA PROBLEM-113 use a specific CheckBoxPanel to avoid the write problem on Boolean component
        // CheckBox theViewer = new CheckBox();
        CheckBoxPanel theViewer = new CheckBoxPanel();
        theViewer.setOpaque(false);
        return theViewer;
    }

    @Override
    public BooleanComboBox createSetter() {
        BooleanComboBox theSetter = new BooleanComboBox();
        theSetter.setOpaque(false);
        return theSetter;
    }

    @Override
    public void doConnect() {
        super.doConnect();

        BooleanScalarBox booleanBox = getItemViewer().getBooleanBox();
        booleanBox.connectWidget((IBooleanTarget) viewer, readKey);
        if (writeKey != null) {
            booleanBox.connectWidget((IBooleanTarget) setter, writeKey);
        }
    }

    @Override
    protected void disconnectImmediately() {
        BooleanScalarBox booleanBox = getItemViewer().getBooleanBox();
        booleanBox.disconnectWidget((IBooleanTarget) viewer, readKey);
        if (writeKey != null) {
            booleanBox.disconnectWidget(setter, writeKey);
        }
    }

}
