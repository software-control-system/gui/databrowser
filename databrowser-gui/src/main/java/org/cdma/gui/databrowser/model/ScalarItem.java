/*
 * This file is part of databrowser-gui.
 * 
 * databrowser-gui is free software: you can redistribute it and/or modify it under the terms of the GNU General Public
 * License as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later
 * version.
 * 
 * databrowser-gui is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the
 * implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for more
 * details.
 * 
 * You should have received a copy of the GNU General Public License along with databrowser-gui. If not, see
 * <https://www.gnu.org/licenses/>.
 */
package org.cdma.gui.databrowser.model;

import javax.swing.JComponent;

import org.cdma.gui.databrowser.DataBrowserController;
import org.cdma.gui.databrowser.interfaces.Item;
import org.cdma.gui.databrowser.view.item.ScalarViewer;

import fr.soleil.data.target.scalar.IScalarTarget;

public abstract class ScalarItem<C extends JComponent & IScalarTarget, S extends JComponent & IScalarTarget>
        extends ARowContainerItem<C, S, ScalarViewer> {

    protected ScalarItem(final ScalarViewer scalarViewer, final Item item, DataBrowserController controller) {
        super(item, controller);
        itemViewer = scalarViewer;
        init(item, controller);
    }

    @Override
    protected void doConnect() {
        rowContainer.setSetterVisible(writeKey != null);
    }

}
