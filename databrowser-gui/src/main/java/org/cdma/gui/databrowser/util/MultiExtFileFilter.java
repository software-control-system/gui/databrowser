/*
 * This file is part of databrowser-gui.
 * 
 * databrowser-gui is free software: you can redistribute it and/or modify it under the terms of the GNU General Public
 * License as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later
 * version.
 * 
 * databrowser-gui is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the
 * implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for more
 * details.
 * 
 * You should have received a copy of the GNU General Public License along with databrowser-gui. If not, see
 * <https://www.gnu.org/licenses/>.
 */
package org.cdma.gui.databrowser.util;

import java.io.File;
import java.util.ArrayList;
import java.util.List;

import javax.swing.filechooser.FileFilter;

import fr.soleil.lib.project.file.FileUtils;

/**
 * A file filter that can accept multiple extensions (as for jpg, jpe, jpeg).
 * The displayed description is automatically computed with the given one
 * followed by the list of extensions.
 * 
 * @author MAINGUY
 * 
 */
public class MultiExtFileFilter extends FileFilter implements SoleilFileFilter {

    protected String description;
    protected List<String> extensions;
    protected boolean showExtensions = true;

    /**
     * @param filterDescription
     *            the description of the type of accepted files
     * @param extensions
     *            the allowed file extensions for this filter. First extension
     *            is the default one.
     */
    public MultiExtFileFilter(final String filterDescription, final String... extensions) {
        this(filterDescription, true, extensions);
    }

    /**
     * @param filterDescription
     *            the description of the type of accepted files
     * @param displayExtensions
     *            true to display the list of extensions in the filter
     *            description, false otherwise
     * @param extensions
     *            the allowed file extensions for this filter. First extension
     *            is the default one.
     */
    public MultiExtFileFilter(final String filterDescription, final boolean displayExtensions,
            final String... extensions) {
        if (extensions == null || extensions.length == 0) {
            throw new IllegalArgumentException("Extensions must be non-null and not empty");
        }

        showExtensions = displayExtensions;
        if (filterDescription == null && !showExtensions) {
            throw new IllegalArgumentException("Filter has to show at least either description or extensions");
        }

        this.extensions = new ArrayList<>(extensions.length);
        for (String ext : extensions) {
            if (ext == null || ext.length() == 0) {
                throw new IllegalArgumentException("Each extension must be non-null and not empty");
            }
            this.extensions.add(ext.toLowerCase());
        }

        computeDescription(filterDescription);
    }

    @Override
    public boolean accept(final File f) {
        boolean result = false;
        if (f != null) {
            // we accept directories, for navigation
            result |= f.isDirectory();

            // file extension must be in the list of extensions
            String fileExtension = FileUtils.getExtension(f);
            // FileUtils.getExtension returns in lower case
            if (fileExtension != null) {
                result |= isAccepted(fileExtension);
            }
        }
        return result;
    }

    /**
     * Return true if the specified extension is accepted by the filter.
     * 
     * @param fileExtension
     *            the fileExtension to test, in lower case
     * @return true if the filter should accept this extension, false otherwise
     */
    public boolean isAccepted(final String fileExtension) {
        boolean result = false;
        for (String extension : extensions) {
            result |= extension.equals(fileExtension);// in lower case
        }
        return result;
    }

    @Override
    public String getDescription() {
        return description;
    }

    /**
     * Return the default extension (i.e. the first one). Useful when saving a
     * file whose filename has no extension specified.
     * 
     * @return the default extension.
     */
    @Override
    public String getDefaultExtension() {
        return extensions.get(0);
    }

    /**
     * Returns the list of extensions allowed for this filter.
     * 
     * @return a list of the extensions
     */
    public List<String> getExtensions() {
        return new ArrayList<>(extensions);
    }

    /**
     * Compute the whole description for the filter. User description is
     * followed by allowed extensions as a list.
     * 
     * @param filterDescription
     *            the description of the filter
     */
    protected void computeDescription(final String filterDescription) {
        StringBuilder sb = new StringBuilder();

        if (filterDescription != null) {
            sb.append(filterDescription);
        }

        if (filterDescription != null && showExtensions) {
            sb.append(" ");
        }

        if (showExtensions) {
            sb.append("(");
            // constructor ensures there is at least one valid extension
            sb.append("*.").append(extensions.get(0));
            for (int i = 1; i < extensions.size(); i++) {
                sb.append(", ").append("*.").append(extensions.get(i));
            }
            sb.append(")");
        }

        description = sb.toString();
    }

    @Override
    public String toString() {
        return description;
    }

}
