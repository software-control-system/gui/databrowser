/*
 * This file is part of databrowser-gui.
 * 
 * databrowser-gui is free software: you can redistribute it and/or modify it under the terms of the GNU General Public
 * License as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later
 * version.
 * 
 * databrowser-gui is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the
 * implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for more
 * details.
 * 
 * You should have received a copy of the GNU General Public License along with databrowser-gui. If not, see
 * <https://www.gnu.org/licenses/>.
 */
package org.cdma.gui.databrowser.view.displaymanager;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Component;
import java.awt.Font;
import java.awt.event.ActionEvent;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import javax.swing.Action;
import javax.swing.JComponent;
import javax.swing.JLabel;
import javax.swing.JMenuItem;
import javax.swing.JPanel;
import javax.swing.JPopupMenu;
import javax.swing.JScrollPane;
import javax.swing.JTable;
import javax.swing.ListSelectionModel;
import javax.swing.ToolTipManager;
import javax.swing.event.ListSelectionEvent;
import javax.swing.event.ListSelectionListener;
import javax.swing.table.TableCellRenderer;
import javax.swing.table.TableModel;

import org.cdma.gui.databrowser.DataBrowser;
import org.cdma.gui.databrowser.DataBrowserController;
import org.cdma.gui.databrowser.DataBrowserItemEvent;
import org.cdma.gui.databrowser.interfaces.AxisType;
import org.cdma.gui.databrowser.interfaces.IDataBrowserItemListener;
import org.cdma.gui.databrowser.interfaces.IDataSourceBrowser;
import org.cdma.gui.databrowser.interfaces.Item;
import org.cdma.gui.databrowser.util.MyJMenuItem;
import org.cdma.gui.databrowser.view.displaymanager.DisplayManagerTableModel.Columns;
import org.cdma.gui.databrowser.view.displaymanager.DisplayManagerTableModel.DisplayedItem;
import org.cdma.gui.databrowser.view.item.ItemTableCellRenderer;
import org.jdesktop.swingx.JXTable;
import org.jdesktop.swingx.action.AbstractActionExt;
import org.jdesktop.swingx.table.ColumnFactory;
import org.jdesktop.swingx.table.TableColumnExt;

import fr.soleil.comete.swing.util.EDTManager;
import fr.soleil.data.service.IKey;
import fr.soleil.lib.project.ObjectUtils;
import fr.soleil.lib.project.swing.ui.ReverseLabelUI;

public class DisplayManagerViewer extends JPanel implements IDataBrowserItemListener, ListSelectionListener {

    private static final long serialVersionUID = -5663564051429137430L;

    private final DataBrowserController controller;
    private JXTable table;
    private DisplayManagerTableModel tableModel;
    private AxisTableCellEditor tableCellEditor;
    private ScaleTableCellEditor xScaleCellEditor;
    private ScaleTableCellEditor yScaleCellEditor;
    private boolean canListen;
    private Columns[] displayedColumns;

    private JPopupMenu popupMenu;
    private Action removeItemAction;

    public DisplayManagerViewer(final DataBrowserController controller) {
        this.controller = controller;
        canListen = true;
        displayedColumns = null;
        initComponents();
        layoutComponents();
    }

    private void initComponents() {
        removeItemAction = new RemoveItemAction();

        popupMenu = new JPopupMenu();
        JMenuItem closeItem = new MyJMenuItem(removeItemAction);
        popupMenu.add(closeItem);

        tableModel = new ItemTransmissionTableModel();

        table = new DisplayManagerTable();

        table.setAutoResizeMode(JTable.AUTO_RESIZE_OFF);
        // set a specific column factory to allow referring each column with its enum constant
        table.setColumnFactory(new ColumnFactory() {
            @Override
            public void configureTableColumn(final TableModel model, final TableColumnExt columnExt) {
                super.configureTableColumn(model, columnExt);
                columnExt.setIdentifier(Columns.values()[columnExt.getModelIndex()]);
            }
        });
        // set the model after the column factory
        table.setModel(tableModel);
        setDisplayedColumns(Columns.ITEM, Columns.AXIS);
        table.setColumnControlVisible(true);

        table.setSelectionMode(ListSelectionModel.MULTIPLE_INTERVAL_SELECTION);

        // cell editor for axis
        tableCellEditor = new AxisTableCellEditor();
        table.getColumnExt(Columns.AXIS).setCellEditor(tableCellEditor);

        ItemTableCellRenderer itemRenderer = new ItemTableCellRenderer();
        // cell renderer for xScale and yscale
        table.getColumnExt(Columns.XSCALE).setCellRenderer(itemRenderer);
        table.getColumnExt(Columns.YSCALE).setCellRenderer(itemRenderer);

        // cell editor for xScale
        xScaleCellEditor = new ScaleTableCellEditor();
        table.getColumnExt(Columns.XSCALE).setCellEditor(xScaleCellEditor);

        // cell editor for xScale
        yScaleCellEditor = new ScaleTableCellEditor();
        table.getColumnExt(Columns.YSCALE).setCellEditor(yScaleCellEditor);

        table.getSelectionModel().addListSelectionListener(this);

        table.addMouseListener(new DisplayManagerTableMouseListener());

        ToolTipManager.sharedInstance().registerComponent(table);
    }

    public Columns[] getDisplayedColumns() {
        if (table != null) {
            Columns[] columns = Columns.values();
            List<Columns> columnsList = new ArrayList<>();
            for (Columns col : columns) {
                if (table.getColumnExt(col).isVisible()) {
                    columnsList.add(col);
                }
            }

            if (!columnsList.isEmpty()) {
                displayedColumns = columnsList.toArray(new Columns[columnsList.size()]);
            }
        }
        return displayedColumns;
    }

    public void setDisplayedColumns(Columns... columns) {
        if ((columns != null) && !Arrays.toString(displayedColumns).equals(Arrays.toString(columns))) {
            displayedColumns = columns;
            setDisplayedColumns();
        }
    }

    private void setDisplayedColumns() {
        hideAllColumns();
        if ((displayedColumns != null) && (table != null)) {
            for (Columns col : displayedColumns) {
                table.getColumnExt(col).setVisible(true);
            }
        }
    }

    private void hideAllColumns() {
        Columns[] columns = Columns.values();
        for (Columns col : columns) {
            table.getColumnExt(col).setVisible(false);
        }
    }

    private void layoutComponents() {
        JScrollPane scrollPane = new JScrollPane(table);
        setLayout(new BorderLayout());
        add(scrollPane, BorderLayout.CENTER);
    }

    private void closeItem(final IKey ikey) {
        controller.closeItem(ikey);
    }

    private void selectItem(final IKey itemKey) {
        controller.selectItems(itemKey);
    }

    private void setItemAxis(final IKey itemKey, final AxisType axis) {
        controller.setItemAxis(itemKey, axis);
    }

    private void setItemScale(final Item item) {
        controller.setItemScale(item);
    }

    // ---------------------------------------
    // IDataBrowserItemListener implementation
    // ---------------------------------------

    @Override
    public void onItemOpened(final DataBrowserItemEvent event) {
        EDTManager.INSTANCE.runInDrawingThread(() -> {
            Item item = event.getItem();
            tableModel.addItem(item);
            if (xScaleCellEditor != null) {
                xScaleCellEditor.addScaleItem(item);
            }
            if (yScaleCellEditor != null) {
                yScaleCellEditor.addScaleItem(item);
            }
            table.packAll();
        });
    }

    @Override
    public void onItemClosed(final DataBrowserItemEvent event) {
        EDTManager.INSTANCE.runInDrawingThread(() -> {
            Item item = event.getItem();
            tableModel.removeItem(item);
            if (xScaleCellEditor != null) {
                xScaleCellEditor.removeScaleItem(item);
            }
            if (yScaleCellEditor != null) {
                yScaleCellEditor.removeScaleItem(item);
            }
            table.packAll();
        });
    }

    @Override
    public void onItemSelected(final DataBrowserItemEvent event) {
        EDTManager.INSTANCE.runInDrawingThread(() -> {
            // avoid looping in ListSelectionListener
            canListen = false;
            Item item = event.getItem();
            int index = tableModel.indexOfItem(item);
            if (index != -1) {
                int viewIndex = table.convertRowIndexToView(index);
                if (!table.isRowSelected(viewIndex)) {
                    table.addRowSelectionInterval(viewIndex, viewIndex);
                }
            }
            canListen = true;
        });
    }

    @Override
    public void onItemDeselected(final DataBrowserItemEvent event) {
        EDTManager.INSTANCE.runInDrawingThread(() -> {
            // avoid looping in ListSelectionListener
            canListen = false;
            Item item = event.getItem();
            int index = tableModel.indexOfItem(item);
            if (index != -1) {
                int viewIndex = table.convertRowIndexToView(index);
                if (table.isRowSelected(viewIndex)) {
                    table.removeRowSelectionInterval(viewIndex, viewIndex);
                }
            }
            canListen = true;
        });
    }

    @Override
    public void onItemAxisChanged(final DataBrowserItemEvent event) {
        EDTManager.INSTANCE.runInDrawingThread(() -> {
            tableModel.updateItem(event.getItem());
        });
    }

    // ------------------------------------
    // ListSelectionListener implementation
    // ------------------------------------

    @Override
    public void valueChanged(final ListSelectionEvent e) {
        if (canListen && !e.getValueIsAdjusting()) {
            if (table.getSelectedRowCount() == 0) {
                controller.deselectAllItems();
            } else if (table.getSelectedRowCount() == 1) {

                int selectedRow = table.getSelectedRow();
                int index = table.convertRowIndexToModel(selectedRow);
                DisplayedItem displayedItem = tableModel.getDisplayedItem(index);
                selectItem(displayedItem.getKey());
            }
            // multiple selection is only for remove
        }
    }

    // ///////////// //
    // Inner classes //
    // ///////////// //

    protected class RemoveItemAction extends AbstractActionExt {

        private static final long serialVersionUID = -974632433120182361L;

        public RemoveItemAction() {
            super(DataBrowser.MESSAGES.getString("Action.DisplayManager.RemoveItem"),
                    DataBrowser.ICONS.getIcon("Action.RemoveItems"));
        }

        @Override
        public void actionPerformed(final ActionEvent e) {
            int[] selectedRows = table.getSelectedRows();
            if (selectedRows.length > 0) {
                List<DisplayedItem> selectedItems = new ArrayList<>(selectedRows.length);
                for (int i : selectedRows) {
                    int index = table.convertRowIndexToModel(i);
                    DisplayedItem displayedItem = tableModel.getDisplayedItem(index);
                    selectedItems.add(displayedItem);
                }

                for (DisplayedItem displayedItem : selectedItems) {
                    closeItem(displayedItem.getKey());
                }
            }
        }
    }

    protected class ItemTransmissionTableModel extends DisplayManagerTableModel {

        private static final long serialVersionUID = -5434517978039203923L;

        // override setValue from the tableModel to be notified of real axis changes.
        // This avoids problems when listening to the editor, which is also notified when preparing to display
        // (selecting the current value).
        @Override
        public void setValueAt(final Object aValue, final int rowIndex, final int columnIndex) {
            DisplayedItem displayedItem = openedItemsList.get(rowIndex);
            if ((columnIndex == Columns.AXIS.ordinal()) && (aValue instanceof AxisType)
                    && (displayedItem.getAxis() != null)) {
                AxisType newAxis = (AxisType) aValue;

                if (!displayedItem.getAxis().equals(newAxis)) {
                    setItemAxis(displayedItem.getKey(), newAxis);
                }
            } else {
                Item scaleItem = null;
                if (aValue instanceof Item) {
                    scaleItem = (Item) aValue;
                }
                boolean packColumn;
                if (columnIndex == Columns.XSCALE.ordinal()) {
                    displayedItem.setXItem(scaleItem);
                    packColumn = true;
                } else if (columnIndex == Columns.YSCALE.ordinal()) {
                    displayedItem.setYItem(scaleItem);
                    packColumn = true;
                } else {
                    packColumn = false;
                }
                setItemScale(displayedItem);
                if (packColumn) {
                    EDTManager.INSTANCE.runInDrawingThread(() -> {
                        int tableColumnIndex = table.convertColumnIndexToView(columnIndex);
                        if (tableColumnIndex > -1) {
                            table.packColumn(tableColumnIndex, 5);
                        }
                    });
                }
            }
        }
    }

    protected class DisplayManagerTable extends JXTable {

        private static final long serialVersionUID = 4664867484839408477L;

        @Override
        public Component prepareRenderer(final TableCellRenderer renderer, final int row, final int column) {
            Component c = super.prepareRenderer(renderer, row, column);

            if (c instanceof JComponent) {
                JComponent jc = (JComponent) c;
                // tooltip
                String tooltip = null;
                int columnIndex = convertColumnIndexToModel(column);
                int index = convertRowIndexToModel(row);
                DisplayedItem displayedItem = tableModel.getDisplayedItem(index);
                if (columnIndex == Columns.ITEM.ordinal()) {
                    IDataSourceBrowser browser = displayedItem.getBrowser();
                    IKey itemKey = displayedItem.getKey();
                    String description = browser.getDescription(itemKey);

                    if (description != null) {
                        tooltip = description;
                    }
                    if (jc instanceof JLabel) {
                        Font oldFont = jc.getFont();
                        Color background = jc.getBackground();
                        ((JLabel) jc).setUI(new ReverseLabelUI());
                        jc.setFont(oldFont);
                        jc.setBackground(background);

                    }

                } else if ((columnIndex == Columns.XSCALE.ordinal()) || (columnIndex == Columns.YSCALE.ordinal())) {
                    Item scaleItem = displayedItem.getXItem();
                    if (columnIndex == Columns.YSCALE.ordinal()) {
                        scaleItem = displayedItem.getYItem();
                    }
                    if (scaleItem != null) {
                        IDataSourceBrowser browser = scaleItem.getBrowser();
                        String description = browser.getDescription(scaleItem.getKey());
                        if (description != null) {
                            tooltip = description;
                        }
                        ((JLabel) jc).setText(browser.getDisplayName(scaleItem.getKey()));
                    } else {
                        ((JLabel) jc).setText(ObjectUtils.EMPTY_STRING);
                    }
                }
                if ((tooltip == null) && (c.getPreferredSize().width > this.getCellRect(row, column, false).width)) {
                    tooltip = String.valueOf(getValueAt(row, column));
                }
                jc.setToolTipText(tooltip);
            }
            return c;
        }
    }

    protected class DisplayManagerTableMouseListener extends MouseAdapter {

        @Override
        public void mousePressed(final MouseEvent e) {
            onPopupTrigger(e);
        }

        @Override
        public void mouseReleased(final MouseEvent e) {
            onPopupTrigger(e);
        }

        @Override
        public void mouseClicked(final MouseEvent e) {
            onPopupTrigger(e);
        }

        private void onPopupTrigger(final MouseEvent e) {
            if (e.isPopupTrigger()) {
                int pointedRow = table.rowAtPoint(e.getPoint());
                // if user clicked on a selected row, show popup
                if (pointedRow != -1) {
                    if (table.isRowSelected(pointedRow)) {
                        popupMenu.show(e.getComponent(), e.getX(), e.getY());
                    }
                }
            }
        }
    }

}
