/*
 * This file is part of databrowser-gui.
 * 
 * databrowser-gui is free software: you can redistribute it and/or modify it under the terms of the GNU General Public
 * License as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later
 * version.
 * 
 * databrowser-gui is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the
 * implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for more
 * details.
 * 
 * You should have received a copy of the GNU General Public License along with databrowser-gui. If not, see
 * <https://www.gnu.org/licenses/>.
 */
package org.cdma.gui.databrowser.util;

import javax.swing.filechooser.FileFilter;

import fr.soleil.lib.project.ObjectUtils;

/**
 * A {@link FileFilter} based on file extensions. This {@link FileFilter}
 * accepts directories and files that have a given extension
 * 
 * @author girardot
 */
public class ExtensionFileFilter extends MultiExtFileFilter {

    /**
     * Constructs a new {@link ExtensionFileFilter}
     * 
     * @param fileExtension
     *            The compatible file extension
     */
    public ExtensionFileFilter(final String fileExtension) {
        this(fileExtension, null);
    }

    /**
     * Constructs a new {@link ExtensionFileFilter}, describing its
     * corresponding files
     * 
     * @param fileExtension
     *            The compatible file extension
     * @param fileDescription
     *            The file description
     */
    public ExtensionFileFilter(final String fileExtension, final String fileDescription) {
        // true to bypass validation test in super constructor
        super(fileDescription, true, fileExtension);
    }

    @Override
    protected void computeDescription(final String filterDescription) {
        StringBuilder sb = new StringBuilder();

        if (filterDescription != null) {
            sb.append(filterDescription);
        } else {
            sb.append(getDefaultExtension()).append(" files");
        }

        description = sb.toString();
    }

    /**
     * Returns the compatible file extension
     * 
     * @return A {@link String}
     */
    public String getExtension() {
        return getDefaultExtension();
    }

    @Override
    public boolean equals(final Object obj) {
        boolean equals;
        if (obj == null) {
            equals = false;
        } else if (obj == this) {
            equals = true;
        } else if (getClass().equals(obj.getClass())) {
            ExtensionFileFilter selector = (ExtensionFileFilter) obj;
            equals = ObjectUtils.sameObject(getExtension(), selector.getExtension())
                    && ObjectUtils.sameObject(getDescription(), selector.getDescription());
        } else {
            equals = false;
        }
        return equals;
    }

    @Override
    public int hashCode() {
        int code = 0xF11E;
        int mult = 0xF117E13;
        code = code * mult + getClass().hashCode();
        code = code * mult + ObjectUtils.getHashCode(getExtension());
        code = code * mult + ObjectUtils.getHashCode(getDescription());
        return code;
    }

}
