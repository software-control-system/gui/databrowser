/*
 * This file is part of databrowser-gui.
 * 
 * databrowser-gui is free software: you can redistribute it and/or modify it under the terms of the GNU General Public
 * License as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later
 * version.
 * 
 * databrowser-gui is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the
 * implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for more
 * details.
 * 
 * You should have received a copy of the GNU General Public License along with databrowser-gui. If not, see
 * <https://www.gnu.org/licenses/>.
 */
package org.cdma.gui.databrowser.util;

import java.awt.datatransfer.DataFlavor;
import java.awt.datatransfer.Transferable;
import java.awt.datatransfer.UnsupportedFlavorException;
import java.awt.dnd.InvalidDnDOperationException;
import java.io.File;
import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.net.MalformedURLException;
import java.net.URL;
import java.net.URLDecoder;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.LinkedList;
import java.util.List;
import java.util.StringTokenizer;

import javax.swing.TransferHandler;

import org.cdma.gui.databrowser.DataBrowserController;
import org.cdma.gui.databrowser.LoadParameter;
import org.cdma.gui.databrowser.PathParameter;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import fr.soleil.comete.definition.widget.ITree;
import fr.soleil.lib.project.swing.TextTansfertHandler;

public class DropFileTransferHandler extends TransferHandler {

    private static final long serialVersionUID = 3077939091026570390L;

    private static final Logger LOGGER = LoggerFactory.getLogger(DropFileTransferHandler.class);
    private static final String TREE_SOURCE = "org.cdma.gui.databrowser.view.tree";
    private final DataBrowserController controller;

    private final DataFlavor uriListFlavor;

    public DropFileTransferHandler(final DataBrowserController controller, ITree atree) {
        this.controller = controller;
        uriListFlavor = TextTansfertHandler.getUriListFlavor();
    }

    @Override
    public boolean canImport(final TransferHandler.TransferSupport support) {
        boolean canImport = false;
        if (support.isDrop()) {
            try {
                if (support.isDataFlavorSupported(DataFlavor.javaFileListFlavor)) {

                    // get list of dropped files
                    List<File> fileList = getTransferableFileList(support);
                    // tests if at least one file can be opened
                    for (File file : fileList) {
                        canImport = controller.canOpenSource(file.getPath());
                        if (canImport) {
                            break;
                        }
                    }

                } else {
                    // this case's purpose is only to recover files drop on
                    // linux
                    if (support.isDataFlavorSupported(uriListFlavor)) {
                        try {
                            // get list of dropped files as URI
                            List<String> pathList = getTransferableUriList(support);
                            // tests if at least one file can be opened
                            for (String path : pathList) {
                                canImport = controller.canOpenSource(path);
                                if (canImport) {
                                    break;
                                }
                            }
                        } catch (MalformedURLException ignore) {
                            LOGGER.debug("Cannot import{}", ignore.getMessage());
                            LOGGER.debug("Stack trace", ignore);
                        }
                    }

                    // this case is not designed to split multiple paths
                    if (!canImport && support.isDataFlavorSupported(DataFlavor.stringFlavor)) {
                        // get path of running device or valid file
                        String path = getTransferableString(support);
                        if ((path != null) && !path.contains(TREE_SOURCE)) {
                            canImport = controller.canOpenSource(path);
                        }
                    }
                }
            } catch (InvalidDnDOperationException e) {
                // workaround for bug in java
                // http://bugs.sun.com/bugdatabase/view_bug.do?bug_id=6759788
                // When releasing mouse button in the acceptable case, canImport
                // is
                // called one more time with a bad transferable object. So it is
                // ok to
                // import anyway.
                LOGGER.debug("Cannot import{}", e.getMessage());
                LOGGER.debug("Stack trace", e);
                canImport = true;
            } catch (Exception e) {
                LOGGER.error("Cannot import{}", e.getMessage());
                LOGGER.debug("Stack trace", e);
            }
        }

        return canImport;
    }

    @Override
    public boolean importData(final TransferSupport support) {
        if (support.isDrop()) {
            try {
                if (support.isDataFlavorSupported(DataFlavor.javaFileListFlavor)) {
                    List<File> fileList = getTransferableFileList(support);

                    // convert to LoadParam list
                    List<LoadParameter> paramList = new ArrayList<>(fileList.size());
                    for (File file : fileList) {
                        paramList.add(new PathParameter(file.getPath()));
                    }
                    controller.loadSources(paramList);
                } else {
                    boolean canImport = false;

                    if (support.isDataFlavorSupported(uriListFlavor)) {
                        try {
                            List<String> pathList = getTransferableUriList(support);
                            canImport = true;

                            // convert to LoadParam list
                            List<LoadParameter> paramList = new ArrayList<>(pathList.size());
                            for (String path : pathList) {
                                LOGGER.trace("Import data {}", Arrays.toString(paramList.toArray()));
                                paramList.add(new PathParameter(path));
                            }
                            controller.loadSources(paramList);
                        } catch (MalformedURLException ignore) {
                            LOGGER.debug("Cannot import data {}", ignore.getMessage());
                            LOGGER.debug("Stack trace", ignore);
                        }
                    }

                    if (!canImport && support.isDataFlavorSupported(DataFlavor.stringFlavor)) {
                        String path = getTransferableString(support);

                        // convert to LoadParam list
                        List<LoadParameter> paramList = new ArrayList<>(1);
                        paramList.add(new PathParameter(path));
                        controller.loadSources(paramList);
                    }
                }
            } catch (Exception e) {
                LOGGER.error("Cannot import data {}", e.getMessage());
                LOGGER.debug("Stack trace", e);
            }
        }
        return true;
    }

    private static List<File> getTransferableFileList(final TransferHandler.TransferSupport support)
            throws UnsupportedFlavorException, IOException {
        Transferable transferable = support.getTransferable();
        @SuppressWarnings("unchecked")
        List<File> fileList = (List<File>) transferable.getTransferData(DataFlavor.javaFileListFlavor);
        return fileList;
    }

    private static List<String> getTransferableUriList(final TransferHandler.TransferSupport support)
            throws UnsupportedFlavorException, IOException, MalformedURLException, UnsupportedEncodingException {
        String urls = getTransferableString(support);
        LOGGER.trace("urls= {}", urls);
        List<String> pathList = new LinkedList<>();
        StringTokenizer tokens = new StringTokenizer(urls, "\t\n\r\f");// allow
        // spaces

        String urlString = null;
        while (tokens.hasMoreTokens()) {
            urlString = tokens.nextToken().trim();
            if (urlString != null) {
                LOGGER.trace("getTransferableUriList {}", urlString);
                URL url = new URL(urlString);
                String path = URLDecoder.decode(url.getFile(), "UTF-8");
                pathList.add(path);
            }
        }
        LOGGER.trace("pathList = {}", Arrays.toString(pathList.toArray()));
        return pathList;
    }

    private static String getTransferableString(final TransferHandler.TransferSupport support)
            throws UnsupportedFlavorException, IOException {
        Transferable transferable = support.getTransferable();
        String path = (String) transferable.getTransferData(DataFlavor.stringFlavor);
        return path;
    }
/*
public class TreeTransferable implements Transferable {

@Override
public DataFlavor[] getTransferDataFlavors() {
return null;
}

@Override
public boolean isDataFlavorSupported(DataFlavor flavor) {
return null;
}

@Override
public Object getTransferData(DataFlavor flavor) throws UnsupportedFlavorException, IOException {
List<IKey> keyList = new ArrayList<>();
if (tree != null) {
ITreeNode[] selectedNodes = tree.getSelectedNodes();
Object data = null;
for (ITreeNode treeNode : selectedNodes) {
data = treeNode.getData();
if (data instanceof IKey) {
keyList.add((IKey) data);
}
}

}
return keyList;
}

@SuppressWarnings("unchecked")
public List<IKey> getKeyList(DataFlavor flavor) throws UnsupportedFlavorException, IOException {
Object object = getTransferData(flavor);
List<IKey> keyList = null;
if (object != null) {
keyList = (List<IKey>) object;
}
return keyList;
}

}

 */

}
