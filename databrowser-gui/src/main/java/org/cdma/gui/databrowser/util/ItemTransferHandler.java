/*
 * This file is part of databrowser-gui.
 * 
 * databrowser-gui is free software: you can redistribute it and/or modify it under the terms of the GNU General Public
 * License as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later
 * version.
 * 
 * databrowser-gui is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the
 * implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for more
 * details.
 * 
 * You should have received a copy of the GNU General Public License along with databrowser-gui. If not, see
 * <https://www.gnu.org/licenses/>.
 */
package org.cdma.gui.databrowser.util;

import javax.swing.TransferHandler;

import org.cdma.gui.databrowser.DataBrowserController;
import org.cdma.gui.databrowser.interfaces.AxisType;
import org.cdma.gui.databrowser.interfaces.DataType;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class ItemTransferHandler extends TransferHandler {

    private static final long serialVersionUID = -453672780933601667L;

    protected static final Logger LOGGER = LoggerFactory.getLogger(ItemTransferHandler.class);

    protected final DataBrowserController controller;
    protected DataType forcedType = null;
    protected AxisType type = null;

    public ItemTransferHandler(final DataBrowserController controller) {
        this(controller, null, null);
    }

    public ItemTransferHandler(final DataBrowserController controller, final DataType forcedType, final AxisType type) {
        this.controller = controller;
        this.forcedType = forcedType;
        this.type = type;
    }

    @Override
    public boolean canImport(final TransferHandler.TransferSupport support) {
        boolean canImport = false;
        if (controller != null) {
            DataType datatype = forcedType;
            if (datatype == null) {
                datatype = DataType.SCALAR;
            }
            canImport = isDropEnable(datatype);
        }
        return canImport;
    }

    @Override
    public boolean importData(final TransferSupport support) {
        boolean importe = true;
        if (support.isDrop()) {
            importe = importData();
        }
        return importe;
    }

    protected boolean importData() {
        boolean importe = true;
        if ((type == null) || (forcedType == null)) {
            controller.openItem();
        } else {
            LOGGER.trace("openItem ({},{})", forcedType, type);
            controller.openItem(forcedType, type);
        }
        return importe;
    }

    protected boolean isDropEnable(DataType datatype) {
        return controller.isDropEnable(datatype);
    }
}
