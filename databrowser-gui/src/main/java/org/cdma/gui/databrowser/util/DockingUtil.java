/*
 * This file is part of databrowser-gui.
 * 
 * databrowser-gui is free software: you can redistribute it and/or modify it under the terms of the GNU General Public
 * License as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later
 * version.
 * 
 * databrowser-gui is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the
 * implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for more
 * details.
 * 
 * You should have received a copy of the GNU General Public License along with databrowser-gui. If not, see
 * <https://www.gnu.org/licenses/>.
 */
package org.cdma.gui.databrowser.util;

import javax.swing.ImageIcon;
import javax.swing.JComponent;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import fr.soleil.docking.ADockingManager;
import fr.soleil.docking.exception.DockingException;
import fr.soleil.docking.infonode.view.InfoNodeViewFactory;
import fr.soleil.docking.perspective.IPerspective;
import fr.soleil.docking.perspective.IPerspectiveFactory;
import fr.soleil.docking.view.IView;
import fr.soleil.docking.view.IViewFactory;

public class DockingUtil {

    private static final Logger LOGGER = LoggerFactory.getLogger(DockingUtil.class);

    public static final int TREE_VIEW_ID = 0;
    public static final int ITEM_VIEW_ID = 1;
    public static final int INFORMATION_VIEW_ID = 2;
    public static final int DISPLAY_MANAGER_VIEW_ID = 3;
    public static final int SCALAR_VIEW_ID = 4;
    public static final int SPECTRUM_VIEW_ID = 5;
    public static final int IMAGE_VIEW_ID = 6;

    public static final byte[] DEFAULT_LAYOUT = new byte[] { -84, -19, 0, 5, 122, 0, 0, 2, -88, 0, 0, 0, 4, 0, 0, 0, 0,
            4, 0, 0, 0, 87, -84, -19, 0, 5, 115, 114, 0, 17, 106, 97, 118, 97, 46, 108, 97, 110, 103, 46, 73, 110, 116,
            101, 103, 101, 114, 18, -30, -96, -92, -9, -127, -121, 56, 2, 0, 1, 73, 0, 5, 118, 97, 108, 117, 101, 120,
            114, 0, 16, 106, 97, 118, 97, 46, 108, 97, 110, 103, 46, 78, 117, 109, 98, 101, 114, -122, -84, -107, 29,
            11, -108, -32, -117, 2, 0, 0, 120, 112, 0, 0, 0, 3, 119, 4, -1, -1, -1, -1, 0, 0, 0, 87, -84, -19, 0, 5,
            115, 114, 0, 17, 106, 97, 118, 97, 46, 108, 97, 110, 103, 46, 73, 110, 116, 101, 103, 101, 114, 18, -30,
            -96, -92, -9, -127, -121, 56, 2, 0, 1, 73, 0, 5, 118, 97, 108, 117, 101, 120, 114, 0, 16, 106, 97, 118, 97,
            46, 108, 97, 110, 103, 46, 78, 117, 109, 98, 101, 114, -122, -84, -107, 29, 11, -108, -32, -117, 2, 0, 0,
            120, 112, 0, 0, 0, 0, 119, 4, -1, -1, -1, -1, 0, 0, 0, 87, -84, -19, 0, 5, 115, 114, 0, 17, 106, 97, 118,
            97, 46, 108, 97, 110, 103, 46, 73, 110, 116, 101, 103, 101, 114, 18, -30, -96, -92, -9, -127, -121, 56, 2,
            0, 1, 73, 0, 5, 118, 97, 108, 117, 101, 120, 114, 0, 16, 106, 97, 118, 97, 46, 108, 97, 110, 103, 46, 78,
            117, 109, 98, 101, 114, -122, -84, -107, 29, 11, -108, -32, -117, 2, 0, 0, 120, 112, 0, 0, 0, 1, 119, 4, -1,
            -1, -1, -1, 0, 0, 0, 87, -84, -19, 0, 5, 115, 114, 0, 17, 106, 97, 118, 97, 46, 108, 97, 110, 103, 46, 73,
            110, 116, 101, 103, 101, 114, 18, -30, -96, -92, -9, -127, -121, 56, 2, 0, 1, 73, 0, 5, 118, 97, 108, 117,
            101, 120, 114, 0, 16, 106, 97, 118, 97, 46, 108, 97, 110, 103, 46, 78, 117, 109, 98, 101, 114, -122, -84,
            -107, 29, 11, -108, -32, -117, 2, 0, 0, 120, 112, 0, 0, 0, 2, 119, 4, -1, -1, -1, -1, 0, 0, 0, 1, 0, 0, 0,
            0, 0, 0, 0, 2, 0, 0, 0, 1, 0, 0, 0, 1, 0, 0, 0, 2, 0, 0, 0, 1, 1, -1, -1, -1, -1, -1, -1, -1, -1, 1, 0, 0,
            0, 0, 0, 0, 0, 2, 0, 0, 0, 0, 0, 0, 0, 2, 0, 0, 0, 1, 0, 0, 0, 1, 0, 0, 0, 2, 0, 0, 0, 2, 1, -1, -1, -1, -1,
            -1, -1, -1, -1, 1, 0, 0, 0, 1, 0, 0, 0, 1, 0, 0, 0, 2, 0, 0, 0, 0, 1, -1, -1, -1, -1, -1, -1, -1, -1, 1, 1,
            63, 41, 1, -87, -1, -1, -1, -1, 1, 0, 0, 0, 1, 0, 0, 0, 1, 0, 0, 0, 2, 0, 0, 0, 3, 1, -1, -1, -1, -1, -1,
            -1, -1, -1, 1, 0, 63, 78, 75, 37, -1, -1, -1, -1, 1, 1, 62, 65, 105, 8, -1, -1, -1, -1, 1, -1, -1, -1, -1,
            1, 0, 0, 0, -56, 0, -1, -1, -1, -1, -1, -1, -1, -1, 0, 0, 0, 0, 0, 0, 0, -56, 0, -1, -1, -1, -1, -1, -1, -1,
            -1, 0, 0, 0, 0, 0, 0, 0, -56, 1, -1, -1, -1, -1, -1, -1, -1, -1, 0, 0, 0, 0, 0, 0, 0, -56, 0, -1, -1, -1,
            -1, -1, -1, -1, -1, 0, 0, 0, 0, 0, 0, 0, 0, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1,
            -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1,
            -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1 };

    public static final byte[] DEFAULT_DATAITEM_LAYOUT = new byte[] { -84, -19, 0, 5, 122, 0, 0, 1, -65, 0, 0, 0, 4, 0,
            0, 0, 0, 3, 0, 0, 0, 87, -84, -19, 0, 5, 115, 114, 0, 17, 106, 97, 118, 97, 46, 108, 97, 110, 103, 46, 73,
            110, 116, 101, 103, 101, 114, 18, -30, -96, -92, -9, -127, -121, 56, 2, 0, 1, 73, 0, 5, 118, 97, 108, 117,
            101, 120, 114, 0, 16, 106, 97, 118, 97, 46, 108, 97, 110, 103, 46, 78, 117, 109, 98, 101, 114, -122, -84,
            -107, 29, 11, -108, -32, -117, 2, 0, 0, 120, 112, 0, 0, 0, 4, 119, 4, -1, -1, -1, -1, 0, 0, 0, 87, -84, -19,
            0, 5, 115, 114, 0, 17, 106, 97, 118, 97, 46, 108, 97, 110, 103, 46, 73, 110, 116, 101, 103, 101, 114, 18,
            -30, -96, -92, -9, -127, -121, 56, 2, 0, 1, 73, 0, 5, 118, 97, 108, 117, 101, 120, 114, 0, 16, 106, 97, 118,
            97, 46, 108, 97, 110, 103, 46, 78, 117, 109, 98, 101, 114, -122, -84, -107, 29, 11, -108, -32, -117, 2, 0,
            0, 120, 112, 0, 0, 0, 5, 119, 4, -1, -1, -1, -1, 0, 0, 0, 87, -84, -19, 0, 5, 115, 114, 0, 17, 106, 97, 118,
            97, 46, 108, 97, 110, 103, 46, 73, 110, 116, 101, 103, 101, 114, 18, -30, -96, -92, -9, -127, -121, 56, 2,
            0, 1, 73, 0, 5, 118, 97, 108, 117, 101, 120, 114, 0, 16, 106, 97, 118, 97, 46, 108, 97, 110, 103, 46, 78,
            117, 109, 98, 101, 114, -122, -84, -107, 29, 11, -108, -32, -117, 2, 0, 0, 120, 112, 0, 0, 0, 6, 119, 4, -1,
            -1, -1, -1, 0, 0, 0, 1, 0, 0, 0, 1, 0, 0, 0, 3, 0, 0, 0, 2, 0, 0, 0, 0, 1, 0, 0, 0, 2, 0, 0, 0, 1, 1, 0, 0,
            0, 2, 0, 0, 0, 2, 1, -1, -1, -1, -1, 0, 0, 0, 1, 1, -1, -1, -1, -1, 1, 0, 0, 0, -56, 0, -1, -1, -1, -1, -1,
            -1, -1, -1, 0, 0, 0, 0, 0, 0, 0, -56, 0, -1, -1, -1, -1, -1, -1, -1, -1, 0, 0, 0, 0, 0, 0, 0, -56, 1, -1,
            -1, -1, -1, -1, -1, -1, -1, 0, 0, 0, 0, 0, 0, 0, -56, 0, -1, -1, -1, -1, -1, -1, -1, -1, 0, 0, 0, 0, 0, 0,
            0, 0, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1,
            -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1 };

    private IViewFactory dockingFactory = null;
    private ADockingManager dockingManager = null;
    private IPerspectiveFactory perspectiveFactory = null;

    private void initFactory() {
        // ----------------
        // DOCKING CREATION
        // ----------------
        if (dockingFactory == null) {
            dockingFactory = new InfoNodeViewFactory();
        }
        if (dockingManager == null) {
            dockingManager = dockingFactory.generateDockingManager();
            dockingManager.setAutomaticallySavePerspective(true);
        }

        if (perspectiveFactory == null) {
            perspectiveFactory = dockingManager.getPerspectiveFactory();
        }

    }

    public final IView createView(Object id, ImageIcon displayIcon, String title, JComponent content,
            JComponent parent) {
        initFactory();
        IView addView = dockingFactory.addView(title, displayIcon, content, id, parent);
        if (addView != null) {
            addView.setVisible(true);
            addView.setClosable(false);
            addView.select();
        }
        return addView;
    }

    public final JComponent createDockingArea(boolean undockEnabled, boolean root) {
        initFactory();
        JComponent dockingArea = null;
        if (root) {
            dockingArea = dockingManager.getDockingArea();
        } else {
            dockingArea = dockingManager.createNewDockingArea(null);
        }
        dockingManager.setUndockEnabled(undockEnabled, dockingArea);
        return dockingArea;
    }

    public final void setLayout(byte[] byteArray, JComponent area, Object idView) {
        initFactory();
        IPerspective currentPerspective = getCurrentPerspective(idView);
        currentPerspective.setByteArray(byteArray);
        try {
            dockingManager.applyPerspective(currentPerspective, area);
        } catch (DockingException e) {
            LOGGER.error("Error when loading docking = {}", e.getMessage());
            LOGGER.debug("Stack trace", e);
        }
    }

    public final void removeView(final Object key) {
        dockingFactory.removeView(key);
    }

    public final byte[] getLayout(Object idView, JComponent area) {
        initFactory();
        IPerspective currentPerspective = getCurrentPerspective(idView);
        try {
            if (idView == null) {
                dockingManager.saveSelectedPerspective();
            } else {
                dockingManager.updatePerspective(currentPerspective, area);
            }
        } catch (DockingException e) {
            LOGGER.error("Save docking error = {}", e.getMessage());
            LOGGER.debug("Stack trace", e);
        }

        byte[] byteArray = null;
        if (currentPerspective != null) {
            byteArray = currentPerspective.getByteArray();
        }
        return byteArray;
    }

    private IPerspective getCurrentPerspective(Object idView) {
        IPerspective perspective = null;
        if (idView == null) {
            perspective = perspectiveFactory.getSelectedPerspective();
        } else {
            perspective = perspectiveFactory.getPerspective(idView.toString());
            if (perspective == null) {
                perspective = perspectiveFactory.createPerspective(idView.toString());
            }
        }

        return perspective;
    }

}
