/*
 * This file is part of databrowser-gui.
 * 
 * databrowser-gui is free software: you can redistribute it and/or modify it under the terms of the GNU General Public
 * License as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later
 * version.
 * 
 * databrowser-gui is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the
 * implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for more
 * details.
 * 
 * You should have received a copy of the GNU General Public License along with databrowser-gui. If not, see
 * <https://www.gnu.org/licenses/>.
 */
package org.cdma.gui.databrowser.plugintree;

import java.awt.BorderLayout;
import java.awt.Dialog;
import java.awt.Font;

import javax.swing.JDialog;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JScrollPane;

import org.cdma.gui.databrowser.DataBrowser;
import org.cdma.gui.databrowser.DataSourceManager;
import org.jdesktop.swingx.JXTreeTable;

public class PluginTreeDialog extends JDialog {

    private static final long serialVersionUID = -1802954623621601414L;

    private static final Font NO_PLUGIN_FONT = new Font(Font.DIALOG, Font.ITALIC, 12);

    private JXTreeTable pluginTreeTable;
    private JLabel noPluginLabel;

    public PluginTreeDialog(final Dialog owner) {
        super(owner);

        setModal(true);
        setTitle(DataBrowser.MESSAGES.getString("About.Plugins.Title"));

        initComponents();
        layoutComponents();
    }

    private void initComponents() {
        PluginTreeTableModel pluginTreeModel = DataSourceManager.getPluginTreeTableModel();
        pluginTreeTable = new JXTreeTable(pluginTreeModel);
        pluginTreeTable.setTreeCellRenderer(new PluginTreeCellRenderer());
        pluginTreeTable.setHorizontalScrollEnabled(true);
        pluginTreeTable.expandAll();
        pluginTreeTable.packAll();

        noPluginLabel = new JLabel(DataBrowser.MESSAGES.getString("About.Plugins.No"));
        noPluginLabel.setFont(NO_PLUGIN_FONT);

        // shows one extra line, because root is not displayed
        pluginTreeTable.setVisibleRowCount(pluginTreeModel.getNodeCount(pluginTreeModel.getRoot()));
    }

    private void layoutComponents() {
        if (pluginTreeTable.getRowCount() > 0) {
            JScrollPane scrollPane = new JScrollPane(pluginTreeTable);

            JPanel contentPane = new JPanel();
            contentPane.setLayout(new BorderLayout());
            contentPane.add(scrollPane, BorderLayout.CENTER);

            setContentPane(contentPane);
        } else {
            setContentPane(noPluginLabel);
        }
    }

    @Override
    public void setVisible(final boolean b) {
        pluginTreeTable.clearSelection();
        super.setVisible(b);
    }

}
