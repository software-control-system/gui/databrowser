/*
 * This file is part of databrowser-gui.
 * 
 * databrowser-gui is free software: you can redistribute it and/or modify it under the terms of the GNU General Public
 * License as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later
 * version.
 * 
 * databrowser-gui is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the
 * implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for more
 * details.
 * 
 * You should have received a copy of the GNU General Public License along with databrowser-gui. If not, see
 * <https://www.gnu.org/licenses/>.
 */
package org.cdma.gui.databrowser.model;

import org.cdma.gui.databrowser.DataBrowserController;
import org.cdma.gui.databrowser.interfaces.Item;
import org.cdma.gui.databrowser.view.item.MatrixViewer;

import fr.soleil.comete.box.matrixbox.NumberMatrixBox;
import fr.soleil.comete.definition.listener.IDataTransferListener;
import fr.soleil.comete.definition.listener.ISnapshotListener;
import fr.soleil.comete.definition.util.ArrayPositionConvertor;
import fr.soleil.comete.definition.widget.IImageViewer;
import fr.soleil.comete.definition.widget.properties.ImageProperties;
import fr.soleil.data.target.matrix.INumberMatrixTarget;
import fr.soleil.lib.project.ObjectUtils;

public class NumberMatrixItem extends ImageMatrixItem<INumberMatrixTarget, NumberMatrixBox> {

    public NumberMatrixItem(final MatrixViewer matrixViewer, final Item item, DataBrowserController controller) {
        super(matrixViewer, item, controller);
    }

    @Override
    protected NumberMatrixBox generateMatrixBox() {
        return new NumberMatrixBox();
    }

    @Override
    protected NumberMatrixBox getMainMatrixBox() {
        MatrixViewer matrixViewer = ObjectUtils.recoverObject(matrixViewerRef);
        return matrixViewer == null ? null : matrixViewer.getNumberMatrixBox();
    }

    public void setXAxisConvertor(final ArrayPositionConvertor convertor) {
        viewer.setXAxisConvertor(convertor);
    }

    public void setYAxisConvertor(final ArrayPositionConvertor convertor) {
        viewer.setYAxisConvertor(convertor);
    }

    public void addSnapshotListener(final ISnapshotListener listener) {
        ((IImageViewer) viewer).addSnapshotListener(listener);
    }

    public void removeSnapshotListener(final ISnapshotListener listener) {
        ((IImageViewer) viewer).removeSnapshotListener(listener);
    }

    public void setSnapshotLocation(final String path) {
        ((IImageViewer) viewer).setSnapshotDirectory(path);
    }

    public String getSnapshotLocation() {
        return ((IImageViewer) viewer).getSnapshotDirectory();
    }

    public void addDataListener(final IDataTransferListener listener) {
        ((IImageViewer) viewer).addDataTransferListener(listener);
    }

    public void removeDataListener(final IDataTransferListener listener) {
        ((IImageViewer) viewer).removeDataTransferListener(listener);
    }

    public void setDataLocation(final String path) {
        ((IImageViewer) viewer).setDataDirectory(path);
    }

    public String getDataLocation() {
        return ((IImageViewer) viewer).getDataDirectory();
    }

    public ImageProperties getImageProperties() {
        return ((IImageViewer) viewer).getImageProperties();
    }

    public void setImageProperties(final ImageProperties imageProperties) {
        ((IImageViewer) viewer).setImageProperties(imageProperties);
    }
}
