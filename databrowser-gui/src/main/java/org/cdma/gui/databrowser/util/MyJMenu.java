/*
 * This file is part of databrowser-gui.
 * 
 * databrowser-gui is free software: you can redistribute it and/or modify it under the terms of the GNU General Public
 * License as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later
 * version.
 * 
 * databrowser-gui is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the
 * implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for more
 * details.
 * 
 * You should have received a copy of the GNU General Public License along with databrowser-gui. If not, see
 * <https://www.gnu.org/licenses/>.
 */
package org.cdma.gui.databrowser.util;

import java.awt.Component;
import java.awt.Container;
import java.awt.Dimension;
import java.awt.GraphicsConfiguration;
import java.awt.GraphicsDevice;
import java.awt.GraphicsEnvironment;
import java.awt.Insets;
import java.awt.Point;
import java.awt.Rectangle;
import java.awt.Toolkit;

import javax.swing.JMenu;
import javax.swing.JPopupMenu;
import javax.swing.UIManager;

public class MyJMenu extends JMenu {

    private static final long serialVersionUID = 8087902046591407303L;

    // take screen insets (e.g. taskbar) into account
    // see workaround for Java 7 X11 slowness JIRA PROBLEM-516
    protected Insets screenInsets = new Insets(0, 0, 0, 0);

    protected Insets componentInsets = new Insets(5, 5, 5, 5);

    public MyJMenu(String text) {
        super(text);
    }

    @Override
    public Insets getInsets() {
        return componentInsets;
    }

    /**
     * Computes the origin for the <code>JMenu</code>'s popup menu.
     * This method uses Look and Feel properties named <code>Menu.menuPopupOffsetX</code>,
     * <code>Menu.menuPopupOffsetY</code>, <code>Menu.submenuPopupOffsetX</code>, and
     * <code>Menu.submenuPopupOffsetY</code> to adjust the exact location of popup.
     *
     * @return a <code>Point</code> in the coordinate space of the
     *         menu which should be used as the origin
     *         of the <code>JMenu</code>'s popup menu
     *
     * @since 1.3
     */
    @Override
    protected Point getPopupMenuOrigin() {
        int x;
        int y;
        JPopupMenu pm = getPopupMenu();
        // Figure out the sizes needed to caclulate the menu position
        Dimension s = getSize();
        Dimension pmSize = pm.getSize();
        // For the first time the menu is popped up,
        // the size has not yet been initiated
        if (pmSize.width == 0) {
            pmSize = pm.getPreferredSize();
        }
        Point position = getLocationOnScreen();
        Toolkit toolkit = Toolkit.getDefaultToolkit();
        GraphicsConfiguration gc = getGraphicsConfiguration();
        Rectangle screenBounds = new Rectangle(toolkit.getScreenSize());
        GraphicsEnvironment ge = GraphicsEnvironment.getLocalGraphicsEnvironment();
        GraphicsDevice[] gd = ge.getScreenDevices();
        for (int i = 0; i < gd.length; i++) {
            if (gd[i].getType() == GraphicsDevice.TYPE_RASTER_SCREEN) {
                GraphicsConfiguration dgc = gd[i].getDefaultConfiguration();
                if (dgc.getBounds().contains(position)) {
                    gc = dgc;
                    break;
                }
            }
        }

        if (gc != null) {
            screenBounds = gc.getBounds();
            screenBounds.width -= Math.abs(screenInsets.left + screenInsets.right);
            screenBounds.height -= Math.abs(screenInsets.top + screenInsets.bottom);
            position.x -= Math.abs(screenInsets.left);
            position.y -= Math.abs(screenInsets.top);
        }

        Container parent = getParent();
        if (parent instanceof JPopupMenu) {
            // We are a submenu (pull-right)
            int xOffset = UIManager.getInt("Menu.submenuPopupOffsetX");
            int yOffset = UIManager.getInt("Menu.submenuPopupOffsetY");

            if (isLeftToRight(this)) {
                // First determine x:
                x = s.width + xOffset; // Prefer placement to the right
                if (position.x + x + pmSize.width >= screenBounds.width + screenBounds.x &&
                // popup doesn't fit - place it wherever there's more room
                        screenBounds.width - s.width < 2 * (position.x - screenBounds.x)) {

                    x = 0 - xOffset - pmSize.width;
                }
            } else {
                // First determine x:
                x = 0 - xOffset - pmSize.width; // Prefer placement to the left
                if (position.x + x < screenBounds.x &&
                // popup doesn't fit - place it wherever there's more room
                        screenBounds.width - s.width > 2 * (position.x - screenBounds.x)) {

                    x = s.width + xOffset;
                }
            }
            // Then the y:
            y = yOffset; // Prefer dropping down
            if (position.y + y + pmSize.height >= screenBounds.height + screenBounds.y &&
            // popup doesn't fit - place it wherever there's more room
                    screenBounds.height - s.height < 2 * (position.y - screenBounds.y)) {

                y = s.height - yOffset - pmSize.height;
            }
        } else {
            // We are a toplevel menu (pull-down)
            int xOffset = UIManager.getInt("Menu.menuPopupOffsetX");
            int yOffset = UIManager.getInt("Menu.menuPopupOffsetY");

            if (isLeftToRight(this)) {
                // First determine the x:
                x = xOffset; // Extend to the right
                if (position.x + x + pmSize.width >= screenBounds.width + screenBounds.x &&
                // popup doesn't fit - place it wherever there's more room
                        screenBounds.width - s.width < 2 * (position.x - screenBounds.x)) {

                    x = s.width - xOffset - pmSize.width;
                }
            } else {
                // First determine the x:
                x = s.width - xOffset - pmSize.width; // Extend to the left
                if (position.x + x < screenBounds.x &&
                // popup doesn't fit - place it wherever there's more room
                        screenBounds.width - s.width > 2 * (position.x - screenBounds.x)) {

                    x = xOffset;
                }
            }
            // Then the y:
            y = s.height + yOffset; // Prefer dropping down
            if (position.y + y + pmSize.height >= screenBounds.height &&
            // popup doesn't fit - place it wherever there's more room
                    screenBounds.height - s.height < 2 * (position.y - screenBounds.y)) {

                y = 0 - yOffset - pmSize.height; // Otherwise drop 'up'
            }
        }

        return new Point(x, y);
    }

    /*
     * Convenience function for determining ComponentOrientation.  Helps us
     * avoid having Munge directives throughout the code.
     */
    private static boolean isLeftToRight(Component c) {
        return c.getComponentOrientation().isLeftToRight();
    }

}
