/*
 * This file is part of databrowser-gui.
 * 
 * databrowser-gui is free software: you can redistribute it and/or modify it under the terms of the GNU General Public
 * License as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later
 * version.
 * 
 * databrowser-gui is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the
 * implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for more
 * details.
 * 
 * You should have received a copy of the GNU General Public License along with databrowser-gui. If not, see
 * <https://www.gnu.org/licenses/>.
 */
package org.cdma.gui.databrowser.plugintree;

import org.cdma.gui.databrowser.interfaces.IDataSourceBrowser;
import org.jdesktop.swingx.treetable.AbstractMutableTreeTableNode;

public abstract class PluginTreeTableNode extends AbstractMutableTreeTableNode {

    public PluginTreeTableNode(final Object userObject) {
        super(userObject);
    }

    @Override
    public int getColumnCount() {
        return PluginTreeTableModel.PluginTreeColumns.values().length;
    }

    @Override
    public Object getValueAt(final int columnIndex) {
        Object value = null;

        IDataSourceBrowser browser = (IDataSourceBrowser) getUserObject();
        PluginTreeTableModel.PluginTreeColumns column = PluginTreeTableModel.PluginTreeColumns.values()[columnIndex];

        switch (column) {
            case FIRST:
                // column 0 is handled by TreeCellRenderer
                break;
            case VERSION:
                value = browser.getVersion();
                break;
            case LABEL:
                value = browser.getLabel();
                break;
            case DESCRIPTION:
                value = browser.getDescription();
                break;
        }

        return value;
    }

}
