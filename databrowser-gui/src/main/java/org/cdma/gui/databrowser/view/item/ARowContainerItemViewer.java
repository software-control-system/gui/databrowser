/*
 * This file is part of databrowser-gui.
 * 
 * databrowser-gui is free software: you can redistribute it and/or modify it under the terms of the GNU General Public
 * License as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later
 * version.
 * 
 * databrowser-gui is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the
 * implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for more
 * details.
 * 
 * You should have received a copy of the GNU General Public License along with databrowser-gui. If not, see
 * <https://www.gnu.org/licenses/>.
 */
package org.cdma.gui.databrowser.view.item;

import java.awt.BorderLayout;
import java.awt.Component;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.Insets;
import java.util.ArrayList;
import java.util.Collection;

import javax.swing.JComponent;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JSplitPane;

import org.cdma.gui.databrowser.DataBrowserController;
import org.cdma.gui.databrowser.interfaces.DataFormat;
import org.cdma.gui.databrowser.interfaces.IDataSourceBrowser;
import org.cdma.gui.databrowser.interfaces.IItemViewer;
import org.cdma.gui.databrowser.interfaces.Item;
import org.cdma.gui.databrowser.model.ARowContainerItem;

import fr.soleil.comete.box.scalarbox.StringScalarBox;
import fr.soleil.comete.swing.util.EDTManager;

public abstract class ARowContainerItemViewer<V extends ARowContainerItemViewer<?>>
        extends AbstractItemViewer<ARowContainerItem<?, ?, V>> implements IItemViewer {

    private static final long serialVersionUID = 3820562033182486006L;

    protected static Insets LABEL_INSETS = new Insets(2, 0, 2, 5);
    protected static Insets VIEWER_INSETS = new Insets(2, 0, 2, 0);
    protected static Insets SETTER_INSETS = new Insets(2, 20, 2, 0);

    protected JSplitPane splitPane;
    protected JScrollPane itemScrollPane;
    protected JPanel itemPanel;
    protected JScrollPane playerScrollPane;

    // Needed box
    protected StringScalarBox stringBox;

    public ARowContainerItemViewer(DataBrowserController controller) {
        super(controller, true);
        layoutComponents();
    }

    @Override
    protected void initComponents() {
        stringBox = new StringScalarBox();
        splitPane = new JSplitPane(JSplitPane.VERTICAL_SPLIT);
        splitPane.setOneTouchExpandable(true);
        itemPanel = new JPanel(new GridBagLayout());
        itemScrollPane = new JScrollPane(itemPanel);
        playerScrollPane = new JScrollPane();
    }

    protected int getNextY() {
        int y;
        Component[] comps = itemPanel.getComponents();
        if ((comps == null) || (comps.length == 0)) {
            y = 0;
        } else {
            y = ((GridBagLayout) itemPanel.getLayout()).getConstraints(comps[comps.length - 1]).gridy + 1;
        }
        return y;
    }

    protected GridBagConstraints createLabelConstraints(int y) {
        GridBagConstraints constraints = new GridBagConstraints();
        constraints.fill = GridBagConstraints.HORIZONTAL;
        constraints.gridx = 0;
        constraints.gridy = y;
        constraints.weightx = 0;
        constraints.weighty = 0;
        constraints.insets = LABEL_INSETS;
        return constraints;
    }

    protected GridBagConstraints createViewerConstraints(int y) {
        GridBagConstraints constraints = new GridBagConstraints();
        constraints.fill = GridBagConstraints.HORIZONTAL;
        constraints.gridx = 1;
        constraints.gridy = y;
        constraints.weightx = 1;
        constraints.weighty = 0;
        constraints.insets = VIEWER_INSETS;
        constraints.anchor = GridBagConstraints.WEST;
        return constraints;
    }

    protected GridBagConstraints createSetterConstraints(int y) {
        GridBagConstraints constraints = new GridBagConstraints();
        constraints.fill = GridBagConstraints.HORIZONTAL;
        constraints.gridx = 2;
        constraints.gridy = y;
        constraints.weightx = 0;
        constraints.weighty = 0;
        constraints.insets = SETTER_INSETS;
        constraints.anchor = GridBagConstraints.WEST;
        return constraints;
    }

    protected void layoutComponents() {
        splitPane.setRightComponent(itemScrollPane);
        setLayout(new BorderLayout());
        add(splitPane, BorderLayout.CENTER);
    }

    public StringScalarBox getStringBox() {
        return stringBox;
    }

    protected abstract ARowContainerItem<?, ?, V> getRowContainerItem(final Item item, final DataFormat format);

    @Override
    public Collection<Runnable> addItem(final Item item) {
        Collection<Runnable> toRunInEDT = new ArrayList<>();
        ARowContainerItem<?, ?, V> rowContainerItem = displayedItems.get(item.getKey());
        if (rowContainerItem == null) {
            IDataSourceBrowser browser = item.getBrowser();
            if (browser != null) {
                DataFormat format = item.getFormat();
                rowContainerItem = getRowContainerItem(item, format);
                if (rowContainerItem != null) {
                    rowContainerItem.connect();
                    displayedItems.put(item.getKey(), rowContainerItem);
                    RowContainer<?, ?> container = rowContainerItem.getRowContainer();
                    toRunInEDT.add(() -> {
                        int y = getNextY();
                        JLabel label = container.getLabel();
                        if (label != null) {
                            itemPanel.add(label, createLabelConstraints(y));
                        }
                        JComponent viewer = container.getViewer();
                        if (viewer != null) {
                            itemPanel.add(viewer, createViewerConstraints(y));
                        }
                        JComponent setter = container.getSetter();
                        if (setter != null) {
                            itemPanel.add(setter, createSetterConstraints(y));
                        }
                        revalidate();
                        repaint();
                    });
                }
            }
        }
        return toRunInEDT;
    }

    @Override
    public Collection<Runnable> removeItem(final Item item) {
        Collection<Runnable> toRunInEDT = new ArrayList<>();
        ARowContainerItem<?, ?, V> rowContainerItem = displayedItems.remove(item.getKey());
        if (rowContainerItem != null) {
            EDTManager.INSTANCE.runInDrawingThread(() -> {
                // remove the components of the row
                RowContainer<?, ?> container = rowContainerItem.getRowContainer();
                JComponent[] comps = container.getComponents();
                for (JComponent comp : comps) {
                    itemPanel.remove(comp);
                }
                revalidate();
                repaint();
            });
            // disconnect item (viewer and setter)
            rowContainerItem.disconnect();
        }
        return toRunInEDT;
    }

    @Override
    public Collection<Runnable> selectItem(final Item item) {
        Collection<Runnable> toRunInEDT = new ArrayList<>();
        // select new key
        final ARowContainerItem<?, ?, V> rowContainerItem = displayedItems.get(item.getKey());
        if (rowContainerItem != null) {
            selectedKey = item.getKey();
            rowContainerItem.setSelected(true);

            PlayerPanel playerPanel = rowContainerItem.getPlayerPanel();
            if (playerPanel != null) {
                playerScrollPane.setViewportView(playerPanel);
                splitPane.setLeftComponent(playerScrollPane);
                // splitPane.resetToPreferredSizes();
                splitPane.setResizeWeight(0.3);
            }

            // scroll panel to let the selected row appear on screen
            toRunInEDT.add(() -> {
                RowContainer<?, ?> container = rowContainerItem.getRowContainer();
                itemPanel.scrollRectToVisible(container.getViewer().getBounds());
            });
        }
        return toRunInEDT;
    }

    @Override
    public Collection<Runnable> deselectItem(final Item item) {
        Collection<Runnable> toRunInEDT = new ArrayList<>();
        if (item != null) {
            if (item.getKey() == selectedKey) {
                selectedKey = null;
                ARowContainerItem<?, ?, V> rowContainerItem = displayedItems.get(item.getKey());
                if (rowContainerItem != null) {
                    rowContainerItem.setSelected(false);
                }
                toRunInEDT.add(() -> {
                    splitPane.setLeftComponent(null);
                    splitPane.setResizeWeight(1.0);
                });
            }
        }
        return toRunInEDT;
    }

    @Override
    public Collection<Runnable> clearSelection() {
        Collection<Runnable> toRunInEDT = null;
        if (selectedKey != null) {
            ARowContainerItem<?, ?, V> selectedItem = displayedItems.get(selectedKey);
            if (selectedItem != null) {
                toRunInEDT = deselectItem(selectedItem);
            }
        }
        if (toRunInEDT == null) {
            toRunInEDT = new ArrayList<>();
        }
        return toRunInEDT;
    }

}
