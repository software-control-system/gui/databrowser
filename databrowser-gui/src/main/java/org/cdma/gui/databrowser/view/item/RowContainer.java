/*
 * This file is part of databrowser-gui.
 * 
 * databrowser-gui is free software: you can redistribute it and/or modify it under the terms of the GNU General Public
 * License as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later
 * version.
 * 
 * databrowser-gui is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the
 * implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for more
 * details.
 * 
 * You should have received a copy of the GNU General Public License along with databrowser-gui. If not, see
 * <https://www.gnu.org/licenses/>.
 */
package org.cdma.gui.databrowser.view.item;

import java.awt.Component;
import java.awt.Font;
import java.util.Arrays;

import javax.swing.BorderFactory;
import javax.swing.JComponent;
import javax.swing.JLabel;

import fr.soleil.comete.swing.TextField;
import fr.soleil.data.target.scalar.IScalarTarget;
import fr.soleil.lib.project.swing.ui.ReverseLabelUI;

public class RowContainer<C extends JComponent & IScalarTarget, S extends JComponent & IScalarTarget> {
    // private static final Color BACKGROUND_SELECTION_COLOR = Color.CYAN;
    private static final int LABEL_WIDTH = 150;
    // private static final int AREA_WIDTH = 500;
    private static final int FIELD_WIDTH = 200;
    private static final int LABEL_HEIGHT = 25;
    // TODO avisto: we could use a matte border to highlight the selected row

    private final JLabel label;
    private final C viewer;
    private final S setter;
    private final ReverseLabelUI reversUI = new ReverseLabelUI();

    private boolean setterVisible;
    private boolean selected;

    public RowContainer(final JLabel labelComp, final C viewerComp, final S setterComp) {
        label = labelComp;
        viewer = viewerComp;
        setter = setterComp;
        setterVisible = true;
        selected = false;
        setupComponents();
    }

    protected void setupTextField(TextField field) {
        field.setSize(FIELD_WIDTH, LABEL_HEIGHT);
        field.setPreferredSize(field.getSize());
        field.setMinimumSize(field.getSize());
    }

    private void setupLabel() {
        if (label != null) {
            label.setBorder(BorderFactory.createEmptyBorder(5, 5, 5, 5));
            // Force a fixed size and set revert ui
            // Take a bit margin for "italic" case
            int width = Math.min(LABEL_WIDTH, label.getPreferredSize().width + 10);
            label.setSize(width, LABEL_HEIGHT);
            label.setPreferredSize(label.getSize());
            label.setMinimumSize(label.getSize());
            label.setUI(reversUI);
        }
    }

    private void setupViewer() {
        if (viewer instanceof TextField) {
            setupTextField((TextField) viewer);
        }
    }

    private void setupSetter() {
        if (setter instanceof TextField) {
            setupTextField((TextField) setter);
        }
    }

    private void setupComponents() {
        if (label != null) {
            setupLabel();
        }
        if (viewer != null) {
            setupViewer();
        }
        if (setter != null) {
            setupSetter();
        }
    }

    private void setSelected(boolean selected, JComponent comp) {
        if (comp != null) {
            Font font = comp.getFont();
            int newStyle;
            if (font.isBold()) {
                newStyle = selected ? Font.BOLD | Font.ITALIC : Font.BOLD;
            } else {
                newStyle = selected ? Font.ITALIC : Font.PLAIN;

            }
            comp.setFont(font.deriveFont(newStyle));
            comp.repaint();
        }
    }

    public boolean isSelected() {
        return selected;
    }

    public void setSelected(boolean selected) {
        this.selected = selected;
        setSelected(selected, label);
        setSelected(selected, viewer);
        setSelected(selected, setter);
    }

    public JLabel getLabel() {
        return label;
    }

    public C getViewer() {
        return viewer;
    }

    public S getSetter() {
        return setter;
    }

    public boolean isSetterVisible() {
        return setterVisible;
    }

    public void setSetterVisible(final boolean setterVisible) {
        if ((setter != null) && (setterVisible != this.setterVisible)) {
            this.setterVisible = setterVisible;
            ((JComponent) setter).setBorder(setterVisible ? BorderFactory.createEmptyBorder(5, 10, 5, 5) : null);
            ((Component) setter).setVisible(setterVisible);
        }
    }

    public JComponent[] getComponents() {
        JComponent[] components = new JComponent[3];
        int index = 0;
        if (label != null) {
            components[index++] = label;
        }
        if (viewer != null) {
            components[index++] = viewer;
        }
        if (setter != null) {
            components[index++] = setter;
        }
        if (index != components.length) {
            components = Arrays.copyOf(components, index);
        }
        return components;
    }
}
