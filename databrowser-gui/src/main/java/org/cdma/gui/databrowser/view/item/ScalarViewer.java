/*
 * This file is part of databrowser-gui.
 * 
 * databrowser-gui is free software: you can redistribute it and/or modify it under the terms of the GNU General Public
 * License as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later
 * version.
 * 
 * databrowser-gui is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the
 * implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for more
 * details.
 * 
 * You should have received a copy of the GNU General Public License along with databrowser-gui. If not, see
 * <https://www.gnu.org/licenses/>.
 */
package org.cdma.gui.databrowser.view.item;

import org.cdma.gui.databrowser.DataBrowserController;
import org.cdma.gui.databrowser.interfaces.DataFormat;
import org.cdma.gui.databrowser.interfaces.IItemViewer;
import org.cdma.gui.databrowser.interfaces.Item;
import org.cdma.gui.databrowser.model.BooleanScalarItem;
import org.cdma.gui.databrowser.model.NumberScalarItem;
import org.cdma.gui.databrowser.model.ScalarItem;
import org.cdma.gui.databrowser.model.TextScalarItem;

import fr.soleil.comete.box.scalarbox.BooleanScalarBox;
import fr.soleil.comete.box.scalarbox.NumberScalarBox;

public class ScalarViewer extends ARowContainerItemViewer<ScalarViewer> implements IItemViewer {

    private static final long serialVersionUID = -7380390755727391641L;

    // Needed box
    private BooleanScalarBox booleanBox;
    private NumberScalarBox numberBox;

    public ScalarViewer(DataBrowserController controller) {
        super(controller);
    }

    @Override
    protected void initComponents() {
        super.initComponents();
        booleanBox = new BooleanScalarBox();
        numberBox = new NumberScalarBox();
    }

    public NumberScalarBox getNumberBox() {
        return numberBox;
    }

    public BooleanScalarBox getBooleanBox() {
        return booleanBox;
    }

    @Override
    protected ScalarItem<?, ?> getRowContainerItem(final Item item, final DataFormat format) {
        ScalarItem<?, ?> scalarItem = null;
        switch (format) {
            case TEXT:
                scalarItem = new TextScalarItem(this, item, controller);
                break;
            case NUMERICAL:
                scalarItem = new NumberScalarItem(this, item, controller);
                break;
            case BOOLEAN:
                scalarItem = new BooleanScalarItem(this, item, controller);
                break;
            default:
                // nop
                break;
        }
        return scalarItem;
    }

}
