/*
 * This file is part of databrowser-gui.
 * 
 * databrowser-gui is free software: you can redistribute it and/or modify it under the terms of the GNU General Public
 * License as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later
 * version.
 * 
 * databrowser-gui is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the
 * implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for more
 * details.
 * 
 * You should have received a copy of the GNU General Public License along with databrowser-gui. If not, see
 * <https://www.gnu.org/licenses/>.
 */
package org.cdma.gui.databrowser.view.tree;

import java.awt.BorderLayout;
import java.awt.Component;
import java.awt.Cursor;
import java.awt.Frame;
import java.awt.event.ActionEvent;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Set;
import java.util.Vector;

import javax.swing.Action;
import javax.swing.Box;
import javax.swing.JButton;
import javax.swing.JDialog;
import javax.swing.JLabel;
import javax.swing.JMenu;
import javax.swing.JMenuItem;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JPopupMenu;
import javax.swing.JScrollPane;
import javax.swing.JTextField;
import javax.swing.SwingUtilities;
import javax.swing.event.DocumentEvent;
import javax.swing.event.DocumentListener;
import javax.swing.event.TreeExpansionEvent;
import javax.swing.event.TreeWillExpandListener;
import javax.swing.table.DefaultTableModel;
import javax.swing.table.TableColumn;
import javax.swing.table.TableModel;
import javax.swing.tree.DefaultMutableTreeNode;
import javax.swing.tree.ExpandVetoException;
import javax.swing.tree.TreePath;

import org.cdma.gui.databrowser.DataBrowser;
import org.cdma.gui.databrowser.DataBrowserController;
import org.cdma.gui.databrowser.DataBrowserItemEvent;
import org.cdma.gui.databrowser.DataSourceManager;
import org.cdma.gui.databrowser.KeyParameter;
import org.cdma.gui.databrowser.LoadParameter;
import org.cdma.gui.databrowser.interfaces.AxisType;
import org.cdma.gui.databrowser.interfaces.DataFormat;
import org.cdma.gui.databrowser.interfaces.DataType;
import org.cdma.gui.databrowser.interfaces.IDataBrowserItemListener;
import org.cdma.gui.databrowser.interfaces.IDataSourceBrowser;
import org.cdma.gui.databrowser.interfaces.IDataSourceSeeker;
import org.cdma.gui.databrowser.interfaces.Item;
import org.cdma.gui.databrowser.util.DropFileTransferHandler;
import org.cdma.gui.databrowser.view.item.ItemTableCellRenderer;
import org.jdesktop.swingx.JXTable;
import org.jdesktop.swingx.action.AbstractActionExt;

import fr.soleil.comete.definition.event.TreeNodeSelectionEvent;
import fr.soleil.comete.definition.listener.ITreeNodeSelectionListener;
import fr.soleil.comete.definition.widget.util.ITreeNode;
import fr.soleil.comete.swing.util.CometeTreeModel;
import fr.soleil.comete.swing.util.CometeUtils;
import fr.soleil.comete.swing.util.EDTManager;
import fr.soleil.data.service.HistoryKey;
import fr.soleil.data.service.IKey;
import fr.soleil.lib.project.swing.WindowSwingUtils;

public class TreeViewer extends JPanel implements MouseListener, ITreeNodeSelectionListener, DocumentListener,
        TreeWillExpandListener, IDataBrowserItemListener {

    private static final long serialVersionUID = -393333457681231010L;

    private final DataBrowserController controller;
    private Map<DataType, List<IKey>> selectedMap = new HashMap<>();

    private Action openSourceAction;
    private Action closeSourceAction;
    private Action reloadSourceAction;
    private Action removeItemsAction;
    private Action expandAction;
    private Action collapseAction;
    private Action findNextAction;
    private Action findPreviousAction;
    private Action showOnY1;
    private Action showOnY2;
    private Action showOnX;
    private Action showAsImage;
    private Action showOnY1ROW;
    private Action showOnY1COLUMN;
    private Action showOnY2ROW;
    private Action showOnY2COLUMN;
    private Action setXForOpenedSpectrumItemAction;
    private Action openDialogXMatrixItemAction;
    private Action openDialogYMatrixItemAction;
    private Action openItem;
    private Action closeItem;
    private Action controlePanelAction;

    private JMenuItem controlPanelSourceMenu;
    private JMenuItem controlPanelDataItemMenu;

    private JButton openSourceButton;
    private JButton closeSourceButton;

    private JPopupMenu sourcePopupMenu;
    private JPopupMenu browserPopupMenu;
    private JPopupMenu itemPopupMenu;

    private DataBrowserTree tree;
    private JScrollPane treeScrollPane;

    // Tree management
    private JButton expandButton;
    private JButton collapseButton;

    // Tree search tool
    private JTextField searchField;
    private JButton findNextButton;
    private JButton findPreviousButton;

    // For dialog x attribute
    private JDialog displayOpenItemDialog;

    private boolean spectrumMenu;
    private boolean putKeyOnX;
    private boolean canListenToModel;
    private IKey lastSourceKey;

    public TreeViewer(final DataBrowserController controller) {
        super();
        this.controller = controller;
        displayOpenItemDialog = null;
        spectrumMenu = false;
        putKeyOnX = false;
        canListenToModel = true;

        initComponents();
        layoutComponents();
        if (controller != null) {
            controller.addDataBrowserItemListener(this);
        }
    }

    public DataBrowserTree getTree() {
        return tree;
    }

    private void initComponents() {

        openSourceAction = new AbstractActionExt(DataBrowser.MESSAGES.getString("Action.OpenSource"),
                DataBrowser.ICONS.getIcon("Action.OpenSource")) {
            private static final long serialVersionUID = -3701364906730320182L;

            @Override
            public void actionPerformed(final ActionEvent e) {
                openSource();
            }
        };
        ((AbstractActionExt) openSourceAction)
                .setShortDescription(DataBrowser.MESSAGES.getString("Action.OpenSource.ToolTip"));
        closeSourceAction = new AbstractActionExt(DataBrowser.MESSAGES.getString("Action.CloseSource"),
                DataBrowser.ICONS.getIcon("Action.CloseSource")) {

            private static final long serialVersionUID = 8151797741825555919L;

            @Override
            public void actionPerformed(final ActionEvent e) {
                closeSource();
            }
        };

        controlePanelAction = new AbstractActionExt(DataBrowser.MESSAGES.getString("Action.MonitoringDevice"),
                DataBrowser.ICONS.getIcon("SourceTree.MonitoringDevice")) {
            private static final long serialVersionUID = -3701364906730320182L;

            @Override
            public void actionPerformed(final ActionEvent e) {
                if ((DataBrowser.BATCH_EXECUTOR.getBatch() != null)
                        && !DataBrowser.BATCH_EXECUTOR.getBatch().isEmpty()) {
                    String deviceName = getSelectedDevice();
                    IDataSourceBrowser selectedBrowser = tree.getSelectedBrowser();
                    deviceName = selectedBrowser.getCompletePath(deviceName);
                    if ((deviceName != null) && !deviceName.isEmpty()) {
                        List<String> param = new ArrayList<>();
                        param.add(deviceName);
                        DataBrowser.BATCH_EXECUTOR.setBatchParameters(param);
                        DataBrowser.BATCH_EXECUTOR.execute();
                    }
                }
            }
        };

        reloadSourceAction = new AbstractActionExt(DataBrowser.MESSAGES.getString("Action.ReloadSource"),
                DataBrowser.ICONS.getIcon("Action.ReloadSource")) {

            private static final long serialVersionUID = -4555433727285615537L;

            @Override
            public void actionPerformed(final ActionEvent e) {
                reloadSource();
            }
        };
        removeItemsAction = new AbstractActionExt(DataBrowser.MESSAGES.getString("Action.RemoveItems"),
                DataBrowser.ICONS.getIcon("Action.RemoveItems")) {

            private static final long serialVersionUID = -4555433727285615537L;

            @Override
            public void actionPerformed(final ActionEvent e) {
                removeSourceItems();
            }
        };
        showOnX = new AbstractActionExt(DataBrowser.MESSAGES.getString("Action.SourceTree.ShowOnX"),
                DataBrowser.ICONS.getIcon("DataItem.Spectrum.Tab.Chart")) {

            private static final long serialVersionUID = 6169104284928960215L;

            @Override
            public void actionPerformed(ActionEvent e) {
                openSelectedItem(DataType.SPECTRUM, AxisType.X);
            }
        };

        showOnY1 = new AbstractActionExt(DataBrowser.MESSAGES.getString("Action.SourceTree.ShowOnY1"),
                DataBrowser.ICONS.getIcon("DataItem.Spectrum.Tab.Chart")) {

            /**
             * 
             */
            private static final long serialVersionUID = -233856472549456282L;

            @Override
            public void actionPerformed(ActionEvent e) {
                openSelectedItem(DataType.SPECTRUM, AxisType.Y1);
            }
        };
        showOnY2 = new AbstractActionExt(DataBrowser.MESSAGES.getString("Action.SourceTree.ShowOnY2"),
                DataBrowser.ICONS.getIcon("DataItem.Spectrum.Tab.Chart")) {

            private static final long serialVersionUID = -233856472549456282L;

            @Override
            public void actionPerformed(ActionEvent e) {
                openSelectedItem(DataType.SPECTRUM, AxisType.Y2);
            }
        };
        showAsImage = new AbstractActionExt(DataBrowser.MESSAGES.getString("Action.SourceTree.ShowAsImage"),
                DataBrowser.ICONS.getIcon("DataItem.Tab.Matrix")) {

            private static final long serialVersionUID = -36433978374468250L;

            @Override
            public void actionPerformed(ActionEvent e) {
                openSelectedItem(DataType.IMAGE, AxisType.IMAGE);
            }
        };
        showOnY1ROW = new AbstractActionExt(DataBrowser.MESSAGES.getString("Action.SourceTree.ShowOnY1ROW"),
                DataBrowser.ICONS.getIcon("DataItem.Spectrum.Tab.Chart")) {

            private static final long serialVersionUID = -5736532226387899705L;

            @Override
            public void actionPerformed(ActionEvent e) {
                openSelectedItem(DataType.SPECTRUM, AxisType.Y1_ROW);

            }
        };
        showOnY1COLUMN = new AbstractActionExt(DataBrowser.MESSAGES.getString("Action.SourceTree.ShowOnY1COLUMN"),
                DataBrowser.ICONS.getIcon("DataItem.Spectrum.Tab.Chart")) {

            private static final long serialVersionUID = -3271885383982578620L;

            @Override
            public void actionPerformed(ActionEvent e) {
                openSelectedItem(DataType.SPECTRUM, AxisType.Y1_COLUMN);

            }
        };
        showOnY2ROW = new AbstractActionExt(DataBrowser.MESSAGES.getString("Action.SourceTree.ShowOnY2ROW"),
                DataBrowser.ICONS.getIcon("DataItem.Spectrum.Tab.Chart")) {

            private static final long serialVersionUID = -5736532226387899705L;

            @Override
            public void actionPerformed(ActionEvent e) {
                openSelectedItem(DataType.SPECTRUM, AxisType.Y2_ROW);

            }
        };
        showOnY2COLUMN = new AbstractActionExt(DataBrowser.MESSAGES.getString("Action.SourceTree.ShowOnY2COLUMN"),
                DataBrowser.ICONS.getIcon("DataItem.Spectrum.Tab.Chart")) {

            private static final long serialVersionUID = -3271885383982578620L;

            @Override
            public void actionPerformed(ActionEvent e) {
                openSelectedItem(DataType.SPECTRUM, AxisType.Y2_COLUMN);

            }
        };
        openItem = new AbstractActionExt(DataBrowser.MESSAGES.getString("Action.SourceTree.OpenItem"),
                DataBrowser.ICONS.getIcon("View.DataItem")) {

            private static final long serialVersionUID = 6581422153408721280L;

            @Override
            public void actionPerformed(ActionEvent e) { // Menu scalar
                openSelectedItem();

            }
        };
        closeItem = new AbstractActionExt(DataBrowser.MESSAGES.getString("Action.SourceTree.CloseItem"),
                DataBrowser.ICONS.getIcon("Action.RemoveItems")) {

            private static final long serialVersionUID = 6581422153408721280L;

            @Override
            public void actionPerformed(ActionEvent e) {
                closeItem();
            }

            private void closeItem() {
                ITreeNode[] selectedNodes = tree.getSelectedNodes();
                IKey selectedSource = tree.getSelectedSource();
                for (ITreeNode node : selectedNodes) {
                    controller.closeItem(selectedSource, (IKey) node.getData(), false);
                }
                controller.callGC();
            }
        };
        expandAction = new AbstractActionExt(null, DataBrowser.ICONS.getIcon("SourceTree.ExpandAll")) {

            private static final long serialVersionUID = -4514371096669874299L;

            @Override
            public void actionPerformed(final ActionEvent e) {
                expandSelection(true);
            }
        };
        collapseAction = new AbstractActionExt(null, DataBrowser.ICONS.getIcon("SourceTree.CollapseAll")) {

            private static final long serialVersionUID = 3410687013763485057L;

            @Override
            public void actionPerformed(final ActionEvent e) {
                expandSelection(false);
            }
        };
        findNextAction = new AbstractActionExt(null, DataBrowser.ICONS.getIcon("SourceTree.Search.Next")) {

            private static final long serialVersionUID = -36433978374468250L;

            @Override
            public void actionPerformed(final ActionEvent e) {
                String textToFind = searchField.getText();
                tree.searchNextNode(textToFind);

                tree.scrollPathToVisible(tree.getSelectionPath());
            }
        };
        findPreviousAction = new AbstractActionExt(null, DataBrowser.ICONS.getIcon("SourceTree.Search.Previous")) {

            private static final long serialVersionUID = -3642373556268077192L;

            @Override
            public void actionPerformed(final ActionEvent e) {
                String textToFind = searchField.getText();
                tree.searchPreviousNode(textToFind);

                tree.scrollPathToVisible(tree.getSelectionPath());
            }
        };

        setXForOpenedSpectrumItemAction = new AbstractActionExt(
                DataBrowser.MESSAGES.getString("Action.SourceTree.xAttributeForOpenAttributes"),
                DataBrowser.ICONS.getIcon("DataItem.Spectrum.Tab.Chart")) {

            private static final long serialVersionUID = 6169104284928960215L;

            @Override
            public void actionPerformed(ActionEvent e) {
                openDialogForOpenedSpectrumItems();
            }
        };

        openDialogXMatrixItemAction = new AbstractActionExt(
                DataBrowser.MESSAGES.getString("Action.SourceTree.ShowImageWithXScale"),
                DataBrowser.ICONS.getIcon("DataItem.Tab.Matrix")) {

            private static final long serialVersionUID = 7652912288337465398L;

            @Override
            public void actionPerformed(ActionEvent e) {
                openDialogXForOpenedMatrixItems();
            }
        };

        openDialogYMatrixItemAction = new AbstractActionExt(
                DataBrowser.MESSAGES.getString("Action.SourceTree.ShowImageWithYScale"),
                DataBrowser.ICONS.getIcon("DataItem.Tab.Matrix")) {

            private static final long serialVersionUID = 7652912288337465398L;

            @Override
            public void actionPerformed(ActionEvent e) {
                openDialogYForOpenedMatrixItems();
            }
        };

        openSourceButton = new JButton(openSourceAction);
        closeSourceButton = new JButton(closeSourceAction);
        closeSourceButton.setToolTipText(DataBrowser.MESSAGES.getString("Action.CloseSource.ToolTip"));

        browserPopupMenu = new JPopupMenu();
        browserPopupMenu.add(openSourceAction);

        itemPopupMenu = new JPopupMenu();
        itemPopupMenu.add(openItem);
        itemPopupMenu.add(closeItem);
        itemPopupMenu.addSeparator();
        itemPopupMenu.add(showOnY1);
        itemPopupMenu.add(showOnY2);
        itemPopupMenu.add(showOnX);
        itemPopupMenu.add(setXForOpenedSpectrumItemAction);
        itemPopupMenu.addSeparator();
        itemPopupMenu.add(showAsImage);
        itemPopupMenu.add(openDialogXMatrixItemAction);
        itemPopupMenu.add(openDialogYMatrixItemAction);
        itemPopupMenu.add(showOnY1COLUMN);
        itemPopupMenu.add(showOnY1ROW);
        itemPopupMenu.add(showOnY2COLUMN);
        itemPopupMenu.add(showOnY2ROW);

        sourcePopupMenu = new JPopupMenu();
        sourcePopupMenu.add(removeItemsAction);
        sourcePopupMenu.add(reloadSourceAction);
        sourcePopupMenu.add(closeSourceAction);

        initControlPanelMenu();

        tree = new DataBrowserTree(this, controller);
        tree.addMouseListener(this);
        tree.addTreeNodeSelectionListener(this);

        tree.setDragEnabled(true);
        tree.addTreeWillExpandListener(this);

        treeScrollPane = new JScrollPane();
        treeScrollPane.setViewportView(tree);
        treeScrollPane.setAutoscrolls(true);
        treeScrollPane.setTransferHandler(new DropFileTransferHandler(controller, tree));

        expandButton = new JButton(expandAction);
        collapseButton = new JButton(collapseAction);

        searchField = new JTextField();
        searchField.getDocument().addDocumentListener(this);

        findNextButton = new JButton(findNextAction);
        findNextButton.setToolTipText(DataBrowser.MESSAGES.getString("Action.SourceTree.FindNext"));
        findPreviousButton = new JButton(findPreviousAction);
        findPreviousButton.setToolTipText(DataBrowser.MESSAGES.getString("Action.SourceTree.FindPrevious"));

        updateButtonsState();
        updateSearchButtonsState();
    }

    private void initControlPanelMenu() {
        controlPanelSourceMenu = initControlPanelMenu(sourcePopupMenu);
        controlPanelDataItemMenu = initControlPanelMenu(itemPopupMenu);
    }

    private JMenuItem initControlPanelMenu(JPopupMenu popupMenu) {
        popupMenu.add(controlePanelAction);
        Component[] components = popupMenu.getComponents();
        JMenuItem controlMenu = null;
        for (Component comp : components) {
            if (comp instanceof JMenuItem) {
                controlMenu = (JMenuItem) comp;
                if (!(comp instanceof JMenu)) {
                    if (controlMenu.getAction().equals(controlePanelAction)) {
                        controlMenu.setVisible(false);
                    }
                }
            }
        }

        return controlMenu;
    }

    private void layoutComponents() {
        Box buttonPanel = Box.createHorizontalBox();
        buttonPanel.add(openSourceButton);
        buttonPanel.add(Box.createHorizontalStrut(5));
        buttonPanel.add(closeSourceButton);
        buttonPanel.add(Box.createHorizontalStrut(5));
        buttonPanel.add(Box.createHorizontalGlue());
        buttonPanel.add(expandButton);
        buttonPanel.add(Box.createHorizontalStrut(5));
        buttonPanel.add(collapseButton);

        JLabel searchLabel = new JLabel(DataBrowser.ICONS.getIcon("SourceTree.Search"));
        searchLabel.setToolTipText(DataBrowser.MESSAGES.getString("Action.SourceTree.Search"));

        Box searchPanel = Box.createHorizontalBox();
        searchPanel.add(searchLabel);
        searchPanel.add(Box.createHorizontalStrut(5));
        searchPanel.add(searchField);
        searchPanel.add(Box.createHorizontalStrut(5));
        searchPanel.add(findPreviousButton);
        searchPanel.add(Box.createHorizontalStrut(5));
        searchPanel.add(findNextButton);

        setLayout(new BorderLayout(0, 5));
        add(buttonPanel, BorderLayout.NORTH);
        add(treeScrollPane, BorderLayout.CENTER);
        add(searchPanel, BorderLayout.SOUTH);
    }

    private void openDialogYForOpenedMatrixItems() {
        spectrumMenu = false;
        putKeyOnX = false;
        createSpectrumImageDialog();
        displayDialog();
    }

    private void openDialogXForOpenedMatrixItems() {
        spectrumMenu = false;
        putKeyOnX = true;
        createSpectrumImageDialog();
        displayDialog();
    }

    private void displayDialog() {
        if (displayOpenItemDialog != null) {
            displayOpenItemDialog.setLocationRelativeTo(this);
            displayOpenItemDialog.setVisible(true);
            displayOpenItemDialog.toFront();
        }
    }

    private void openDialogForOpenedSpectrumItems() {
        spectrumMenu = true;
        putKeyOnX = true;

        Vector<Vector<String>> dataVector = new Vector<>();
        dataVector.clear();

        // get opened attributes for table

        List<Item> openedNumericalSpectrumItems = controller.getOpenedNumericalSpectrumItems();
        IKey selectedKey = getSelectedKey().get(0);
        IKey key = null;
        IDataSourceBrowser browser = null;

        for (Item item : openedNumericalSpectrumItems) {
            key = item.getKey();
            if (key != selectedKey) {
                browser = item.getBrowser();
                String itemName = browser.getDisplayName(key);
                Vector<String> data = new Vector<>();
                data.addAll(Arrays.asList(itemName));
                dataVector.add(data);
            }
        }
        createSpectrumImageDialog();
        displayDialog();
    }

    private void createSpectrumImageDialog() {
        lastSourceKey = tree.getSelectedSource();
        if (displayOpenItemDialog == null) {
            displayOpenItemDialog = new JDialog(WindowSwingUtils.getWindowForComponent(this));
            displayOpenItemDialog.addWindowListener(new WindowAdapter() {
                @Override
                public void windowClosing(WindowEvent e) {
                    lastSourceKey = null;
                }
            });
            JPanel panel = new JPanel();
            panel.setLayout(new BorderLayout());
            final JXTable table = new JXTable();

            table.setSortable(true);
            table.setShowGrid(false);
            table.setShowHorizontalLines(true);
            table.setShowVerticalLines(true);
            table.setFillsViewportHeight(true);

            JScrollPane scrollPane = new JScrollPane(table);
            panel.add(scrollPane, BorderLayout.CENTER);

            JButton okButton = new JButton(
                    DataBrowser.MESSAGES.getString("DialogxAttributeForOpenAttributes.SourceTree.ButtonOk"));
            JButton cancelButton = new JButton(
                    DataBrowser.MESSAGES.getString("DialogxAttributeForOpenAttributes.SourceTree.ButtonCancel"));
            JPanel panelButton = new JPanel();
            panelButton.add(okButton);
            panelButton.add(cancelButton);

            panel.add(panelButton, BorderLayout.SOUTH);

            // Create table model
            TableModel model = new DefaultTableModel() {
                private static final long serialVersionUID = 5638421691050219305L;

                @Override
                public String getColumnName(int column) {
                    return DataBrowser.MESSAGES.getString("DialogxAttributeForOpenAttributes.SourceTree.HeaderTable");
                }

                @Override
                public int getColumnCount() {
                    return 1;
                }

                @Override
                public int getRowCount() {
                    int row = 0;
                    if (spectrumMenu) {
                        row = controller.getOpenedNumericalSpectrumItems().size();
                    } else {
                        row = controller.getOpenedNumericalMatrixItems().size();
                    }
                    return row;
                }

                @Override
                public Object getValueAt(int row, int column) {
                    Item item = null;
                    if (spectrumMenu) {
                        if (controller.getOpenedNumericalSpectrumItems() != null
                                && row < controller.getOpenedNumericalSpectrumItems().size()) {
                            item = controller.getOpenedNumericalSpectrumItems().get(row);
                        }
                    } else {
                        if (controller.getOpenedNumericalMatrixItems() != null
                                && row < controller.getOpenedNumericalMatrixItems().size()) {
                            item = controller.getOpenedNumericalMatrixItems().get(row);
                        }
                    }
                    return item;
                }
            };

            table.setModel(model);

            // Create renderer for Item
            ItemTableCellRenderer renderer = new ItemTableCellRenderer();
            TableColumn columnDate = table.getColumnModel().getColumn(0);
            columnDate.setCellRenderer(renderer);

            displayOpenItemDialog.setContentPane(panel);
            displayOpenItemDialog.setSize(600, 600);
            displayOpenItemDialog
                    .setTitle(DataBrowser.MESSAGES.getString("DialogxAttributeForOpenAttributes.SourceTree.Title"));
            displayOpenItemDialog.setAlwaysOnTop(true);
            displayOpenItemDialog.setModal(true);

            // Ok action

            okButton.addActionListener((e) -> {
                IKey iKey = getSelectedKey().get(0);
                IDataSourceBrowser selectedBrowser = tree.getSelectedBrowser();
                DataType dataType = selectedBrowser.getType(iKey);
                DataFormat format = selectedBrowser.getFormat(iKey);

                int[] selectedRows = table.getSelectedRows();
                Item item = null;
                for (int index : selectedRows) {
                    item = (Item) table.getValueAt(index, 0);
                    if (putKeyOnX) {
                        item.setXKey(iKey, lastSourceKey, selectedBrowser, dataType, format);
                    } else {
                        item.setYKey(iKey, lastSourceKey, selectedBrowser, dataType, format);
                    }
                    controller.setItemScale(item);
                }
                lastSourceKey = null;
                displayOpenItemDialog.dispose();
            });

            cancelButton.addActionListener((e) -> {
                lastSourceKey = null;
                displayOpenItemDialog.dispose();
            });
        }
    }

    public void openSelectedItem() {
        openSelectedItem(null, null);
    }

    public void openSelectedItem(DataType forcedDataType, AxisType axisType) {
        if (tree != null) {
            IDataSourceBrowser selectedBrowser = tree.getSelectedBrowser();
            IKey sourceKey = tree.getSelectedSource();
            if ((selectedBrowser != null) && (sourceKey != null)) {
                buildSelectedKey();
                Set<Entry<DataType, List<IKey>>> entrySet = selectedMap.entrySet();
                DataType dataType = null;
                List<IKey> keyList = null;
                for (Entry<DataType, List<IKey>> entry : entrySet) {
                    dataType = entry.getKey();
                    keyList = entry.getValue();
                    for (IKey key : keyList) {
                        if ((dataType == DataType.SCALAR)
                                || ((key instanceof HistoryKey) && (forcedDataType != DataType.SPECTRUM))) {
                            openItem(selectedBrowser, sourceKey, key, null, selectedBrowser.getAxis(key));
                        } else {
                            openItem(selectedBrowser, sourceKey, key, forcedDataType, axisType);
                        }
                    }
                }
            }
        }
    }

    private void updateButtonsState() {
        updateOpenButtonState();
        updateCloseButtonState();

        ITreeNode[] selectedNodes = tree.getSelectedNodes();
        if (selectedNodes != null) {
            expandButton.setToolTipText(DataBrowser.MESSAGES.getString("Action.SourceTree.ExpandSelected"));
            collapseButton.setToolTipText(DataBrowser.MESSAGES.getString("Action.SourceTree.CollapseSelected"));
        } else {
            expandButton.setToolTipText(DataBrowser.MESSAGES.getString("Action.SourceTree.ExpandAll"));
            collapseButton.setToolTipText(DataBrowser.MESSAGES.getString("Action.SourceTree.CollapseAll"));
        }
    }

    private void updateSelection() {
        canListenToModel = false;
        ITreeNode[] selectedNodes = tree.getSelectedNodes();
        controller.deselectAllItems();
        if ((selectedNodes != null) && (selectedNodes.length > 0)) {
            Collection<IKey> keys = new ArrayList<>(selectedNodes.length);
            for (ITreeNode node : selectedNodes) {
                if (node != null && node.getData() instanceof IKey) {
                    IKey sourceKey = (IKey) node.getData();
                    if (controller.isItemOpened(sourceKey)) {
                        keys.add(sourceKey);
                    }
                }
            }
            if (!keys.isEmpty()) {
                controller.selectItems(keys.toArray(new IKey[keys.size()]));
                keys.clear();
            }
        } // end if ((selectedNodes != null) && (selectedNodes.length > 0))
        canListenToModel = true;
    }

    private void updateOpenButtonState() {
        boolean openEnabled = DataSourceManager.isSingleBrowser() || (tree.getSelectedBrowser() != null);
        openSourceButton.setEnabled(openEnabled);
    }

    private void updateCloseButtonState() {
        boolean closeEnabled = false;
        ITreeNode[] selectedNodes = tree.getSelectedNodes();
        if (selectedNodes != null) {
            for (ITreeNode selectedNode : selectedNodes) {
                closeEnabled = (selectedNode instanceof SourceTreeNode);
            }
        }
        closeSourceButton.setEnabled(closeEnabled);
    }

    /**
     * This is called when user wants to open a source. A node has to be
     * selected in the tree, that is one of the browsers or any nodes under the
     * desired browser.
     */
    public void openSource() {
        IDataSourceBrowser selectedBrowser = null;

        if (DataSourceManager.isSingleBrowser()) {
            selectedBrowser = DataSourceManager.getBrowsers().get(0);
        } else {
            selectedBrowser = tree.getSelectedBrowser();
        }

        if (selectedBrowser != null) {
            IDataSourceSeeker seeker = selectedBrowser.getSeeker();
            if (seeker != null) {
                SeekerDialog dialog = new SeekerDialog(controller.getDataBrowserPanel(), seeker);
                dialog.setTitle(selectedBrowser.getName() + " seeker");
                dialog.setVisible(true);
                final List<IKey> keyList = dialog.getResult();
                if (keyList != null) {
                    // convert to LoadParam list
                    List<LoadParameter> paramList = new ArrayList<>(keyList.size());
                    for (IKey key : keyList) {
                        paramList.add(new KeyParameter(selectedBrowser, key));
                    }
                    if (!paramList.isEmpty()) {
                        controller.loadSources(paramList);
                    }
                }
            }
        } else {
            JOptionPane.showMessageDialog(controller.getDataBrowserPanel(),
                    DataBrowser.MESSAGES.getString("Error.NoPluginSelected"));
        }
    }

    private void closeSource() {
        ITreeNode[] selectedNodes = tree.getSelectedNodes();
        if ((selectedNodes != null) && (selectedNodes.length > 0)) {
            for (ITreeNode node : selectedNodes) {
                IKey sourceKey = (IKey) node.getData();
                controller.closeSource(tree.getSelectedBrowser(), sourceKey);
            }
        }
    }

    public void reloadSource() {
        if (tree.getSelectionCount() > 0) {
            ITreeNode node = tree.getSelectedNodes()[0];
            if ((node != null) && (node instanceof SourceTreeNode)) {
                IKey sourceKey = (IKey) node.getData();

                IDataSourceBrowser selectedBrowser = null;
                if (DataSourceManager.isSingleBrowser()) {
                    selectedBrowser = DataSourceManager.getBrowsers().get(0);
                } else {
                    selectedBrowser = tree.getSelectedBrowser();
                }

                controller.closeSource(selectedBrowser, sourceKey);

                // convert to LoadParam list
                List<LoadParameter> paramList = new ArrayList<>(1);
                paramList.add(new KeyParameter(selectedBrowser, sourceKey));
                controller.loadSources(paramList);
            }
        }
    }

    public void removeSourceItems() {
        if (tree.getSelectionCount() > 0) {
            ITreeNode node = tree.getSelectedNodes()[0];
            if ((node != null) && (node instanceof SourceTreeNode)) {
                IKey sourceKey = (IKey) node.getData();
                controller.removeSourceItems(sourceKey);
            }
        }
    }

    protected void expandSelection(final boolean expand) {
        if (tree.getSelectionCount() > 0) {
            tree.expandSelectedNode(expand);
        } else {
            tree.expandAll(expand);
        }
    }

    public void openItem(final IDataSourceBrowser browser, final IKey sourceKey, final IKey itemKey,
            DataType forcedType, AxisType forcedAxis) {
        if ((forcedType == null) && (forcedAxis == null)) {
            controller.openItem(browser, sourceKey, itemKey);
        } else {
            controller.openItem(browser, sourceKey, itemKey, forcedType, forcedAxis);
        }
    }

    public void closeItem(final IKey sourceKey, final IKey itemKey) {
        controller.closeItem(sourceKey, itemKey);
    }

    public void sourceOpened(final IDataSourceBrowser browser, final IKey sourceKey, final ITreeNode treeNode) {
        tree.addSource(browser, sourceKey, treeNode);
    }

    public void sourceClosed(final IDataSourceBrowser browser, final IKey sourceKey) {
        tree.removeSource(browser, sourceKey);
    }

    // -------------------------------
    // MouseListener
    // -------------------------------

    @Override
    public void mousePressed(final MouseEvent e) {
        // select on right click too
        if (SwingUtilities.isRightMouseButton(e)) {
            int pointedRow = tree.getRowForLocation(e.getX(), e.getY());
            // if(selectedNodes != );
            // if user clicked on a node
            if (pointedRow != -1) {
                if (!tree.isRowSelected(pointedRow)) {
                    tree.setSelectionRow(pointedRow);
                }
            }
        }

        onPopupTrigger(e);
    }

    @Override
    public void mouseReleased(final MouseEvent e) {
        buildSelectedKey();
        onPopupTrigger(e);
    }

    @Override
    public void mouseClicked(final MouseEvent e) {
        buildSelectedKey();
        onPopupTrigger(e);
        if ((e.getClickCount() == 2) && SwingUtilities.isLeftMouseButton(e)) {
            int pointedRow = tree.getRowForLocation(e.getX(), e.getY());
            // if user double-click on a node
            if (pointedRow != -1) {
                ITreeNode[] selectedNodes = tree.getSelectedNodes();
                if (selectedNodes != null) {// not empty
                    ITreeNode selectedNode = selectedNodes[0];
                    if (selectedNode instanceof BrowserTreeNode) {
                        openSource();
                    } else if (selectedNode instanceof ItemTreeNode) {
                        openSelectedItem();
                    }
                }
            }
        }
    }

    private void onPopupTrigger(final MouseEvent e) {
        if (e.isPopupTrigger()) {

            int pointedRow = tree.getRowForLocation(e.getX(), e.getY());
            // if user click on a node
            if (pointedRow != -1) {
                // decide whether or not to display popup menu
                // TODO enable actions in menu depending on the type of selected
                // node
                boolean deviceRunning = false;
                // sourcePopupMenu.remove(controlePanelAction);
                IDataSourceBrowser selectedBrowser = tree.getSelectedBrowser();
                ITreeNode[] selectedNodes = tree.getSelectedNodes();
                if (selectedBrowser.isSourceBrowserEnabled() && (selectedNodes != null) && (selectedNodes.length > 0)) {
                    ITreeNode iTreeNode = selectedNodes[0];
                    deviceRunning = selectedBrowser.isNodeReachable(iTreeNode);
                }
                if (isSourceSelected(selectedNodes)) {
                    setControlPanelVisible(controlPanelSourceMenu, deviceRunning);
                    sourcePopupMenu.show(e.getComponent(), e.getX(), e.getY());

                } else if (isItemSelected(selectedNodes)) {
                    disableAllPopupMenu();
                    setControlPanelVisible(controlPanelDataItemMenu, deviceRunning);
                    enablePopupMenu();
                    itemPopupMenu.show(e.getComponent(), e.getX(), e.getY());
                } else if (isBrowserSelected(selectedNodes)) {
                    browserPopupMenu.show(e.getComponent(), e.getX(), e.getY());
                }
            }
        }
    }

    private void disableAllPopupMenu() {
        showOnY1.setEnabled(false);
        showOnY2.setEnabled(false);
        showOnX.setEnabled(false);
        showOnX.setEnabled(false);
        showAsImage.setEnabled(false);
        openDialogXMatrixItemAction.setEnabled(false);
        openDialogYMatrixItemAction.setEnabled(false);
        showOnY1ROW.setEnabled(false);
        showOnY1COLUMN.setEnabled(false);
        showOnY2ROW.setEnabled(false);
        showOnY2COLUMN.setEnabled(false);
        openItem.setEnabled(false);
        setXForOpenedSpectrumItemAction.setEnabled(false);

        setControlPanelVisible(controlPanelSourceMenu, false);
        setControlPanelVisible(controlPanelDataItemMenu, false);
    }

    private void setControlPanelVisible(JMenuItem menu, boolean visible) {
        if (menu != null) {
            menu.setVisible(visible);
        }
    }

    private boolean isItemSelected(ITreeNode[] selectedNodes) {
        boolean selected = false;
        if ((selectedNodes != null) && (selectedNodes.length > 0)) {
            for (ITreeNode selectedNode : selectedNodes) {
                if (selectedNode instanceof ItemTreeNode) {
                    selected = true;
                    break;
                }
            }
        }

        return selected;
    }

    private boolean isSourceSelected(ITreeNode[] selectedNodes) {
        boolean selected = false;
        if ((selectedNodes != null) && (selectedNodes.length > 0)) {
            for (ITreeNode selectedNode : selectedNodes) {
                if (selectedNode instanceof SourceTreeNode) {
                    selected = true;
                    break;
                }
            }
        }

        return selected;
    }

    private boolean isBrowserSelected(ITreeNode[] selectedNodes) {
        boolean selected = false;
        if ((selectedNodes != null) && (selectedNodes.length > 0)) {
            for (ITreeNode selectedNode : selectedNodes) {
                if (selectedNode instanceof BrowserTreeNode) {
                    selected = true;
                    break;
                }
            }
        }

        return selected;
    }

    public boolean isDropEnable(DataType dataType) {
        boolean dropEnable = false;
        ItemTreeNode itemNode = null;
        DataFormat format = null;
        if (tree != null) {
            ITreeNode[] selectedNodes = tree.getSelectedNodes();
            if ((selectedNodes != null) && (selectedNodes.length > 0)) {
                DataType type = null;
                boolean historicKey = false;
                for (ITreeNode selectedNode : selectedNodes) {
                    if (selectedNode instanceof ItemTreeNode) {
                        itemNode = (ItemTreeNode) selectedNode;
                        historicKey = itemNode.getData() instanceof HistoryKey;
                        type = itemNode.getType();
                        format = itemNode.getFormat();
                        if ((type == dataType) || ((type != DataType.SCALAR) && (dataType != DataType.SCALAR))) {
                            switch (type) {
                                case SCALAR:
                                    // HistoricKey are not openable in SCALAR
                                    dropEnable = true;
                                    break;
                                case SPECTRUM:
                                    // HistoricKey are not openable in Image
                                    if (historicKey) {
                                        dropEnable = dataType == DataType.SPECTRUM;
                                    } else {
                                        switch (format) {
                                            case BOOLEAN:
                                            case NUMERICAL:
                                                dropEnable = true;
                                                break;
                                            case TEXT:
                                                if (dataType != DataType.IMAGE) {
                                                    dropEnable = true;
                                                }
                                                break;
                                            default:
                                                break;
                                        }
                                    }
                                    break;
                                case IMAGE:
                                    if (format != DataFormat.TEXT) {
                                        dropEnable = true;
                                    }
                                    break;
                                default:
                                    break;
                            }
                            if (dropEnable) {
                                break;
                            }
                        }
                    }
                }
            }
        }
        return dropEnable;
    }

    // Refresh in a map all the selected IKey
    private void buildSelectedKey() {
        selectedMap.clear();
        if (tree != null) {
            ITreeNode[] selectedNodes = tree.getSelectedNodes();
            if ((selectedNodes != null) && (selectedNodes.length > 0)) {
                List<IKey> keyList = null;
                DataType dataType = null;
                ItemTreeNode itemNode = null;
                Object data = null;
                IKey key = null;
                for (ITreeNode selectedNode : selectedNodes) {
                    data = selectedNode.getData();
                    if ((selectedNode instanceof ItemTreeNode) && (data instanceof IKey)) {
                        itemNode = (ItemTreeNode) selectedNode;
                        key = (IKey) data;
                        dataType = itemNode.getType();
                        keyList = selectedMap.get(dataType);
                        if (keyList == null) {
                            keyList = new ArrayList<>();
                            selectedMap.put(dataType, keyList);
                        }
                        keyList.add(key);
                    }
                }
            }
        }
    }

    private List<IKey> getSelectedKey() {
        List<IKey> selectedKey = new ArrayList<>();
        Collection<List<IKey>> values = selectedMap.values();
        for (List<IKey> keyList : values) {
            for (IKey key : keyList) {
                selectedKey.add(key);
            }
        }
        return selectedKey;
    }

    private void enablePopupMenu() {
        enablePopupMenu(DataType.SPECTRUM);
        enablePopupMenu(DataType.IMAGE);

        // Check the menu open and closed
        IKey selectedSource = tree.getSelectedSource();
        boolean isOneIsOpen = false;
        boolean isOneIsClosed = false;
        ITreeNode[] selectedNodes = tree.getSelectedNodes();
        for (ITreeNode node : selectedNodes) {
            IKey key = (IKey) node.getData();
            boolean open = controller.isItemOpened(selectedSource, key);
            if (open) {
                isOneIsOpen = true;
            } else {
                isOneIsClosed = true;
            }

            if (isOneIsOpen && isOneIsClosed) {
                break;
            }
        }
        openItem.setEnabled(isOneIsClosed);
        closeItem.setEnabled(isOneIsOpen);

    }

    private void enablePopupMenu(DataType dataType) {
        if ((dataType != null) && isDropEnable(dataType)) {
            IDataSourceBrowser selectedBrowser = tree.getSelectedBrowser();
            List<IKey> keyList = selectedMap.get(dataType);
            if ((keyList != null) && (selectedBrowser != null)) {
                DataFormat format = null;
                AxisType currentAxis = null;
                for (IKey key : keyList) {
                    format = selectedBrowser.getFormat(key);
                    currentAxis = controller.getCurrentAxisForKey(key);
                    switch (dataType) {
                        case SPECTRUM:
                            switch (format) {
                                case BOOLEAN:
                                case NUMERICAL:
                                    enablePopuMenu(currentAxis, AxisType.Y1);
                                    enablePopuMenu(currentAxis, AxisType.Y2);
                                    enablePopuMenu(currentAxis, AxisType.X);
                                    enablePopuMenu(currentAxis, AxisType.IMAGE);
                                    break;
                                default:
                                    break;
                            }
                            break;
                        case IMAGE:
                            switch (format) {
                                case NUMERICAL:
                                    enablePopuMenu(currentAxis, AxisType.X);
                                case BOOLEAN:
                                    enablePopuMenu(currentAxis, AxisType.Y1_COLUMN);
                                    enablePopuMenu(currentAxis, AxisType.Y2_COLUMN);
                                    enablePopuMenu(currentAxis, AxisType.Y1_ROW);
                                    enablePopuMenu(currentAxis, AxisType.Y2_ROW);
                                    enablePopuMenu(currentAxis, AxisType.IMAGE);
                                    break;
                                default:
                                    break;
                            }
                            break;
                        default:
                            break;
                    }
                }
            }
        }
    }

    private void enablePopuMenu(AxisType currentAxis, AxisType displayAxis) {
        if ((currentAxis == null) || ((currentAxis != null) && (currentAxis != displayAxis))) {
            enablePopupMenu(displayAxis);
        }
    }

    private void enablePopupMenu(AxisType axistype) {
        if (axistype != null) {
            switch (axistype) {
                case IMAGE:
                    showAsImage.setEnabled(true);
                    if ((tree.getSelectedNodes() != null) && (tree.getSelectedNodes().length == 1)
                            && !controller.getOpenedNumericalMatrixItems().isEmpty()) {
                        openDialogXMatrixItemAction.setEnabled(true);
                        openDialogYMatrixItemAction.setEnabled(true);
                    }
                    break;
                case X:
                    if ((tree.getSelectedNodes() != null) && (tree.getSelectedNodes().length == 1)
                            && !controller.getOpenedNumericalSpectrumItems().isEmpty()) {
                        showOnX.setEnabled(true);
                        setXForOpenedSpectrumItemAction.setEnabled(true);
                    }
                    break;
                case Y1:
                    showOnY1.setEnabled(true);
                    break;
                case Y2:
                    showOnY2.setEnabled(true);
                    break;
                case Y1_COLUMN:
                    showOnY1COLUMN.setEnabled(true);
                    break;
                case Y1_ROW:
                    showOnY1ROW.setEnabled(true);
                    break;
                case Y2_COLUMN:
                    showOnY2COLUMN.setEnabled(true);
                    break;
                case Y2_ROW:
                    showOnY2ROW.setEnabled(true);
                    break;
                default:
                    break;

            }
        }
    }

    private String getSelectedDevice() {
        String selected = null;
        ITreeNode[] selectedNodes = tree.getSelectedNodes();
        if ((selectedNodes != null) && (selectedNodes.length > 0)) {
            selected = selectedNodes[0].getName();
        }
        return selected;
    }

    @Override
    public void mouseEntered(final MouseEvent e) {
        // nop
    }

    @Override
    public void mouseExited(final MouseEvent e) {
        // nop
    }

    // -------------------------------
    // TreeNodeSelectionListener
    // -------------------------------

    @Override
    public void selectionChanged(final TreeNodeSelectionEvent event) {
        // update open and close buttons' state
        updateButtonsState();
        updateSelection();
    }

    // -------------------------------
    // DocumentListener
    // -------------------------------

    @Override
    public void insertUpdate(final DocumentEvent e) {
        updateSearchButtonsState();
    }

    @Override
    public void removeUpdate(final DocumentEvent e) {
        updateSearchButtonsState();
    }

    @Override
    public void changedUpdate(final DocumentEvent e) {
        updateSearchButtonsState();
    }

    private void updateSearchButtonsState() {
        boolean empty = searchField.getText().isEmpty();
        findNextAction.setEnabled(!empty);
        findPreviousAction.setEnabled(!empty);
    }

    private void addNode(ITreeNode node, Collection<ITreeNode> nodes, IKey key) {
        if ((node != null) && (nodes != null) && (key != null)) {
            if (key.equals(node.getData())) {
                nodes.add(node);
            } else {
                List<ITreeNode> children = node.getChildren();
                if ((children != null) && (!children.isEmpty())) {
                    for (ITreeNode child : children) {
                        addNode(child, nodes, key);
                    }
                }
            }
        }
    }

    private Collection<ITreeNode> getNodes(Item item) {
        Collection<ITreeNode> nodes = new ArrayList<>();
        if (item != null) {
            IKey key = item.getKey();
            if (key != null) {
                ITreeNode node = tree.getNode(key);
                if (node == null) {
                    List<IKey> keys = controller.getOpenedSourcesForItem().get(key);
                    if (keys != null) {
                        for (IKey subKey : keys) {
                            if (subKey != null) {
                                addNode(tree.getNode(subKey), nodes, key);
                            }

                        }
                    }
                } else {
                    nodes.add(node);
                }
            }
        }
        return nodes;
    }

    // -------------------------------
    // TreeWillExpandListener
    // -------------------------------

    @Override
    public void treeWillExpand(final TreeExpansionEvent event) throws ExpandVetoException {
        final Frame frameForComponent = CometeUtils.getFrameForComponent(tree);
        final Cursor cursor = (frameForComponent == null ? null : frameForComponent.getCursor());
        EDTManager.INSTANCE.runInDrawingThread(() -> {
            if (frameForComponent != null) {
                frameForComponent.setCursor(Cursor.getPredefinedCursor(Cursor.WAIT_CURSOR));
            }
        });

        // fill item properties for the renderer
        TreePath path = event.getPath();
        DefaultMutableTreeNode lastPathComponent = (DefaultMutableTreeNode) path.getLastPathComponent();
        CometeTreeModel model = tree.getModel();
        ITreeNode treeNode = model.getTreeNode(lastPathComponent);

        if ((treeNode instanceof SourceTreeNode) || (treeNode instanceof GroupTreeNode)) {
            IDataSourceBrowser browser = tree.getBrowserForNode(treeNode);

            for (ITreeNode child : treeNode.getChildren()) {
                if ((child instanceof ItemTreeNode) && !((ItemTreeNode) child).isPropertySet()) {
                    ItemTreeNode itemTreeNode = (ItemTreeNode) child;
                    IKey key = (IKey) child.getData();
                    itemTreeNode.setDescription(browser.getDescription(key));
                    itemTreeNode.setType(browser.getType(key));
                    itemTreeNode.setFormat(browser.getFormat(key));
                }
            }
        }

        EDTManager.INSTANCE.runInDrawingThread(() -> {
            // cursor is null only if frame is null
            if (cursor != null) {
                frameForComponent.setCursor(cursor);
            }
        });
    }

    @Override
    public void treeWillCollapse(final TreeExpansionEvent event) throws ExpandVetoException {
        TreePath path = event.getPath();
        DefaultMutableTreeNode lastPathComponent = (DefaultMutableTreeNode) path.getLastPathComponent();
        CometeTreeModel model = tree.getModel();
        ITreeNode treeNode = model.getTreeNode(lastPathComponent);

        if (treeNode instanceof BrowserTreeNode) {
            throw new ExpandVetoException(event);
        }
    }

    // -------------------------------
    // IDataBrowserItemListener
    // -------------------------------

    @Override
    public void onItemOpened(DataBrowserItemEvent event) {
        // not managed
    }

    @Override
    public void onItemClosed(DataBrowserItemEvent event) {
        // not managed
    }

    @Override
    public void onItemSelected(DataBrowserItemEvent event) {
        if (canListenToModel && (event != null)) {
            Collection<ITreeNode> nodes = getNodes(event.getItem());
            if (!nodes.isEmpty()) {
                tree.selectNodes(false, nodes.toArray(new ITreeNode[nodes.size()]));
                nodes.clear();
            }
        }
    }

    @Override
    public void onItemDeselected(DataBrowserItemEvent event) {
        if (canListenToModel && (event != null)) {
            Item item = event.getItem();
            if (item != null) {
                Collection<ITreeNode> nodes = getNodes(event.getItem());
                if (!nodes.isEmpty()) {
                    tree.deselectNodes(nodes.toArray(new ITreeNode[nodes.size()]));
                    nodes.clear();
                }
            }
        }
    }

    @Override
    public void onItemAxisChanged(DataBrowserItemEvent event) {
        // not managed
    }

}
