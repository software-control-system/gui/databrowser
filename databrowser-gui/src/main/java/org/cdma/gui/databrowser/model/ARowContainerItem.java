/*
 * This file is part of databrowser-gui.
 * 
 * databrowser-gui is free software: you can redistribute it and/or modify it under the terms of the GNU General Public
 * License as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later
 * version.
 * 
 * databrowser-gui is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the
 * implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for more
 * details.
 * 
 * You should have received a copy of the GNU General Public License along with databrowser-gui. If not, see
 * <https://www.gnu.org/licenses/>.
 */
package org.cdma.gui.databrowser.model;

import java.awt.Component;

import javax.swing.JComponent;
import javax.swing.JLabel;
import javax.swing.JTextField;
import javax.swing.SwingConstants;

import org.cdma.gui.databrowser.DataBrowserController;
import org.cdma.gui.databrowser.interfaces.IDataSourceBrowser;
import org.cdma.gui.databrowser.interfaces.Item;
import org.cdma.gui.databrowser.view.DynamicForegroundAdaptableTextArea;
import org.cdma.gui.databrowser.view.TitleLabel;
import org.cdma.gui.databrowser.view.item.ARowContainerItemViewer;
import org.cdma.gui.databrowser.view.item.RowContainer;

import fr.soleil.comete.swing.AdaptableTextArea;
import fr.soleil.data.target.scalar.IScalarTarget;

public abstract class ARowContainerItem<C extends JComponent & IScalarTarget, S extends JComponent & IScalarTarget, V extends ARowContainerItemViewer<?>>
        extends AbstractItem<C> {

    protected static final String SLASH = "/";

    protected static final int DEFAULT_TEXTAREA_NB_ROWS = 1;
    protected static final int MINIMUM_WIDTH = 200;
    protected static final int MINIMUN_COLUMN_WITH = 15;

    protected V itemViewer;

    protected RowContainer<C, S> rowContainer;
    protected S setter;

    public ARowContainerItem(Item item, DataBrowserController controller) {
        super(item, controller);
    }

    protected void init(Item item, DataBrowserController controller) {
        JLabel label = createLabel(item);
        viewer = createViewer();
        setComponentSize(viewer);
        setter = createSetter();
        setComponentSize(setter);

        updateLabelText(label, controller);

        rowContainer = new RowContainer<>(label, viewer, setter);
    }

    protected void updateLabelText(JLabel label, DataBrowserController controller) {
        // Cut the label name if boolean is true show all label
        // Else show only the attribute name
        boolean showFullScalarLabel = controller.isShowFullScalarLabel();
        if (!showFullScalarLabel) {
            String labelName = label.getText();
            int lastIndexOf = labelName.lastIndexOf(SLASH) + 1;
            labelName = labelName.substring(lastIndexOf);
            label.setText(labelName);
        }
    }

    public AdaptableTextArea createArea() {
        DynamicForegroundAdaptableTextArea adaptableTextArea = new DynamicForegroundAdaptableTextArea();
        adaptableTextArea.setOpaque(false);
        adaptableTextArea.setRows(DEFAULT_TEXTAREA_NB_ROWS);
        return adaptableTextArea;
    }

    protected JLabel createLabel(final Item item) {
        String text = null;
        IDataSourceBrowser browser = item.getBrowser();
        if (browser != null) {
            text = browser.getDisplayName(item.getKey());
        }
        TitleLabel label = new TitleLabel(text);
        label.setVerticalAlignment(SwingConstants.CENTER);
        return label;
    }

    protected abstract C createViewer();

    protected abstract S createSetter();

    protected void setComponentSize(Component component) {
        if (component instanceof JTextField) {
            ((JTextField) component).setColumns(MINIMUN_COLUMN_WITH);
            component.setSize(MINIMUM_WIDTH, 25);
            component.setPreferredSize(component.getSize());
            component.setMinimumSize(component.getSize());
            component.repaint();
        }
    }

    protected V getItemViewer() {
        return itemViewer;
    }

    public RowContainer<C, S> getRowContainer() {
        return rowContainer;
    }

    @Override
    public void setSelected(final boolean selected) {
        super.setSelected(selected);
        rowContainer.setSelected(selected);
    }

}
