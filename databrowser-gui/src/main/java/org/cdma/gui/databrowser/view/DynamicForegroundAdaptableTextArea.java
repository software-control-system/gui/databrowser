/*
 * This file is part of databrowser-gui.
 * 
 * databrowser-gui is free software: you can redistribute it and/or modify it under the terms of the GNU General Public
 * License as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later
 * version.
 * 
 * databrowser-gui is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the
 * implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for more
 * details.
 * 
 * You should have received a copy of the GNU General Public License along with databrowser-gui. If not, see
 * <https://www.gnu.org/licenses/>.
 */
package org.cdma.gui.databrowser.view;

import java.awt.Color;

import javax.swing.JTextArea;

import fr.soleil.comete.awt.util.ColorTool;
import fr.soleil.comete.definition.widget.util.CometeColor;
import fr.soleil.comete.swing.AdaptableTextArea;
import fr.soleil.comete.swing.util.DynamicSizeTextArea;
import fr.soleil.lib.project.awt.ColorUtils;

/**
 * An {@link AdaptableTextArea} that automatically adapts its text area's foreground color.
 * 
 * @author GIRARDOT
 */
public class DynamicForegroundAdaptableTextArea extends AdaptableTextArea {

    private static final long serialVersionUID = -6601535463699390179L;

    protected static final String NEW_LINE = "\n";
    protected static final int MAX_LINES = 10;

    protected Color fg;
    protected boolean textSet;

    public DynamicForegroundAdaptableTextArea() {
        super();
    }

    public DynamicForegroundAdaptableTextArea(String text) {
        super(text);
    }

    @Override
    public void setCometeForeground(CometeColor color) {
        super.setCometeForeground(color);
        fg = color == null ? null : ColorTool.getColor(getCometeForeground());
        updateForeground();
    }

    @Override
    public void setCometeBackground(CometeColor color) {
        super.setCometeBackground(color);
        updateForeground();
    }

    protected void updateForeground() {
        if (getTextArea() != null) {
            CometeColor cfg;
            if (fg == null) {
                if (((UpdateForegroundTextArea) textArea).getTextArea().isOpaque()) {
                    cfg = ColorTool
                            .getCometeColor(ColorUtils.getBestForeground(ColorTool.getColor(getCometeBackground())));
                } else {
                    cfg = CometeColor.BLACK;
                }
            } else {
                cfg = ColorTool.getCometeColor(fg);
            }
            super.setCometeForeground(cfg);
        }
    }

    @Override
    protected DynamicSizeTextArea generateTextArea() {
        return new UpdateForegroundTextArea();
    }

    @Override
    public void setText(String text) {
        super.setText(text);
        if (!textSet) {
            if (text != null) {
                int count = text.split(NEW_LINE).length;
                if (count > 1) {
                    rows = Math.min(count, MAX_LINES);
                    updateRows();
                }
            }
            textSet = true;
        }
    }

    // ///////////// //
    // Inner classes //
    // ///////////// //

    /**
     * A {@link DynamicSizeTextArea} that updates its foreground color on {@link #setOpaque(boolean)} and gives acces to
     * its {@link JTextArea} through {@link #getTextArea()} method.
     * 
     * @author GIRARDOT
     */
    protected class UpdateForegroundTextArea extends DynamicSizeTextArea {

        private static final long serialVersionUID = 6899422065725162230L;

        public UpdateForegroundTextArea() {
            super();
            textArea.setOpaque(true);
        }

        @Override
        public void setOpaque(boolean isOpaque) {
            super.setOpaque(isOpaque);
            updateForeground();
        }

        public JTextArea getTextArea() {
            return textArea;
        }
    }
}
