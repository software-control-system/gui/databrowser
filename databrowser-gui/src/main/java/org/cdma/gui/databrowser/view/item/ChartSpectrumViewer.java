/*
 * This file is part of databrowser-gui.
 * 
 * databrowser-gui is free software: you can redistribute it and/or modify it under the terms of the GNU General Public
 * License as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later
 * version.
 * 
 * databrowser-gui is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the
 * implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for more
 * details.
 * 
 * You should have received a copy of the GNU General Public License along with databrowser-gui. If not, see
 * <https://www.gnu.org/licenses/>.
 */
package org.cdma.gui.databrowser.view.item;

import java.awt.BorderLayout;
import java.awt.Color;
import java.util.ArrayList;
import java.util.Collection;

import javax.swing.JScrollPane;
import javax.swing.JSplitPane;

import org.cdma.gui.databrowser.DataBrowserController;
import org.cdma.gui.databrowser.interfaces.AxisType;
import org.cdma.gui.databrowser.interfaces.IChartSpectrumItemListener;
import org.cdma.gui.databrowser.interfaces.IDataSourceBrowser;
import org.cdma.gui.databrowser.interfaces.IItemViewer;
import org.cdma.gui.databrowser.interfaces.Item;
import org.cdma.gui.databrowser.model.AbstractItem;
import org.cdma.gui.databrowser.model.ChartSpectrumItem;

import fr.soleil.comete.awt.util.ColorTool;
import fr.soleil.comete.box.complexbox.ChartViewerBox;
import fr.soleil.comete.definition.widget.IChartViewer;
import fr.soleil.comete.definition.widget.properties.ChartProperties;
import fr.soleil.comete.definition.widget.properties.PlotProperties;
import fr.soleil.comete.swing.Chart;
import fr.soleil.comete.swing.chart.util.ShowInFrameMenu;
import fr.soleil.data.mediator.Mediator;
import fr.soleil.data.service.IKey;
import fr.soleil.data.target.scalar.IBooleanTarget;
import fr.soleil.lib.project.ObjectUtils;

public class ChartSpectrumViewer extends AbstractItemViewer<ChartSpectrumItem>
        implements IItemViewer, IChartSpectrumItemListener, IBooleanTarget {

    private static final long serialVersionUID = -3053094010328245462L;

    private static final Color[] COLOR_ARRAY = new Color[] { Color.BLUE, Color.RED, Color.GREEN, Color.ORANGE,
            Color.CYAN, Color.MAGENTA, Color.PINK, Color.DARK_GRAY, Color.BLACK };

    private Chart chartViewer;

    private ChartViewerBox chartBox;

    private JSplitPane splitPane;
    private JScrollPane playerScrollPane;
    private PlayerPanel playerPanel;
    private WriteDataFrameMenu writeDataFrame;

    private boolean settableMenu;
    private boolean markerEnabled;

    // TODO a voir
    // private List<IKey> spectrumY1Items;
    // private List<IKey> spectrumY2Items;
    private AbstractItem<?> xItem;

    public ChartSpectrumViewer(final DataBrowserController controller) {
        super(controller, false);
        playerPanel = null;
        settableMenu = false;
        markerEnabled = true;
        initComponents();
        layoutComponents();
    }

    @Override
    protected void initComponents() {
        chartViewer = new Chart();
        ShowInFrameMenu showInFrameMenu = new ShowInFrameMenu();
        writeDataFrame = new WriteDataFrameMenu();
        chartViewer.addMenuItem(showInFrameMenu);
        chartViewer.addMenuSeparator();
        chartViewer.addMenuItem(writeDataFrame);
        chartViewer.setUseDisplayNameForDataSaving(true);
        chartViewer.setDataViewsSortedOnX(false);

        // important to avoid reinit curve properties on each player step
        chartViewer.setCleanDataViewConfigurationOnRemoving(false);
        chartViewer.setFreezePanelVisible(true);
        chartViewer.setManagementPanelVisible(false);
        chartViewer.setDataViewRemovingEnabled(false);
        chartViewer.setAxisSelectionVisible(false);
        chartViewer.setCustomCometeColor(ColorTool.getCometeColors(COLOR_ARRAY));
        chartViewer.setCyclingCustomMap(true);
        chartViewer.setMathExpressionEnabled(true);
        chartViewer.setAutoHighlightOnLegend(true);
        chartViewer.setGridStyle(IChartViewer.STYLE_DASH, IChartViewer.Y1);
        chartViewer.setGridStyle(IChartViewer.STYLE_DASH, IChartViewer.Y2);
        chartViewer.setGridStyle(IChartViewer.STYLE_DASH, IChartViewer.X);

        chartBox = new ChartViewerBox();

        splitPane = new JSplitPane(JSplitPane.VERTICAL_SPLIT);
        splitPane.setOneTouchExpandable(true);
        playerScrollPane = new JScrollPane();

        // spectrumY1Items = new ArrayList<>();
        // spectrumY2Items = new ArrayList<>();
        xItem = null;
    }

    private void layoutComponents() {
        splitPane.setRightComponent(chartViewer);

        setLayout(new BorderLayout());
        add(splitPane, BorderLayout.CENTER);
    }

    public Chart getChartViewer() {
        return chartViewer;
    }

    public ChartViewerBox getChartBox() {
        return chartBox;
    }

    protected String getDataId(IKey itemKey) {
        String id;
        if (itemKey == null) {
            id = null;
        } else {
            ChartSpectrumItem spectrumItem = displayedItems.get(itemKey);
            if (spectrumItem == null) {
                id = itemKey.getInformationKey();
            } else {
                IKey readKey = spectrumItem.getReadKey();
                if (readKey == null) {
                    id = itemKey.getInformationKey();
                } else {
                    id = readKey.getInformationKey();
                }
            }
        }
        return id;
    }

    @Override
    public Collection<Runnable> addItem(final Item item) {
        Collection<Runnable> toRunInEDT = new ArrayList<>();
        // Check if the item is already add in the viewer
        writeDataFrame.addItem(item);
        ChartSpectrumItem spectrumItem = displayedItems.get(item.getKey());
        // Menu of writeDataFrame is visible if add source writable
        boolean settableSource = writeDataFrame.isSettableSource(item);
        if (settableSource) {
            settableMenu = true;
        }

        toRunInEDT.add(() -> {
            writeDataFrame.setVisible(settableMenu);
        });

        // Should be always null
        if (spectrumItem == null) {
            spectrumItem = new ChartSpectrumItem(this, item, controller, markerEnabled);

            displayedItems.put(item.getKey(), spectrumItem);
            spectrumItem.addChartSpectrumItemListener(this);
        }

        if ((item != null) && (spectrumItem != null)) {
            AxisType axis = item.getAxis();
            if (axis == AxisType.X) {
                setXItem(spectrumItem);
            } else {
                spectrumItem.connect();
            }
        }

        // IRFU Katy SAINTIN Code issue
        // https://drf-gitlab.cea.fr/LDISC/rdepics/issues/421
        // Set PlotProperties and ChartProperties for a IKey
        IDataSourceBrowser browser = item.getBrowser();
        IKey key = item.getKey();
        if (browser != null) {
            PlotProperties plotProperties = browser.getPlotProperties(key);
            if (plotProperties != null) {
                setChartDataViewPlotProperties(key, plotProperties, item);
            }
            // Set only axis properties because there is an issue
            ChartProperties chartProperties = browser.getChartProperties(key);
            if (chartProperties != null) {
                setChartProperties(chartProperties);
            }
        }
        return toRunInEDT;
    }

    @Override
    public Collection<Runnable> removeItem(final Item item) {
        Collection<Runnable> toRunInEDT = new ArrayList<>();
        ChartSpectrumItem spectrumItem = displayedItems.remove(item.getKey());
        writeDataFrame.removeItem(item);
        settableMenu = false;

        // Menu of writeDataFrame is visible if add source writable
        for (ChartSpectrumItem chartSpectrumItem : displayedItems.values()) {
            boolean settableSource = writeDataFrame.isSettableSource(chartSpectrumItem);
            if (settableSource) {
                settableMenu = true;
            }
        }
        toRunInEDT.add(() -> {
            writeDataFrame.setVisible(settableMenu);
        });

        if (spectrumItem != null) {
            spectrumItem.removeChartSpectrumItemListener(this);
            spectrumItem.disconnect();
            AxisType axis = item.getAxis();
            if (axis == AxisType.X) {
                chartViewer.setAxisName(ObjectUtils.EMPTY_STRING, IChartViewer.X);
                setXItem(null);
            }
        }
        return toRunInEDT;
    }

    @Override
    public Collection<Runnable> selectItem(final Item item) {
        Collection<Runnable> toRunInEDT = new ArrayList<>();
        // select new key
        ChartSpectrumItem spectrumItem = displayedItems.get(item.getKey());
        if (spectrumItem != null) {

            selectedKey = item.getKey();

            spectrumItem.setSelected(true);

            playerPanel = spectrumItem.getPlayerPanel();
            if (playerPanel != null) {
                toRunInEDT.add(() -> {
                    playerScrollPane.setViewportView(playerPanel);
                    splitPane.setLeftComponent(playerScrollPane);
                    // splitPane.resetToPreferredSizes();
                    splitPane.setResizeWeight(0.3);
                });
            }
        }
        return toRunInEDT;
    }

    public void setValueConvertor(IKey key) {
        if (playerPanel != null) {
            playerPanel.setValueConvertor(key);
        }
    }

    @Override
    public Collection<Runnable> deselectItem(final Item item) {
        Collection<Runnable> toRunInEDT = new ArrayList<>();
        if (item != null) {
            if (item.getKey() == selectedKey) {
                selectedKey = null;
                ChartSpectrumItem spectrumItem = displayedItems.get(item.getKey());
                toRunInEDT.add(() -> {
                    if (spectrumItem != null) {
                        spectrumItem.setSelected(false);
                    }
                    splitPane.setLeftComponent(null);
                    splitPane.setResizeWeight(1.0);
                });
            }
        }
        return toRunInEDT;
    }

    @Override
    public Collection<Runnable> clearSelection() {
        Collection<Runnable> toRunInEDT;
        if (selectedKey == null) {
            toRunInEDT = new ArrayList<>();
        } else {
            ChartSpectrumItem selectedItem = displayedItems.get(selectedKey);
            toRunInEDT = deselectItem(selectedItem);
        }
        return toRunInEDT;
    }

    private void setXItem(final AbstractItem<?> item) {
        // Set the x item object
        xItem = item;
        Collection<ChartSpectrumItem> values = displayedItems.values();
        for (ChartSpectrumItem spectrumItem : values) {
            spectrumItem.reconnect();
        }

    }

    public AbstractItem<?> getXItem() {
        return xItem;
    }

    @Override
    public void onItemPlayerIndexChanged(final ChartSpectrumItem item) {
        if (item.getAxis() == AxisType.X) {
            item.computeKeys();

            // reconnect all items except X one
            for (ChartSpectrumItem chartSpectrumItem : displayedItems.values()) {
                if (chartSpectrumItem != xItem) {
                    chartSpectrumItem.reconnect();
                }
            }
        } else {
            item.reconnect();
        }
    }

    @Override
    public void addMediator(Mediator<?> mediator) {
        // not managed
    }

    @Override
    public void removeMediator(Mediator<?> mediator) {
        // not managed
    }

    @Override
    public boolean isSelected() {
        return markerEnabled;
    }

    @Override
    public void setSelected(boolean markerEnabled) {
        this.markerEnabled = markerEnabled;
        for (ChartSpectrumItem chartSpectrumItem : displayedItems.values()) {
            if (chartSpectrumItem != null) {
                chartSpectrumItem.setMarkerEnabled(markerEnabled);
            }
        }
    }

    public ChartProperties getChartProperties() {
        return chartViewer.getChartProperties();
    }

    public void setChartProperties(final ChartProperties properties) {
        if (properties != null) {
            chartViewer.setChartProperties(properties);
        }
    }

    public String getChartSnapshotLocation() {
        return chartViewer.getSnapshotDirectory();
    }

    public void setChartSnapshotLocation(final String path) {
        chartViewer.setSnapshotDirectory(path);
    }

    public String getChartDataLocation() {
        return chartViewer.getDataDirectory();
    }

    public void setChartDataLocation(final String path) {
        chartViewer.setDataDirectory(path);
    }

    public PlotProperties getChartDataViewPlotProperties(final String id) {
        return chartViewer.getDataViewPlotProperties(id);
    }

    public PlotProperties getChartDataViewPlotProperties(final IKey key) {
        return chartViewer.getDataViewPlotProperties(getDataId(key));
    }

    public Collection<String> getChartKnownDataIds() {
        return new ArrayList<>(chartViewer.getData().keySet());
    }

    protected void setChartDataViewPlotProperties(final String id, final PlotProperties plotProperties,
            final Item item) {
        chartViewer.setDataViewPlotProperties(id, plotProperties);
        ChartSpectrumItem spectrumItem = displayedItems.get(item.getKey());
        if (spectrumItem != null) {
            spectrumItem.applySelectionStyle();
        }
    }

    public void setChartDataViewPlotProperties(final IKey key, final PlotProperties plotProperties, final Item item) {
        setChartDataViewPlotProperties(getDataId(key), plotProperties, item);
    }

}
