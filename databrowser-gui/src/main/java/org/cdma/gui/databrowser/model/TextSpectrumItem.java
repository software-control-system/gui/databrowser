/*
 * This file is part of databrowser-gui.
 * 
 * databrowser-gui is free software: you can redistribute it and/or modify it under the terms of the GNU General Public
 * License as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later
 * version.
 * 
 * databrowser-gui is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the
 * implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for more
 * details.
 * 
 * You should have received a copy of the GNU General Public License along with databrowser-gui. If not, see
 * <https://www.gnu.org/licenses/>.
 */
package org.cdma.gui.databrowser.model;

import org.cdma.gui.databrowser.DataBrowserController;
import org.cdma.gui.databrowser.interfaces.Item;
import org.cdma.gui.databrowser.view.item.RowContainer;
import org.cdma.gui.databrowser.view.item.TextSpectrumViewer;

import fr.soleil.comete.box.scalarbox.StringScalarBox;
import fr.soleil.comete.definition.widget.ITextArea;
import fr.soleil.comete.swing.AdaptableTextArea;

public class TextSpectrumItem extends ARowContainerItem<AdaptableTextArea, AdaptableTextArea, TextSpectrumViewer> {

    public TextSpectrumItem(final TextSpectrumViewer spectrumViewer, final Item item,
            DataBrowserController controller) {
        super(item, controller);
        itemViewer = spectrumViewer;
        init(item, controller);
    }

    @Override
    public RowContainer<AdaptableTextArea, AdaptableTextArea> getRowContainer() {
        return rowContainer;
    }

    @Override
    public AdaptableTextArea createViewer() {
        return createArea();
    }

    @Override
    protected AdaptableTextArea createSetter() {
        // No setter available
        return null;
    }

    @Override
    public void doConnect() {
        StringScalarBox stringBox = itemViewer.getStringBox();
        stringBox.connectWidget((ITextArea) viewer, readKey);
    }

    @Override
    protected void disconnectImmediately() {
        StringScalarBox stringBox = itemViewer.getStringBox();
        stringBox.disconnectWidget((ITextArea) viewer, readKey);
    }

}
