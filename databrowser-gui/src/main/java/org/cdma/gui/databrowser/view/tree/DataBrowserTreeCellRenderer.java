/*
 * This file is part of databrowser-gui.
 * 
 * databrowser-gui is free software: you can redistribute it and/or modify it under the terms of the GNU General Public
 * License as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later
 * version.
 * 
 * databrowser-gui is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the
 * implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for more
 * details.
 * 
 * You should have received a copy of the GNU General Public License along with databrowser-gui. If not, see
 * <https://www.gnu.org/licenses/>.
 */
package org.cdma.gui.databrowser.view.tree;

import java.awt.Color;
import java.awt.Component;
import java.net.URISyntaxException;
import java.util.ResourceBundle;

import javax.swing.JComponent;
import javax.swing.JLabel;
import javax.swing.JTree;

import org.cdma.gui.databrowser.DataBrowser;
import org.cdma.gui.databrowser.DataBrowserController;
import org.cdma.gui.databrowser.interfaces.DataFormat;
import org.cdma.gui.databrowser.interfaces.DataType;
import org.cdma.gui.databrowser.interfaces.IDataSourceBrowser;

import fr.soleil.comete.definition.widget.util.CometeImage;
import fr.soleil.comete.definition.widget.util.ITreeNode;
import fr.soleil.comete.swing.util.CometeTreeCellRenderer;
import fr.soleil.data.service.IKey;
import fr.soleil.lib.project.swing.ui.ReverseLabelUI;

public class DataBrowserTreeCellRenderer extends CometeTreeCellRenderer {

    private static final long serialVersionUID = 1280838231163257583L;

    private static final ResourceBundle ICONS = ResourceBundle.getBundle(DataBrowser.ICONS_PATH);
    private static final CometeImage BROWSER_IMAGE;
    private static final CometeImage SOURCE_IMAGE;
    private static final CometeImage GROUP_IMAGE;
    private static final CometeImage SCALAR_IMAGE;
    private static final CometeImage SPECTRUM_NUM_IMAGE;
    private static final CometeImage SPECTRUM_IMAGE;
    private static final CometeImage IMAGE_NUM_IMAGE;
    private static final CometeImage IMAGE_IMAGE;
    static {
        String iconKey = ICONS.getString("SourceTree.Node.Browser");
        CometeImage image;
        try {
            image = new CometeImage(DataBrowser.class.getResource(iconKey).toURI());
        } catch (URISyntaxException e) {
            image = null;
        }
        BROWSER_IMAGE = image;

        iconKey = ICONS.getString("SourceTree.Node.Source");
        try {
            image = new CometeImage(DataBrowser.class.getResource(iconKey).toURI());
        } catch (URISyntaxException e) {
            image = null;
        }
        SOURCE_IMAGE = image;

        iconKey = ICONS.getString("SourceTree.Node.Group");
        try {
            image = new CometeImage(DataBrowser.class.getResource(iconKey).toURI());
        } catch (URISyntaxException e) {
            image = null;
        }
        GROUP_IMAGE = image;

        iconKey = ICONS.getString("SourceTree.Node.Item.Scalar");
        try {
            image = new CometeImage(DataBrowser.class.getResource(iconKey).toURI());
        } catch (URISyntaxException e) {
            image = null;
        }
        SCALAR_IMAGE = image;

        iconKey = ICONS.getString("SourceTree.Node.Item.Spectrum.Numerical");
        try {
            image = new CometeImage(DataBrowser.class.getResource(iconKey).toURI());
        } catch (URISyntaxException e) {
            image = null;
        }
        SPECTRUM_NUM_IMAGE = image;

        iconKey = ICONS.getString("SourceTree.Node.Item.Spectrum");
        try {
            image = new CometeImage(DataBrowser.class.getResource(iconKey).toURI());
        } catch (URISyntaxException e) {
            image = null;
        }
        SPECTRUM_IMAGE = image;

        iconKey = ICONS.getString("SourceTree.Node.Item.Image.Numerical");
        try {
            image = new CometeImage(DataBrowser.class.getResource(iconKey).toURI());
        } catch (URISyntaxException e) {
            image = null;
        }
        IMAGE_NUM_IMAGE = image;

        iconKey = ICONS.getString("SourceTree.Node.Item.Image");
        try {
            image = new CometeImage(DataBrowser.class.getResource(iconKey).toURI());
        } catch (URISyntaxException e) {
            image = null;
        }
        IMAGE_IMAGE = image;

    }
    private ReverseLabelUI reverseUI;

    private DataBrowserController controller = null;

    public DataBrowserTreeCellRenderer(JComponent parent, DataBrowserController controller) {
        if (reverseUI == null) {
            reverseUI = new ReverseLabelUI(parent);
        }
        this.controller = controller;
    }

    @Override
    public Component getTreeCellRendererComponent(final JTree tree, final Object value, final boolean sel,
            final boolean expanded, final boolean leaf, final int row, final boolean hasTheFocus) {
        // Step 1: get the ITreeNode and update it if necessary
        ITreeNode treeNode = getTreeNode(value, tree);
        if ((treeNode != null) && (treeNode.getImage() == null)
                && ((treeNode.getToolTip() == null) || treeNode.getToolTip().isEmpty())) {
            Object data = treeNode.getData();
            CometeImage icon = null;
            String tooltip = null;
            treeNode.setToolTip(null);
            // tooltip
            if (data instanceof IDataSourceBrowser) {
                tooltip = ((IDataSourceBrowser) data).getDescription();
                icon = BROWSER_IMAGE;
            } else if (treeNode instanceof SourceTreeNode) {
                tooltip = ((SourceTreeNode) treeNode).getName();
                icon = SOURCE_IMAGE;
            } else if (treeNode instanceof GroupTreeNode) {
                icon = GROUP_IMAGE;
            } else if (treeNode instanceof ItemTreeNode) {
                ItemTreeNode itemNode = (ItemTreeNode) treeNode;
                tooltip = itemNode.getDescription();
                // icon
                DataType type = itemNode.getType();
                DataFormat format = itemNode.getFormat();
                if ((type != null) && (format != null)) {
                    switch (type) {
                        case SCALAR:
                            icon = SCALAR_IMAGE;
                            break;
                        case SPECTRUM:
                            switch (format) {
                                case BOOLEAN:
                                case NUMERICAL:
                                    icon = SPECTRUM_NUM_IMAGE;
                                    break;
                                default:
                                    icon = SPECTRUM_IMAGE;
                                    break;
                            }
                            break;
                        case IMAGE:
                            switch (format) {
                                case NUMERICAL:
                                    icon = IMAGE_NUM_IMAGE;
                                    break;
                                default:
                                    icon = IMAGE_IMAGE;
                                    break;
                            }
                            break;
                        default:
                            // should not happen
                            break;
                    }

                }
            }

            treeNode.setToolTip(tooltip);

            if (icon != null) {
                treeNode.setImage(icon);
            }
        }

        // Step 2: with the correctly configured ITreeNode, call parent method to render tree
        Component superComponent = super.getTreeCellRendererComponent(tree, value, sel, expanded, leaf, row,
                hasTheFocus);

        // Step 3: update UI if necessary
        if ((superComponent instanceof JLabel) && (treeNode instanceof SourceTreeNode)) {
            JLabel label = (JLabel) superComponent;
            if (label.getUI() != reverseUI) {
                label.setUI(reverseUI);
            }
        }

        // Step 4: update foreground if node matches an opened item
        if ((treeNode != null) && leaf && (treeNode.getData() instanceof IKey)) {
            boolean sourceOpened = controller.isItemOpened((IKey) treeNode.getData());
            if (sourceOpened) {
                superComponent.setForeground(Color.BLUE);
            }
        }
        return superComponent;
    }
}
