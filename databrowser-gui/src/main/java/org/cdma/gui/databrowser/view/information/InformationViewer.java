/*
 * This file is part of databrowser-gui.
 * 
 * databrowser-gui is free software: you can redistribute it and/or modify it under the terms of the GNU General Public
 * License as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later
 * version.
 * 
 * databrowser-gui is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the
 * implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for more
 * details.
 * 
 * You should have received a copy of the GNU General Public License along with databrowser-gui. If not, see
 * <https://www.gnu.org/licenses/>.
 */
package org.cdma.gui.databrowser.view.information;

import java.awt.BorderLayout;
import java.awt.Component;
import java.awt.Font;
import java.awt.Point;
import java.awt.Rectangle;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.MouseEvent;
import java.util.Map;

import javax.swing.DefaultCellEditor;
import javax.swing.JComboBox;
import javax.swing.JComponent;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTable;
import javax.swing.SortOrder;
import javax.swing.table.DefaultTableCellRenderer;
import javax.swing.table.TableCellRenderer;
import javax.swing.table.TableColumn;
import javax.swing.table.TableModel;

import org.cdma.gui.databrowser.DataBrowserController;
import org.cdma.gui.databrowser.DataBrowserItemEvent;
import org.cdma.gui.databrowser.DataSourceManager;
import org.cdma.gui.databrowser.interfaces.DataType;
import org.cdma.gui.databrowser.interfaces.IDataBrowserItemListener;
import org.cdma.gui.databrowser.interfaces.IDataSourceBrowser;
import org.cdma.gui.databrowser.interfaces.Item;
import org.cdma.gui.databrowser.view.information.InformationTableModel.Columns;
import org.cdma.gui.databrowser.view.tree.DataBrowserTree;
import org.jdesktop.swingx.JXTable;
import org.jdesktop.swingx.table.ColumnFactory;
import org.jdesktop.swingx.table.TableColumnExt;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import fr.soleil.comete.definition.event.TreeNodeSelectionEvent;
import fr.soleil.comete.definition.listener.ITreeNodeSelectionListener;
import fr.soleil.comete.definition.widget.util.ITreeNode;
import fr.soleil.comete.swing.util.EDTManager;
import fr.soleil.data.service.IKey;

public class InformationViewer extends JPanel implements IDataBrowserItemListener, ITreeNodeSelectionListener {

    private static final long serialVersionUID = -7422558401980435691L;

    private static final Logger LOGGER = LoggerFactory.getLogger(InformationViewer.class);

    private JXTable table;
    private InformationTableModel tableModel;
    private IKey currentItemKey;
    private final DataBrowserController controller;

    public InformationViewer(final DataBrowserController controller) {
        this.controller = controller;
        initComponents();
        layoutComponents();
    }

    private void initComponents() {
        tableModel = new InformationTableModel();

        table = new JXTable() {

            private static final long serialVersionUID = -6649326773223734002L;

            @Override
            public Component prepareRenderer(final TableCellRenderer renderer, final int row, final int column) {
                Component c = super.prepareRenderer(renderer, row, column);
                if (c instanceof JComponent) {
                    JComponent jc = (JComponent) c;
                    // set a tooltip on cells with hidden text
                    if (c.getPreferredSize().width > this.getCellRect(row, column, false).width) {
                        jc.setToolTipText((String) getValueAt(row, column));
                    } else {
                        jc.setToolTipText(null);
                    }
                }
                return c;
            }

            @Override
            public Point getToolTipLocation(MouseEvent event) {
                Point result = super.getToolTipLocation(event);

                if (result == null) {
                    int rowAtPoint = rowAtPoint(event.getPoint());
                    int columnAtPoint = columnAtPoint(event.getPoint());
                    Rectangle cellRect = getCellRect(rowAtPoint, columnAtPoint, true);
                    result = cellRect.getLocation();
                    result.translate(-3, -1);
                }

                return result;
            }
        };
        // set a specific column factory to allow referring each column with its enum constant
        table.setColumnFactory(new ColumnFactory() {
            @Override
            public void configureTableColumn(final TableModel model, final TableColumnExt columnExt) {
                super.configureTableColumn(model, columnExt);
                columnExt.setIdentifier(Columns.values()[columnExt.getModelIndex()]);
            }
        });
        // set the model after the column factory
        table.setModel(tableModel);

        table.setFillsViewportHeight(false);
        table.getTableHeader().setReorderingAllowed(false);
        // Value column is not sortable
        table.getColumnExt(Columns.VALUE).setSortable(false);
        // initially sort Property column in ascending order
        table.setSortOrder(Columns.PROPERTY, SortOrder.ASCENDING);

        TableColumn dataTypeColumn = table.getColumnExt(Columns.VALUE);
        DataType[] editableTypes = new DataType[] { DataType.SPECTRUM, DataType.IMAGE };
        final JComboBox<DataType> dataTypeCombo = new JComboBox<>(editableTypes);
        dataTypeCombo.setEditable(false);
        dataTypeColumn.setCellEditor(new DefaultCellEditor(dataTypeCombo));
        JComboBox<DataType> allDataTypesCombo = new JComboBox<>(editableTypes);
        allDataTypesCombo.setEditable(false);
        ComboBoxTableCellRenderer<DataType> dataTypeRenderer = new ComboBoxTableCellRenderer<>(allDataTypesCombo);
        table.setDefaultRenderer(Object.class, dataTypeRenderer);

        dataTypeCombo.addActionListener(new ActionListener() {

            @Override
            public void actionPerformed(final ActionEvent e) {
                if (controller != null) {
                    DataType dataType = (DataType) dataTypeCombo.getSelectedItem();
                    // TODO found bug
                    LOGGER.trace("controller.changeItemType({}, {})", currentItemKey, dataType);
                    controller.changeItemType(currentItemKey, dataType);
                }
            }
        });

    }

    private void layoutComponents() {
        JScrollPane tableScrollPane = new JScrollPane(table);

        setLayout(new BorderLayout());
        add(tableScrollPane, BorderLayout.CENTER);
    }

    private void setKey(final IKey itemKey) {
        setKey(itemKey == null ? null : DataSourceManager.getProducerFromKey(itemKey), itemKey);
    }

    private void setKey(final IDataSourceBrowser browser, final IKey itemKey) {
        if (currentItemKey != itemKey) {
            currentItemKey = itemKey;
            if (currentItemKey == null) {
                EDTManager.INSTANCE.runInDrawingThread(() -> {
                    tableModel.clear();
                });
            } else {
                if (browser != null) {
                    tableModel.setEditable(browser.isTypeModifiable());
                    final Map<String, String> informationMap = browser.getInformations(currentItemKey);
                    EDTManager.INSTANCE.runInDrawingThread(() -> {
                        tableModel.setInformations(informationMap);
                        // deactivated as it had some undesired side effects
//                            table.packAll();
                    });
                }
            }
        }
    }

    protected void todo() {

    }

    // ---------------------------------------
    // IDataBrowserItemListener implementation
    // ---------------------------------------

    @Override
    public void onItemSelected(final DataBrowserItemEvent event) {
        setKey(event.getItem().getKey());
    }

    @Override
    public void onItemDeselected(final DataBrowserItemEvent event) {
        if (event.getItem().getKey() == currentItemKey) {
            setKey(null);
        }
    }

    @Override
    public void onItemOpened(final DataBrowserItemEvent event) {
        Item item = event.getItem();
        if (item.getKey() != null) {
            IDataSourceBrowser browser = DataSourceManager.getProducerFromKey(item.getKey());
            if (browser != null) {
                tableModel.setEditable(browser.isTypeModifiable());
            }
        }
    }

    @Override
    public void onItemClosed(final DataBrowserItemEvent event) {
        // nop
    }

    @Override
    public void onItemAxisChanged(final DataBrowserItemEvent event) {
        // nop
    }

    // -----------------------------------------
    // ITreeNodeSelectionListener implementation
    // -----------------------------------------

    @Override
    public void selectionChanged(final TreeNodeSelectionEvent event) {
        boolean selected = event.isSelected();

        if (selected) {
            DataBrowserTree tree = (DataBrowserTree) event.getSource();
            IDataSourceBrowser selectedBrowser = tree.getSelectedBrowser();

            ITreeNode[] concernedNodes = event.getConcernedNodes();
            IKey key = null;
            for (ITreeNode selectedNode : concernedNodes) {
                Object data = selectedNode.getData();
                if ((data != null) && (data instanceof IKey)) {
                    key = (IKey) data;
                    setKey(selectedBrowser, key);
                } else {
                    setKey(null, null);
                }
            }
        } else {
            setKey(null, null);
        }
    }

    // ///////////// //
    // Inner classes //
    // ///////////// //

    protected class ComboBoxTableCellRenderer<T> extends DefaultTableCellRenderer implements TableCellRenderer {

        private static final long serialVersionUID = -2221517937430093527L;

        protected final JComboBox<T> combo;
        protected Font classicFont;
        protected Font editFont;

        public ComboBoxTableCellRenderer(JComboBox<T> combo) {
            this.combo = combo;
            this.classicFont = getFont();
            deriveFont();
        }

        @Override
        public void setFont(Font font) {
            super.setFont(font);
            if ((font != null) && (!font.isBold()) && (!font.isItalic())) {
                this.classicFont = font;
                deriveFont();
            }
        }

        protected void deriveFont() {
            if (classicFont == null) {
                editFont = null;
            } else {
                editFont = classicFont.deriveFont(classicFont.getStyle() | Font.BOLD | Font.ITALIC);
            }
        }

        @Override
        public Component getTableCellRendererComponent(JTable table, Object value, boolean isSelected, boolean hasFocus,
                int row, int column) {
            Component component = super.getTableCellRendererComponent(table, value, isSelected, hasFocus, row, column);
            if (value != null) {
                combo.setSelectedItem(value);
                if (value.equals(combo.getSelectedItem())) {
                    component = combo;
                } else {
                    boolean editable = false;
                    for (int i = 0; i < table.getColumnCount() && (!editable); i++) {
                        if (table.isCellEditable(row, i)) {
                            editable = true;
                        }
                    }
                    if (editable) {
                        component.setFont(editFont);
                    } else {
                        component.setFont(classicFont);
                    }
                }
            }
            return component;
        }
    }
}
