/*
 * This file is part of databrowser-gui.
 * 
 * databrowser-gui is free software: you can redistribute it and/or modify it under the terms of the GNU General Public
 * License as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later
 * version.
 * 
 * databrowser-gui is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the
 * implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for more
 * details.
 * 
 * You should have received a copy of the GNU General Public License along with databrowser-gui. If not, see
 * <https://www.gnu.org/licenses/>.
 */
package org.cdma.gui.databrowser.view.item;

import org.cdma.gui.databrowser.DataBrowserController;
import org.cdma.gui.databrowser.interfaces.DataFormat;
import org.cdma.gui.databrowser.interfaces.Item;
import org.cdma.gui.databrowser.model.TextSpectrumItem;

import fr.soleil.comete.box.scalarbox.StringScalarBox;

public class TextSpectrumViewer extends ARowContainerItemViewer<TextSpectrumViewer> {

    private static final long serialVersionUID = -1219029750339263153L;

    private static final int DEFAULT_TEXTAREA_NB_ROWS = 5;

    private int nbRowsForTextArea;

    public TextSpectrumViewer(DataBrowserController controller) {
        super(controller);
    }

    @Override
    protected void initComponents() {
        nbRowsForTextArea = DEFAULT_TEXTAREA_NB_ROWS;
        super.initComponents();
    }

    public int getNbRowsForTextArea() {
        return nbRowsForTextArea;
    }

    public void setNbRowsForTextArea(final int nbRowsForTextArea) {
        this.nbRowsForTextArea = nbRowsForTextArea;
    }

    @Override
    public StringScalarBox getStringBox() {
        return stringBox;
    }

    @Override
    protected TextSpectrumItem getRowContainerItem(Item item, DataFormat format) {
        TextSpectrumItem spectrumItem;
        switch (format) {
            case TEXT:
                spectrumItem = new TextSpectrumItem(this, item, controller);
                break;

            default:
                spectrumItem = null;
                break;
        }
        return spectrumItem;
    }

}
