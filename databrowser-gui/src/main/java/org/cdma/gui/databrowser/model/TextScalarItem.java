/*
 * This file is part of databrowser-gui.
 * 
 * databrowser-gui is free software: you can redistribute it and/or modify it under the terms of the GNU General Public
 * License as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later
 * version.
 * 
 * databrowser-gui is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the
 * implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for more
 * details.
 * 
 * You should have received a copy of the GNU General Public License along with databrowser-gui. If not, see
 * <https://www.gnu.org/licenses/>.
 */
package org.cdma.gui.databrowser.model;

import org.cdma.gui.databrowser.DataBrowserController;
import org.cdma.gui.databrowser.interfaces.Item;
import org.cdma.gui.databrowser.view.item.ScalarViewer;

import fr.soleil.comete.box.scalarbox.StringScalarBox;
import fr.soleil.comete.swing.AdaptableTextArea;
import fr.soleil.comete.swing.TextField;

public class TextScalarItem extends ScalarItem<AdaptableTextArea, TextField> {

    public TextScalarItem(final ScalarViewer scalarViewer, final Item item, DataBrowserController controller) {
        super(scalarViewer, item, controller);
    }

    @Override
    public AdaptableTextArea createViewer() {
        return createArea();
    }

    @Override
    public TextField createSetter() {
        TextField theSetter = new TextField();
        theSetter.setOpaque(false);

        return theSetter;
    }

    @Override
    public void doConnect() {
        super.doConnect();

        StringScalarBox stringBox = getItemViewer().getStringBox();

        stringBox.connectWidget(viewer, readKey);
        if (writeKey != null) {
            stringBox.connectWidget(setter, writeKey);
        }
    }

    @Override
    protected void disconnectImmediately() {
        StringScalarBox stringBox = getItemViewer().getStringBox();

        stringBox.disconnectWidget(viewer, readKey);
        if (writeKey != null) {
            stringBox.disconnectWidget(setter, writeKey);
        }
    }

}
