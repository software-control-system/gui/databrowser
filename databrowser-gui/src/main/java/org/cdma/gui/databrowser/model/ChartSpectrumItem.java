/*
 * This file is part of databrowser-gui.
 * 
 * databrowser-gui is free software: you can redistribute it and/or modify it under the terms of the GNU General Public
 * License as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later
 * version.
 * 
 * databrowser-gui is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the
 * implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for more
 * details.
 * 
 * You should have received a copy of the GNU General Public License along with databrowser-gui. If not, see
 * <https://www.gnu.org/licenses/>.
 */
package org.cdma.gui.databrowser.model;

import java.lang.ref.WeakReference;
import java.util.HashSet;
import java.util.Set;

import org.cdma.gui.databrowser.DataBrowserController;
import org.cdma.gui.databrowser.interfaces.AxisType;
import org.cdma.gui.databrowser.interfaces.DataFormat;
import org.cdma.gui.databrowser.interfaces.DataType;
import org.cdma.gui.databrowser.interfaces.IChartSpectrumItemListener;
import org.cdma.gui.databrowser.interfaces.IChartSpectrumManager;
import org.cdma.gui.databrowser.interfaces.Item;
import org.cdma.gui.databrowser.view.item.ChartSpectrumViewer;

import fr.soleil.comete.box.complexbox.ChartViewerBox;
import fr.soleil.comete.definition.widget.IChartViewer;
import fr.soleil.comete.definition.widget.properties.PlotProperties;
import fr.soleil.comete.swing.Chart;
import fr.soleil.data.service.IKey;
import fr.soleil.lib.project.ObjectUtils;

public class ChartSpectrumItem extends AbstractItem<Chart> implements IChartSpectrumManager {

    private static final double HIGHLIGHT_COEFFICIENT = 3;

    private final Set<IChartSpectrumItemListener> chartSpectrumItemListeners;
    private final WeakReference<ChartSpectrumViewer> chartSpectrumViewerRef;

    private DataBrowserController controller;
    private IKey currentReadKey;

    private double highlightCoeff;
    private int highlightMethod;

    private boolean markerEnabled;

    public ChartSpectrumItem(final ChartSpectrumViewer chartSpectrumViewer, final Item item,
            DataBrowserController controller, boolean markerEnabled) {
        super(item, controller);

        this.chartSpectrumViewerRef = chartSpectrumViewer == null ? null : new WeakReference<>(chartSpectrumViewer);
        this.controller = controller;
        this.markerEnabled = markerEnabled;

        chartSpectrumItemListeners = new HashSet<>();

        viewer = chartSpectrumViewer.getChartViewer();

        highlightCoeff = Double.NaN;
        highlightMethod = -1;
    }

    @Override
    public void computeKeys() {
        IChartViewer chartViewer = viewer;
        chartViewer.setAxisName(ObjectUtils.EMPTY_STRING, IChartViewer.X);
        super.computeKeys();

        Item xLocalItem = getXItem();
        if (xLocalItem == null) {
            ChartSpectrumViewer chartSpectrumViewer = ObjectUtils.recoverObject(chartSpectrumViewerRef);
            if (chartSpectrumViewer != null) {
                xLocalItem = chartSpectrumViewer.getXItem();
                if (xLocalItem != null) {
                    String xName = xLocalItem.getBrowser().getDisplayName(xLocalItem.getKey());
                    chartViewer.setAxisName(xName, IChartViewer.X);
                }
            }
        }

        if ((xLocalItem != null) && (xLocalItem != this)) {
            currentReadKey = ChartViewerBox.createDualKey(xLocalItem.getKey(), readKey);
        } else {
            currentReadKey = readKey;
        }
    }

    @Override
    protected void doConnect() {
        ChartSpectrumViewer chartSpectrumViewer = ObjectUtils.recoverObject(chartSpectrumViewerRef);
        if (chartSpectrumViewer != null) {
            Item xLocalItem = chartSpectrumViewer.getXItem();
            if (xLocalItem != this) {
                IChartViewer chartViewer = viewer;
                ChartViewerBox chartBox = chartSpectrumViewer.getChartBox();

                if (getType() == DataType.IMAGE) {
                    chartBox.setSplitMatrixThroughColumns(currentReadKey,
                            ((axis == AxisType.Y1_COLUMN) || (axis == AxisType.Y2_COLUMN)));
                }
                if (controller != null) {
                    controller.setChartViewer(chartViewer);
                }
                chartBox.connectWidget(chartViewer, currentReadKey);

                applySelectionStyle();

                applyCurveStyle();

                applyDisplayName();

                applyAxisStyle();
            }
        }
    }

    @Override
    protected void disconnectImmediately() {
        ChartSpectrumViewer chartSpectrumViewer = ObjectUtils.recoverObject(chartSpectrumViewerRef);
        if (chartSpectrumViewer != null) {
            IChartViewer chartViewer = viewer;
            ChartViewerBox chartBox = chartSpectrumViewer.getChartBox();
            chartBox.disconnectWidget(chartViewer, currentReadKey);
        }
    }

    @Override
    public void setSelected(final boolean selected) {
        super.setSelected(selected);

        applySelectionStyle();
        applyAxisStyle();
    }

    @Override
    public void setAxis(final AxisType axis) {
        super.setAxis(axis);

        applyAxisStyle();
    }

    private int getNbCurves() {
        int result = 0;

        switch (getType()) {
            case SPECTRUM:
                result = 1;
                break;
            case IMAGE:
                switch (axis) {
                    case Y1_COLUMN:
                    case Y2_COLUMN:
                        result = shape[shape.length - 1];
                        break;
                    case Y1_ROW:
                    case Y2_ROW:
                        result = shape[shape.length - 2];
                        break;

                    default:
                        // nop
                        break;
                }
                break;

            default:
                // nop
                break;
        }

        return result;
    }

    protected void applyDisplayName() {
        ChartSpectrumViewer chartSpectrumViewer = ObjectUtils.recoverObject(chartSpectrumViewerRef);
        if ((chartSpectrumViewer != null) && (readKey != null)) {
            IChartViewer chartViewer = chartSpectrumViewer.getChartViewer();

            // Get display name
            String readDataViewName = browser.getDisplayName(readKey);

            // Set display name for image data item
            int nbCurves = getNbCurves();
            if (nbCurves > 1) {
                String readDataViewId = readKey.getInformationKey();
                for (int i = 0; i < nbCurves; i++) {
                    String readId = readDataViewId + "_" + i;
                    String readName = readDataViewName + "_" + i;
                    chartViewer.setDataViewDisplayName(readId, readName);
                }
            } else {
                String readDataViewId = readKey.getInformationKey();
                chartViewer.setDataViewDisplayName(readDataViewId, readDataViewName);
            }
        }
    }

    protected void applyCurveStyle() {
        ChartSpectrumViewer chartSpectrumViewer = ObjectUtils.recoverObject(chartSpectrumViewerRef);
        if ((chartSpectrumViewer != null) && (readKey != null)) {
            IChartViewer chartViewer = chartSpectrumViewer.getChartViewer();

            int nbCurves = getNbCurves();
            int pointCount;
            int[] shape = this.shape;
            if ((shape != null) && (shape.length > 0)) {
                pointCount = shape[shape.length - 1];
            } else {
                pointCount = 1;
            }
            if (nbCurves > 1) {
                String readDataViewId = readKey.getInformationKey();

                for (int i = 0; i < nbCurves; i++) {
                    String readId = readDataViewId + "_" + i;

                    if (format == DataFormat.BOOLEAN) {
                        chartViewer.setDataViewStyle(readId, IChartViewer.TYPE_STAIRS);
                    }
                    if (markerEnabled && (pointCount <= MAX_MARKER_COUNT)) {
                        // set marker style
                        chartViewer.setDataViewMarkerStyle(readId, IChartViewer.MARKER_DOT);
                    }
                }
            } else {
                String readDataViewId = readKey.getInformationKey();

                if (format == DataFormat.BOOLEAN) {
                    chartViewer.setDataViewStyle(readDataViewId, IChartViewer.TYPE_STAIRS);
                }
                // set marker style
                if (markerEnabled && (pointCount <= MAX_MARKER_COUNT)) {
                    chartViewer.setDataViewMarkerStyle(readDataViewId, IChartViewer.MARKER_DOT);
                }
            }
        }
    }

    public void applySelectionStyle() {
        ChartSpectrumViewer chartSpectrumViewer = ObjectUtils.recoverObject(chartSpectrumViewerRef);
        if ((chartSpectrumViewer != null) && (readKey != null)) {
            IChartViewer chartViewer = chartSpectrumViewer.getChartViewer();

            String readDataViewId = readKey.getInformationKey();

            int nbCurves = getNbCurves();
            if (nbCurves > 1) {
                for (int i = 0; i < nbCurves; i++) {
                    String readId = readDataViewId + "_" + i;
                    if (Double.isNaN(highlightCoeff)) {
                        highlightCoeff = chartViewer.getDataViewHighlightCoefficient(readId);
                        highlightMethod = chartViewer.getDataViewHighlightMethod(readId);
                    }
                    if (selected) {
                        chartViewer.setDataViewHighlightCoefficient(readId, HIGHLIGHT_COEFFICIENT);
                        chartViewer.setDataViewHighlightMethod(readId, IChartViewer.HIGHLIGHT_ADD);
                    } else {
                        chartViewer.setDataViewHighlightCoefficient(readId, highlightCoeff);
                        chartViewer.setDataViewHighlightMethod(readId, highlightMethod);
                    }
                    chartViewer.setDataViewHighlighted(readId, selected);
                }

            } else {
                if (Double.isNaN(highlightCoeff)) {
                    highlightCoeff = chartViewer.getDataViewHighlightCoefficient(readDataViewId);
                    highlightMethod = chartViewer.getDataViewHighlightMethod(readDataViewId);
                }
                if (selected) {
                    chartViewer.setDataViewHighlightCoefficient(readDataViewId, HIGHLIGHT_COEFFICIENT);
                    chartViewer.setDataViewHighlightMethod(readDataViewId, IChartViewer.HIGHLIGHT_ADD);
                } else {
                    chartViewer.setDataViewHighlightCoefficient(readDataViewId, highlightCoeff);
                    chartViewer.setDataViewHighlightMethod(readDataViewId, highlightMethod);
                }
                chartViewer.setDataViewHighlighted(readDataViewId, selected);
            }
        }
    }

    protected void applyAxisStyle() {
        ChartSpectrumViewer chartSpectrumViewer = ObjectUtils.recoverObject(chartSpectrumViewerRef);
        if ((chartSpectrumViewer != null) && (readKey != null)) {
            IChartViewer chartViewer = chartSpectrumViewer.getChartViewer();
            PlotProperties dataViewPlotProperties = null;
            int chartAxis = -1;
            int nbCurves = getNbCurves();
            if (nbCurves > 1) {
                String readDataViewId = readKey.getInformationKey();
                if ((axis == AxisType.Y1_ROW) || (axis == AxisType.Y1_COLUMN)) {
                    for (int i = 0; i < nbCurves; i++) {
                        String readId = readDataViewId + "_" + i;
                        dataViewPlotProperties = chartViewer.getDataViewPlotProperties(readId);
                        if (dataViewPlotProperties != null) {
                            dataViewPlotProperties.setAxisChoice(IChartViewer.Y1);
                            chartViewer.setDataViewPlotProperties(readId, dataViewPlotProperties);
                        }
                        chartViewer.setDataViewAxis(readId, IChartViewer.Y1);
                    }
                } else if ((axis == AxisType.Y2_ROW) || (axis == AxisType.Y2_COLUMN)) {
                    for (int i = 0; i < nbCurves; i++) {
                        String readId = readDataViewId + "_" + i;
                        dataViewPlotProperties = chartViewer.getDataViewPlotProperties(readId);
                        if (dataViewPlotProperties != null) {
                            dataViewPlotProperties.setAxisChoice(IChartViewer.Y2);
                            chartViewer.setDataViewPlotProperties(readId, dataViewPlotProperties);
                        }
                        chartViewer.setDataViewAxis(readId, IChartViewer.Y2);
                    }
                }

            } else {
                String readDataViewId = readKey.getInformationKey();
                dataViewPlotProperties = chartViewer.getDataViewPlotProperties(readDataViewId);

                if (axis == AxisType.Y1) {
                    chartAxis = IChartViewer.Y1;

                } else if (axis == AxisType.Y2) {
                    chartAxis = IChartViewer.Y2;
                }

                if (dataViewPlotProperties != null) {
                    dataViewPlotProperties.setAxisChoice(chartAxis);
                    chartViewer.setDataViewPlotProperties(readDataViewId, dataViewPlotProperties);
                }
                chartViewer.setDataViewAxis(readDataViewId, chartAxis);
            }
        }
    }

    @Override
    public void selectedIndexChanged(final int[] newOrigins) {
        origins = newOrigins;

        fireItemPlayerIndexChanged(this);
    }

    public void addChartSpectrumItemListener(final IChartSpectrumItemListener listener) {
        if (listener != null) {
            chartSpectrumItemListeners.add(listener);
        }
    }

    public void removeChartSpectrumItemListener(final IChartSpectrumItemListener listener) {
        if (listener != null) {
            chartSpectrumItemListeners.remove(listener);
        }
    }

    private void fireItemPlayerIndexChanged(final ChartSpectrumItem item) {
        for (IChartSpectrumItemListener listener : chartSpectrumItemListeners) {
            listener.onItemPlayerIndexChanged(item);
        }
    }

    public boolean isMarkerEnabled() {
        return markerEnabled;
    }

    public void setMarkerEnabled(boolean markerEnabled) {
        this.markerEnabled = markerEnabled;
    }

}
