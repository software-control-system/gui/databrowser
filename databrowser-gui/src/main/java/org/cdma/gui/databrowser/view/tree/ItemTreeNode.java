/*
 * This file is part of databrowser-gui.
 * 
 * databrowser-gui is free software: you can redistribute it and/or modify it under the terms of the GNU General Public
 * License as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later
 * version.
 * 
 * databrowser-gui is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the
 * implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for more
 * details.
 * 
 * You should have received a copy of the GNU General Public License along with databrowser-gui. If not, see
 * <https://www.gnu.org/licenses/>.
 */
package org.cdma.gui.databrowser.view.tree;

import org.cdma.gui.databrowser.interfaces.DataFormat;
import org.cdma.gui.databrowser.interfaces.DataType;

import fr.soleil.comete.definition.widget.util.BasicTreeNode;

public class ItemTreeNode extends BasicTreeNode {

    private boolean isPropertySet = false;

    private String description;
    private DataType type;
    private DataFormat format;

    public String getDescription() {
        return description;
    }

    public void setDescription(final String description) {
        this.description = description;
        isPropertySet = true;
    }

    public DataType getType() {
        return type;
    }

    public void setType(final DataType type) {
        this.type = type;
        isPropertySet = true;
    }

    public DataFormat getFormat() {
        return format;
    }

    public void setFormat(final DataFormat format) {
        this.format = format;
        isPropertySet = true;
    }

    public boolean isPropertySet() {
        return isPropertySet;
    }

}
