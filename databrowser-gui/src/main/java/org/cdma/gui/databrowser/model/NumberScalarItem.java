/*
 * This file is part of databrowser-gui.
 * 
 * databrowser-gui is free software: you can redistribute it and/or modify it under the terms of the GNU General Public
 * License as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later
 * version.
 * 
 * databrowser-gui is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the
 * implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for more
 * details.
 * 
 * You should have received a copy of the GNU General Public License along with databrowser-gui. If not, see
 * <https://www.gnu.org/licenses/>.
 */
package org.cdma.gui.databrowser.model;

import org.cdma.gui.databrowser.DataBrowserController;
import org.cdma.gui.databrowser.interfaces.Item;
import org.cdma.gui.databrowser.view.item.ScalarViewer;

import fr.soleil.comete.box.scalarbox.NumberScalarBox;
import fr.soleil.comete.box.scalarbox.StringScalarBox;
import fr.soleil.comete.swing.TextField;
import fr.soleil.comete.swing.WheelSwitch;

public class NumberScalarItem extends ScalarItem<TextField, WheelSwitch> {

    public NumberScalarItem(final ScalarViewer scalarViewer, final Item item, DataBrowserController controller) {
        super(scalarViewer, item, controller);
    }

    @Override
    public TextField createViewer() {
        TextField theViewer = new TextField();

        return theViewer;
    }

    @Override
    public WheelSwitch createSetter() {
        WheelSwitch theSetter = new WheelSwitch();
        theSetter.setInvertionLogic(true);
        theSetter.setOpaque(false);
        return theSetter;
    }

    @Override
    public void doConnect() {
        super.doConnect();

        StringScalarBox stringBox = getItemViewer().getStringBox();
        stringBox.connectWidget(viewer, readKey);

        if (writeKey != null) {
            NumberScalarBox numberBox = getItemViewer().getNumberBox();
            numberBox.connectWidget(setter, writeKey);
        }
    }

    @Override
    protected void disconnectImmediately() {
        StringScalarBox stringBox = getItemViewer().getStringBox();
        stringBox.disconnectWidget(viewer, readKey);

        if (writeKey != null) {
            NumberScalarBox numberBox = getItemViewer().getNumberBox();
            numberBox.disconnectWidget(setter, writeKey);
        }
    }

}
