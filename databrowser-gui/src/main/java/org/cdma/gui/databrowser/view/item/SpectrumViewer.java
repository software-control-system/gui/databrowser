/*
 * This file is part of databrowser-gui.
 * 
 * databrowser-gui is free software: you can redistribute it and/or modify it under the terms of the GNU General Public
 * License as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later
 * version.
 * 
 * databrowser-gui is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the
 * implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for more
 * details.
 * 
 * You should have received a copy of the GNU General Public License along with databrowser-gui. If not, see
 * <https://www.gnu.org/licenses/>.
 */
package org.cdma.gui.databrowser.view.item;

import java.awt.Component;
import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.Map;

import javax.swing.JComponent;
import javax.swing.JFrame;
import javax.swing.JTabbedPane;
import javax.swing.WindowConstants;

import org.cdma.gui.databrowser.DataBrowser;
import org.cdma.gui.databrowser.DataBrowserController;
import org.cdma.gui.databrowser.interfaces.DataFormat;
import org.cdma.gui.databrowser.interfaces.IItemViewer;
import org.cdma.gui.databrowser.interfaces.Item;
import org.cdma.gui.databrowser.model.AbstractItem;

import fr.soleil.comete.definition.widget.properties.ChartProperties;
import fr.soleil.comete.definition.widget.properties.PlotProperties;
import fr.soleil.data.service.IKey;
import fr.soleil.data.target.scalar.IBooleanTarget;
import fr.soleil.docking.view.IView;

public class SpectrumViewer extends JTabbedPane implements IItemViewer {

    private static final long serialVersionUID = 7637203608555412018L;

    private ChartSpectrumViewer chartSpectrumViewer;
    private TextSpectrumViewer stringSpectrumViewer;
    private Map<String, IView> spectrumViewerMap = new HashMap<>();

    private DataBrowserController controller;

    private boolean activTabForWritableItem = false;
    private boolean openTabWithManyItem = false;
    private boolean allItemsIsInTab = false;

    private ChartSpectrumViewer manyTabChartSpectrum = null;
    private int counter = 0;

    private JComponent dockingArea = null;

    public SpectrumViewer(DataBrowserController controller) {
        this(controller, false);
        this.controller = controller;
    }

    public SpectrumViewer(final DataBrowserController controller, boolean activTabForWritableItem) {
        this.activTabForWritableItem = activTabForWritableItem;
        this.controller = controller;
        initComponents(controller);
        layoutComponent();
    }

    public void initComponents(DataBrowserController controller) {
        chartSpectrumViewer = new ChartSpectrumViewer(controller);
        stringSpectrumViewer = new TextSpectrumViewer(controller);
    }

    public IBooleanTarget getMarkerEnabledTarget() {
        return chartSpectrumViewer;
    }

    private void layoutComponent() {
        addTab(DataBrowser.MESSAGES.getString("DataItem.Spectrum.Tab.String"),
                DataBrowser.ICONS.getIcon("DataItem.Spectrum.Tab.String"), stringSpectrumViewer);

        if (activTabForWritableItem) {
            addTab(DataBrowser.MESSAGES.getString("DataItem.Spectrum.Tab.Chart"),
                    DataBrowser.ICONS.getIcon("DataItem.Spectrum.Tab.Chart"), getDockingArea());

            controller.getDockingUtil().createView("MultiChart",
                    DataBrowser.ICONS.getIcon("DataItem.Spectrum.Tab.Chart"), "MULTI CHART", chartSpectrumViewer,
                    getDockingArea());
        } else {
            addTab(DataBrowser.MESSAGES.getString("DataItem.Spectrum.Tab.Chart"),
                    DataBrowser.ICONS.getIcon("DataItem.Spectrum.Tab.Chart"), chartSpectrumViewer);
        }

    }

    private IItemViewer getItemViewer(final DataFormat format) {
        IItemViewer itemViewer = null;
        switch (format) {
            case NUMERICAL:
            case BOOLEAN:
                itemViewer = chartSpectrumViewer;
                break;
            case TEXT:
                itemViewer = stringSpectrumViewer;
                break;
            default:
                // nop
                break;
        }

        return itemViewer;
    }

    @Override
    public Collection<Runnable> addItem(Item item) {
        Collection<Runnable> toRunInEDT = new ArrayList<>();
        if (item != null) {
            IItemViewer viewer = getItemViewer(item.getFormat());
            if (viewer != null) {
                viewer.addItem(item);
            }

            DataFormat format = item.getFormat();
            Runnable r = null;
            if (activTabForWritableItem && format != DataFormat.TEXT && !openTabWithManyItem) {
                r = addSettableItem(item);
            } else if (openTabWithManyItem) {
                r = createTabWithManyItems(item);
            }
            if (r != null) {
                toRunInEDT.add(r);
            }
        }
        return toRunInEDT;
    }

    private Runnable createTabWithManyItems(Item item) {
        Runnable toRunInEDT;
        if (manyTabChartSpectrum == null) {
            toRunInEDT = createDockingViewForManyItems();
        } else if (allItemsIsInTab) {
            toRunInEDT = createDockingViewForManyItems();
            allItemsIsInTab = false;
        } else {
            toRunInEDT = null;
        }
        manyTabChartSpectrum.addItem(item);
        return toRunInEDT;
    }

    private Runnable createDockingViewForManyItems() {
        counter = counter + 1;
        String name = "Custom chart n°" + String.valueOf(counter);
        manyTabChartSpectrum = new ChartSpectrumViewer(null);
        dockingArea = getDockingArea();
        return () -> {
            IView createView = controller.getDockingUtil().createView(name,
                    DataBrowser.ICONS.getIcon("DataItem.Spectrum.Tab.Chart"), name, manyTabChartSpectrum, dockingArea);
            spectrumViewerMap.put(name, createView);
            createView.setClosable(true);
        };
    }

    private Runnable addSettableItem(Item item) {
        Runnable toRunInEDT = null;
        String name = getDataItemName(item);

        ChartSpectrumViewer tabChartSpectrumViewer = new ChartSpectrumViewer(null);
        tabChartSpectrumViewer.addItem(item);

        // Manage settable key
        IKey settableKey = item.getBrowser().generateSettableKey(item.getKey());
        if (settableKey != null) {
            Item itemWritable = new Item(settableKey, item.getSourceKey(), item.getBrowser(), item.getType(),
                    item.getFormat());
            tabChartSpectrumViewer.addItem(itemWritable);
        }

        dockingArea = getDockingArea();

        if ((dockingArea != null) && (tabChartSpectrumViewer != null)) {
            toRunInEDT = () -> {
                IView createView = controller.getDockingUtil().createView(name,
                        DataBrowser.ICONS.getIcon("DataItem.Spectrum.Tab.Chart"), name, tabChartSpectrumViewer,
                        dockingArea);
                spectrumViewerMap.put(name, createView);
                createView.setClosable(true);
            };
        }
        return toRunInEDT;
    }

    private String getDataItemName(Item item) {
        String name = item.getBrowser().getDisplayName(item.getKey());
        if (name.contains("/")) {
            String[] split = name.split("/");
            name = split[split.length - 1];
        }
        return name;
    }

    public JComponent getDockingArea() {
        if (dockingArea == null) {
            dockingArea = controller.getDockingUtil().createDockingArea(true, true);
        }
        return dockingArea;
    }

    @Override
    public Collection<Runnable> removeItem(Item item) {
        Collection<Runnable> toRunInEDT = new ArrayList<>();
        if (item != null) {
            IItemViewer viewer = getItemViewer(item.getFormat());
            if (viewer != null) {
                viewer.removeItem(item);
            }
        }

        if (activTabForWritableItem) {
            String dataItemName = getDataItemName(item);
            toRunInEDT.add(() -> {
                spectrumViewerMap.remove(dataItemName);
                controller.getDockingUtil().removeView(dataItemName);
            });
        }
        return toRunInEDT;
    }

    public void setValueConvertor(IKey key) {
        chartSpectrumViewer.setValueConvertor(key);
    }

    public AbstractItem<?> getXSpectrumItem() {
        return chartSpectrumViewer.getXItem();
    }

    public ChartProperties getChartProperties() {
        return chartSpectrumViewer.getChartProperties();
    }

    public void setChartProperties(final ChartProperties properties) {
        chartSpectrumViewer.setChartProperties(properties);
    }

    public String getChartSnapshotLocation() {
        return chartSpectrumViewer.getChartSnapshotLocation();
    }

    public void setChartSnapshotLocation(final String path) {
        chartSpectrumViewer.setChartSnapshotLocation(path);
    }

    public String getChartDataLocation() {
        return chartSpectrumViewer.getChartDataLocation();
    }

    public void setChartDataLocation(final String path) {
        chartSpectrumViewer.setChartDataLocation(path);
    }

    public PlotProperties getChartDataViewPlotProperties(final String id) {
        return chartSpectrumViewer.getChartDataViewPlotProperties(id);
    }

    public void setChartDataViewPlotProperties(final IKey key, final PlotProperties plotProperties, final Item item) {
        chartSpectrumViewer.setChartDataViewPlotProperties(key, plotProperties, item);
    }

    public Collection<String> getChartKnownDataIds() {
        return chartSpectrumViewer.getChartKnownDataIds();
    }

    public PlotProperties getChartDataViewPlotProperties(final IKey key) {
        return chartSpectrumViewer.getChartDataViewPlotProperties(key);
    }

    public boolean isOpenTabWithManyItem() {
        return openTabWithManyItem;
    }

    public void setOpenTabWithManyItem(boolean openTabWithManyItem) {
        this.openTabWithManyItem = openTabWithManyItem;
    }

    public boolean isAllItemsIsInTab() {
        return allItemsIsInTab;
    }

    public void setAllItemsIsInTab(boolean allItemsIsInTab) {
        this.allItemsIsInTab = allItemsIsInTab;
    }

    public static void main(String[] args) {
        JFrame frame = new JFrame();
        frame.setContentPane(new SpectrumViewer(null, false));
        frame.setVisible(true);
        frame.setSize(500, 500);
        frame.setDefaultCloseOperation(WindowConstants.EXIT_ON_CLOSE);

    }

    @Override
    public Collection<Runnable> selectItem(Item item) {
        Collection<Runnable> toRunInEDT = new ArrayList<>();
        IItemViewer viewer = getItemViewer(item.getFormat());
        if (viewer != null) {
            Collection<Runnable> tmp = viewer.selectItem(item);
            toRunInEDT.addAll(tmp);
            tmp.clear();
        }

        toRunInEDT.add(() -> {
            if (activTabForWritableItem && viewer == chartSpectrumViewer) {
                setSelectedComponent(getDockingArea());
            } else {
                setSelectedComponent((Component) viewer);
            }
        });

        if (item != null && activTabForWritableItem) {
            String dataItemName = getDataItemName(item);
            IView iView = spectrumViewerMap.get(dataItemName);
            if (iView != null) {
                toRunInEDT.add(() -> {
                    iView.select();
                });
            }
        }
        return toRunInEDT;
    }

    @Override
    public Collection<Runnable> deselectItem(Item item) {
        Collection<Runnable> toRunInEDT = null;
        if (item != null) {
            IItemViewer viewer = getItemViewer(item.getFormat());
            if (viewer != null) {
                toRunInEDT = viewer.deselectItem(item);
            }
        }
        if (toRunInEDT == null) {
            toRunInEDT = new ArrayList<>();
        }
        return toRunInEDT;
    }

    @Override
    public Collection<Runnable> clearSelection() {
        Collection<Runnable> toRunInEDT = new ArrayList<>();
        Collection<Runnable> tmp = chartSpectrumViewer.clearSelection();
        toRunInEDT.addAll(tmp);
        tmp.clear();
        tmp = stringSpectrumViewer.clearSelection();
        toRunInEDT.addAll(tmp);
        tmp.clear();
        return toRunInEDT;
    }
}
