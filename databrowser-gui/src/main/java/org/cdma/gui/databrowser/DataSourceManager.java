/*
 * This file is part of databrowser-gui.
 * 
 * databrowser-gui is free software: you can redistribute it and/or modify it under the terms of the GNU General Public
 * License as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later
 * version.
 * 
 * databrowser-gui is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the
 * implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for more
 * details.
 * 
 * You should have received a copy of the GNU General Public License along with databrowser-gui. If not, see
 * <https://www.gnu.org/licenses/>.
 */
package org.cdma.gui.databrowser;

import java.lang.reflect.Modifier;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.LinkedList;
import java.util.List;
import java.util.Set;

import org.cdma.gui.databrowser.impl.DataSourceSeeker;
import org.cdma.gui.databrowser.interfaces.IDataSourceBrowser;
import org.cdma.gui.databrowser.plugintree.BrowserPluginNode;
import org.cdma.gui.databrowser.plugintree.PluginTreeTableModel;
import org.cdma.gui.databrowser.plugintree.SourceBrowserNode;
import org.jdesktop.swingx.treetable.DefaultMutableTreeTableNode;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import fr.soleil.data.service.IKey;

public class DataSourceManager {

    private static final Logger LOGGER = LoggerFactory.getLogger(DataSourceManager.class);

    private static final String DISCOVERED_PLUGIN = "Discovered plugin %s";

    // list of all available browsers or plug-ins exposed to user
    private static final Collection<IDataSourceBrowser> DATA_SOURCE_BROWSER_LIST = new LinkedList<>();

    // list of underlying data source browsers used to search for producer
    private static final Collection<IDataSourceBrowser> DATA_SOURCE_BROWSER_IMPL_LIST = new LinkedList<>();

    // EXPDATA-127: Keep trace of discovered plugins
    private static final Collection<String> PLUGIN_INFO_LIST = new ArrayList<>();
    public static final Collection<String> PLUGIN_INFO = Collections.unmodifiableCollection(PLUGIN_INFO_LIST);

    private static PluginTreeTableModel treeModel;

    static {
        loadBrowserMaps();
    }

    private static void loadBrowserMaps() {
        // get available browsers
        Set<Class<? extends IDataSourceBrowser>> implementationSet = DataSourceSeeker
                .getDataSourceBrowserImplementations();
        IDataSourceBrowser sourceBrowser = null;
        for (Class<? extends IDataSourceBrowser> implementation : implementationSet) {
            try {
                // Don't try to run the constructor of an abstract class
                if (!Modifier.isAbstract(implementation.getModifiers())) {
                    sourceBrowser = implementation.newInstance();
                    if (sourceBrowser != null) {
                        boolean sourceBrowserEnabled = sourceBrowser.isSourceBrowserEnabled();
                        if (sourceBrowserEnabled) {
                            DATA_SOURCE_BROWSER_IMPL_LIST.add(sourceBrowser);
                        } else {
                            LOGGER.debug("{} source browsing is disabled", sourceBrowser.getName());
                        }
                        // Each IDataSourceBrowser can have sub-sources, that we try to check here
                        List<IDataSourceBrowser> dataBrowserSourceList = sourceBrowser.getDataSourceBrowserList();
                        if (dataBrowserSourceList != null) {
                            for (IDataSourceBrowser source : dataBrowserSourceList) {
                                // whether source is active and can be used
                                if (source.isSourceBrowserEnabled()) {
                                    DATA_SOURCE_BROWSER_LIST.add(source);
                                    // EXPDATA-127: trace discovered plugin
                                    String message = String.format(DISCOVERED_PLUGIN, source.getName());
                                    LOGGER.info(message);
                                    PLUGIN_INFO_LIST.add(message);
                                }
                            }
                        } else if (sourceBrowserEnabled) {
                            DATA_SOURCE_BROWSER_LIST.add(sourceBrowser);
                            // EXPDATA-127: trace discovered plugin
                            String message = String.format(DISCOVERED_PLUGIN, sourceBrowser.getName());
                            LOGGER.info(DISCOVERED_PLUGIN, sourceBrowser.getName());
                            PLUGIN_INFO_LIST.add(message);
                        }
                    } // end if (sourceBrowser != null)
                } // end if (!Modifier.isAbstract(implementation.getModifiers()))
            } catch (Exception ignore) {
                // EXPDATA-127: trace plugin error
                String message = String.format("Impossible to load browsers %s: %s",
                        sourceBrowser == null ? implementation.getSimpleName() : sourceBrowser.getName(),
                        ignore.getMessage());
                LOGGER.error(message);
                PLUGIN_INFO_LIST.add(message);
                LOGGER.debug("Browsers loading error stack trace", ignore);
            } finally {
                sourceBrowser = null;
            }
        }
    }

    private static void createTreeTableModel() {
        DefaultMutableTreeTableNode root = new DefaultMutableTreeTableNode("Available sources");
        treeModel = new PluginTreeTableModel(root);
        for (IDataSourceBrowser sourceBrowser : DATA_SOURCE_BROWSER_IMPL_LIST) {
            if (sourceBrowser != null) {
                SourceBrowserNode sourceNode = new SourceBrowserNode(sourceBrowser);
                root.add(sourceNode);
                List<IDataSourceBrowser> dataBrowserSourceList = sourceBrowser.getDataSourceBrowserList();
                if (dataBrowserSourceList != null) {
                    for (IDataSourceBrowser plugin : dataBrowserSourceList) {
                        if (plugin != null) {
                            sourceNode.add(new BrowserPluginNode(plugin));
                        }
                    }
                }
            }
        }
    }

    public static IDataSourceBrowser getDataSourceBrowserForId(final String producterId) {
        IDataSourceBrowser browser = null;
        for (IDataSourceBrowser sourceBrowser : DATA_SOURCE_BROWSER_IMPL_LIST) {
            if (sourceBrowser.getId().equals(producterId)) {
                browser = sourceBrowser;
                break;
            }
        }
        return browser;
    }

    public static PluginTreeTableModel getPluginTreeTableModel() {
        if (treeModel == null) {
            createTreeTableModel();
        }
        return treeModel;
    }

    public static List<IDataSourceBrowser> getBrowsers() {
        return new ArrayList<>(DATA_SOURCE_BROWSER_LIST);
    }

    public static boolean isSingleBrowser() {
        return (DATA_SOURCE_BROWSER_LIST.size() == 1);
    }

    public static IDataSourceBrowser getProducerFromString(final String sourcePath) {
        IDataSourceBrowser result = null;
        for (IDataSourceBrowser browser : DATA_SOURCE_BROWSER_IMPL_LIST) {
            result = browser.getDataSourceBrowser(sourcePath);
            if (result != null) {
                break;
            }
        }
        return result;
    }

    public static IDataSourceBrowser getProducerFromKey(final IKey sourceKey) {
        IDataSourceBrowser result = null;
        for (IDataSourceBrowser browser : DATA_SOURCE_BROWSER_IMPL_LIST) {
            result = browser.getDataSourceBrowser(sourceKey);
            if (result != null) {
                break;
            }
        }
        return result;
    }

}
