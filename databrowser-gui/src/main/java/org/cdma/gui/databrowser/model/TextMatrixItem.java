/*
 * This file is part of databrowser-gui.
 * 
 * databrowser-gui is free software: you can redistribute it and/or modify it under the terms of the GNU General Public
 * License as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later
 * version.
 * 
 * databrowser-gui is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the
 * implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for more
 * details.
 * 
 * You should have received a copy of the GNU General Public License along with databrowser-gui. If not, see
 * <https://www.gnu.org/licenses/>.
 */
package org.cdma.gui.databrowser.model;

import javax.swing.JPanel;

import org.cdma.gui.databrowser.DataBrowserController;
import org.cdma.gui.databrowser.interfaces.Item;
import org.cdma.gui.databrowser.view.item.MatrixViewer;

import fr.soleil.comete.box.matrixbox.StringMatrixBox;
import fr.soleil.comete.swing.StringMatrixTable;
import fr.soleil.lib.project.ObjectUtils;

public class TextMatrixItem extends MatrixItem<StringMatrixTable> {

    public TextMatrixItem(final MatrixViewer matrixViewer, final Item item, DataBrowserController controller) {
        super(matrixViewer, item, controller);
    }

    @Override
    protected JPanel getTopPanel() {
        // no top panel
        return null;
    }

    @Override
    protected StringMatrixTable createViewer() {
        StringMatrixTable theViewer = new StringMatrixTable();

        return theViewer;
    }

    @Override
    public void doConnect() {
        MatrixViewer matrixViewer = ObjectUtils.recoverObject(matrixViewerRef);
        if (matrixViewer != null) {
            StringMatrixBox stringMatrixBox = matrixViewer.getStringMatrixBox();
            stringMatrixBox.connectWidget(viewer, readKey);
        }
    }

    @Override
    protected void disconnectImmediately() {
        MatrixViewer matrixViewer = ObjectUtils.recoverObject(matrixViewerRef);
        if (matrixViewer != null) {
            StringMatrixBox stringMatrixBox = matrixViewer.getStringMatrixBox();
            stringMatrixBox.disconnectWidget(viewer, readKey);
        }
    }

}
