/*
 * This file is part of databrowser-gui.
 * 
 * databrowser-gui is free software: you can redistribute it and/or modify it under the terms of the GNU General Public
 * License as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later
 * version.
 * 
 * databrowser-gui is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the
 * implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for more
 * details.
 * 
 * You should have received a copy of the GNU General Public License along with databrowser-gui. If not, see
 * <https://www.gnu.org/licenses/>.
 */
package org.cdma.gui.databrowser.view.item;

import java.awt.BorderLayout;
import java.util.ArrayList;
import java.util.Collection;
import java.util.function.Consumer;

import javax.swing.JPanel;
import javax.swing.JTabbedPane;

import org.cdma.gui.databrowser.DataBrowserController;
import org.cdma.gui.databrowser.interfaces.DataFormat;
import org.cdma.gui.databrowser.interfaces.IItemViewer;
import org.cdma.gui.databrowser.interfaces.Item;
import org.cdma.gui.databrowser.model.BooleanMatrixItem;
import org.cdma.gui.databrowser.model.ImageMatrixItem;
import org.cdma.gui.databrowser.model.MatrixItem;
import org.cdma.gui.databrowser.model.NumberMatrixItem;
import org.cdma.gui.databrowser.model.TextMatrixItem;
import org.cdma.gui.databrowser.util.ScaleConstants;

import fr.soleil.comete.box.matrixbox.BooleanMatrixBox;
import fr.soleil.comete.box.matrixbox.NumberMatrixBox;
import fr.soleil.comete.box.matrixbox.StringMatrixBox;
import fr.soleil.comete.definition.event.SnapshotEvent;
import fr.soleil.comete.definition.event.TransferEvent;
import fr.soleil.comete.definition.listener.IDataTransferListener;
import fr.soleil.comete.definition.listener.ISnapshotListener;
import fr.soleil.comete.definition.widget.properties.ImageProperties;
import fr.soleil.data.mediator.Mediator;
import fr.soleil.data.service.IKey;
import fr.soleil.data.target.scalar.ITextTarget;
import fr.soleil.lib.project.Format;
import fr.soleil.lib.project.ObjectUtils;

public class MatrixViewer extends AbstractItemViewer<MatrixItem<?>>
        implements IItemViewer, ISnapshotListener, IDataTransferListener, ScaleConstants {

    private static final long serialVersionUID = 328669394458756052L;

    private JTabbedPane tabbedPane;

    // Needed box
    private NumberMatrixBox numberMatrixBox;
    private StringMatrixBox stringMatrixBox;
    private BooleanMatrixBox booleanMatrixBox;

    private String imageSnapshotDirectory;
    private String imageDataDirectory;

    private final StringApplyTarget xAxisFormatTarget, yAxisFormatTarget;

    public MatrixViewer(DataBrowserController controller) {
        super(controller, true);
        xAxisFormatTarget = new StringApplyTarget(this::setXAxisFormat);
        yAxisFormatTarget = new StringApplyTarget(this::setYAxisFormat);
        layoutComponent();
    }

    @Override
    protected void initComponents() {
        numberMatrixBox = new NumberMatrixBox();
        stringMatrixBox = new StringMatrixBox();
        booleanMatrixBox = new BooleanMatrixBox();
        tabbedPane = new JTabbedPane();
    }

    private void layoutComponent() {
        setLayout(new BorderLayout(0, 5));
        add(tabbedPane, BorderLayout.CENTER);
    }

    public NumberMatrixBox getNumberMatrixBox() {
        return numberMatrixBox;
    }

    public BooleanMatrixBox getBooleanMatrixBox() {
        return booleanMatrixBox;
    }

    public StringMatrixBox getStringMatrixBox() {
        return stringMatrixBox;
    }

    public Collection<MatrixItem<?>> getDisplayedMatrixItems() {
        return displayedItems.values();
    }

    public void addScaleItem(final Item item) {
        for (MatrixItem<?> matrixItem : getDisplayedMatrixItems()) {
            if (matrixItem instanceof ImageMatrixItem<?, ?>) {
                ((ImageMatrixItem<?, ?>) matrixItem).addScaleItem(item);
            }
        }
    }

    public void removeScaleItem(final Item item) {
        for (MatrixItem<?> matrixItem : getDisplayedMatrixItems()) {
            if (matrixItem instanceof ImageMatrixItem<?, ?>) {
                ((ImageMatrixItem<?, ?>) matrixItem).removeScaleItem(item);
            }
        }
    }

    @Override
    public Collection<Runnable> addItem(final Item item) {
        Collection<Runnable> toRunInEDT = new ArrayList<>();
        MatrixItem<?> tmpItem = displayedItems.get(item.getKey());
        final MatrixItem<?> matrixItem;
        if (tmpItem == null) {
            // creating ImageViewer is time consuming
            // TODO put the item creation in a swingworker

            DataFormat format = item.getFormat();
            switch (format) {
                case NUMERICAL:
                    NumberMatrixItem nbItem = new NumberMatrixItem(this, item, controller);
                    nbItem.setXAxisFormat(xAxisFormatTarget.getText());
                    nbItem.setYAxisFormat(yAxisFormatTarget.getText());
                    tmpItem = nbItem;
                    break;

                case TEXT:
                    tmpItem = new TextMatrixItem(this, item, controller);
                    break;

                case BOOLEAN:
                    BooleanMatrixItem bItem = new BooleanMatrixItem(this, item, controller);
                    bItem.setXAxisFormat(xAxisFormatTarget.getText());
                    bItem.setYAxisFormat(yAxisFormatTarget.getText());
                    tmpItem = bItem;
                    break;

                default:
                    // nop
                    break;
            }
            matrixItem = tmpItem;

            if (matrixItem != null) {
                matrixItem.connect();

                displayedItems.put(item.getKey(), matrixItem);

                toRunInEDT.add(() -> {
                    MatrixPanel panel = matrixItem.getPanel();
                    panel.setFirstPlayerVisible();
                    tabbedPane.addTab(item.getBrowser().getDisplayName(item.getKey()), panel);
                    // tabbedPane.addTab(createTabName(itemKey), null, panel, createTabToolTip(itemKey));
                });

                if (matrixItem instanceof NumberMatrixItem) {
                    NumberMatrixItem numberMatrixItem = (NumberMatrixItem) matrixItem;
                    numberMatrixItem.setSnapshotLocation(imageSnapshotDirectory);
                    numberMatrixItem.addSnapshotListener(this);
                    numberMatrixItem.setDataLocation(imageDataDirectory);
                    numberMatrixItem.addDataListener(this);
                }
            }
        } else {
            matrixItem = tmpItem;
        }
        if (matrixItem != null) {
            boolean reconnect = false;
            if (!ObjectUtils.sameObject(matrixItem.getXItem(), item.getXItem())) {
                matrixItem.setXItem(item.getXItem());
                reconnect = true;
            }
            if (!ObjectUtils.sameObject(matrixItem.getYItem(), item.getYItem())) {
                matrixItem.setYItem(item.getYItem());
                reconnect = true;
            }
            if (reconnect) {
                matrixItem.reconnect();
            }
        }
        return toRunInEDT;
    }

    @Override
    public Collection<Runnable> removeItem(final Item item) {
        Collection<Runnable> toRunInEDT = new ArrayList<>();
        MatrixItem<?> matrixItem = displayedItems.remove(item.getKey());
        if (matrixItem != null) {
            toRunInEDT.add(() -> {
                // remove the component
                JPanel panel = matrixItem.getPanel();
                tabbedPane.remove(panel);
            });
            // disconnect item (viewer and setter)
            matrixItem.disconnect();
            if (matrixItem instanceof NumberMatrixItem) {
                NumberMatrixItem numberMatrixItem = (NumberMatrixItem) matrixItem;
                numberMatrixItem.removeSnapshotListener(this);
                numberMatrixItem.removeDataListener(this);
            }
        }
        return toRunInEDT;
    }

    @Override
    public Collection<Runnable> selectItem(final Item item) {
        Collection<Runnable> toRunInEDT = new ArrayList<>();
        // select new key
        final MatrixItem<?> matrixItem = displayedItems.get(item.getKey());
        if (matrixItem != null) {
            selectedKey = item.getKey();
            matrixItem.setSelected(true);
            toRunInEDT.add(() -> {
                tabbedPane.setSelectedComponent(matrixItem.getPanel());
            });
        }
        return toRunInEDT;
    }

    @Override
    public Collection<Runnable> deselectItem(final Item item) {
        if (item != null) {
            if (item.getKey() == selectedKey) {
                selectedKey = null;
                final MatrixItem<?> matrixItem = displayedItems.get(item.getKey());
                if (matrixItem != null) {
                    matrixItem.setSelected(false);
                }
            }
        }
        return new ArrayList<>();
    }

    @Override
    public Collection<Runnable> clearSelection() {
        Collection<Runnable> toRunInEDT;
        if (selectedKey == null) {
            toRunInEDT = new ArrayList<>();
        } else {
            MatrixItem<?> selectedItem = displayedItems.get(selectedKey);
            toRunInEDT = deselectItem(selectedItem);
        }
        return toRunInEDT;
    }

    @Override
    public void snapshotChanged(final SnapshotEvent event) {
        imageSnapshotDirectory = event.getSource().getSnapshotDirectory();

        updateImageSnapshotLocation();
    }

    public void setImageSnapshotLocation(final String path) {
        imageSnapshotDirectory = path;

        updateImageSnapshotLocation();
    }

    public String getImageSnapshotLocation() {
        return imageSnapshotDirectory;
    }

    private void updateImageSnapshotLocation() {
        for (MatrixItem<?> item : displayedItems.values()) {
            if (item instanceof NumberMatrixItem) {
                NumberMatrixItem numberMatrixItem = (NumberMatrixItem) item;
                numberMatrixItem.removeSnapshotListener(this);
                numberMatrixItem.setSnapshotLocation(imageSnapshotDirectory);
                numberMatrixItem.addSnapshotListener(this);
            }
        }
    }

    @Override
    public void dataTransfered(final TransferEvent event) {
        imageDataDirectory = event.getSource().getDataDirectory();

        updateImageDataLocation();
    }

    public void setImageDataLocation(final String path) {
        imageDataDirectory = path;

        updateImageDataLocation();
    }

    public String getImageDataLocation() {
        return imageDataDirectory;
    }

    private void updateImageDataLocation() {
        for (MatrixItem<?> item : displayedItems.values()) {
            if (item instanceof NumberMatrixItem) {
                NumberMatrixItem numberMatrixItem = (NumberMatrixItem) item;
                numberMatrixItem.removeDataListener(this);
                numberMatrixItem.setDataLocation(imageDataDirectory);
                numberMatrixItem.addDataListener(this);
            }
        }
    }

    protected boolean isNoString(String value) {
        return (value == null) || value.trim().isEmpty();
    }

    protected boolean shouldApplyFormat(String format, String currentFormat) {
        boolean shouldApply;
        if (Format.isNumberFormatOK(format)) {
            if (Format.isNumberFormatOK(currentFormat)) {
                shouldApply = !ObjectUtils.sameObject(format, currentFormat);
            } else {
                shouldApply = (!ObjectUtils.sameObject(format, currentFormat))
                        && !(isNoString(format) && isNoString(currentFormat));
            }
        } else if (Format.isNumberFormatOK(currentFormat)) {
            shouldApply = true;
        } else {
            shouldApply = (!ObjectUtils.sameObject(format, currentFormat))
                    && !(isNoString(format) && isNoString(currentFormat));
        }
        return shouldApply;
    }

    private void setXAxisFormat(String format) {
        for (MatrixItem<?> item : displayedItems.values()) {
            if (item instanceof ImageMatrixItem<?, ?>) {
                ImageMatrixItem<?, ?> matrixItem = (ImageMatrixItem<?, ?>) item;
                String currentFormat = matrixItem.getXAxisFormat();
                if (ObjectUtils.sameObject(xAxisFormatTarget.getFormerValue(), currentFormat)
                        && shouldApplyFormat(format, currentFormat)) {
                    matrixItem.setXAxisFormat(format);
                }
            }
        }
    }

    private void setYAxisFormat(String format) {
        for (MatrixItem<?> item : displayedItems.values()) {
            if (item instanceof ImageMatrixItem<?, ?>) {
                ImageMatrixItem<?, ?> matrixItem = (ImageMatrixItem<?, ?>) item;
                String currentFormat = matrixItem.getYAxisFormat();
                if (ObjectUtils.sameObject(yAxisFormatTarget.getFormerValue(), currentFormat)
                        && shouldApplyFormat(format, currentFormat)) {
                    matrixItem.setYAxisFormat(format);
                }
            }
        }
    }

    public Item getImageXScaleItem(final IKey imageKey) {
        Item result = null;
        MatrixItem<?> imageItem = displayedItems.get(imageKey);
        if (imageItem instanceof ImageMatrixItem<?, ?>) {
            result = ((ImageMatrixItem<?, ?>) imageItem).getImageXScaleItem();
        }
        return result;
    }

    public void setImageXScaleItem(final IKey imageKey, final Item item) {
        MatrixItem<?> imageItem = displayedItems.get(imageKey);
        if (imageItem instanceof ImageMatrixItem<?, ?>) {
            ((ImageMatrixItem<?, ?>) imageItem).setImageXScaleItem(item);
        }
    }

    public Item getImageYScaleItem(final IKey imageKey) {
        Item result = null;
        MatrixItem<?> imageItem = displayedItems.get(imageKey);
        if (imageItem instanceof ImageMatrixItem<?, ?>) {
            result = ((ImageMatrixItem<?, ?>) imageItem).getImageYScaleItem();
        }
        return result;
    }

    public void setImageYScaleItem(final IKey imageKey, final Item item) {
        MatrixItem<?> imageItem = displayedItems.get(imageKey);
        if (imageItem instanceof ImageMatrixItem<?, ?>) {
            ((ImageMatrixItem<?, ?>) imageItem).setImageYScaleItem(item);
        }
    }

    public ImageProperties getImageProperties(final IKey itemKey) {
        ImageProperties prop = null;

        MatrixItem<?> matrixItem = displayedItems.get(itemKey);
        if (matrixItem instanceof NumberMatrixItem) {
            prop = ((NumberMatrixItem) matrixItem).getImageProperties();
        }

        return prop;
    }

    public void setImageProperties(final IKey itemKey, final ImageProperties imageProperties) {
        MatrixItem<?> matrixItem = displayedItems.get(itemKey);
        if (matrixItem instanceof NumberMatrixItem) {
            ((NumberMatrixItem) matrixItem).setImageProperties(imageProperties);
        }
    }

    public ITextTarget getXAxisFormatTarget() {
        return xAxisFormatTarget;
    }

    public ITextTarget getYAxisFormatTarget() {
        return yAxisFormatTarget;
    }

    @Override
    protected void finalize() throws Throwable {
        xAxisFormatTarget.clear();
        yAxisFormatTarget.clear();
        super.finalize();
    }

    // ///////////// //
    // Inner classes //
    // ///////////// //

    protected class StringApplyTarget implements ITextTarget {
        protected Consumer<String> applyFunction;
        protected String value, formerValue;

        public StringApplyTarget(Consumer<String> applyFunction) {
            this.applyFunction = applyFunction;
        }

        @Override
        public void addMediator(Mediator<?> mediator) {
            // not managed
        }

        @Override
        public void removeMediator(Mediator<?> mediator) {
            // not managed
        }

        public String getFormerValue() {
            return formerValue;
        }

        @Override
        public String getText() {
            return value;
        }

        @Override
        public void setText(String text) {
            String toSet = text == null || text.isEmpty() ? text : text.trim();
            if ((!ObjectUtils.sameObject(this.value, toSet)) && !(isNoString(toSet) && isNoString(value))) {
                formerValue = value;
                value = toSet;
                applyFunction.accept(toSet);
            }
        }

        public void clear() {
            applyFunction = null;
            value = null;
        }

        @Override
        protected void finalize() throws Throwable {
            clear();
            super.finalize();
        }
    }
}
