/*
 * This file is part of databrowser-gui.
 * 
 * databrowser-gui is free software: you can redistribute it and/or modify it under the terms of the GNU General Public
 * License as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later
 * version.
 * 
 * databrowser-gui is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the
 * implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for more
 * details.
 * 
 * You should have received a copy of the GNU General Public License along with databrowser-gui. If not, see
 * <https://www.gnu.org/licenses/>.
 */
package org.cdma.gui.databrowser.view.displaymanager;

import java.awt.Component;

import javax.swing.DefaultCellEditor;
import javax.swing.DefaultComboBoxModel;
import javax.swing.JComboBox;
import javax.swing.JTable;

import org.cdma.gui.databrowser.interfaces.AxisType;
import org.cdma.gui.databrowser.interfaces.DataFormat;
import org.cdma.gui.databrowser.view.displaymanager.DisplayManagerTableModel.DisplayedItem;

public class AxisTableCellEditor extends DefaultCellEditor {

    private static final long serialVersionUID = 491626280335777471L;

    private static final AxisType[] spectrumChoices = { AxisType.Y1, AxisType.Y2, AxisType.X, AxisType.IMAGE,
            AxisType.INVISIBLE };
    private static final AxisType[] imageChoices = { AxisType.IMAGE, AxisType.Y1_ROW, AxisType.Y1_COLUMN,
            AxisType.Y2_ROW, AxisType.Y2_COLUMN, AxisType.X, AxisType.INVISIBLE };
    private static final AxisType[] booleanChoices = { AxisType.IMAGE, AxisType.Y1_ROW, AxisType.Y1_COLUMN,
            AxisType.Y2_ROW, AxisType.Y2_COLUMN, AxisType.INVISIBLE };

    private final DefaultComboBoxModel<AxisType> spectrumComboBoxModel;
    private final DefaultComboBoxModel<AxisType> imageComboBoxModel;
    private final DefaultComboBoxModel<AxisType> booleanComboBoxModel;

    public AxisTableCellEditor() {
        super(new JComboBox<AxisType>());

        spectrumComboBoxModel = new DefaultComboBoxModel<>(spectrumChoices);
        imageComboBoxModel = new DefaultComboBoxModel<>(imageChoices);
        booleanComboBoxModel = new DefaultComboBoxModel<>(booleanChoices);
    }

    @SuppressWarnings("unchecked")
    @Override
    public Component getTableCellEditorComponent(final JTable table, final Object value, final boolean isSelected,
            final int row, final int column) {

        int index = table.convertRowIndexToModel(row);
        DisplayedItem displayedItem = ((DisplayManagerTableModel) table.getModel()).getDisplayedItem(index);

        switch (displayedItem.getType()) {
            case SPECTRUM:
                ((JComboBox<AxisType>) editorComponent).setModel(spectrumComboBoxModel);
                break;
            case IMAGE:
                if (displayedItem.getFormat() == DataFormat.NUMERICAL) {
                    ((JComboBox<AxisType>) editorComponent).setModel(imageComboBoxModel);
                }
                if (displayedItem.getFormat() == DataFormat.BOOLEAN) {
                    ((JComboBox<AxisType>) editorComponent).setModel(booleanComboBoxModel);
                }
                break;
            default:
                // should not happen
                break;
        }

        return super.getTableCellEditorComponent(table, value, isSelected, row, column);
    }

}
