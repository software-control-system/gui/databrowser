/*
 * This file is part of databrowser-gui.
 * 
 * databrowser-gui is free software: you can redistribute it and/or modify it under the terms of the GNU General Public
 * License as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later
 * version.
 * 
 * databrowser-gui is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the
 * implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for more
 * details.
 * 
 * You should have received a copy of the GNU General Public License along with databrowser-gui. If not, see
 * <https://www.gnu.org/licenses/>.
 */
package org.cdma.gui.databrowser.view.item;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.ItemEvent;
import java.awt.event.ItemListener;
import java.util.EventObject;

import fr.soleil.comete.definition.listener.ICheckBoxListener;
import fr.soleil.comete.definition.widget.ICheckBox;
import fr.soleil.comete.definition.widget.IComponent;
import fr.soleil.comete.definition.widget.util.CometeColor;
import fr.soleil.comete.definition.widget.util.CometeFont;
import fr.soleil.comete.swing.CheckBox;
import fr.soleil.comete.swing.Label;
import fr.soleil.comete.swing.Panel;

public class CheckBoxPanel extends Panel implements ICheckBox, ActionListener, ItemListener {

    private static final long serialVersionUID = -4189092075168726364L;

    private AccessibleCheckBox checkBox;
    private Label label;

    public CheckBoxPanel() {
        setLayout(new BorderLayout());
        label = new Label();
        label.setOpaque(false);
        checkBox = new AccessibleCheckBox();
        addCenter(label);
        add((IComponent) checkBox, BorderLayout.WEST);
    }

    @Override
    public boolean isSelected() {
        return checkBox.isSelected();
    }

    @Override
    public void setSelected(boolean bool) {
        checkBox.setSelected(bool);
    }

    @Override
    public void setEditable(boolean b) {
        checkBox.setEditable(b);
    }

    @Override
    public boolean isEditable() {
        return checkBox.isEditable();
    }

    @Override
    public void itemStateChanged(ItemEvent e) {
        checkBox.transmitItemEvent(e);
    }

    @Override
    public void actionPerformed(ActionEvent e) {
        checkBox.transmitActionEvent(e);
    }

    @Override
    public void addCheckBoxListener(ICheckBoxListener listener) {
        checkBox.addCheckBoxListener(listener);
    }

    @Override
    public void removeCheckBoxListener(ICheckBoxListener listener) {
        checkBox.removeCheckBoxListener(listener);
    }

    @Override
    public void fireSelectedChanged(EventObject event) {
        checkBox.fireSelectedChanged(event);
    }

    @Override
    public void setFalseLabel(String falseLabel) {
        checkBox.setFalseLabel(falseLabel);
    }

    @Override
    public String getFalseLabel() {
        return checkBox.getFalseLabel();
    }

    @Override
    public void setTrueLabel(String trueLabel) {
        checkBox.setTrueLabel(trueLabel);
    }

    @Override
    public void setBackground(Color bg) {
        super.setBackground(bg);
        if (checkBox != null) {
            checkBox.setBackground(bg);
        }
    }

    @Override
    public String getTrueLabel() {
        return checkBox.getTrueLabel();
    }

    @Override
    public void setCometeBackground(final CometeColor color) {
        super.setCometeBackground(color);
        checkBox.setCometeBackground(color);
    }

    @Override
    public void setCometeFont(final CometeFont font) {
        super.setCometeFont(font);
        checkBox.setCometeFont(font);
    }

    @Override
    public void setCometeForeground(final CometeColor color) {
        super.setCometeForeground(color);
        checkBox.setCometeForeground(color);
    }

    protected static class AccessibleCheckBox extends CheckBox {

        private static final long serialVersionUID = -826345267870475153L;

        public AccessibleCheckBox() {
            super();
        }

        @Override
        public void transmitActionEvent(ActionEvent evt) {
            super.transmitActionEvent(evt);
        }

        @Override
        public void transmitItemEvent(ItemEvent evt) {
            super.transmitItemEvent(evt);
        }
    }
}
