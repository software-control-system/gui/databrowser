/*
 * This file is part of databrowser-gui.
 * 
 * databrowser-gui is free software: you can redistribute it and/or modify it under the terms of the GNU General Public
 * License as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later
 * version.
 * 
 * databrowser-gui is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the
 * implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for more
 * details.
 * 
 * You should have received a copy of the GNU General Public License along with databrowser-gui. If not, see
 * <https://www.gnu.org/licenses/>.
 */
package org.cdma.gui.databrowser.view.item;

import java.awt.BorderLayout;
import java.awt.Component;
import java.awt.Dimension;

import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JSplitPane;

import fr.soleil.data.target.matrix.IMatrixTarget;

public class MatrixPanel extends JPanel {

    private static final long serialVersionUID = 8138198313706723711L;

    private final IMatrixTarget viewer;
    private final PlayerPanel playerPanel;
    private final JPanel topPanel;
    private JSplitPane splitPane;

    public MatrixPanel(final IMatrixTarget viewerComp, final PlayerPanel playerPanel, final JPanel topPanel) {
        super(new BorderLayout());
        viewer = viewerComp;
        this.playerPanel = playerPanel;
        this.topPanel = topPanel;
        layoutComponents();
    }

    private void layoutComponents() {
        if (playerPanel == null) {
            add((Component) viewer, BorderLayout.CENTER);
        } else {
            JScrollPane playerScrollPane = new JScrollPane(playerPanel);

            splitPane = new JSplitPane(JSplitPane.VERTICAL_SPLIT);
            splitPane.setOneTouchExpandable(true);
            splitPane.setLeftComponent(playerScrollPane);
            splitPane.setRightComponent((Component) viewer);

            add(splitPane, BorderLayout.CENTER);
        }
        if (topPanel != null) {
            add(topPanel, BorderLayout.NORTH);
        }
    }

    public void setFirstPlayerVisible() {
        if (playerPanel != null) {
            Dimension sizePlayer = playerPanel.getPreferredSize();
            int nbPlayers = playerPanel.getNbPlayers();
            int h1 = sizePlayer.height / nbPlayers;
            splitPane.setDividerLocation(h1 + splitPane.getDividerSize());
        }
    }

}
