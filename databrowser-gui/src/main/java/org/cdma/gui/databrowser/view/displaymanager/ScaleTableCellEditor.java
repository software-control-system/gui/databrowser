/*
 * This file is part of databrowser-gui.
 * 
 * databrowser-gui is free software: you can redistribute it and/or modify it under the terms of the GNU General Public
 * License as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later
 * version.
 * 
 * databrowser-gui is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the
 * implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for more
 * details.
 * 
 * You should have received a copy of the GNU General Public License along with databrowser-gui. If not, see
 * <https://www.gnu.org/licenses/>.
 */
package org.cdma.gui.databrowser.view.displaymanager;

import java.awt.Component;
import java.awt.GraphicsConfiguration;
import java.awt.Insets;
import java.awt.Point;
import java.awt.Rectangle;
import java.awt.Toolkit;
import java.beans.PropertyChangeEvent;
import java.beans.PropertyChangeListener;
import java.util.LinkedList;
import java.util.List;

import javax.swing.DefaultCellEditor;
import javax.swing.DefaultComboBoxModel;
import javax.swing.DefaultListCellRenderer;
import javax.swing.JComboBox;
import javax.swing.JComponent;
import javax.swing.JLabel;
import javax.swing.JList;
import javax.swing.JPopupMenu;
import javax.swing.ListCellRenderer;
import javax.swing.ListSelectionModel;
import javax.swing.SwingUtilities;
import javax.swing.UIManager;
import javax.swing.event.PopupMenuEvent;
import javax.swing.event.PopupMenuListener;
import javax.swing.plaf.ComboBoxUI;
import javax.swing.plaf.basic.BasicComboBoxUI;
import javax.swing.plaf.basic.BasicComboPopup;
import javax.swing.plaf.basic.ComboPopup;
import javax.swing.plaf.metal.MetalComboBoxUI;
import javax.swing.plaf.synth.SynthComboBoxUI;

import org.cdma.gui.databrowser.interfaces.DataFormat;
import org.cdma.gui.databrowser.interfaces.DataType;
import org.cdma.gui.databrowser.interfaces.Item;
import org.cdma.gui.databrowser.util.ScaleConstants;

public class ScaleTableCellEditor extends DefaultCellEditor implements ScaleConstants {

    private static final long serialVersionUID = -9028293208451588216L;

    private DefaultComboBoxModel<Object> scaleModel;
    private List<Item> scaleItemsList;

    public ScaleTableCellEditor() {
        super(new ComboxEditor());
        scaleItemsList = new LinkedList<>();
        scaleModel = new DefaultComboBoxModel<>();
        scaleModel.addElement(DEFAULT_SCALE);
        ListCellRenderer<Object> listCellRenderer = new DefaultListCellRenderer() {

            private static final long serialVersionUID = 7922922623945094022L;

            @Override
            public Component getListCellRendererComponent(final JList<?> list, final Object value, final int index,
                    final boolean isSelected, final boolean cellHasFocus) {

                Component component = super.getListCellRendererComponent(list, value, index, isSelected, cellHasFocus);

                if (component instanceof JLabel) {
                    if (value != DEFAULT_SCALE) {
                        if (value instanceof Item) {
                            Item item = (Item) value;
                            String displayName = item.getBrowser().getDisplayName(item.getKey());
                            String description = item.getBrowser().getDescription(item.getKey());

                            ((JLabel) component).setText(displayName);
                            ((JLabel) component).setToolTipText(description);
                        }
                    }
                }

                return component;
            }
        };
        ((ComboxEditor) editorComponent).setRenderer(listCellRenderer);
        ((ComboxEditor) editorComponent).setModel(scaleModel);

    }

    public void addScaleItem(final Item item) {
        DataType type = item.getType();
        DataFormat format = item.getFormat();

        if ((type == DataType.SPECTRUM || type == DataType.IMAGE) && (format == DataFormat.NUMERICAL)) {
            if (!scaleItemsList.contains(item)) {
                scaleItemsList.add(item);
                scaleModel.addElement(item);
            }
        }
    }

    public void removeScaleItem(final Item item) {
        if (item != null && scaleItemsList != null) {
            DataType type = item.getType();
            DataFormat format = item.getFormat();

            if ((type == DataType.SPECTRUM || type == DataType.IMAGE) && (format == DataFormat.NUMERICAL)) {
                scaleItemsList.remove(item);

                if (scaleModel.getSelectedItem() != null && scaleModel.getSelectedItem().equals(item)) {
                    scaleModel.setSelectedItem(DEFAULT_SCALE);
                }
                scaleModel.removeElement(item);
            }
        }
    }

    protected static void preparePopupForVisibility(Object popup) {
        if (popup instanceof JPopupMenu) {
            JPopupMenu popupMenu = (JPopupMenu) popup;
            popupMenu.pack();
            popupMenu.setSize(popupMenu.getPreferredSize());
        }
    }

    // ///////////// //
    // Inner classes //
    // ///////////// //

    protected static class ComboxEditor extends JComboBox<Object> {

        private static final long serialVersionUID = 7710508768040757171L;

        public ComboxEditor() {
            super();
            addPopupMenuListener(new PopupMenuListener() {

                @Override
                public void popupMenuWillBecomeVisible(PopupMenuEvent e) {
                    ComboBoxUI ui = getUI();
                    if (ui instanceof PopupComboBoxUI) {
                        preparePopupForVisibility(((PopupComboBoxUI) ui).getPopup());
                    }

                }

                @Override
                public void popupMenuWillBecomeInvisible(PopupMenuEvent e) {
                    // not managed
                }

                @Override
                public void popupMenuCanceled(PopupMenuEvent e) {
                    // not managed
                }
            });
        }

        @Override
        public void setOpaque(boolean isOpaque) {
            super.setOpaque(true);
        }

        /**
         * Resets the UI property to a value from the current look and feel.
         *
         * @see JComponent#updateUI
         */
        @Override
        public void updateUI() {
            ComboBoxUI ui = (ComboBoxUI) UIManager.getUI(this);
            if (ui instanceof SynthComboBoxUI) {
                ui = new NimbusAdaptablePopupUI();
            } else if (ui instanceof MetalComboBoxUI) {
                ui = new MetalAdaptablePopupUI();
            } else {
                ui = new BasicAdaptablePopupUI();
            }
            setUI(ui);

            ListCellRenderer<?> renderer = getRenderer();
            if (renderer instanceof Component) {
                SwingUtilities.updateComponentTreeUI((Component) renderer);
            }
            if (renderer instanceof JComponent) {
                ((JComponent) renderer).setOpaque(true);
            }
        }

    }

    protected static interface PopupComboBoxUI {
        public ComboPopup getPopup();
    }

    protected static class BestSizeComboPopup extends BasicComboPopup {

        private static final long serialVersionUID = 3838735676643450776L;

        public BestSizeComboPopup(JComboBox<?> combo) {
            super(combo);
        }

        @Override
        protected Rectangle computePopupBounds(int px, int py, int pw, int ph) {
            Rectangle rect = super.computePopupBounds(px, py, pw, ph);
            pack();
            int width = getWidth();
            if (rect.width < width) {
                rect.width = width;
            }
            Toolkit toolkit = Toolkit.getDefaultToolkit();
            Rectangle screenBounds;

            // Calculate the desktop dimensions relative to the combo box.
            GraphicsConfiguration gc = comboBox.getGraphicsConfiguration();
            Point p = new Point();
            SwingUtilities.convertPointFromScreen(p, comboBox);
            if (gc != null) {
                Insets screenInsets = toolkit.getScreenInsets(gc);
                screenBounds = gc.getBounds();
                screenBounds.width -= (screenInsets.left + screenInsets.right);
                screenBounds.height -= (screenInsets.top + screenInsets.bottom);
                screenBounds.x += (p.x + screenInsets.left);
                screenBounds.y += (p.y + screenInsets.top);
            } else {
                screenBounds = new Rectangle(p, toolkit.getScreenSize());
            }
            if (rect.x + width > screenBounds.width) {
                rect.x -= Math.abs(width - screenBounds.width);
            }
            if (rect.x < 0) {
                rect.x = 0;
            }
            return rect;
        }

    }

    protected static class NimbusBestSizeComboPopup extends BestSizeComboPopup {

        private static final long serialVersionUID = -2817868855065863058L;

        public NimbusBestSizeComboPopup(JComboBox<?> combo) {
            super(combo);
        }

        @SuppressWarnings("unchecked")
        @Override
        protected void configureList() {
            list.setFont(comboBox.getFont());
            list.setCellRenderer(comboBox.getRenderer());
            list.setFocusable(false);
            list.setSelectionMode(ListSelectionModel.SINGLE_SELECTION);
            int selectedIndex = comboBox.getSelectedIndex();
            if (selectedIndex == -1) {
                list.clearSelection();
            } else {
                list.setSelectedIndex(selectedIndex);
                list.ensureIndexIsVisible(selectedIndex);
            }
            installListListeners();
        }

    }

    protected static class NimbusAdaptablePopupUI extends SynthComboBoxUI implements PopupComboBoxUI {

        public NimbusAdaptablePopupUI() {
            super();
        }

        @Override
        protected ListCellRenderer<?> createRenderer() {
            ListCellRenderer<?> renderer = super.createRenderer();
            if (renderer instanceof JComponent) {
                final JComponent comp = (JComponent) renderer;
                comp.setOpaque(true);
                comp.addPropertyChangeListener(new PropertyChangeListener() {
                    @Override
                    public void propertyChange(PropertyChangeEvent evt) {
                        if ("opaque".equals(evt.getPropertyName()) && Boolean.FALSE.equals(evt.getNewValue())) {
                            comp.setOpaque(true);
                        }
                    }
                });
            }
            return renderer;
        }

        @Override
        protected ComboPopup createPopup() {
            return new NimbusBestSizeComboPopup(comboBox);
        }

        @Override
        public ComboPopup getPopup() {
            return popup;
        }

    }

    protected static class MetalAdaptablePopupUI extends MetalComboBoxUI implements PopupComboBoxUI {

        public MetalAdaptablePopupUI() {
            super();
        }

        @Override
        protected ComboPopup createPopup() {
            return new BestSizeComboPopup(comboBox);
        }

        @Override
        public ComboPopup getPopup() {
            return popup;
        }
    }

    protected static class BasicAdaptablePopupUI extends BasicComboBoxUI implements PopupComboBoxUI {

        public BasicAdaptablePopupUI() {
            super();
        }

        @Override
        protected ComboPopup createPopup() {
            return new BestSizeComboPopup(comboBox);
        }

        @Override
        public ComboPopup getPopup() {
            return popup;
        }
    }

}
