/*
 * This file is part of databrowser-gui.
 * 
 * databrowser-gui is free software: you can redistribute it and/or modify it under the terms of the GNU General Public
 * License as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later
 * version.
 * 
 * databrowser-gui is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the
 * implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for more
 * details.
 * 
 * You should have received a copy of the GNU General Public License along with databrowser-gui. If not, see
 * <https://www.gnu.org/licenses/>.
 */
package org.cdma.gui.databrowser;

import java.awt.BorderLayout;
import java.awt.Desktop;
import java.awt.FlowLayout;
import java.awt.Frame;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;
import java.beans.PropertyChangeEvent;
import java.beans.PropertyChangeListener;
import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.net.URI;
import java.net.URL;
import java.util.Arrays;
import java.util.concurrent.atomic.AtomicBoolean;
import java.util.jar.JarFile;

import javax.swing.BorderFactory;
import javax.swing.JButton;
import javax.swing.JDialog;
import javax.swing.JEditorPane;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.SwingWorker;
import javax.swing.UIManager;
import javax.swing.event.HyperlinkEvent;
import javax.swing.event.HyperlinkListener;
import javax.swing.text.html.HTMLEditorKit;

import org.cdma.gui.databrowser.plugintree.PluginTreeDialog;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import fr.soleil.lib.project.file.FileUtils;
import fr.soleil.lib.project.swing.dialog.JDialogUtils;

public class AboutDialog extends JDialog {

    private static final long serialVersionUID = -4536210088745453107L;

    private static final Logger LOGGER = LoggerFactory.getLogger(AboutDialog.class);
    private static final String ABOUT_FILENAME = "About.html";

    private JEditorPane htmlPanel;
    private JButton treeButton;

    private PluginTreeDialog pluginTreeDialog;

    // this tells if the url is loaded or not
    private final AtomicBoolean urlIsLoaded = new AtomicBoolean(false);
    // swingworker that will wait until the url is loaded
    private WaitingWorker waitingWorker;

    public AboutDialog(final Frame owner) {
        super(owner);

        setModal(true);
        setTitle(DataBrowser.MESSAGES.getString("About.Title"));

        initComponents();
        layoutComponents();
    }

    private void initComponents() {
        waitingWorker = new WaitingWorker();

        htmlPanel = new JEditorPane();
        htmlPanel.setEditorKit(new HTMLEditorKit());
        htmlPanel.setEditable(false);
        // listen for the "page" property, useful for worker to know when load is completed
        htmlPanel.addPropertyChangeListener("page", waitingWorker);

        // let's mimic panel background color
        htmlPanel.setBackground(UIManager.getColor("Panel.background"));

        htmlPanel.addHyperlinkListener(new HyperlinkListener() {
            @Override
            public void hyperlinkUpdate(final HyperlinkEvent e) {
                if (e.getEventType() == HyperlinkEvent.EventType.ACTIVATED) {
                    // if uri is a file, get a temp version extracted from the jar and ask OS to open it,
                    // else ask OS to browse the link
                    if (Desktop.isDesktopSupported()) {
                        try {
                            URI uri = e.getURL().toURI();
                            String scheme = uri.getScheme();
                            String file = e.getURL().getFile();

                            if (scheme.equalsIgnoreCase("jar")) {
                                String[] pathInfo = file.split("!/");
                                LOGGER.trace("Jar file for help ", Arrays.toString(pathInfo));
                                String jarFileName = pathInfo[0].replaceFirst("file:", "");
                                String entryName = pathInfo[1];

                                String extension = FileUtils.getExtension(file);
                                String fileName = file.substring(file.lastIndexOf("/") + 1, file.lastIndexOf("."));
                                openFileInJar(jarFileName, entryName, fileName, extension);
                            } else {
                                Desktop.getDesktop().browse(uri);
                            }
                        } catch (Exception e1) {
                            LOGGER.error("Impossible to open hyperlink {}", e1.getMessage());
                            LOGGER.debug("Stack trace", e1);
                        }
                    }
                }
            }
        });

        URL aboutURL = DataBrowser.class.getResource(ABOUT_FILENAME);
        try {
            htmlPanel.setPage(aboutURL);
        } catch (IOException e) {
            LOGGER.error("Impossible to set htmp page {} because {}", aboutURL, e.getMessage());
            LOGGER.debug("Stack trace", e);
        }

        treeButton = new JButton(DataBrowser.MESSAGES.getString("Action.About.PluginsDetails"));
        treeButton.addActionListener((e) -> {
            if (pluginTreeDialog == null) {
                pluginTreeDialog = new PluginTreeDialog(AboutDialog.this);
            }
            pluginTreeDialog.pack();
            // Ensure minimum width to display dialog title
            pluginTreeDialog.setSize(Math.max(pluginTreeDialog.getWidth(), 300), pluginTreeDialog.getHeight());
            pluginTreeDialog.setLocationRelativeTo(AboutDialog.this);
            pluginTreeDialog.setVisible(true);
        });

        // allow to close the dialog with Esc
        JDialogUtils.installEscapeCloseOperation(this);
    }

    private static void openFileInJar(final String jarFileName, final String entryName, final String fileName,
            final String extension) {
        String tempFileName = null;
        try (JarFile jarFile = new JarFile(jarFileName);
                InputStream is = jarFile.getInputStream(jarFile.getJarEntry(entryName));) {
            // create a temporary file named after the one from the jar, with the same extension
            File tempFile = File.createTempFile(fileName, "." + extension);
            FileUtils.duplicateFile(is, tempFile, true);
            tempFile.deleteOnExit();
            tempFileName = tempFile.getAbsolutePath();
            Desktop.getDesktop().open(tempFile);
            LOGGER.trace("Open help file {}", tempFileName);
        } catch (IOException e) {
            LOGGER.error("Impossible to open help file {}", tempFileName);
        }
    }

    private void layoutComponents() {
        JScrollPane scrollPane = new JScrollPane(htmlPanel);

        JPanel buttonPanel = new JPanel(new FlowLayout(FlowLayout.LEADING));
        buttonPanel.setBorder(BorderFactory.createEmptyBorder(5, 5, 5, 5));
        buttonPanel.add(treeButton);

        JPanel contentPane = new JPanel();
        contentPane.setLayout(new BorderLayout());
        contentPane.add(scrollPane, BorderLayout.CENTER);
        contentPane.add(buttonPanel, BorderLayout.SOUTH);

        setContentPane(contentPane);
    }

    private void adjustToRealSize() {
        pack();
        // Ensure minimum size to display image and title
        setSize(Math.max(getWidth(), 750), Math.max(getHeight(), 600));
    }

    private void reallySetVisible() {
        // adjust to real size
        adjustToRealSize();
        // center the dialog
        setLocationRelativeTo(getOwner());
        // now set it visible
        super.setVisible(true);
    }

    /**
     * Overridden to ensure the url is loaded before showing dialog
     * 
     * @see java.awt.Dialog#setVisible(boolean)
     */
    @Override
    public void setVisible(final boolean b) {
        if (b) {
            if (urlIsLoaded.get()) {
                // skip waiting worker
                reallySetVisible();
            } else {
                waitingWorker.execute();
            }
        } else {
            super.setVisible(b);
        }
    }

    public static void main(final String[] args) {
        AboutDialog aboutDialog = new AboutDialog(null);
        aboutDialog.setIconImage(DataBrowser.ICONS.getIcon("Application.Icon.32").getImage());

        aboutDialog.setDefaultCloseOperation(DO_NOTHING_ON_CLOSE);
        aboutDialog.addWindowListener(new WindowAdapter() {
            @Override
            public void windowClosing(WindowEvent e) {
                System.exit(0);
            }
        });

        aboutDialog.setLocationRelativeTo(null);
        aboutDialog.setVisible(true);
        DataBrowser.SPLASH.setVisible(false);
    }

    // ///////////// //
    // Inner classes //
    // ///////////// //

    /**
     * This class is only here to delay the dialog visibility until the url is
     * loaded!! Once the url is loaded, the JEditorPane knows its size, and
     * pack() on the dialog will be accurate.
     * 
     * Either propertyChange or doInBackground can be called first, so we need
     * to check if we have to wait, in doInBackground.
     * 
     * @author mainguy
     */
    private class WaitingWorker extends SwingWorker<Void, Void> implements PropertyChangeListener {

        @Override
        public void propertyChange(final PropertyChangeEvent evt) {
            // the page has changed, meaning it is loaded
            urlIsLoaded.set(true);
            try {
                notify();
            } catch (IllegalMonitorStateException e) {
                // Ignore: it is not that important
            }
        }

        @Override
        protected Void doInBackground() throws Exception {
            while (!urlIsLoaded.get()) {
                wait(10);
            }
            return null;
        }

        @Override
        protected void done() {
            reallySetVisible();
        }
    }

}
