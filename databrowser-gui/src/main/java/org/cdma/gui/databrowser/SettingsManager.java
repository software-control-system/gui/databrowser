/*
 * This file is part of databrowser-gui.
 * 
 * databrowser-gui is free software: you can redistribute it and/or modify it under the terms of the GNU General Public
 * License as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later
 * version.
 * 
 * databrowser-gui is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the
 * implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for more
 * details.
 * 
 * You should have received a copy of the GNU General Public License along with databrowser-gui. If not, see
 * <https://www.gnu.org/licenses/>.
 */
package org.cdma.gui.databrowser;

import java.awt.GraphicsEnvironment;
import java.awt.Rectangle;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.util.ArrayList;
import java.util.List;
import java.util.prefs.BackingStoreException;
import java.util.prefs.InvalidPreferencesFormatException;
import java.util.prefs.Preferences;

import org.cdma.gui.databrowser.interfaces.IDataSourceBrowser;
import org.cdma.gui.databrowser.view.displaymanager.DisplayManagerTableModel.Columns;
import org.cdma.gui.databrowser.view.item.ScaledPlayer;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.w3c.dom.Node;

import fr.soleil.comete.definition.widget.properties.ChartProperties;
import fr.soleil.comete.definition.widget.properties.xml.ChartPropertiesXmlManager;
import fr.soleil.comete.swing.ImageViewer;
import fr.soleil.lib.project.xmlhelpers.XMLUtils;
import fr.soleil.lib.project.xmlhelpers.exception.XMLWarning;

public class SettingsManager {

    private static final Logger LOGGER = LoggerFactory.getLogger(SettingsManager.class);

    private static final String DATABROWSER_NODE = "DataBrowser";
    private static final String WINDOW_NODE = "window";
    private static final String CHART_NODE = "chart";
    private static final String IMAGE_NODE = "image";
    private static final String PLAYER_NODE = "player";
    private static final String CONTROL_NODE = "controlPanel";
    private static final String DISPLAY_MANAGER_NODE = "displayManager";
    private static final String BROWSERS_NODE = "browsers";
    private static final String SLIDER_NODE = "slider";

    private static final String DATAITEM_LAYOUT = "dataitemlayout";

    private static final String VIEW_CONFIG_LOCATION = "viewConfigLocation";
    private static final String MARKERS_ENABLED = "markersEnabled";
    private static final String WINDOW_LAYOUT = "layout";
    private static final String WINDOW_BOUNDS_X = "x";
    private static final String WINDOW_BOUNDS_Y = "y";
    private static final String WINDOW_BOUNDS_WIDTH = "width";
    private static final String WINDOW_BOUNDS_HEIGHT = "height";

    private static final String CHART_PROPERTIES = "properties";
    private static final String CHART_SNAPSHOT_LOCATION = "snapshotLocation";
    private static final String CHART_DATA_LOCATION = "dataLocation";

    private static final String IMAGE_SNAPSHOT_LOCATION = "snapshotLocation";
    private static final String IMAGE_DATA_LOCATION = "dataLocation";
    private static final String X_AXIS_FORMAT = ImageViewer.X_AXIS_FORMAT;
    private static final String Y_AXIS_FORMAT = "yAxisFormat";

    private static final String DISPLAYED_COLUMNS = "displayedColumn";

    private static final String PLAYER_VIDEO_LOCATION = "videoLocation";

    private static final String CONTROL_LOCATION = "controlLocation";

    private static final String SEEKER_LOCATION = "seekerLocation";
    private static final String BROWSER_NAME = "browserName";

    private static final String DATE_FORMAT = "dateFormat";
    private static final String TIME_FORMAT = "timeFormat";

    private static final String COLUMN_SEPARATOR = ";";
    private static final String DEFAULT_CONTROL_PANEL = "devicepanel";

    private final String configFilename;

    public SettingsManager(final String filename) {
        configFilename = filename;

        loadSettings();
    }

    protected Preferences getPreferences(String... node) {
        Preferences parentNode = Preferences.userRoot().node(DATABROWSER_NODE);
        if (node != null) {
            for (String key : node) {
                parentNode = parentNode.node(key);
            }
        }
        return parentNode;
    }

    private void loadSettings() {

        // first clean old data that may be in system memory
        Preferences dataBrowserNode = null;
        try {
            dataBrowserNode = getPreferences();
        } catch (Exception e1) {
            LOGGER.error("Impossible to create root preferences {}", e1.getMessage());
            LOGGER.debug("Stack trace", e1);
        }
        if (dataBrowserNode != null) {
            try {
                dataBrowserNode.removeNode();
            } catch (BackingStoreException e1) {
                LOGGER.error("Impossible to read java preferences {}", e1.getMessage());
                LOGGER.debug("Stack trace", e1);
            }

            if (configFilename != null) {
                // If we don't remove preferences, it is persisted in the system memory.
                // This allows temporary preferences to be used from one launch to another without specifying a config
                // file.
                InputStream in = null;
                try {
                    in = new FileInputStream(configFilename);
                    Preferences.importPreferences(in);
                } catch (FileNotFoundException e) {
                    LOGGER.error("{} file does not exist", configFilename);
                    LOGGER.debug("Stack trace", e);
                } catch (IOException e) {
                    LOGGER.error("Impossible to read file {} because {}", configFilename, e.getMessage());
                    LOGGER.debug("Stack trace", e);
                } catch (InvalidPreferencesFormatException e) {
                    LOGGER.error("Unrecognize format for file {} because {}", configFilename, e.getMessage());
                    LOGGER.debug("Stack trace", e);
                } finally {
                    try {
                        if (in != null) {
                            in.close();
                        }
                    } catch (IOException e) {
                        LOGGER.error("Cannot close file {} because {}", configFilename, e.getMessage());
                        LOGGER.debug("Stack trace", e);
                    }
                }
            }
        } else {
            LOGGER.error("Impossible to load settings");
        }
    }

    public void saveSettings() {
        if (configFilename != null) {
            OutputStream out = null;
            Preferences dataBrowserNode = getPreferences();
            try {
                dataBrowserNode.flush();
                out = new FileOutputStream(configFilename);
                dataBrowserNode.exportSubtree(out);

            } catch (FileNotFoundException e) {
                LOGGER.error("{} file does not exist", configFilename);
                LOGGER.debug("Stack trace", e);
            } catch (Exception e) {
                LOGGER.error("Impossible to save file {} because {}", configFilename, e.getMessage());
                LOGGER.debug("Stack trace", e);
            } finally {
                try {
                    if (out != null) {
                        out.close();
                    }
                } catch (IOException e) {
                    LOGGER.error("Cannot close file {} because {}", configFilename, e.getMessage());
                    LOGGER.debug("Stack trace", e);
                }
            }
        }
    }

    protected String getProperty(String key, String... node) {
        String property = null;
        Preferences parentNode = getPreferences(node);
        property = parentNode.get(key, null);
        return property;
    }

    protected void setProperty(String key, String value, String... node) {
        if ((value != null) && !value.trim().isEmpty()) {
            getPreferences(node).put(key, value);
        } else {
            getPreferences(node).remove(key);
        }
    }

    protected boolean getBooleanProperty(String key, boolean defaultValue, String... node) {
        return getPreferences(node).getBoolean(key, defaultValue);
    }

    protected void setBooleanProperty(String key, boolean value, String... node) {
        getPreferences(node).putBoolean(key, value);
    }

    protected byte[] getByteArrayProperty(String key, String... node) {
        return getPreferences(node).getByteArray(key, null);
    }

    protected void setByteArrayProperty(String key, byte[] value, String... node) {
        if (value != null) {
            getPreferences(node).putByteArray(key, value);
        }
    }

    public String getViewConfigLocation() {
        return getProperty(VIEW_CONFIG_LOCATION);
    }

    public void setViewConfigLocation(final String path) {
        setProperty(VIEW_CONFIG_LOCATION, path);
    }

    public boolean isMarkerEnabled() {
        boolean formerMarker = getBooleanProperty(MARKERS_ENABLED, true);
        boolean marker = getBooleanProperty(MARKERS_ENABLED, true, CHART_NODE);
        if (formerMarker != marker) {
            if (!formerMarker) {
                marker = formerMarker;
            }
            try {
                // In former versions, "markersEnabled" property was in DataBrowser node.
                // Now, it is expected to be in chart node as it is a chart property.
                // So, clean former case if possible.
                if (getProperty(MARKERS_ENABLED) != null) {
                    setMarkerEnabled(marker);
                    saveSettings();
                }
            } catch (Exception e) {
                // Ignore that case. We just try to remove a former node
            }
        }
        return marker;
    }

    public void setMarkerEnabled(boolean markerEnabled) {
        // In former versions, "markersEnabled" property was in DataBrowser node.
        // Now, it is expected to be in chart node as it is a chart property.
        getPreferences().remove(MARKERS_ENABLED);
        setBooleanProperty(MARKERS_ENABLED, markerEnabled, CHART_NODE);
    }

    public byte[] getLayout() {
        return getByteArrayProperty(WINDOW_LAYOUT, WINDOW_NODE);
    }

    public void setLayout(final byte[] layout) {
        setByteArrayProperty(WINDOW_LAYOUT, layout, WINDOW_NODE);
    }

    public byte[] getDataItemLayout() {
        return getByteArrayProperty(DATAITEM_LAYOUT, WINDOW_NODE);
    }

    public void setDataItemLayout(final byte[] layout) {
        setByteArrayProperty(DATAITEM_LAYOUT, layout, WINDOW_NODE);
    }

    public Rectangle getWindowBounds() {
        // default values: full screen
        GraphicsEnvironment ge = GraphicsEnvironment.getLocalGraphicsEnvironment();
        Rectangle maximumWindowBounds = ge.getMaximumWindowBounds();
        int x = maximumWindowBounds.x;
        int y = maximumWindowBounds.y;
        int w = maximumWindowBounds.width;
        int h = maximumWindowBounds.height;

        Rectangle result = new Rectangle(x, y, w, h);

        Preferences window = getPreferences(WINDOW_NODE);

        result.x = window.getInt(WINDOW_BOUNDS_X, x);
        result.y = window.getInt(WINDOW_BOUNDS_Y, y);
        result.width = window.getInt(WINDOW_BOUNDS_WIDTH, w - result.x);
        result.height = window.getInt(WINDOW_BOUNDS_HEIGHT, h - result.y);

        return result;
    }

    public void setWindowBounds(final Rectangle windowBounds) {
        Preferences window = getPreferences(WINDOW_NODE);

        window.putInt(WINDOW_BOUNDS_X, windowBounds.x);
        window.putInt(WINDOW_BOUNDS_Y, windowBounds.y);
        window.putInt(WINDOW_BOUNDS_WIDTH, windowBounds.width);
        window.putInt(WINDOW_BOUNDS_HEIGHT, windowBounds.height);
    }

    public ChartProperties getChartProperties() {
        ChartProperties result = null;

        Preferences chartNode = getPreferences(CHART_NODE);
        try {
            String xmlProperties = chartNode.get(CHART_PROPERTIES, null);
            if (xmlProperties != null) {
                Node node = XMLUtils.getRootNodeFromFileContent(xmlProperties);
                result = ChartPropertiesXmlManager.loadChartProperties(node);
            }
        } catch (XMLWarning e) {
            LOGGER.error("Cannot read Chart properties because {}", e.getMessage());
            LOGGER.debug("Stack trace", e);
        }

        // Force default display duration to Infinity see JIRA EXPDATA-427
        if (result != null) {
            result.setDisplayDuration(Double.POSITIVE_INFINITY);
        }
        return result;
    }

    public void setChartProperties(final ChartProperties chartProperties) {
        String xmlString = ChartPropertiesXmlManager.toXmlString(chartProperties);
        setProperty(CHART_PROPERTIES, xmlString, CHART_NODE);
    }

    public String getChartSnapshotLocation() {
        return getProperty(CHART_SNAPSHOT_LOCATION, CHART_NODE);
    }

    public void setChartSnapshotLocation(final String path) {
        setProperty(CHART_SNAPSHOT_LOCATION, path, CHART_NODE);
    }

    public String getChartDataLocation() {
        return getProperty(CHART_DATA_LOCATION, CHART_NODE);
    }

    public void setChartDataLocation(final String path) {
        setProperty(CHART_DATA_LOCATION, path, CHART_NODE);
    }

    public String getImageSnapshotLocation() {
        return getProperty(IMAGE_SNAPSHOT_LOCATION, IMAGE_NODE);
    }

    public void setImageSnapshotLocation(final String path) {
        setProperty(IMAGE_SNAPSHOT_LOCATION, path, IMAGE_NODE);
    }

    public String getImageDataLocation() {
        return getProperty(IMAGE_DATA_LOCATION, IMAGE_NODE);
    }

    public void setImageDataLocation(final String path) {
        setProperty(IMAGE_DATA_LOCATION, path, IMAGE_NODE);
    }

    public String getXAxisFormat() {
        return getProperty(X_AXIS_FORMAT, IMAGE_NODE);
    }

    public void setXAxisFormat(String format) {
        setProperty(X_AXIS_FORMAT, format, IMAGE_NODE);
    }

    public String getYAxisFormat() {
        return getProperty(Y_AXIS_FORMAT, IMAGE_NODE);
    }

    public void setYAxisFormat(String format) {
        setProperty(Y_AXIS_FORMAT, format, IMAGE_NODE);
    }

    public String getPlayerVideoLocation() {
        return getProperty(PLAYER_VIDEO_LOCATION, PLAYER_NODE);
    }

    public void setPlayerLocation(final String path) {
        setProperty(PLAYER_VIDEO_LOCATION, path, PLAYER_NODE);
    }

    public String getSeekerPath(final IDataSourceBrowser browser) {
        String id = browser.getId();
        return getProperty(SEEKER_LOCATION, BROWSERS_NODE, id);
    }

    public void setSeekerPath(final IDataSourceBrowser browser, final String path) {
        String id = browser.getId();
        setProperty(SEEKER_LOCATION, path, BROWSERS_NODE, id);
    }

    public Columns[] getDisplayedColumns() {
        Columns[] columns = null;
        String result = getProperty(DISPLAYED_COLUMNS, DISPLAY_MANAGER_NODE);
        if (result != null) {
            String[] columnsStr = result.split(COLUMN_SEPARATOR);
            if ((columnsStr != null) && (columnsStr.length > 0)) {
                List<Columns> columnsList = new ArrayList<Columns>();
                Columns colValue = null;
                for (String colStr : columnsStr) {
                    try {
                        colValue = Columns.valueOf(colStr);
                        if (colValue != null) {
                            columnsList.add(colValue);
                        }
                    } catch (Exception e) {
                        LOGGER.debug("Cannot read displayed columns {}", e.getMessage());
                        LOGGER.debug("Stack trace", e);
                    }
                }
                if (!columnsList.isEmpty()) {
                    columns = columnsList.toArray(new Columns[columnsList.size()]);
                }
            }
        }
        return columns;
    }

    public void setDisplayedColumns(Columns[] columns) {
        if (columns != null) {
            StringBuilder builder = new StringBuilder();
            for (Columns col : columns) {
                builder.append(col.name());
                builder.append(COLUMN_SEPARATOR);
            }
            if (columns.length > 0) {
                builder.deleteCharAt(builder.length() - 1);
            }
            setProperty(DISPLAYED_COLUMNS, builder.toString(), DISPLAY_MANAGER_NODE);
            builder.delete(0, builder.length());
        }
    }

    public String getBrowserName(final IDataSourceBrowser browser) {
        String id = browser.getId();
        String result = getProperty(BROWSER_NAME, BROWSERS_NODE, id);
        return result;
    }

    public void setBrowserName(final IDataSourceBrowser browser, final String name) {
        String id = browser.getId();
        setProperty(BROWSER_NAME, name, BROWSERS_NODE, id);
    }

    public String getControlPanel() {
        String result = getProperty(CONTROL_LOCATION, CONTROL_NODE);
        if (result == null) {
            result = DEFAULT_CONTROL_PANEL;
        }
        return result;
    }

    public void setControlPanel(final String path) {
        setProperty(CONTROL_LOCATION, path, CONTROL_NODE);
    }

    public String getDateFormat() {
        String result = getProperty(DATE_FORMAT, SLIDER_NODE);
        if (result == null) {
            result = ScaledPlayer.DEFAULT_DATE_FORMAT;
        }
        return result;
    }

    public void setDateFormat(String dateFormat) {
        if ((dateFormat == null) || dateFormat.trim().isEmpty()) {
            dateFormat = ScaledPlayer.NULL_FORMAT;
        }
        setProperty(DATE_FORMAT, dateFormat, SLIDER_NODE);
    }

    public String getTimeFormat() {
        String result = getProperty(TIME_FORMAT, SLIDER_NODE);
        if (result == null) {
            result = ScaledPlayer.DEFAULT_TIME_FORMAT;
        }
        return result;
    }

    public void setTimeFormat(String timeFormat) {
        if ((timeFormat == null) || timeFormat.trim().isEmpty()) {
            timeFormat = ScaledPlayer.NULL_FORMAT;
        }
        setProperty(TIME_FORMAT, timeFormat, SLIDER_NODE);
    }

}
