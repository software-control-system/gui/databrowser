/*
 * This file is part of databrowser-gui.
 * 
 * databrowser-gui is free software: you can redistribute it and/or modify it under the terms of the GNU General Public
 * License as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later
 * version.
 * 
 * databrowser-gui is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the
 * implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for more
 * details.
 * 
 * You should have received a copy of the GNU General Public License along with databrowser-gui. If not, see
 * <https://www.gnu.org/licenses/>.
 */
package org.cdma.gui.databrowser.model;

import java.awt.Component;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.Insets;
import java.awt.event.ActionEvent;
import java.awt.event.ItemEvent;
import java.awt.event.ItemListener;
import java.util.LinkedList;
import java.util.List;

import javax.swing.DefaultComboBoxModel;
import javax.swing.DefaultListCellRenderer;
import javax.swing.JComboBox;
import javax.swing.JComponent;
import javax.swing.JLabel;
import javax.swing.JList;
import javax.swing.JPanel;
import javax.swing.JPopupMenu;
import javax.swing.JSeparator;

import org.cdma.gui.databrowser.DataBrowser;
import org.cdma.gui.databrowser.DataBrowserController;
import org.cdma.gui.databrowser.interfaces.DataFormat;
import org.cdma.gui.databrowser.interfaces.DataType;
import org.cdma.gui.databrowser.interfaces.IDataSourceBrowser;
import org.cdma.gui.databrowser.interfaces.Item;
import org.cdma.gui.databrowser.util.ScaleConstants;
import org.cdma.gui.databrowser.view.item.MatrixViewer;
import org.cdma.gui.databrowser.view.item.WriteDataFrameMenu;
import org.jdesktop.swingx.action.AbstractActionExt;
import org.jdesktop.swingx.action.ActionManager;

import fr.soleil.comete.box.matrixbox.AbstractMatrixBox;
import fr.soleil.comete.definition.event.ValueConvertorEvent;
import fr.soleil.comete.definition.listener.IValueConvertorListener;
import fr.soleil.comete.definition.util.ArrayPositionConvertor;
import fr.soleil.comete.swing.ImageViewer;
import fr.soleil.comete.swing.NumberMatrixArea;
import fr.soleil.comete.swing.util.EDTManager;
import fr.soleil.data.target.matrix.IMatrixTarget;
import fr.soleil.lib.project.ObjectUtils;
import fr.soleil.lib.project.swing.ConstrainedCheckBox;

public abstract class ImageMatrixItem<T extends IMatrixTarget, B extends AbstractMatrixBox<T>>
        extends MatrixItem<ImageViewer> implements ScaleConstants, ItemListener, IValueConvertorListener {

    private static final String DATABROWSER = "DATABROWSER";
    private static final String WRITE_DATA = "Write data";

    protected ImageViewer theViewer;
    protected String informationKey;
    protected WriteDataFrameMenu writeFrameMenu;
    protected B matrixBox = generateMatrixBox();

    private final List<Item> scaleItemsList;
    protected JPanel topPanel;
    protected ArrayPositionConvertor xValueConvertor, yValueConvertor;
    protected JComboBox<Object> xScaleCombo, yScaleCombo;
    protected DefaultComboBoxModel<Object> xScaleModel, yScaleModel;
    protected ConstrainedCheckBox xOrdinateBox, yOrdinateBox;

    public ImageMatrixItem(MatrixViewer matrixViewer, Item item, DataBrowserController controller) {
        super(matrixViewer, item, controller);
        scaleItemsList = new LinkedList<>();
    }

    protected abstract B generateMatrixBox();

    protected abstract B getMainMatrixBox();

    @Override
    protected ImageViewer createViewer() {
        theViewer = new ImageViewer();
        theViewer.setApplicationId(DATABROWSER);
        theViewer.setCleanOnDataSetting(false);
        theViewer.setAlwaysFitMaxSize(true);
        xValueConvertor = generateConvertor();
        yValueConvertor = generateConvertor();
        xOrdinateBox = generateCheckBox("MatrixViewer.Scale.Concertor.Ordinate.Text",
                "MatrixViewer.Scale.Concertor.Ordinate.Tooltip.X", xValueConvertor);
        yOrdinateBox = generateCheckBox("MatrixViewer.Scale.Concertor.Ordinate.Text",
                "MatrixViewer.Scale.Concertor.Ordinate.Tooltip.Y", yValueConvertor);
        theViewer.setXAxisConvertor(xValueConvertor);
        theViewer.setYAxisConvertor(yValueConvertor);
        MatrixViewer matrixViewer = ObjectUtils.recoverObject(matrixViewerRef);
        if (matrixViewer != null) {
            matrixViewer.getNumberMatrixBox().setCleanWidgetOnDisconnect(theViewer, false);
        }

        // init scale comboboxes with ImageViewer
        xScaleModel = new DefaultComboBoxModel<>();
        yScaleModel = new DefaultComboBoxModel<>();
        xScaleModel.addElement(DEFAULT_SCALE);
        yScaleModel.addElement(DEFAULT_SCALE);

        xScaleCombo = new JComboBox<>(xScaleModel);
        yScaleCombo = new JComboBox<>(yScaleModel);

        xScaleCombo.setRenderer(new ScaleComboRenderer());
        yScaleCombo.setRenderer(new ScaleComboRenderer());

        xScaleCombo.addItemListener((e) -> {
            MatrixViewer parent = ObjectUtils.recoverObject(matrixViewerRef);
            if (parent != null) {
                if (e.getStateChange() == ItemEvent.DESELECTED) {
                    parent.getNumberMatrixBox().disconnectWidgetFromAll(xValueConvertor);
                    for (MatrixItem<?> matrixItem : parent.getDisplayedMatrixItems()) {
                        if (matrixItem instanceof NumberMatrixItem) {
                            ((NumberMatrixItem) matrixItem).setXAxisConvertor(null);
                        }
                    }
                } else if (e.getStateChange() == ItemEvent.SELECTED) {
                    Object selectedItem = xScaleCombo.getSelectedItem();
                    if (selectedItem != DEFAULT_SCALE) {
                        parent.getNumberMatrixBox().connectWidget(xValueConvertor, ((Item) selectedItem).getKey());
                        for (MatrixItem<?> matrixItem : parent.getDisplayedMatrixItems()) {
                            if (matrixItem instanceof NumberMatrixItem) {
                                ((NumberMatrixItem) matrixItem).setXAxisConvertor(xValueConvertor);
                            }
                        }
                    }
                }
            }
        });

        yScaleCombo.addItemListener((e) -> {
            MatrixViewer parent = ObjectUtils.recoverObject(matrixViewerRef);
            if (parent != null) {
                if (e.getStateChange() == ItemEvent.DESELECTED) {
                    parent.getNumberMatrixBox().disconnectWidgetFromAll(yValueConvertor);
                    for (MatrixItem<?> matrixItem : parent.getDisplayedMatrixItems()) {
                        if (matrixItem instanceof NumberMatrixItem) {
                            ((NumberMatrixItem) matrixItem).setYAxisConvertor(null);
                        }
                    }
                } else if (e.getStateChange() == ItemEvent.SELECTED) {
                    Object selectedItem = yScaleCombo.getSelectedItem();
                    if (selectedItem != DEFAULT_SCALE) {
                        parent.getNumberMatrixBox().connectWidget(yValueConvertor, ((Item) selectedItem).getKey());
                        for (MatrixItem<?> matrixItem : parent.getDisplayedMatrixItems()) {
                            if (matrixItem instanceof NumberMatrixItem) {
                                ((NumberMatrixItem) matrixItem).setYAxisConvertor(yValueConvertor);
                            }
                        }
                    }
                }
            }
        });
        return theViewer;
    }

    @Override
    protected JPanel getTopPanel() {
        if (topPanel == null) {
            topPanel = new JPanel(new GridBagLayout());
            int x = 0, y = 0, gap = 10;
            x = addComponents(topPanel, x, y, gap, new JLabel(DataBrowser.MESSAGES.getString("MatrixViewer.XScale")),
                    xScaleCombo);
            addComponents(topPanel, x, y++, 0, new JLabel(DataBrowser.MESSAGES.getString("MatrixViewer.YScale")),
                    yScaleCombo);
            x = 0;
            x = addComponents(topPanel, x, y, gap, null, xOrdinateBox);
            addComponents(topPanel, ++x, y++, 0, null, yOrdinateBox);
        }
        return topPanel;
    }

    protected int addComponents(JPanel panel, int x, int y, int gap, JComponent comp1, JComponent comp2) {
        if (comp1 != null) {
            GridBagConstraints constraints = new GridBagConstraints();
            constraints.fill = GridBagConstraints.HORIZONTAL;
            constraints.gridx = x++;
            constraints.gridy = y;
            constraints.weightx = 0;
            constraints.weighty = 0;
            constraints.insets = new Insets(0, 0, 0, 5);
            panel.add(comp1, constraints);
        }
        if (comp2 != null) {
            GridBagConstraints constraints = new GridBagConstraints();
            constraints.fill = GridBagConstraints.HORIZONTAL;
            constraints.gridx = x++;
            constraints.gridy = y;
            constraints.weightx = 0.5;
            constraints.weighty = 0;
            if (comp1 == null) {
                constraints.gridwidth = 2;
            }
            constraints.insets = new Insets(0, 0, 0, gap);
            panel.add(comp2, constraints);
        }
        return x;
    }

    protected ArrayPositionConvertor generateConvertor() {
        ArrayPositionConvertor convertor = new ArrayPositionConvertor();
        convertor.addValueConvertorListener(this);
        return convertor;
    }

    protected ConstrainedCheckBox generateCheckBox(String textKey, String tooltipKey,
            ArrayPositionConvertor convertor) {
        ConstrainedCheckBox checkBox = new ConstrainedCheckBox(DataBrowser.MESSAGES.getString(textKey));
        checkBox.setToolTipText(DataBrowser.MESSAGES.getString(tooltipKey));
        checkBox.setSelected(convertor.isFollowOrdinate());
        checkBox.addItemListener(this);
        return checkBox;
    }

    protected T getReadTarget() {
        @SuppressWarnings("unchecked")
        T target = (T) viewer;
        return target;
    }

    protected T getWriteMatrixArea() {
        @SuppressWarnings("unchecked")
        T area = writeFrameMenu == null ? null : (T) writeFrameMenu.numberMatrixArea;
        return area;
    }

    protected void addMenuToSettableKey() {
        IDataSourceBrowser browser = getBrowser();
        boolean settable = browser.isSettable(readKey);
        if (settable) {
            addWriteDataMenu();
            informationKey = readKey.getInformationKey();
        }
    }

    public void addWriteDataMenu() {
        if (writeFrameMenu == null) {
            writeFrameMenu = new WriteDataFrameMenu();
        } else {
            writeFrameMenu.clear();
        }
        if (theViewer != null) {
            EDTManager.INSTANCE.runInDrawingThread(() -> {
                JPopupMenu popupMenu = theViewer.getPopupMenu();
                AbstractActionExt writeAction = new AbstractActionExt() {

                    private static final long serialVersionUID = -1106314827312150711L;

                    @Override
                    public void actionPerformed(ActionEvent e) {
                        if (informationKey != null) {
                            manageDataViews(informationKey);
                        }
                    }
                };
                writeAction.setName(WRITE_DATA);
                ActionManager.getInstance().addAction(writeAction);
                popupMenu.add(new JSeparator());
                popupMenu.add(writeAction);
            });
        }
    }

    public void manageDataViews(String dataViewId) {
        if (writeFrameMenu.numberMatrixArea == null) {
            writeFrameMenu.numberMatrixArea = new NumberMatrixArea();
        }
        if (!writeFrameMenu.numberMatrixArea.isVisible()) {
            writeFrameMenu.numberMatrixArea.setVisible(true);
        }
        T area = getWriteMatrixArea();
        if ((dataViewId != null) && (!dataViewId.isEmpty())) {
            matrixBox.disconnectWidgetFromAll(area);
            if (key != null) {
                matrixBox.connectWidget(area, key);
            }
        }
        writeFrameMenu.showMatrixAreaView();
    }

    @Override
    protected void doConnect() {
        B matrixBox = getMainMatrixBox();
        addMenuToSettableKey();
        matrixBox.connectWidget(getReadTarget(), readKey);
    }

    @Override
    protected void disconnectImmediately() {
        B matrixBox = getMainMatrixBox();
        matrixBox.connectWidget(getReadTarget(), key);
        if (writeFrameMenu != null) {
            writeFrameMenu.clear();
            matrixBox.disconnectWidgetFromAll(getWriteMatrixArea());
        }
    }

    @Override
    public void itemStateChanged(ItemEvent e) {
        if ((e != null) && (e.getItemSelectable() != null)) {
            if (e.getItemSelectable() == xOrdinateBox) {
                xValueConvertor.removeValueConvertorListener(this);
                xValueConvertor.setFollowOrdinate(xOrdinateBox.isSelected());
                xValueConvertor.addValueConvertorListener(this);
            } else if (e.getItemSelectable() == yOrdinateBox) {
                yValueConvertor.removeValueConvertorListener(this);
                yValueConvertor.setFollowOrdinate(yOrdinateBox.isSelected());
                yValueConvertor.addValueConvertorListener(this);
            }
        }
    }

    @Override
    public void convertorChanged(ValueConvertorEvent event) {
        if ((event != null) && (event.getSource() != null)) {
            if (event.getSource() == xValueConvertor) {
                EDTManager.INSTANCE.runInDrawingThread(() -> {
                    if (xValueConvertor.isFollowOrdinate() != xOrdinateBox.isSelected()) {
                        xOrdinateBox.removeItemListener(this);
                        xOrdinateBox.setSelected(xValueConvertor.isFollowOrdinate());
                        xOrdinateBox.addItemListener(this);
                    }
                });
            } else if (event.getSource() == yValueConvertor) {
                EDTManager.INSTANCE.runInDrawingThread(() -> {
                    if (yValueConvertor.isFollowOrdinate() != yOrdinateBox.isSelected()) {
                        yOrdinateBox.removeItemListener(this);
                        yOrdinateBox.setSelected(yValueConvertor.isFollowOrdinate());
                        yOrdinateBox.addItemListener(this);
                    }
                });
            }
        }
    }

    public String getXAxisFormat() {
        return theViewer == null ? null : theViewer.getXAxisFormat();
    }

    public void setXAxisFormat(String format) {
        if (theViewer != null) {
            theViewer.setXAxisFormat(format);
        }
    }

    public String getYAxisFormat() {
        return theViewer == null ? null : theViewer.getYAxisFormat();
    }

    public void setYAxisFormat(String format) {
        if (theViewer != null) {
            theViewer.setYAxisFormat(format);
        }
    }

    public void addScaleItem(final Item item) {
        DataType type = item.getType();
        DataFormat format = item.getFormat();
        if ((type == DataType.SPECTRUM || type == DataType.IMAGE) && (format == DataFormat.NUMERICAL)) {
            if (scaleItemsList.contains(item)) {
                if (xScaleModel.getIndexOf(item) < 0) {
                    xScaleModel.addElement(item);
                }
                if (yScaleModel.getIndexOf(item) < 0) {
                    yScaleModel.addElement(item);
                }
            } else {
                scaleItemsList.add(item);
                xScaleModel.addElement(item);
                yScaleModel.addElement(item);
            }
        }
    }

    public void removeScaleItem(final Item item) {
        DataType type = item.getType();
        DataFormat format = item.getFormat();

        if ((type == DataType.SPECTRUM || type == DataType.IMAGE) && (format == DataFormat.NUMERICAL)) {
            scaleItemsList.remove(item);
            if ((xScaleModel != null) && (yScaleModel != null)) {
                if (ObjectUtils.sameObject(xScaleModel.getSelectedItem(), item)) {
                    xScaleModel.setSelectedItem(DEFAULT_SCALE);
                }
                if (ObjectUtils.sameObject(yScaleModel.getSelectedItem(), item)) {
                    yScaleModel.setSelectedItem(DEFAULT_SCALE);
                }
            }
            xScaleModel.removeElement(item);
            yScaleModel.removeElement(item);
        }
    }

    public Item getImageXScaleItem() {
        Item result = null;
        Object selectedItem = xScaleCombo.getSelectedItem();
        if (selectedItem != null && selectedItem != DEFAULT_SCALE) {
            result = (Item) selectedItem;
        }
        return result;
    }

    public void setImageXScaleItem(final Item item) {
        if ((xScaleModel != null) && (item != null)) {
            addScaleItem(item);
            xScaleModel.setSelectedItem(item);
        }
    }

    public Item getImageYScaleItem() {
        Item result = null;
        if (yScaleCombo != null) {
            Object selectedItem = yScaleCombo.getSelectedItem();
            if (selectedItem != null && selectedItem != DEFAULT_SCALE) {
                result = (Item) selectedItem;
            }
        }
        return result;
    }

    public void setImageYScaleItem(final Item item) {
        if ((yScaleModel != null) && (item != null)) {
            addScaleItem(item);
            yScaleModel.setSelectedItem(item);
        }
    }

    // ///////////// //
    // Inner classes //
    // ///////////// //

    protected class ScaleComboRenderer extends DefaultListCellRenderer {

        private static final long serialVersionUID = 8834948205842512022L;

        public ScaleComboRenderer() {
            super();
        }

        @Override
        public Component getListCellRendererComponent(final JList<?> list, final Object value, final int index,
                final boolean isSelected, final boolean cellHasFocus) {
            Component component = super.getListCellRendererComponent(list, value, index, isSelected, cellHasFocus);
            if (component instanceof JLabel) {
                JLabel label = (JLabel) component;
                if (DEFAULT_SCALE.equals(value)) {
                    label.setToolTipText(null);
                } else {
                    if (value instanceof Item) {
                        Item item = (Item) value;
                        String displayName = item.getBrowser().getDisplayName(item.getKey());
                        String description = item.getBrowser().getDescription(item.getKey());
                        label.setText(displayName);
                        label.setToolTipText(description);
                    }
                }
            }
            return component;
        }
    }

}
