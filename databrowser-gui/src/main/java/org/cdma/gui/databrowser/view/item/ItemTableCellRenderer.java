/*
 * This file is part of databrowser-gui.
 * 
 * databrowser-gui is free software: you can redistribute it and/or modify it under the terms of the GNU General Public
 * License as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later
 * version.
 * 
 * databrowser-gui is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the
 * implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for more
 * details.
 * 
 * You should have received a copy of the GNU General Public License along with databrowser-gui. If not, see
 * <https://www.gnu.org/licenses/>.
 */
package org.cdma.gui.databrowser.view.item;

import java.awt.Component;

import javax.swing.JLabel;
import javax.swing.JTable;
import javax.swing.table.DefaultTableCellRenderer;

import org.cdma.gui.databrowser.interfaces.IDataSourceBrowser;
import org.cdma.gui.databrowser.interfaces.Item;

public class ItemTableCellRenderer extends DefaultTableCellRenderer {

    private static final long serialVersionUID = -2156930127631540679L;

    @Override
    public Component getTableCellRendererComponent(JTable table, Object value, boolean isSelected, boolean hasFocus,
            int row, int column) {
        Component tableCellRendererComponent = super.getTableCellRendererComponent(table, value, isSelected, hasFocus,
                row, column);
        if ((value instanceof Item) && (tableCellRendererComponent instanceof JLabel)) {
            JLabel jlabel = (JLabel) tableCellRendererComponent;
            Item item = (Item) value;
            IDataSourceBrowser browser = item.getBrowser();
            String label = browser.getDisplayName(item.getKey());
            jlabel.setText(label);
            jlabel.setToolTipText(browser.getDescription(item.getKey()));
        }
        return tableCellRendererComponent;
    }

}
