/*
 * This file is part of databrowser-gui.
 * 
 * databrowser-gui is free software: you can redistribute it and/or modify it under the terms of the GNU General Public
 * License as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later
 * version.
 * 
 * databrowser-gui is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the
 * implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for more
 * details.
 * 
 * You should have received a copy of the GNU General Public License along with databrowser-gui. If not, see
 * <https://www.gnu.org/licenses/>.
 */
package org.cdma.gui.databrowser.view;

import javax.swing.Icon;

import fr.soleil.lib.project.swing.text.AntiAliasingDelegate;
import fr.soleil.lib.project.swing.text.scroll.AutoScrolledLabel;
import fr.soleil.lib.project.swing.text.scroll.ScrollDelegate;
import fr.soleil.lib.project.swing.text.scroll.ScrollMode;

public class TitleLabel extends AutoScrolledLabel {

    private static final long serialVersionUID = -5329728128664521959L;

    public TitleLabel() {
        super();
    }

    public TitleLabel(Icon image, int horizontalAlignment) {
        super(image, horizontalAlignment);
    }

    public TitleLabel(Icon image) {
        super(image);
    }

    public TitleLabel(String text, Icon icon, int horizontalAlignment) {
        super(text, icon, horizontalAlignment);
    }

    public TitleLabel(String text, int horizontalAlignment) {
        super(text, horizontalAlignment);
    }

    public TitleLabel(String text) {
        super(text);
    }

    @Override
    protected ScrollDelegate<AutoScrolledLabel> generateScrollDelegate() {
        return new ScrollDelegate<>(this, ScrollMode.PENDULAR);
    }

    @Override
    protected AntiAliasingDelegate generateAntiAliasingDelegate() {
        return new AntiAliasingDelegate(true, this);
    }

}
