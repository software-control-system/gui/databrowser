/*
 * This file is part of databrowser-gui.
 * 
 * databrowser-gui is free software: you can redistribute it and/or modify it under the terms of the GNU General Public
 * License as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later
 * version.
 * 
 * databrowser-gui is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the
 * implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for more
 * details.
 * 
 * You should have received a copy of the GNU General Public License along with databrowser-gui. If not, see
 * <https://www.gnu.org/licenses/>.
 */
package org.cdma.gui.databrowser.view.item;

import java.awt.BorderLayout;
import java.awt.Component;
import java.awt.Window;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.List;

import javax.swing.AbstractAction;
import javax.swing.DefaultListCellRenderer;
import javax.swing.JButton;
import javax.swing.JComboBox;
import javax.swing.JDialog;
import javax.swing.JLabel;
import javax.swing.JList;
import javax.swing.JPanel;
import javax.swing.ListCellRenderer;

import org.cdma.gui.databrowser.DataBrowser;
import org.cdma.gui.databrowser.DataBrowserController;
import org.cdma.gui.databrowser.interfaces.Item;

import fr.soleil.comete.box.matrixbox.NumberMatrixBox;
import fr.soleil.comete.definition.util.ArrayPositionConvertor;
import fr.soleil.comete.swing.Player;
import fr.soleil.comete.swing.Slider;
import fr.soleil.comete.swing.chart.util.DateTimeFormatChooser;
import fr.soleil.data.service.IKey;
import fr.soleil.lib.project.date.IDateFormattable;

public class ScaledPlayer extends Player {

    private static final long serialVersionUID = 2629472341187845082L;

    private JButton dateFormatButton = null;
    private JButton scaleButton = null;
    private DataBrowserController controller = null;
    private final NumberMatrixBox numberMatrixBox;
    private final ArrayPositionConvertor valueConvertor;
    public static final String DEFAULT_DATE_FORMAT = IDateFormattable.DATE_LONG_FORMAT;
    public static final String DEFAULT_TIME_FORMAT = IDateFormattable.HOUR_MINUTE_FORMAT;
    public static final String NULL_FORMAT = "NONE";
    private static final String[] dateKeyWord = new String[] { "time", "date" };

    public static String dateFormat = DEFAULT_DATE_FORMAT;
    public static String timeFormat = DEFAULT_TIME_FORMAT;

    private DateTimeFormatChooser dateTimeFormatChooser;

    private JDialog dialog = null;
    private JPanel mainPanel = null;
    private JPanel panelSouth = null;
    private JPanel panelCenter = null;
    private JLabel labelDialog = null;
    private JButton btnOk = null;
    private JButton btnCancel = null;
    private JComboBox<Object> scaledComboBox = null;
    private Item selectedItem = null;

    public ScaledPlayer() {
        super();
        numberMatrixBox = new NumberMatrixBox();
        valueConvertor = new ArrayPositionConvertor();
        if (getScaledbutton() != null) {
            this.getButtonPanel().add(getDateFormatbutton());
            this.getButtonPanel().add(getScaledbutton());
        }
        Slider slider = getSlider();
        if (slider != null) {
            slider.setDateCompatibility(false);
            slider.setValueConvertor(valueConvertor);
            setDateFormat(dateFormat, timeFormat);
        }
    }

    public JButton getScaledbutton() {
        if (scaleButton == null) {
            String toolTipScaleButton = DataBrowser.MESSAGES.getString("Player.ToolTip");
            scaleButton = new JButton(new AbstractAction(null, DataBrowser.ICONS.getIcon("Player.ChangeScaled")) {

                private static final long serialVersionUID = -8317954333261520005L;

                @Override
                public void actionPerformed(ActionEvent e) {
                    showComboDialog();
                }
            });
            scaleButton.setToolTipText(toolTipScaleButton);
        }
        return scaleButton;
    }

    public JButton getDateFormatbutton() {
        if (dateFormatButton == null) {
            String toolTipScaleButton = DataBrowser.MESSAGES.getString("ScaledPlayer.DateFormatButton");
            dateFormatButton = new JButton(
                    new AbstractAction(null, DataBrowser.ICONS.getIcon("Slider.DateFormatButton")) {

                        private static final long serialVersionUID = -6484946188911830629L;

                        @Override
                        public void actionPerformed(ActionEvent e) {
                            showTimeFormatChooser();
                        }
                    });
            dateFormatButton.setToolTipText(toolTipScaleButton);
            dateFormatButton.setEnabled(false);
        }

        return dateFormatButton;
    }

    private void showTimeFormatChooser() {
        if (dateTimeFormatChooser == null) {
            dateTimeFormatChooser = new DateTimeFormatChooser(dateFormatButton);
            Window owner = dateTimeFormatChooser.getOwner();
            owner.setLocationRelativeTo(dateFormatButton);
        }

        dateTimeFormatChooser.setSelectTimeFormat(timeFormat);
        dateTimeFormatChooser.setSelectDateFormat(dateFormat);

        if (!dateTimeFormatChooser.isVisible()) {
            dateTimeFormatChooser.setVisible(true);
            dateTimeFormatChooser.toFront();
        }

        String[] selectFormat = dateTimeFormatChooser.getSelectedFormat();
        String selectedDateFormat = null;
        String selectedTimeFormat = null;
        if (selectFormat != null) {
            if (selectFormat.length > 0) {
                selectedDateFormat = selectFormat[0];
            }
            if (selectFormat.length > 1) {
                selectedTimeFormat = selectFormat[1];
            }
        }

        setDateFormat(selectedDateFormat, selectedTimeFormat);
    }

    private void setDateFormat(String aDateFormat, String aTimeFormat) {
        StringBuilder completeFormat = new StringBuilder();
        if (!isFormatEmpty(aDateFormat)) {
            dateFormat = aDateFormat;
            completeFormat.append(dateFormat);
        } else {
            dateFormat = NULL_FORMAT;
        }

        if (!isFormatEmpty(aTimeFormat)) {
            timeFormat = aTimeFormat;
            if (!isFormatEmpty(aDateFormat)) {
                completeFormat.append(" ");
            }
            completeFormat.append(timeFormat);
        } else {
            timeFormat = NULL_FORMAT;
        }
        if (completeFormat.length() > 0) {
            slider.setDateFormat(completeFormat.toString());
        } else {
            slider.setDateFormat(null);
        }
    }

    private boolean isFormatEmpty(String aFormat) {
        return ((aFormat == null) || aFormat.isEmpty() || NULL_FORMAT.equalsIgnoreCase(aFormat));
    }

    public void setController(DataBrowserController controller) {
        this.controller = controller;
    }

    private void builComboDialog() {
        if (dialog == null) {
            dialog = new JDialog();
            mainPanel = new JPanel();
            panelSouth = new JPanel();
            panelCenter = new JPanel();
            labelDialog = new JLabel();
            btnOk = new JButton();
            btnCancel = new JButton();
            scaledComboBox = new JComboBox<>();

            // ActionListener of button
            btnOk.addActionListener(new ActionListener() {

                @Override
                public void actionPerformed(ActionEvent e) {
                    Object tmpSelectedItem = scaledComboBox.getSelectedItem();
                    if (tmpSelectedItem instanceof Item) {
                        selectedItem = (Item) tmpSelectedItem;
                    } else {
                        selectedItem = null;
                    }
                    dialog.dispose();
                }
            });

            btnCancel.addActionListener(new ActionListener() {

                @Override
                public void actionPerformed(ActionEvent e) {
                    selectedItem = null;
                    dialog.dispose();
                }
            });

            ListCellRenderer<Object> listCellRenderer = new DefaultListCellRenderer() {

                private static final long serialVersionUID = 7294967144578598315L;

                @Override
                public Component getListCellRendererComponent(final JList<?> list, final Object value, final int index,
                        final boolean isSelected, final boolean cellHasFocus) {

                    Component component = super.getListCellRendererComponent(list, value, index, isSelected,
                            cellHasFocus);

                    if (component instanceof JLabel) {
                        if (value instanceof Item) {
                            Item item = (Item) value;
                            String displayName = item.getBrowser().getDisplayName(item.getKey());
                            String description = item.getBrowser().getDescription(item.getKey());
                            ((JLabel) component).setText(displayName);
                            ((JLabel) component).setToolTipText(description);
                        }
                    }
                    return component;
                }
            };

            // Create dialog and add component
            scaledComboBox.setRenderer(listCellRenderer);
            labelDialog.setText(DataBrowser.MESSAGES.getString("Player.ChangeScaled"));

            scaledComboBox.setSize(250, 50);

            btnOk.setText(DataBrowser.MESSAGES.getString("ScaledPlayer.ButtonOk"));
            btnCancel.setText(DataBrowser.MESSAGES.getString("ScaledPlayer.ButtonCancel"));

            mainPanel.setLayout(new BorderLayout());
            mainPanel.add(labelDialog, BorderLayout.NORTH);

            panelCenter.add(scaledComboBox);

            panelSouth.add(btnOk);
            panelSouth.add(btnCancel);
            mainPanel.add(panelSouth, BorderLayout.SOUTH);
            mainPanel.add(panelCenter, BorderLayout.CENTER);

            dialog.add(mainPanel);

            dialog.setTitle(DataBrowser.MESSAGES.getString("Player.ChangeScaled"));
            dialog.setModal(true);
            dialog.setSize(450, 150);
        }

    }

    private void showComboDialog() {
        builComboDialog();

        if (controller != null) {
            scaledComboBox.removeAllItems();
            scaledComboBox.addItem("-");
            List<Item> openedNumericalMatrixSpectrumItems = controller.getOpenedNumericalMatrixSpectrumItems();
            for (Item item : openedNumericalMatrixSpectrumItems) {
                scaledComboBox.addItem(item);
            }

            dialog.setVisible(true);
            setDateFormat(dateFormat, timeFormat);

            if (selectedItem != null) {
                setValueConvertor(selectedItem.getKey());
            } else {
                setValueConvertor(null);
            }
        }
    }

    public void setValueConvertor(IKey key) {
        numberMatrixBox.disconnectWidgetFromAll(valueConvertor);
        boolean timeOrDate = isTimeOrDate(key);
        dateFormatButton.setEnabled(timeOrDate);
        slider.setDateCompatibility(timeOrDate);
        if (key != null) {
            numberMatrixBox.connectWidget(valueConvertor, key);
        }
        repaint();
    }

    private boolean isTimeOrDate(IKey key) {
        boolean isDate = false;
        if (key != null) {
            String informationKey = key.getInformationKey();
            if ((informationKey != null) && !informationKey.isEmpty()) {
                informationKey = informationKey.toLowerCase();
                for (String timeKey : dateKeyWord) {
                    isDate = informationKey.contains(timeKey);
                    if (isDate) {
                        break;
                    }
                }
            }
        }
        return isDate;
    }

}
