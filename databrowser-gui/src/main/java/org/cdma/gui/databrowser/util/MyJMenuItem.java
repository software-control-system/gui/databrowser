/*
 * This file is part of databrowser-gui.
 * 
 * databrowser-gui is free software: you can redistribute it and/or modify it under the terms of the GNU General Public
 * License as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later
 * version.
 * 
 * databrowser-gui is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the
 * implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for more
 * details.
 * 
 * You should have received a copy of the GNU General Public License along with databrowser-gui. If not, see
 * <https://www.gnu.org/licenses/>.
 */
package org.cdma.gui.databrowser.util;

import java.awt.Insets;

import javax.swing.Action;
import javax.swing.JMenuItem;

public class MyJMenuItem extends JMenuItem {

    // take screen insets (e.g. taskbar) into account
    // see workaround for Java 7 X11 slowness JIRA PROBLEM-516

    private static final long serialVersionUID = -5263769067922779069L;

    private final Insets componentInsets = new Insets(5, 5, 5, 5);

    public MyJMenuItem() {
        super();
    }

    public MyJMenuItem(String text) {
        super(text);
    }

    public MyJMenuItem(Action action) {
        super(action);
    }

    @Override
    public Insets getInsets() {
        return componentInsets;
    }
}
