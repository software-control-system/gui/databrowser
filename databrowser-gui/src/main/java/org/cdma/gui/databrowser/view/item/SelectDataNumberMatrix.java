/*
 * This file is part of databrowser-gui.
 * 
 * databrowser-gui is free software: you can redistribute it and/or modify it under the terms of the GNU General Public
 * License as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later
 * version.
 * 
 * databrowser-gui is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the
 * implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for more
 * details.
 * 
 * You should have received a copy of the GNU General Public License along with databrowser-gui. If not, see
 * <https://www.gnu.org/licenses/>.
 */
package org.cdma.gui.databrowser.view.item;

import java.awt.BorderLayout;
import java.awt.Component;
import java.awt.Dimension;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.Window;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.ArrayList;
import java.util.List;

import javax.swing.JButton;
import javax.swing.JDialog;
import javax.swing.JFrame;
import javax.swing.JList;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.ListSelectionModel;
import javax.swing.WindowConstants;

public class SelectDataNumberMatrix extends JDialog {

    private static final long serialVersionUID = 2457808470569552267L;

    private static JPanel mainPanel;
    private JPanel southPanel;
    private JPanel centerPanel;

    private JButton valide;
    private JButton cancel;

    private JList<String> list1;
    private JScrollPane selector;
    private List<String> displayedDataView;

    private String selectedId = null;

    public SelectDataNumberMatrix(final Window parent, final String title, final boolean modal, final boolean mode) {
        super(parent, title);
        setModal(modal);
        initialize();
    }

    private void initialize() {
        mainPanel = new JPanel(new BorderLayout());
        initializeCenterPanel();
        initializeSouthPanel();
        mainPanel.add(centerPanel, BorderLayout.CENTER);
        mainPanel.add(southPanel, BorderLayout.SOUTH);
        setContentPane(mainPanel);

    }

    private void initializeCenterPanel() {

        centerPanel = new JPanel(new GridBagLayout());
        list1 = new JList<>();
        selector = new JScrollPane(list1);
        list1.setSelectionMode(ListSelectionModel.SINGLE_SELECTION);

        final GridBagConstraints selectorConstraints = new GridBagConstraints();
        selectorConstraints.fill = GridBagConstraints.BOTH;
        selectorConstraints.gridx = 0;
        selectorConstraints.gridy = 0;
        selectorConstraints.weighty = 1;
        selectorConstraints.weightx = 1;
        selectorConstraints.gridwidth = GridBagConstraints.REMAINDER;
        centerPanel.add(selector, selectorConstraints);
    }

    private void initializeSouthPanel() {
        southPanel = new JPanel();
        valide = new JButton("OK");
        cancel = new JButton("Cancel");
        actionPerfomed();

        southPanel.add(valide);
        southPanel.add(cancel);

    }

    public void actionPerfomed() {
        valide.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(final ActionEvent e) {

                Object selectedValue = list1.getSelectedValue();
                if (selectedValue != null) {
                    selectedId = selectedValue.toString();
                }
                dispose();
            }
        });
        cancel.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(final ActionEvent e) {
                selectedId = null;
                dispose();
            }
        });
    }

    public void updateLists(final List<String> views) {
        displayedDataView = views;
        list1.setListData(displayedDataView.toArray(new String[displayedDataView.size()]));
    }

    public String showOptionDialog(final Component caller) {
        pack();
        selectedId = null;
        this.setSize(500, 500);
        setLocationRelativeTo(caller);
        this.setVisible(true);
        return selectedId;
    }

    public String getResult() {
        return selectedId;
    }

    public static void main(final String[] args) {

        JFrame f = new JFrame();

        final JButton openDialog = new JButton("Open Dialog");
        openDialog.setPreferredSize(new Dimension(200, 100));
        openDialog.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {

                List<String> arg = new ArrayList<>();
                for (int l = 0; l < 10; ++l) {
                    String dataview = "Courbe " + l;
                    arg.add(dataview);
                }

                SelectDataNumberMatrix dialog = new SelectDataNumberMatrix(null, "test dialog", true, false);
                dialog.updateLists(arg);
                String res = dialog.showOptionDialog(openDialog);
                if (res != null) {
                    System.out.println("result=" + res);
                }
            }
        });

        JPanel panel = new JPanel();
        panel.add(openDialog);
        f.add(panel);

        f.setSize(400, 200);
        f.setVisible(true);
        f.setDefaultCloseOperation(WindowConstants.EXIT_ON_CLOSE);
    }
}
