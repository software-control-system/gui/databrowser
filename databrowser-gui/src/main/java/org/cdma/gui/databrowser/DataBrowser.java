/*
 * This file is part of databrowser-gui.
 * 
 * databrowser-gui is free software: you can redistribute it and/or modify it under the terms of the GNU General Public
 * License as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later
 * version.
 * 
 * databrowser-gui is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the
 * implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for more
 * details.
 * 
 * You should have received a copy of the GNU General Public License along with databrowser-gui. If not, see
 * <https://www.gnu.org/licenses/>.
 */
package org.cdma.gui.databrowser;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Image;
import java.awt.Rectangle;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;
import java.io.File;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.List;
import java.util.ResourceBundle;

import javax.swing.AbstractAction;
import javax.swing.Box;
import javax.swing.JCheckBoxMenuItem;
import javax.swing.JComponent;
import javax.swing.JFileChooser;
import javax.swing.JFrame;
import javax.swing.JMenu;
import javax.swing.JMenuBar;
import javax.swing.JMenuItem;
import javax.swing.JPanel;
import javax.swing.SwingConstants;
import javax.swing.SwingUtilities;
import javax.swing.WindowConstants;

import org.apache.commons.cli.BasicParser;
import org.apache.commons.cli.CommandLine;
import org.apache.commons.cli.CommandLineParser;
import org.apache.commons.cli.HelpFormatter;
import org.apache.commons.cli.Option;
import org.apache.commons.cli.OptionBuilder;
import org.apache.commons.cli.Options;
import org.apache.commons.cli.ParseException;
import org.cdma.gui.databrowser.interfaces.IDataSourceBrowser;
import org.cdma.gui.databrowser.model.AbstractItem;
import org.cdma.gui.databrowser.model.DataItemViewer;
import org.cdma.gui.databrowser.util.ApplicationUtil;
import org.cdma.gui.databrowser.util.AxisFormatItem;
import org.cdma.gui.databrowser.util.DockingUtil;
import org.cdma.gui.databrowser.util.MarkerEnabledMenuItem;
import org.cdma.gui.databrowser.util.MultiExtFileFilter;
import org.cdma.gui.databrowser.util.MyJMenu;
import org.cdma.gui.databrowser.util.MyJMenuItem;
import org.cdma.gui.databrowser.util.SingleFileChooser;
import org.cdma.gui.databrowser.view.displaymanager.DisplayManagerTableModel.Columns;
import org.cdma.gui.databrowser.view.displaymanager.DisplayManagerViewer;
import org.cdma.gui.databrowser.view.information.InformationViewer;
import org.cdma.gui.databrowser.view.item.ScaledPlayer;
import org.cdma.gui.databrowser.view.tree.TreeViewer;
import org.jdesktop.swingx.JXTitledSeparator;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import fr.soleil.comete.definition.widget.properties.ChartProperties;
import fr.soleil.comete.swing.util.EDTManager;
import fr.soleil.data.controller.BasicBooleanTargetController;
import fr.soleil.data.controller.BasicTextTargetController;
import fr.soleil.data.source.BasicDataSource;
import fr.soleil.lib.project.application.performance.LoggedMemoryChecker;
import fr.soleil.lib.project.file.BatchExecutor;
import fr.soleil.lib.project.swing.Splash;
import fr.soleil.lib.project.swing.icons.Icons;

/**
 * The DataBrowser class
 * 
 * The main idea is to keep it as a JPanel, that could be embedded in any other
 * frame or panel. The method main constructs a frame to put it in. Menu bar is
 * initialized if needed. DataBrowser object offers load parameters and prepare
 * exit methods.
 * 
 * @author mainguy
 * 
 */
public class DataBrowser extends JPanel {

    private static final long serialVersionUID = 1705032374600163850L;

    private static final Logger LOGGER = LoggerFactory.getLogger(DataBrowser.class);

    public static final String ICONS_PATH = "org.cdma.gui.databrowser.icons";
    public static final Icons ICONS = new Icons(ICONS_PATH);
    public static final ResourceBundle MESSAGES = ResourceBundle.getBundle("org.cdma.gui.databrowser.messages");

    private static final String COMMAND_HELP_SHORT_OPTION = "?";
    private static final String COMMAND_HELP_LONG_OPTION = "help";
    private static final String COMMAND_HELP_DESCRIPTION = MESSAGES.getString("Help.CommandLineDescr");
    private static final String COMMAND_VIEWCONF_SHORT_OPTION = "v";
    private static final String COMMAND_VIEWCONF_LONG_OPTION = "viewconf";
    private static final String COMMAND_VIEWCONF_DESCRIPTION = MESSAGES.getString("Config.CommandLineDescr");
    private static final String COMMAND_VIEWCONF_ARGNAME = MESSAGES.getString("Config.CommandLineDescr.ArgName");

    private static final String CONFIG_FILE_PROPERTY = "DATABROWSER_CONFIGFILE";

    private static final String FRAME_TITLE = "DataBrowser";

    // Bath executor to monitoring device
    public static final BatchExecutor BATCH_EXECUTOR = new BatchExecutor();

    // InfoNode docking management
    private final JComponent rootWindow;

    private DataBrowserController controller;

    // docked views
    private TreeViewer treeViewer;
    private DataItemViewer itemViewer;
    private InformationViewer informationViewer;
    private DisplayManagerViewer displayViewer;
    private final SettingsManager settings;

    private JFileChooser viewConfigFilechooser;

    private JMenuBar menuBar;
    private LoggedMemoryChecker memoryChecker;
    private AboutDialog aboutDialog;
    private final BasicDataSource<Boolean> markerEnabledSource;
    private final BasicBooleanTargetController booleanScalarController;
    private final MarkerEnabledMenuItem markerEnabledMenuItem;
    private final BasicDataSource<String> xAxisFormatSource, yAxisFormatSource;
    private final AxisFormatItem xAxisFormatItem, yAxisFormatItem;
    private final BasicTextTargetController stringScalarController;

    public static final Splash SPLASH;
    static {
        SPLASH = new Splash(ICONS.getIcon("Application.Splash"), Color.BLACK);
        SPLASH.setTitle("DataBrowser");
        SPLASH.setCopyright("\u00a9 SOLEIL Synchrotron");
        SPLASH.setMessage("Starting application...");
        SPLASH.progress(0);
        SPLASH.setAlwaysOnTop(true);
    }

    public DataBrowser() {
        markerEnabledSource = generateBasicDataSource();
        markerEnabledMenuItem = new MarkerEnabledMenuItem();
        booleanScalarController = new BasicBooleanTargetController();
        booleanScalarController.addLink(markerEnabledSource, markerEnabledMenuItem);
        xAxisFormatSource = generateBasicDataSource();
        yAxisFormatSource = generateBasicDataSource();
        xAxisFormatItem = new AxisFormatItem(true);
        yAxisFormatItem = new AxisFormatItem(false);
        stringScalarController = new BasicTextTargetController();
        stringScalarController.addLink(xAxisFormatSource, xAxisFormatItem);
        stringScalarController.addLink(yAxisFormatSource, yAxisFormatItem);
        controller = new DataBrowserController(this);
        rootWindow = controller.getDockingUtil().createDockingArea(true, true);
        controller.setRootWindows(rootWindow);

        // ----------------
        // LOADING SETTINGS
        // ----------------
        SPLASH.progress(0);
        SPLASH.setMessage("Loading settings...");
        String configFilename = System.getProperty(CONFIG_FILE_PROPERTY);
        settings = new SettingsManager(configFilename);
        Collection<String> plugins = DataSourceManager.PLUGIN_INFO;
        SPLASH.progress(10);

        // ------------------------------
        // DISPLAYMANAGER VIEWER CREATION
        // ------------------------------
        SPLASH.setMessage("Building Display Manager Viewer...");
        displayViewer = new DisplayManagerViewer(controller);
        controller.addDataBrowserItemListener(displayViewer);
        controller.getDockingUtil().createView(DockingUtil.DISPLAY_MANAGER_VIEW_ID,
                ICONS.getIcon("View.DisplayManager"), MESSAGES.getString("View.DisplayManager"), displayViewer,
                rootWindow);
        SPLASH.progress(30);

        // --------------------
        // TREE VIEWER CREATION
        // --------------------
        SPLASH.setMessage("Building Tree Viewer: " + (plugins.isEmpty() ? "No plugin found" : plugins));
        for (IDataSourceBrowser browser : DataSourceManager.getBrowsers()) {
            String seekerPath = settings.getSeekerPath(browser);
            if (browser.getSeeker() != null) {
                browser.getSeeker().setDirectory(seekerPath);
            }

            String browserName = settings.getBrowserName(browser);
            browser.setName(browserName);
        }

        treeViewer = new TreeViewer(controller);
        controller.setTreeViewer(treeViewer);
        controller.getDockingUtil().createView(DockingUtil.TREE_VIEW_ID, ICONS.getIcon("View.Tree"),
                MESSAGES.getString("View.Tree"), treeViewer, rootWindow);
        SPLASH.progress(50);

        // ------------------------
        // DATAITEM VIEWER CREATION
        // ------------------------
        SPLASH.setMessage("Building DataItem Viewer...");
        itemViewer = new DataItemViewer(controller, false);
        booleanScalarController.addLink(markerEnabledSource, itemViewer.getMarkerEnabledTarget());
        markerEnabledSource.setData(Boolean.valueOf(settings.isMarkerEnabled()));
        stringScalarController.addLink(xAxisFormatSource, itemViewer.getXAxisFormatTarget());
        xAxisFormatSource.setData(settings.getXAxisFormat());
        stringScalarController.addLink(yAxisFormatSource, itemViewer.getYAxisFormatTarget());
        yAxisFormatSource.setData(settings.getXAxisFormat());
        controller.getDockingUtil().createView(DockingUtil.ITEM_VIEW_ID, ICONS.getIcon("View.DataItem"),
                MESSAGES.getString("View.DataItem"), itemViewer.getDockingArea(), rootWindow);
        controller.addDataBrowserItemListener(itemViewer);
        controller.setDataItemViewer(itemViewer);
        // TODO axis format

        SPLASH.progress(70);

        // ---------------------------
        // INFORMATION VIEWER CREATION
        // ---------------------------
        SPLASH.setMessage("Building Information Viewer...");
        informationViewer = new InformationViewer(controller);
        controller.addDataBrowserItemListener(informationViewer);
        treeViewer.getTree().addTreeNodeSelectionListener(informationViewer);
        controller.getDockingUtil().createView(DockingUtil.INFORMATION_VIEW_ID, ICONS.getIcon("View.Information"),
                MESSAGES.getString("View.Information"), informationViewer, rootWindow);

        SPLASH.progress(90);

        // --------------
        // FRAME CREATION
        // --------------
        SPLASH.setMessage("Building application...");

        // set the internal layout
        createDefaultLayout();

        ChartProperties chartProperties = settings.getChartProperties();
        if (chartProperties != null) {
            itemViewer.setChartProperties(chartProperties);
        }

        String chartSnapshotLocation = settings.getChartSnapshotLocation();
        if (chartSnapshotLocation != null) {
            itemViewer.setChartSnapshotLocation(chartSnapshotLocation);
        }

        String chartDataLocation = settings.getChartDataLocation();
        if (chartDataLocation != null) {
            itemViewer.setChartDataLocation(chartDataLocation);
        }

        String imageSnapshotLocation = settings.getImageSnapshotLocation();
        if (imageSnapshotLocation != null) {
            itemViewer.setImageSnapshotLocation(imageSnapshotLocation);
        }

        String imageDataLocation = settings.getImageDataLocation();
        if (imageDataLocation != null) {
            itemViewer.setImageDataLocation(imageDataLocation);
        }

        String dateFormat = settings.getDateFormat();
        if (dateFormat != null) {
            ScaledPlayer.dateFormat = dateFormat;
        }

        String timeFormat = settings.getTimeFormat();
        if (timeFormat != null) {
            ScaledPlayer.timeFormat = timeFormat;
        }

        String playerVideoLocation = settings.getPlayerVideoLocation();
        if (playerVideoLocation != null) {
            AbstractItem.currentPlayerVideoDirectory = playerVideoLocation;
        }

        Columns[] columns = settings.getDisplayedColumns();
        LOGGER.trace("displayd columns={}", Arrays.toString(columns));
        if (columns != null) {
            displayViewer.setDisplayedColumns(columns);
        }

        // Init Batch executor
        String controlPanel = settings.getControlPanel();
        // String controlPanel = "\\\\deviceservers\\tango-root\\bin\\win32\\atkpanel.bat";
        if ((controlPanel != null) && !controlPanel.isEmpty()) {
            BATCH_EXECUTOR.setBatch(controlPanel);
        }
        // BUILD ROOT WINDOW
        setLayout(new BorderLayout());
        add(rootWindow, BorderLayout.CENTER);

        SPLASH.progress(100);
        SPLASH.setVisible(false);
    }

    protected <C> BasicDataSource<C> generateBasicDataSource() {
        BasicDataSource<C> source = new BasicDataSource<>(null);
        source.setIgnoreDuplicationTest(true);
        return source;
    }

    protected void loadDocking() {
        // load the rootWindow docking
        byte[] layout = settings.getLayout();
        if (layout != null) {
            applyLayout(layout, rootWindow, null);
        }

        // load the dataItem docking
        byte[] dataItemLayout = settings.getDataItemLayout();
        if (dataItemLayout != null) {
            applyLayout(dataItemLayout, itemViewer.getDockingArea(), DockingUtil.ITEM_VIEW_ID);
        }
    }

    private void applyLayout(byte[] byteArray, JComponent area, Object id) {
        controller.getDockingUtil().setLayout(byteArray, area, id);
    }

    private void createDefaultLayout() {
        // create the default layout
        applyLayout(DockingUtil.DEFAULT_LAYOUT, rootWindow, null);
        applyLayout(DockingUtil.DEFAULT_DATAITEM_LAYOUT, itemViewer.getDockingArea(), DockingUtil.ITEM_VIEW_ID);
    }

    public Rectangle getWindowBounds() {
        return settings.getWindowBounds();
    }

    public void saveSettings() {

        // rootWindow layout information
        byte[] bytes = controller.getDockingUtil().getLayout(null, rootWindow);
        if ((bytes == null) || (bytes.length == 0)) {
            bytes = DockingUtil.DEFAULT_LAYOUT;
        }
        settings.setLayout(bytes);

        // dataItem layout information
        bytes = controller.getDockingUtil().getLayout(DockingUtil.ITEM_VIEW_ID, itemViewer.getDockingArea());
        if ((bytes != null) && (bytes.length > 0)) {
            settings.setDataItemLayout(bytes);
        }

        if (viewConfigFilechooser != null) {
            File viewConfigDirectory = viewConfigFilechooser.getCurrentDirectory();
            if (viewConfigDirectory != null) {
                settings.setViewConfigLocation(viewConfigDirectory.getAbsolutePath());
            }
        }
        settings.setMarkerEnabled(markerEnabledSource.getData());
        settings.setXAxisFormat(xAxisFormatSource.getData());
        settings.setYAxisFormat(yAxisFormatSource.getData());
        settings.setChartProperties(itemViewer.getChartProperties());
        settings.setChartSnapshotLocation(itemViewer.getChartSnapshotLocation());
        settings.setChartDataLocation(itemViewer.getChartDataLocation());
        settings.setImageSnapshotLocation(itemViewer.getImageSnapshotLocation());
        settings.setImageDataLocation(itemViewer.getImageDataLocation());
        settings.setDateFormat(ScaledPlayer.dateFormat);
        settings.setTimeFormat(ScaledPlayer.timeFormat);
        settings.setPlayerLocation(AbstractItem.currentPlayerVideoDirectory);
        settings.setDisplayedColumns(displayViewer.getDisplayedColumns());
        settings.setControlPanel(settings.getControlPanel());

        for (IDataSourceBrowser browser : DataSourceManager.getBrowsers()) {
            if (browser.getSeeker() != null) {
                String seekerDirectory = browser.getSeeker().getDirectory();
                settings.setSeekerPath(browser, seekerDirectory);
            }
        }

        settings.saveSettings();
    }

    private void createMenuBar() {
        menuBar = new JMenuBar();

        JMenuItem exitMenuItem = new MyJMenuItem(new AbstractAction(MESSAGES.getString("Action.Exit")) {
            private static final long serialVersionUID = 4239843677014722896L;

            @Override
            public void actionPerformed(final ActionEvent e) {
                // tell the frame to close itself
                JFrame topFrame = (JFrame) SwingUtilities.getWindowAncestor(DataBrowser.this);
                if (topFrame != null) {
                    WindowEvent windowClosing = new WindowEvent(topFrame, WindowEvent.WINDOW_CLOSING);
                    topFrame.dispatchEvent(windowClosing);
                }
            }
        });

        JMenuItem loadViewMenuItem = new MyJMenuItem(new AbstractAction(MESSAGES.getString("Action.ViewConfig.Load")) {
            private static final long serialVersionUID = 8914313842449514239L;

            @Override
            public void actionPerformed(final ActionEvent e) {
                initViewConfigFilechooser();

                int result = viewConfigFilechooser.showOpenDialog(DataBrowser.this);
                if (result == JFileChooser.APPROVE_OPTION) {
                    loadViewConfigFile(viewConfigFilechooser.getSelectedFile().getPath());
                }
            }
        });

        JMenuItem saveViewMenuItem = new MyJMenuItem(new AbstractAction(MESSAGES.getString("Action.ViewConfig.Save")) {
            private static final long serialVersionUID = -2308960329500476607L;

            @Override
            public void actionPerformed(final ActionEvent e) {
                initViewConfigFilechooser();

                int result = viewConfigFilechooser.showSaveDialog(DataBrowser.this);
                if (result == JFileChooser.APPROVE_OPTION) {
                    saveViewConfigFile(viewConfigFilechooser.getSelectedFile().getAbsolutePath());
                }
            }
        });

        JMenuItem aboutMenuItem = new MyJMenuItem(new AbstractAction(MESSAGES.getString("Action.About")) {
            private static final long serialVersionUID = 8914313842449514239L;

            @Override
            public void actionPerformed(final ActionEvent e) {
                JFrame topFrame = (JFrame) SwingUtilities.getWindowAncestor(DataBrowser.this);
                if (topFrame != null) {
                    if (aboutDialog == null) {
                        aboutDialog = new AboutDialog(topFrame);
                    }
                    aboutDialog.setVisible(true);
                }
            }
        });

        JMenuItem resetLayoutMenuItem = new MyJMenuItem(new AbstractAction(MESSAGES.getString("Action.ResetLayout")) {

            private static final long serialVersionUID = 9009139435121087121L;

            @Override
            public void actionPerformed(final ActionEvent e) {
                createDefaultLayout();
            }
        });

        final JCheckBoxMenuItem menuShowAllScalarLabel = new JCheckBoxMenuItem(
                MESSAGES.getString("Action.ViewConfig.Label.Full.Show"));
        menuShowAllScalarLabel.setSelected(false);
        menuShowAllScalarLabel.addActionListener(new ActionListener() {

            @Override
            public void actionPerformed(ActionEvent e) {
                boolean showFullScalarLabel = menuShowAllScalarLabel.isSelected();
                controller.setShowFullScalarLabel(showFullScalarLabel);
            }
        });

        JMenu fileMenu = new MyJMenu(MESSAGES.getString("Menu.File"));
        fileMenu.add(loadViewMenuItem);
        fileMenu.add(saveViewMenuItem);
        fileMenu.addSeparator();
        fileMenu.add(exitMenuItem);

        JMenu preferenceMenu = new MyJMenu(MESSAGES.getString("Menu.Preference"));
        preferenceMenu.add(menuShowAllScalarLabel);
        preferenceMenu.add(markerEnabledMenuItem);
        JXTitledSeparator separator = new JXTitledSeparator(MESSAGES.getString("Action.ViewConfig.Axis.Image"),
                SwingConstants.CENTER);
        preferenceMenu.add(separator);
        preferenceMenu.add(xAxisFormatItem);
        preferenceMenu.add(yAxisFormatItem);

        JMenu helpMenu = new MyJMenu(MESSAGES.getString("Menu.Help"));
        helpMenu.add(resetLayoutMenuItem);
        helpMenu.add(aboutMenuItem);

        memoryChecker = new LoggedMemoryChecker(DataBrowser.class.getName());
        memoryChecker.setOpaque(false);
        memoryChecker.setMaximumSize(memoryChecker.getPreferredSize());
        memoryChecker.start();

        menuBar.add(fileMenu);
        menuBar.add(preferenceMenu);
        menuBar.add(helpMenu);
        menuBar.add(Box.createHorizontalGlue());
        menuBar.add(memoryChecker);
    }

    public JMenuBar getMenuBar() {
        if (menuBar == null) {
            createMenuBar();
        }
        return menuBar;
    }

    public void loadSource(final LoadParameter param) {
        if (param != null) {
            controller.loadSource(param);
        }
    }

    public void loadSources(final List<LoadParameter> paramList) {
        if ((paramList != null) && !paramList.isEmpty()) {
            controller.loadSources(paramList);
        }
    }

    private void initViewConfigFilechooser() {
        if (viewConfigFilechooser == null) {
            viewConfigFilechooser = new SingleFileChooser();

            MultiExtFileFilter viewConfigFileFilter = new MultiExtFileFilter(
                    MESSAGES.getString("Config.FileFilterDescr"), "xml");
            viewConfigFilechooser.setFileFilter(viewConfigFileFilter);

            String viewConfigLocation = settings.getViewConfigLocation();
            if (viewConfigLocation != null) {
                viewConfigFilechooser.setCurrentDirectory(new File(viewConfigLocation));
            }
        }
    }

    public void loadViewConfigFile(final String viewFileName) {
        controller.importViewConfiguration(viewFileName);
    }

    public void saveViewConfigFile(final String viewFileName) {
        controller.exportViewConfiguration(viewFileName);
    }

    public void prepareExit() {

        // TODO controller.notifyClosing();
        controller.closeAllSources();

        JFrame topFrame = (JFrame) SwingUtilities.getWindowAncestor(this);
        if (topFrame != null) {
            settings.setWindowBounds(topFrame.getBounds());
        }

        saveSettings();
    }

    private static void createAndShowGUI(final CommandLine commandLine) {
        final DataBrowser dataBrowser = new DataBrowser();

        JFrame frame = getDataBrowserFrame(dataBrowser, true);
        frame.setVisible(true);

        // load view configuration file
        if (commandLine.hasOption(COMMAND_VIEWCONF_LONG_OPTION)) {
            String optionValue = commandLine.getOptionValue(COMMAND_VIEWCONF_LONG_OPTION);
            dataBrowser.loadViewConfigFile(optionValue);
        }

        // load sources from command line
        String[] args = commandLine.getArgs();
        List<LoadParameter> paramList = new ArrayList<>(args.length);

        // prepare arguments to load
        for (String arg : args) {
            paramList.add(new PathParameter(arg));
        }
        dataBrowser.loadSources(paramList);

        // release memory once everything is loaded
        System.gc();
    }

    public static JFrame getDataBrowserFrame(final DataBrowser dataBrowser, final boolean standalone) {

        String frameTitle = FRAME_TITLE;
        String version = ApplicationUtil.getFileJarVersion(DataBrowser.class);
        if ((version != null) && !version.isEmpty()) {
            frameTitle = frameTitle + " " + version;
        }

        final JFrame frame = new JFrame(frameTitle);

        List<Image> iconList = new ArrayList<>(3);
        iconList.add(ICONS.getIcon("Application.Icon.16").getImage());
        iconList.add(ICONS.getIcon("Application.Icon.24").getImage());
        iconList.add(ICONS.getIcon("Application.Icon.32").getImage());
        frame.setIconImages(iconList);

        if (standalone) {
            frame.setDefaultCloseOperation(WindowConstants.EXIT_ON_CLOSE);
        } else {
            frame.setDefaultCloseOperation(WindowConstants.DISPOSE_ON_CLOSE);
        }

        frame.addWindowListener(new WindowAdapter() {
            @Override
            public void windowClosing(final WindowEvent e) {
                dataBrowser.prepareExit();
            }
        });

        frame.setContentPane(dataBrowser);
        frame.setJMenuBar(dataBrowser.getMenuBar());

        frame.setBounds(dataBrowser.getWindowBounds());
        dataBrowser.loadDocking();
        return frame;
    }

    private static void showHelpMessage(final Options options) {
        // automatically generate the help statement
        HelpFormatter helpFormatter = new HelpFormatter();
        helpFormatter.printHelp(DataBrowser.class.getSimpleName(), options);
        System.exit(0);
    }

    public static void main(final String... args) {
        LOGGER.info("DataBrowser launched by " + System.getProperty("user.name"));
        Options options = new Options();

        OptionBuilder.withLongOpt(COMMAND_HELP_LONG_OPTION);
        OptionBuilder.withDescription(COMMAND_HELP_DESCRIPTION);
        Option helpOption = OptionBuilder.create(COMMAND_HELP_SHORT_OPTION);
        options.addOption(helpOption);

        OptionBuilder.withLongOpt(COMMAND_VIEWCONF_LONG_OPTION);
        OptionBuilder.withDescription(COMMAND_VIEWCONF_DESCRIPTION);
        OptionBuilder.withArgName(COMMAND_VIEWCONF_ARGNAME);
        OptionBuilder.hasArg();
        Option option = OptionBuilder.create(COMMAND_VIEWCONF_SHORT_OPTION);
        options.addOption(option);

        CommandLineParser parser = new BasicParser();
        try {
            final CommandLine commandLine = parser.parse(options, args);

            if (commandLine.hasOption(COMMAND_HELP_SHORT_OPTION)) {
                showHelpMessage(options);
            } else {
                EDTManager.INSTANCE.runInDrawingThread(() -> {
                    createAndShowGUI(commandLine);
                });
            }
        } catch (ParseException e) {
            LOGGER.error("Impossible to main arguments {}", e.getMessage());
            LOGGER.debug("Stack trace", e);
            showHelpMessage(options);
        }
    }

}
