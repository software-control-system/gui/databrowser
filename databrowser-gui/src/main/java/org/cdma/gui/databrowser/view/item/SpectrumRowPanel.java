/*
 * This file is part of databrowser-gui.
 * 
 * databrowser-gui is free software: you can redistribute it and/or modify it under the terms of the GNU General Public
 * License as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later
 * version.
 * 
 * databrowser-gui is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the
 * implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for more
 * details.
 * 
 * You should have received a copy of the GNU General Public License along with databrowser-gui. If not, see
 * <https://www.gnu.org/licenses/>.
 */
package org.cdma.gui.databrowser.view.item;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Component;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;

import javax.swing.BorderFactory;
import javax.swing.JLabel;
import javax.swing.JPanel;

import fr.soleil.data.target.scalar.ITextTarget;

public class SpectrumRowPanel extends JPanel {

    private static final long serialVersionUID = -192275591577980079L;

    private static final Color BACKGROUND_SELECTION_COLOR = Color.CYAN;
    // TODO avisto: we could use a matte border to highlight the selected row

    private JLabel label;
    private ITextTarget viewer;

    private JPanel labelPanel;
    private JPanel viewerPanel;

    private boolean selected = false;

    public SpectrumRowPanel(final JLabel labelComp, final ITextTarget viewerComp) {
        label = labelComp;
        viewer = viewerComp;

        initComponents();
        layoutComponents();
    }

    private void initComponents() {
        labelPanel = new JPanel();
        labelPanel.setBackground(BACKGROUND_SELECTION_COLOR);

        viewerPanel = new JPanel();
        viewerPanel.setBackground(BACKGROUND_SELECTION_COLOR);

        setSelected(false);
    }

    private void layoutComponents() {
        if (label != null) {
            layoutLabelPanel();
        }
        if (viewer != null) {
            layoutViewerPanel();
        }
    }

    private void layoutLabelPanel() {
        labelPanel.setBorder(BorderFactory.createEmptyBorder(5, 5, 5, 0));

        labelPanel.setLayout(new BorderLayout());
        labelPanel.add(label, BorderLayout.EAST);
    }

    private void layoutViewerPanel() {
        // empty border stands for the space between label and viewer
        viewerPanel.setBorder(BorderFactory.createEmptyBorder(5, 10, 5, 5));

        viewerPanel.setLayout(new GridBagLayout());
        GridBagConstraints gc = new GridBagConstraints();
        gc.fill = GridBagConstraints.HORIZONTAL;
        gc.weightx = 1d;
        viewerPanel.add((Component) viewer, gc);
    }

    public void setSelected(final boolean selected) {
        this.selected = selected;

        labelPanel.setOpaque(selected);
        viewerPanel.setOpaque(selected);

        labelPanel.repaint();
        viewerPanel.repaint();
    }

    public boolean isSelected() {
        return selected;
    }

    public JLabel getLabel() {
        return label;
    }

    public ITextTarget getViewer() {
        return viewer;
    }

    public JPanel getLabelPanel() {
        return labelPanel;
    }

    public JPanel getViewerPanel() {
        return viewerPanel;
    }

}
