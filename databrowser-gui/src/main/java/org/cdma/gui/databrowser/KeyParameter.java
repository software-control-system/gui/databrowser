/*
 * This file is part of databrowser-gui.
 * 
 * databrowser-gui is free software: you can redistribute it and/or modify it under the terms of the GNU General Public
 * License as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later
 * version.
 * 
 * databrowser-gui is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the
 * implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for more
 * details.
 * 
 * You should have received a copy of the GNU General Public License along with databrowser-gui. If not, see
 * <https://www.gnu.org/licenses/>.
 */
package org.cdma.gui.databrowser;

import org.cdma.gui.databrowser.interfaces.IDataSourceBrowser;

import fr.soleil.data.service.IKey;

public final class KeyParameter implements LoadParameter {

    private final IDataSourceBrowser browser;
    private final IKey key;

    public KeyParameter(final IDataSourceBrowser browser, final IKey key) {
        this.browser = browser;
        this.key = key;
    }

    public IDataSourceBrowser getBrowser() {
        return browser;
    }

    public IKey getKey() {
        return key;
    }

    @Override
    public String toString() {
        return "key param: {" + getBrowser().getName() + ", " + getKey() + "}";
    }

}
