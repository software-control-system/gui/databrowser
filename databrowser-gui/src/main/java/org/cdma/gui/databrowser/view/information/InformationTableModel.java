/*
 * This file is part of databrowser-gui.
 * 
 * databrowser-gui is free software: you can redistribute it and/or modify it under the terms of the GNU General Public
 * License as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later
 * version.
 * 
 * databrowser-gui is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the
 * implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for more
 * details.
 * 
 * You should have received a copy of the GNU General Public License along with databrowser-gui. If not, see
 * <https://www.gnu.org/licenses/>.
 */
package org.cdma.gui.databrowser.view.information;

import java.util.AbstractMap.SimpleEntry;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;

import javax.swing.table.AbstractTableModel;

import org.cdma.gui.databrowser.DataBrowser;
import org.cdma.gui.databrowser.interfaces.DataType;

public class InformationTableModel extends AbstractTableModel {

    private static final long serialVersionUID = -1177009957168449079L;

    private final List<Entry<String, Object>> entryList;

    private boolean editable = false;

    public enum Columns {
        // Order of enum constants stands for columns' order
        PROPERTY, VALUE;

        @Override
        public String toString() {
            String string;
            switch (this) {
                case PROPERTY:
                    string = DataBrowser.MESSAGES.getString("Information.Column.Property");
                    break;
                case VALUE:
                    string = DataBrowser.MESSAGES.getString("Information.Column.Value");
                    break;
                default:
                    string = null;
                    break;
            }
            return string;
        }
    }

    public InformationTableModel() {
        super();
        entryList = new ArrayList<>();
    }

    // to be called in EDT
    public void setInformations(final Map<String, String> informationMap) {
        entryList.clear();
        if (informationMap != null) {
            for (Entry<String, String> entry : informationMap.entrySet()) {
                String valueToString = entry.getValue();
                Object value = parseDataType(valueToString);
                if (value == null) {
                    value = valueToString;
                }
                entryList.add(new SimpleEntry<>(entry.getKey(), value));
            }
        }
        fireTableDataChanged();
    }

    protected DataType parseDataType(String value) {
        DataType result = null;
        for (DataType type : DataType.values()) {
            if (type.toString().equals(value)) {
                result = type;
                break;
            }
        }
        return result;
    }

    // to be called in EDT
    public void clear() {
        setInformations(null);
        setEditable(false);
    }

    @Override
    public Object getValueAt(final int row, final int col) {
        Object returnValue = null;

        Entry<String, Object> entry = entryList.get(row);
        Columns column = Columns.values()[col];

        switch (column) {
            case PROPERTY:
                returnValue = entry.getKey();
                break;
            case VALUE:
                returnValue = entry.getValue();
                break;
        }
        return returnValue;
    }

    @Override
    public void setValueAt(final Object aValue, final int rowIndex, final int columnIndex) {
        if (columnIndex == Columns.VALUE.ordinal()) {
            entryList.get(rowIndex).setValue(aValue);
        }
    }

    @Override
    public boolean isCellEditable(final int rowIndex, final int columnIndex) {
        boolean isCellEditable = false;
        if (editable) {
            if (columnIndex == Columns.VALUE.ordinal()) {
                Object infoType = getValueAt(rowIndex, 0);
                if ((infoType != null) && infoType.toString().equals("INFO : Type")) {
                    Object value = getValueAt(rowIndex, columnIndex);
                    if (value != null) {
                        isCellEditable = DataType.SPECTRUM.equals(value) || DataType.IMAGE.equals(value);
                    }
                }
            }
        }
        return isCellEditable;
    }

    @Override
    public String getColumnName(final int column) {
        return Columns.values()[column].toString();
    }

    @Override
    public int getRowCount() {
        return entryList.size();
    }

    @Override
    public int getColumnCount() {
        return Columns.values().length;
    }

    public void setEditable(boolean editable) {
        this.editable = editable;
    }
}
