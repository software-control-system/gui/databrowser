/*
 * This file is part of databrowser-gui.
 * 
 * databrowser-gui is free software: you can redistribute it and/or modify it under the terms of the GNU General Public
 * License as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later
 * version.
 * 
 * databrowser-gui is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the
 * implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for more
 * details.
 * 
 * You should have received a copy of the GNU General Public License along with databrowser-gui. If not, see
 * <https://www.gnu.org/licenses/>.
 */
package org.cdma.gui.databrowser.view.tree;

import java.awt.BorderLayout;
import java.awt.Component;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.List;

import javax.swing.BorderFactory;
import javax.swing.GroupLayout;
import javax.swing.GroupLayout.Alignment;
import javax.swing.GroupLayout.ParallelGroup;
import javax.swing.GroupLayout.SequentialGroup;
import javax.swing.JButton;
import javax.swing.JComponent;
import javax.swing.JDialog;
import javax.swing.JPanel;
import javax.swing.LayoutStyle.ComponentPlacement;

import org.cdma.gui.databrowser.DataBrowser;
import org.cdma.gui.databrowser.interfaces.IDataSourceSeeker;

import fr.soleil.comete.swing.util.CometeUtils;
import fr.soleil.data.service.IKey;

public class SeekerDialog extends JDialog implements ActionListener {

    private static final long serialVersionUID = 8035789792888636358L;

    private JButton okButton = null;
    private JButton cancelButton = null;

    private IDataSourceSeeker seeker = null;

    private List<IKey> result = null;

    public SeekerDialog(final Component parent, final IDataSourceSeeker seeker) {
        super(CometeUtils.getFrameForComponent(parent));
        this.seeker = seeker;
        setModal(true);
        initComponents();
        layoutComponents();
    }

    private void initComponents() {
        okButton = new JButton(DataBrowser.MESSAGES.getString("SeekerDialog.Ok"));
        okButton.addActionListener(this);

        cancelButton = new JButton(DataBrowser.MESSAGES.getString("SeekerDialog.Cancel"));
        cancelButton.addActionListener(this);
    }

    private void layoutComponents() {
        if (seeker != null) {
            seeker.cleanComponent();
            JComponent component = seeker.getComponent();
            // use groupLayout only to link sizes of buttons, to mimic JFileChooser
            JPanel buttonPanel = new JPanel();
            GroupLayout buttonLayout = new GroupLayout(buttonPanel);
            buttonPanel.setLayout(buttonLayout);
            buttonPanel.setBorder(BorderFactory.createEmptyBorder(0, 0, 5, 5));

            SequentialGroup hGroup = buttonLayout.createSequentialGroup();
            hGroup.addGap(0, 0, Short.MAX_VALUE).addComponent(okButton).addPreferredGap(ComponentPlacement.RELATED)
                    .addComponent(cancelButton);

            ParallelGroup vGroup = buttonLayout.createParallelGroup(Alignment.BASELINE);
            vGroup.addComponent(okButton).addComponent(cancelButton);

            buttonLayout.linkSize(okButton, cancelButton);

            buttonLayout.setHorizontalGroup(hGroup);
            buttonLayout.setVerticalGroup(vGroup);

            JPanel contentPane = new JPanel();

            contentPane.setLayout(new BorderLayout(0, 4));
            contentPane.add(component, BorderLayout.CENTER);
            contentPane.add(buttonPanel, BorderLayout.SOUTH);

            setContentPane(contentPane);
            pack();
            setLocationRelativeTo(getOwner());
        }
    }

    @Override
    public void actionPerformed(final ActionEvent e) {
        if (e.getSource() == okButton && seeker != null) {
            result = seeker.getResult();
            if (result != null && !result.isEmpty()) {
                setVisible(false);
            }
        } else if (e.getSource() == cancelButton) {
            setVisible(false);
        }
    }

    public List<IKey> getResult() {
        return result;
    }

    @Override
    public void setVisible(final boolean visible) {
        if (visible != isVisible()) {
            if (seeker != null) {
                if (visible) {
                    seeker.startSeeking();
                } else {
                    seeker.stopSeeking();
                }
            }
            super.setVisible(visible);
        }
    }

}
