/*
 * This file is part of databrowser-gui.
 * 
 * databrowser-gui is free software: you can redistribute it and/or modify it under the terms of the GNU General Public
 * License as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later
 * version.
 * 
 * databrowser-gui is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the
 * implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for more
 * details.
 * 
 * You should have received a copy of the GNU General Public License along with databrowser-gui. If not, see
 * <https://www.gnu.org/licenses/>.
 */
package org.cdma.gui.databrowser.view.displaymanager;

import java.util.LinkedList;
import java.util.List;

import javax.swing.table.AbstractTableModel;

import org.cdma.gui.databrowser.DataBrowser;
import org.cdma.gui.databrowser.interfaces.DataFormat;
import org.cdma.gui.databrowser.interfaces.DataType;
import org.cdma.gui.databrowser.interfaces.IDataSourceBrowser;
import org.cdma.gui.databrowser.interfaces.Item;

import fr.soleil.data.service.IKey;

public class DisplayManagerTableModel extends AbstractTableModel {

    private static final long serialVersionUID = -4541979323424660122L;

    protected List<DisplayedItem> openedItemsList;

    public DisplayManagerTableModel() {
        initComponents();
    }

    private void initComponents() {
        openedItemsList = new LinkedList<>();
    }

    // to be called in EDT
    public void addItem(final Item item) {
        if (!openedItemsList.contains(item)) {
            IKey key = item.getKey();
            IDataSourceBrowser browser = item.getBrowser();
            String name = browser.getDisplayName(key);
            DisplayedItem displayedItem = new DisplayedItem(item, name);
            openedItemsList.add(displayedItem);
            int index = openedItemsList.lastIndexOf(displayedItem);
            fireTableRowsInserted(index, index);
        }
    }

    // to be called in EDT
    public void removeItem(final Item item) {
        // make use of Item.equals overriding
        int index = openedItemsList.indexOf(item);
        if (index > -1) {
            openedItemsList.remove(index);
            fireTableRowsDeleted(index, index);
        }
    }

    // to be called in EDT
    public void updateItem(final Item item) {
        // make use of Item.equals overriding
        int index = openedItemsList.indexOf(item);
        if (index > -1) {
            DisplayedItem displayedItem = openedItemsList.get(index);
            // for the moment, only axis can change
            displayedItem.setAxis(item.getAxis());
            displayedItem.setXItem(item.getXItem());
            displayedItem.setYItem(item.getYItem());
            fireTableRowsUpdated(index, index);
        }
    }

    // to be called in EDT
    public int indexOfItem(final Item item) {
        // make use of Item.equals overriding
        return openedItemsList.indexOf(item);
    }

    // to be called in EDT
    DisplayedItem getDisplayedItem(final int index) {
        return openedItemsList.get(index);
    }

    @Override
    public Object getValueAt(final int row, final int col) {
        Object returnValue = null;
        DisplayedItem displayedItem = openedItemsList.get(row);
        Columns column = Columns.values()[col];
        switch (column) {
            case ITEM:
                returnValue = displayedItem.getName();
                break;
            case DESCRIPTION:
                IDataSourceBrowser browser = displayedItem.getBrowser();
                returnValue = browser.getDescription(displayedItem.getKey());
                break;
            case TYPE:
                returnValue = displayedItem.getType();
                break;
            case FORMAT:
                returnValue = displayedItem.getFormat();
                break;
            case AXIS:
                returnValue = displayedItem.getAxis();
                break;
            case XSCALE:
                returnValue = displayedItem.getXItem();
                break;
            case YSCALE:
                returnValue = displayedItem.getYItem();
                break;

        }
        return returnValue;
    }

    @Override
    public boolean isCellEditable(final int rowIndex, final int columnIndex) {
        DisplayedItem displayedItem = openedItemsList.get(rowIndex);
        boolean numerical_spectrum = (((displayedItem.getType() == DataType.SPECTRUM)
                && ((displayedItem.getFormat() == DataFormat.NUMERICAL)
                        || (displayedItem.getFormat() == DataFormat.BOOLEAN))));

        boolean numerical_image = ((displayedItem.getType() == DataType.IMAGE)
                && ((displayedItem.getFormat() == DataFormat.NUMERICAL)
                        || (displayedItem.getFormat() == DataFormat.BOOLEAN)));

        return ((((columnIndex == Columns.AXIS.ordinal()) || (columnIndex == Columns.XSCALE.ordinal()))
                && (numerical_spectrum)) || numerical_image)
                || ((columnIndex == Columns.YSCALE.ordinal()) && numerical_image);

    }

    @Override
    public String getColumnName(final int column) {
        return Columns.values()[column].toString();
    }

    @Override
    public int getRowCount() {
        return openedItemsList.size();
    }

    @Override
    public int getColumnCount() {
        return Columns.values().length;
    }

    // ///////////// //
    // Inner classes //
    // ///////////// //

    // this represents a row in the table
    // this is to avoid computing things again and again
    protected class DisplayedItem extends Item {
        private final String name;

        public DisplayedItem(final Item item, final String name) {
            super(item);
            this.name = name;
        }

        public String getName() {
            return name;
        }
    }

    public enum Columns {
        // Order of enum constants stands for columns' order
        ITEM(DataBrowser.MESSAGES.getString("DisplayManager.Column.Item")),
        DESCRIPTION(DataBrowser.MESSAGES.getString("DisplayManager.Column.Alias")),
        TYPE(DataBrowser.MESSAGES.getString("DisplayManager.Column.Type")),
        FORMAT(DataBrowser.MESSAGES.getString("DisplayManager.Column.Format")),
        AXIS(DataBrowser.MESSAGES.getString("DisplayManager.Column.Axis")),
        XSCALE(DataBrowser.MESSAGES.getString("DisplayManager.Column.XScale")),
        YSCALE(DataBrowser.MESSAGES.getString("DisplayManager.Column.YScale"));

        private final String name;

        private Columns(String name) {
            this.name = name;
        }

        @Override
        public String toString() {
            return name;
        }
    }

}
