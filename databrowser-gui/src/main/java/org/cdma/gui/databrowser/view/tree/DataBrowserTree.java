/*
 * This file is part of databrowser-gui.
 * 
 * databrowser-gui is free software: you can redistribute it and/or modify it under the terms of the GNU General Public
 * License as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later
 * version.
 * 
 * databrowser-gui is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the
 * implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for more
 * details.
 * 
 * You should have received a copy of the GNU General Public License along with databrowser-gui. If not, see
 * <https://www.gnu.org/licenses/>.
 */
package org.cdma.gui.databrowser.view.tree;

import java.awt.event.ComponentAdapter;
import java.awt.event.ComponentEvent;
import java.awt.event.ComponentListener;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.swing.JComponent;
import javax.swing.tree.TreePath;

import org.cdma.gui.databrowser.DataBrowserController;
import org.cdma.gui.databrowser.DataSourceManager;
import org.cdma.gui.databrowser.interfaces.IDataSourceBrowser;

import fr.soleil.comete.definition.widget.util.ITreeNode;
import fr.soleil.comete.swing.Tree;
import fr.soleil.comete.swing.util.EDTManager;
import fr.soleil.data.service.IKey;

public class DataBrowserTree extends Tree {

    private static final long serialVersionUID = 6320345347789097160L;

    // <browser, node>
    private final Map<IDataSourceBrowser, ITreeNode> browserNodeMap = new HashMap<>();

    // <source key, node>
    private final Map<IKey, ITreeNode> sourceNodeMap = new HashMap<>();

    // listens to resizing
    private final ComponentListener sizeListener;

    public DataBrowserTree(JComponent parent, DataBrowserController controller) {
        setRootVisible(false);
        sizeListener = new ComponentAdapter() {
            @Override
            public void componentResized(ComponentEvent e) {
                // This seems mandatory to readapt labels size.
                // To explain the problem:
                // - Let's say we have a node labeled "this is a too long text".
                // - At first display with not enough space, you will see [...long text], as expected.
                // - Then user moves the separators so that there is more space, but still not enough.
                // - Without updateUI(), the result will be something like [...a too lon].
                // - With updateUI(), the result will be something like [...a too long text], as expected.
                // Tested many times, this does not provoke too much processor use (like in PROBLEM-1851).
                updateUI();
            }
        };
        addComponentListener(sizeListener);

        setCellRenderer(new DataBrowserTreeCellRenderer(parent, controller));

        loadDataSourceBrowsers();
        expandAll(true);
    }

    private void loadDataSourceBrowsers() {
        // create one node per data browser
        List<IDataSourceBrowser> dataBrowsers = DataSourceManager.getBrowsers();
        List<ITreeNode> browserNodes = new ArrayList<>(dataBrowsers.size());
        for (IDataSourceBrowser browser : dataBrowsers) {
            ITreeNode node = new BrowserTreeNode();
            node.setName(browser.getName());
            node.setData(browser);
            browserNodeMap.put(browser, node);
            browserNodes.add(node);
        }

        // add nodes to the tree
        ITreeNode sourcesRootNode = null;
        if (browserNodes.size() != 1) {
            sourcesRootNode = new RootTreeNode();
            sourcesRootNode.setName("Available sources");
            for (ITreeNode child : browserNodes) {
                sourcesRootNode.addNodes(child);
            }
        } else {
            // if only one source, it is used as root
            sourcesRootNode = browserNodes.get(0);
        }

        setRootNode(sourcesRootNode);
    }

    public IDataSourceBrowser getBrowserForNode(final ITreeNode node) {
        IDataSourceBrowser result = null;

        ITreeNode temp = node;
        while (temp != null) {
            if (temp instanceof BrowserTreeNode) {
                result = (IDataSourceBrowser) temp.getData();
                break;
            }
            temp = temp.getParent();
        }

        return result;
    }

    public IDataSourceBrowser getSelectedBrowser() {
        IDataSourceBrowser result = null;

        ITreeNode[] selectedNodes = getSelectedNodes();
        if (selectedNodes != null) {
            ITreeNode selectedNode = selectedNodes[0];// single selection
            // any node selected under a browser node
            for (ITreeNode browserNode : browserNodeMap.values()) {
                if (browserNode.isAncestorOf(selectedNode)) {
                    result = (IDataSourceBrowser) browserNode.getData();
                    break;
                }
            }
            // TODO remonter depuis le noeud selectionne jusqu au
            // BrowserTreeNode
        }

        return result;
    }

    public IKey getSelectedSource() {
        IKey result = null;

        ITreeNode[] selectedNodes = getSelectedNodes();
        if (selectedNodes != null) {
            ITreeNode selectedNode = selectedNodes[0];// single selection

            ITreeNode currentNode = selectedNode;
            while (!(currentNode instanceof SourceTreeNode) && (currentNode.getParent() != null)) {
                currentNode = currentNode.getParent();
            }
            if (currentNode instanceof SourceTreeNode) {
                result = (IKey) currentNode.getData();
            }
        }

        return result;
    }

    public void addSource(final IDataSourceBrowser browser, final IKey sourceKey, final ITreeNode sourceNode) {
        if (!sourceNodeMap.containsKey(sourceKey)) {
            if (sourceNode != null) {
                sourceNodeMap.put(sourceKey, sourceNode);
                // now that data structure is created, add it to the model
                final ITreeNode browserNode = browserNodeMap.get(browser);
                browserNode.addNodes(sourceNode);
                TreePath[] pathList = getPaths(browserNode);
                for (TreePath path : pathList) {
                    expandPath(path);
                }
            }
        }
    }

    public void removeSource(final IDataSourceBrowser browser, final IKey sourceKey) {
        final ITreeNode sourceNode = sourceNodeMap.remove(sourceKey);
        if (sourceNode != null) {
            final ITreeNode browserNode = browserNodeMap.get(browser);
            EDTManager.INSTANCE.runInDrawingThread(() -> {
                browserNode.removeNode(sourceNode);
            });
        }
    }

    public ITreeNode getNode(IKey sourceKey) {
        ITreeNode sourceNode = null;
        if (sourceKey != null) {
            sourceNode = sourceNodeMap.get(sourceKey);
        }
        return sourceNode;
    }

    @Override
    protected void finalize() throws Throwable {
        browserNodeMap.clear();
        sourceNodeMap.clear();
        removeComponentListener(sizeListener);
        super.finalize();
    }
}
