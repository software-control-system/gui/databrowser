/*
 * This file is part of databrowser-gui.
 * 
 * databrowser-gui is free software: you can redistribute it and/or modify it under the terms of the GNU General Public
 * License as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later
 * version.
 * 
 * databrowser-gui is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the
 * implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for more
 * details.
 * 
 * You should have received a copy of the GNU General Public License along with databrowser-gui. If not, see
 * <https://www.gnu.org/licenses/>.
 */
package org.cdma.gui.databrowser.model;

import java.awt.Component;
import java.util.ArrayList;
import java.util.Collection;
import java.util.LinkedList;
import java.util.List;

import javax.swing.ImageIcon;
import javax.swing.JComponent;
import javax.swing.JTabbedPane;
import javax.swing.TransferHandler;

import org.cdma.gui.databrowser.DataBrowser;
import org.cdma.gui.databrowser.DataBrowserController;
import org.cdma.gui.databrowser.DataBrowserItemEvent;
import org.cdma.gui.databrowser.interfaces.AxisType;
import org.cdma.gui.databrowser.interfaces.DataFormat;
import org.cdma.gui.databrowser.interfaces.DataType;
import org.cdma.gui.databrowser.interfaces.IDataBrowserItemListener;
import org.cdma.gui.databrowser.interfaces.IItemViewer;
import org.cdma.gui.databrowser.interfaces.Item;
import org.cdma.gui.databrowser.util.DockingUtil;
import org.cdma.gui.databrowser.util.ItemTransferHandler;
import org.cdma.gui.databrowser.view.item.MatrixViewer;
import org.cdma.gui.databrowser.view.item.ScalarViewer;
import org.cdma.gui.databrowser.view.item.SpectrumViewer;

import fr.soleil.comete.definition.widget.properties.ChartProperties;
import fr.soleil.comete.definition.widget.properties.ImageProperties;
import fr.soleil.comete.definition.widget.properties.PlotProperties;
import fr.soleil.comete.swing.util.EDTManager;
import fr.soleil.data.service.IKey;
import fr.soleil.data.target.scalar.IBooleanTarget;
import fr.soleil.data.target.scalar.ITextTarget;
import fr.soleil.docking.view.IView;

public class DataItemViewer implements IDataBrowserItemListener {

    private final DataBrowserController controller;

    // <type, item> it is a copy of the item from the controller
    private final List<Item> displayedItems = new LinkedList<>();

    private ScalarViewer scalarViewer;
    private MatrixViewer matrixViewer;
    private SpectrumViewer spectrumViewer;

    private JComponent dockingArea = null;

    private boolean canListen = true;

    private IView scalarView;
    private IView spectrumView;
    private IView matrixView;

    private boolean showWritableItemInTab = false;

    public DataItemViewer(final DataBrowserController controller, boolean showWritableItemInTab) {
        this.controller = controller;
        this.showWritableItemInTab = showWritableItemInTab;
        initComponents();
        layoutComponents();

    }

    public JComponent getDockingArea() {
        if (dockingArea == null) {
            dockingArea = controller.getDockingUtil().createDockingArea(true, false);
        }
        return dockingArea;
    }

    private void initComponents() {
        scalarViewer = new ScalarViewer(controller);
        matrixViewer = new MatrixViewer(controller);

        if (controller != null) {
            spectrumViewer = new SpectrumViewer(controller, showWritableItemInTab);
            scalarViewer.setTransferHandler(new ItemTransferHandler(controller));
            matrixViewer.setTransferHandler(new ItemTransferHandler(controller, DataType.IMAGE, AxisType.IMAGE));
            spectrumViewer.setTransferHandler(new ItemTransferHandler(controller, DataType.SPECTRUM, AxisType.Y1));
        } else {
            spectrumViewer = new SpectrumViewer(null, showWritableItemInTab);
        }
    }

    public IBooleanTarget getMarkerEnabledTarget() {
        return spectrumViewer.getMarkerEnabledTarget();
    }

    public ITextTarget getXAxisFormatTarget() {
        return matrixViewer.getXAxisFormatTarget();
    }

    public ITextTarget getYAxisFormatTarget() {
        return matrixViewer.getYAxisFormatTarget();
    }

    public void transferHandlerForMatrixViewer(TransferHandler handler) {
        if (handler != null) {
            matrixViewer.setTransferHandler(handler);
        }
    }

    public void transferHandlerForSpectrumViewer(TransferHandler handler) {
        if (handler != null) {
            spectrumViewer.setTransferHandler(handler);
        }
    }

    public void transferHandlerForScalarViewer(TransferHandler handler) {
        if (handler != null) {
            scalarViewer.setTransferHandler(handler);
        }
    }

    private void layoutComponents() {

        JComponent tmpDockingArea = getDockingArea();

        String titleView = DataBrowser.MESSAGES.getString("DataItem.Tab.Scalar");
        ImageIcon iconView = DataBrowser.ICONS.getIcon("DataItem.Tab.Scalar");

        scalarView = controller.getDockingUtil().createView(DockingUtil.SCALAR_VIEW_ID, iconView, titleView,
                scalarViewer, tmpDockingArea);

        titleView = DataBrowser.MESSAGES.getString("DataItem.Tab.Spectrum");
        iconView = DataBrowser.ICONS.getIcon("DataItem.Tab.Spectrum");

        spectrumView = controller.getDockingUtil().createView(DockingUtil.SPECTRUM_VIEW_ID, iconView, titleView,
                spectrumViewer, tmpDockingArea);

        titleView = DataBrowser.MESSAGES.getString("DataItem.Tab.Matrix");
        iconView = DataBrowser.ICONS.getIcon("DataItem.Tab.Matrix");

        matrixView = controller.getDockingUtil().createView(DockingUtil.IMAGE_VIEW_ID, iconView, titleView,
                matrixViewer, tmpDockingArea);
    }

    public void setValueConvertor(IKey key) {
        spectrumViewer.setValueConvertor(key);
    }

    private void setSelectedView(IView view) {
        if (view != null) {
            view.setVisible(true);
            view.select();
        }
    }

    private IItemViewer getItemViewer(final DataType type, final DataFormat format, final AxisType axis) {
        IItemViewer result = null;
        switch (type) {
            case SCALAR:
                result = scalarViewer;
                setSelectedView(scalarView);
                break;
            case SPECTRUM:
                if (axis != null) {
                    result = spectrumViewer;
                    setSelectedView(spectrumView);
                }
                break;
            case IMAGE:
                switch (axis) {
                    case IMAGE:
                        result = matrixViewer;
                        break;
                    default:
                        result = spectrumViewer;
                        break;
                }
                setSelectedView(matrixView);
                break;
            default:
                // nop
                break;
        }

        return result;
    }

    private Collection<Runnable> addItem(final Item item) {
        return addItem(item, item.getUserType(), item.getAxis());
    }

    private Collection<Runnable> addItem(final Item item, DataType type, AxisType axis) {
        Collection<Runnable> toRunInEDT = null;
        if (displayedItems.contains(item)) {
            // Should not happen
            toRunInEDT = changeItemAxis(item);
        } else {
            IItemViewer viewer = getItemViewer(type, item.getFormat(), axis);
            if (axis != null) {
                toRunInEDT = viewer.addItem(item);
                displayedItems.add(new Item(item));
            }
        }
        if (toRunInEDT == null) {
            toRunInEDT = new ArrayList<>();
        }
        if (((type == DataType.SPECTRUM) || (type == DataType.IMAGE)) && (item.getFormat() == DataFormat.NUMERICAL)) {
            toRunInEDT.add(() -> {
                matrixViewer.addScaleItem(item);
            });
        }
        if ((item != null) && (type == DataType.IMAGE)) {
            if (item.getXItem() != null) {
                toRunInEDT.add(() -> {
                    setImageXScaleItem(item.getKey(), item.getXItem());
                });
            }
            if (item.getYItem() != null) {
                toRunInEDT.add(() -> {
                    setImageYScaleItem(item.getKey(), item.getYItem());
                });
            }
        }
        return toRunInEDT;
    }

    private Collection<Runnable> removeItem(final Item item) {
        Collection<Runnable> toRunInEDT = new ArrayList<>();
        if (item != null) {
            DataType type = item.getUserType();
            if (displayedItems.contains(item)) {
                IItemViewer viewer = getItemViewer(type, item.getFormat(), item.getAxis());
                if (item.getAxis() != null) {
                    Collection<Runnable> tmp = viewer.removeItem(item);
                    toRunInEDT.addAll(tmp);
                    tmp.clear();
                    displayedItems.remove(item);
                }
            }

            if (((type == DataType.SPECTRUM) || (type == DataType.IMAGE))
                    && (item.getFormat() == DataFormat.NUMERICAL)) {
                toRunInEDT.add(() -> {
                    matrixViewer.removeScaleItem(item);
                });
            }
        }
        return toRunInEDT;
    }

    private Collection<Runnable> selectItem(final Item item) {
        Collection<Runnable> toRunInEDT = null;
        DataType type = item.getUserType();
        if (displayedItems.contains(item)) {
            IItemViewer viewer = getItemViewer(type, item.getFormat(), item.getAxis());
            toRunInEDT = viewer.selectItem(item);
            selectViewer((Component) viewer);
        }
        if (toRunInEDT == null) {
            toRunInEDT = new ArrayList<>();
        }
        return toRunInEDT;
    }

    private static void selectViewer(final Component viewer) {
        Component comp = viewer;
        if (comp != null) {
            while (comp.getParent() instanceof JTabbedPane) {
                JTabbedPane theTabbedPane = (JTabbedPane) comp.getParent();
                theTabbedPane.setSelectedComponent(comp);
                comp = theTabbedPane;
            }
        }
    }

    private Collection<Runnable> deselectItem(final Item item) {
        Collection<Runnable> toRunInEDT = null;
        DataType type = item.getUserType();
        if (displayedItems.contains(item)) {
            IItemViewer viewer = getItemViewer(type, item.getFormat(), item.getAxis());
            if (item.getAxis() != null) {
                toRunInEDT = viewer.deselectItem(item);
            }
        }
        if (toRunInEDT == null) {
            toRunInEDT = new ArrayList<>();
        }
        return toRunInEDT;
    }

    private Collection<Runnable> changeItemAxis(final Item item) {
        Collection<Runnable> toRunInEDT = new ArrayList<>();
        AxisType newAxis = item.getAxis();
        // If it was displayed
        Collection<Runnable> tmp;
        if (displayedItems.contains(item)) {
            Item oldItem = displayedItems.get(displayedItems.indexOf(item));
            if (oldItem != null) {
                tmp = removeItem(oldItem);
                toRunInEDT.addAll(tmp);
                tmp.clear();
            }
        }

        if ((newAxis == AxisType.IMAGE) && (item.getType() == DataType.SPECTRUM)) {
            item.setUserType(DataType.IMAGE);
        }

        if ((newAxis != AxisType.IMAGE) && (item.getType() == DataType.SPECTRUM)) {
            item.setUserType(null);
        }
        tmp = addItem(item);
        toRunInEDT.addAll(tmp);
        tmp.clear();
        tmp = selectItem(item);
        toRunInEDT.addAll(tmp);
        tmp.clear();
        return toRunInEDT;
    }

    public ChartProperties getChartProperties() {
        return spectrumViewer.getChartProperties();
    }

    public String getChartSnapshotLocation() {
        return spectrumViewer.getChartSnapshotLocation();
    }

    public void setChartProperties(final ChartProperties properties) {
        spectrumViewer.setChartProperties(properties);
    }

    public void setChartSnapshotLocation(final String path) {
        spectrumViewer.setChartSnapshotLocation(path);
    }

    public String getChartDataLocation() {
        return spectrumViewer.getChartDataLocation();
    }

    public void setChartDataLocation(final String path) {
        spectrumViewer.setChartDataLocation(path);
    }

    public PlotProperties getChartDataViewPlotProperties(final String id) {
        return spectrumViewer.getChartDataViewPlotProperties(id);
    }

    public void setChartDataViewPlotProperties(final IKey key, final PlotProperties plotProperties, final Item item) {
        spectrumViewer.setChartDataViewPlotProperties(key, plotProperties, item);
    }

    public Collection<String> getChartKnownDataIds() {
        return spectrumViewer.getChartKnownDataIds();
    }

    public PlotProperties getChartDataViewPlotProperties(final IKey key) {
        return spectrumViewer.getChartDataViewPlotProperties(key);
    }

    public boolean isOpenTabWithManyItem() {
        return spectrumViewer.isOpenTabWithManyItem();
    }

    public void setOpenTabWithManyItem(boolean openTabWithManyItem) {
        spectrumViewer.setOpenTabWithManyItem(openTabWithManyItem);
    }

    public boolean isAllItemsIsInTab() {
        return spectrumViewer.isAllItemsIsInTab();
    }

    public void setAllItemsIsInTab(boolean allItemsIsInTab) {
        spectrumViewer.setAllItemsIsInTab(allItemsIsInTab);
    }

    public String getImageSnapshotLocation() {
        return matrixViewer.getImageSnapshotLocation();
    }

    public void setImageSnapshotLocation(final String path) {
        matrixViewer.setImageSnapshotLocation(path);
    }

    public String getImageDataLocation() {
        return matrixViewer.getImageDataLocation();
    }

    public void setImageDataLocation(final String path) {
        matrixViewer.setImageDataLocation(path);
    }

    public Item getImageXScaleItem(final IKey imageKey) {
        return matrixViewer.getImageXScaleItem(imageKey);
    }

    public void setImageXScaleItem(final IKey imageKey, final Item item) {
        matrixViewer.setImageXScaleItem(imageKey, item);
    }

    public Item getImageYScaleItem(final IKey imageKey) {
        return matrixViewer.getImageYScaleItem(imageKey);
    }

    public void setImageYScaleItem(final IKey imageKey, final Item item) {
        matrixViewer.setImageYScaleItem(imageKey, item);
    }

    public ImageProperties getImageProperties(final IKey itemKey) {
        return matrixViewer.getImageProperties(itemKey);
    }

    public void setImageProperties(final IKey itemKey, final ImageProperties imageProperties) {
        matrixViewer.setImageProperties(itemKey, imageProperties);
    }

    // ---------------------------------------
    // IDataBrowserItemListener implementation
    // ---------------------------------------

    protected void runInEDT(Collection<Runnable> toRunInEDT) {
        EDTManager.INSTANCE.runInDrawingThread(() -> {
            for (Runnable runnable : toRunInEDT) {
                runnable.run();
            }
            toRunInEDT.clear();
        });
    }

    @Override
    public void onItemOpened(final DataBrowserItemEvent event) {
        runInEDT(addItem(event.getItem()));
    }

    @Override
    public void onItemClosed(final DataBrowserItemEvent event) {
        runInEDT(removeItem(event.getItem()));
    }

    @Override
    public void onItemSelected(final DataBrowserItemEvent event) {
        runInEDT(selectItem(event.getItem()));
    }

    @Override
    public void onItemDeselected(final DataBrowserItemEvent event) {
        runInEDT(deselectItem(event.getItem()));
    }

    @Override
    public void onItemAxisChanged(final DataBrowserItemEvent event) {
        // avoid cycling with controller
        if (canListen) {
            canListen = false;
            Collection<Runnable> toRunInEDT = changeItemAxis(event.getItem());
            toRunInEDT.add(() -> {
                canListen = true;
            });
            runInEDT(toRunInEDT);
        }
    }

}
