/*
 * This file is part of databrowser-gui.
 * 
 * databrowser-gui is free software: you can redistribute it and/or modify it under the terms of the GNU General Public
 * License as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later
 * version.
 * 
 * databrowser-gui is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the
 * implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for more
 * details.
 * 
 * You should have received a copy of the GNU General Public License along with databrowser-gui. If not, see
 * <https://www.gnu.org/licenses/>.
 */
package org.cdma.gui.databrowser.model;

import java.awt.Component;
import java.lang.ref.WeakReference;

import javax.swing.JPanel;

import org.cdma.gui.databrowser.DataBrowserController;
import org.cdma.gui.databrowser.interfaces.Item;
import org.cdma.gui.databrowser.view.item.MatrixPanel;
import org.cdma.gui.databrowser.view.item.MatrixViewer;

import fr.soleil.comete.swing.util.EDTManager;
import fr.soleil.data.target.matrix.IMatrixTarget;

public abstract class MatrixItem<C extends Component & IMatrixTarget> extends AbstractItem<C> {

    protected WeakReference<MatrixViewer> matrixViewerRef;

    private MatrixPanel panel;
    private final Object panelLock;

    public MatrixItem(final MatrixViewer matrixViewer, final Item item, DataBrowserController controller) {
        super(item, controller);
        this.matrixViewerRef = matrixViewer == null ? null : new WeakReference<MatrixViewer>(matrixViewer);
        panelLock = new Object();
        EDTManager.INSTANCE.runInDrawingThread(() -> {
            viewer = createViewer();
        });
    }

    protected abstract C createViewer();

    protected abstract JPanel getTopPanel();

    public MatrixPanel getPanel() {
        if (panel == null) {
            synchronized (panelLock) {
                if (panel == null) {
                    panel = new MatrixPanel(viewer, playerPanel, getTopPanel());
                }
            }
        }
        return panel;
    }

}
