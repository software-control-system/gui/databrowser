/*
 * This file is part of databrowser-gui.
 * 
 * databrowser-gui is free software: you can redistribute it and/or modify it under the terms of the GNU General Public
 * License as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later
 * version.
 * 
 * databrowser-gui is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the
 * implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for more
 * details.
 * 
 * You should have received a copy of the GNU General Public License along with databrowser-gui. If not, see
 * <https://www.gnu.org/licenses/>.
 */
package org.cdma.gui.databrowser.view.item;

import java.awt.GridLayout;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import javax.swing.JPanel;

import org.cdma.gui.databrowser.DataBrowserController;
import org.cdma.gui.databrowser.interfaces.IPlayerPanelListener;

import fr.soleil.comete.definition.event.PlayerEvent;
import fr.soleil.comete.definition.listener.IPlayerListener;
import fr.soleil.comete.definition.widget.IPlayer;
import fr.soleil.comete.definition.widget.util.IPlayerAnimationBehavior;
import fr.soleil.comete.swing.Player;
import fr.soleil.data.service.IKey;

public class PlayerPanel extends JPanel implements IPlayerListener, IPlayerAnimationBehavior {

    private static final long serialVersionUID = 7372571820090954104L;

    private final int[] completeShape;
    private int nbPlayers = 0;
    private int[] currentOrigin;

    private List<ScaledPlayer> playerList = null;

    private Set<IPlayerPanelListener> playerPanelListeners;

    public PlayerPanel(final int[] completeShape, final int nbPlayers, DataBrowserController controller) {
        this.completeShape = completeShape;
        this.nbPlayers = nbPlayers;

        initComponents();
        layoutComponents();
        setController(controller);
    }

    private void initComponents() {
        playerPanelListeners = new HashSet<>();

        playerList = new ArrayList<>(nbPlayers);

        currentOrigin = new int[completeShape.length];
        Arrays.fill(currentOrigin, 0);

        ScaledPlayer scaledplayer = null;
        for (int playerIndex = 0; playerIndex < nbPlayers; playerIndex++) {
            scaledplayer = new ScaledPlayer();
            // IValueConvertor
            // player.getSlider().setValueConvertor(valueConvertor)

            scaledplayer.setWheelSwitchVisible(true);
            scaledplayer.getSlider().setSnapToTicks(false);
            scaledplayer.setAnimationButtonVisible(true);
            int stackSize = completeShape[playerIndex] - 1;
            scaledplayer.initSlider(stackSize);
            // disable player if only one step
            if (completeShape[playerIndex] <= 1) {
                scaledplayer.setEnabled(false);
                scaledplayer.getButtonPanel().setVisible(false);
            } else {
                scaledplayer.addPlayerListener(this);
                scaledplayer.setPlayerAnimationBehavior(this);
            }

            playerList.add(scaledplayer);
        }
    }

    private void setController(DataBrowserController controller) {
        if (playerList != null) {
            for (ScaledPlayer player : playerList) {
                player.setController(controller);
            }
        }
    }

    private void layoutComponents() {
        setLayout(new GridLayout(0, 1));

        for (int playerIndex = 0; playerIndex < playerList.size(); playerIndex++) {
            add(playerList.get(playerIndex));
        }
    }

    @Override
    public void indexChanged(final PlayerEvent event) {
        Player player = (Player) event.getSource();
        int position = playerList.indexOf(player);
        int index = player.getIndex();
        currentOrigin[position] = index;

        fireSelectedIndexChanged(currentOrigin.clone());
    }

    @Override
    public String generateAnimation(final IPlayer player, final String targetDirectory) {
        return fireAnimationRequested(player, targetDirectory);
    }

    public void addPlayerPanelListener(final IPlayerPanelListener listener) {
        if (listener != null) {
            playerPanelListeners.add(listener);
        }
    }

    public void removePlayerPanelListener(final IPlayerPanelListener listener) {
        if (listener != null) {
            playerPanelListeners.remove(listener);
        }
    }

    private void fireSelectedIndexChanged(final int[] origins) {
        for (IPlayerPanelListener listener : playerPanelListeners) {
            listener.selectedIndexChanged(origins);
        }
    }

    private String fireAnimationRequested(final IPlayer player, final String targetDirectory) {
        String result = null;
        for (IPlayerPanelListener listener : playerPanelListeners) {
            result = listener.generateAnimation(player, targetDirectory);
        }
        return result;
    }

    public void setValueConvertor(IKey key) {
        if (playerList != null && !playerList.isEmpty()) {
            ScaledPlayer scaledPlayer = playerList.get(0);
            scaledPlayer.setValueConvertor(key);
        }
    }

    public int getNbPlayers() {
        return nbPlayers;
    }

}
