/*
 * This file is part of databrowser-gui.
 * 
 * databrowser-gui is free software: you can redistribute it and/or modify it under the terms of the GNU General Public
 * License as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later
 * version.
 * 
 * databrowser-gui is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the
 * implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for more
 * details.
 * 
 * You should have received a copy of the GNU General Public License along with databrowser-gui. If not, see
 * <https://www.gnu.org/licenses/>.
 */
package org.cdma.gui.databrowser.view.item;

import java.util.HashMap;
import java.util.Map;

import javax.swing.JPanel;

import org.cdma.gui.databrowser.DataBrowserController;
import org.cdma.gui.databrowser.interfaces.IItemViewer;
import org.cdma.gui.databrowser.model.AbstractItem;

import fr.soleil.data.service.IKey;

public abstract class AbstractItemViewer<T extends AbstractItem<?>> extends JPanel implements IItemViewer {

    private static final long serialVersionUID = 4069132962276880474L;

    protected final Map<IKey, T> displayedItems;
    protected IKey selectedKey;

    protected DataBrowserController controller;

    public AbstractItemViewer(final DataBrowserController controller, boolean init) {
        super();
        this.controller = controller;
        displayedItems = new HashMap<>();
        selectedKey = null;
        if (init) {
            initComponents();
        }
    }

    protected abstract void initComponents();

}
