/*
 * This file is part of databrowser-gui.
 * 
 * databrowser-gui is free software: you can redistribute it and/or modify it under the terms of the GNU General Public
 * License as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later
 * version.
 * 
 * databrowser-gui is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the
 * implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for more
 * details.
 * 
 * You should have received a copy of the GNU General Public License along with databrowser-gui. If not, see
 * <https://www.gnu.org/licenses/>.
 */
package org.cdma.gui.databrowser.util;

import java.awt.BorderLayout;

import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JTextField;
import javax.swing.event.DocumentEvent;
import javax.swing.event.DocumentListener;

import org.cdma.gui.databrowser.DataBrowser;

import fr.soleil.comete.definition.data.information.TextInformation;
import fr.soleil.data.mediator.Mediator;
import fr.soleil.data.target.TargetDelegate;
import fr.soleil.data.target.scalar.ITextTarget;
import fr.soleil.lib.project.ObjectUtils;

public class AxisFormatItem extends JPanel implements ITextTarget, DocumentListener {

    private static final long serialVersionUID = 3696024359326464547L;

    private static final String X_TEXT, X_TOOLTIP, Y_TEXT, Y_TOOLTIP;
    static {
        String x = DataBrowser.MESSAGES.getString("Action.ViewConfig.Axis.X");
        String y = DataBrowser.MESSAGES.getString("Action.ViewConfig.Axis.Y");
        String text = DataBrowser.MESSAGES.getString("Action.ViewConfig.Axis.Format.Text");
        String tooltip = DataBrowser.MESSAGES.getString("Action.ViewConfig.Axis.Format.Tooltip");
        X_TEXT = String.format(text, x);
        Y_TEXT = String.format(text, y);
        X_TOOLTIP = String.format(tooltip, x);
        Y_TOOLTIP = String.format(tooltip, y);
    }

    private final TargetDelegate delegate;
    protected final JLabel title;
    protected final JTextField field;

    public AxisFormatItem(boolean xAxis) {
        super(new BorderLayout(5, 0));
        delegate = new TargetDelegate();
        if (xAxis) {
            title = new JLabel(X_TEXT);
            title.setToolTipText(X_TOOLTIP);
        } else {
            title = new JLabel(Y_TEXT);
            title.setToolTipText(Y_TOOLTIP);
        }
        field = new JTextField(10);
        field.getDocument().addDocumentListener(this);
        add(title, BorderLayout.WEST);
        add(field, BorderLayout.CENTER);
    }

    protected void warnMediators() {
        String text = getText();
        updateTooltip(text);
        delegate.warnMediators(new TextInformation(this, text));
    }

    protected void updateTooltip(String text) {
        field.setToolTipText(text.isEmpty() ? null : text);
    }

    @Override
    public void addMediator(Mediator<?> mediator) {
        delegate.addMediator(mediator);
    }

    @Override
    public void removeMediator(Mediator<?> mediator) {
        delegate.removeMediator(mediator);
    }

    @Override
    public String getText() {
        String text = field.getText();
        if (text == null) {
            text = ObjectUtils.EMPTY_STRING;
        } else {
            text = text.trim();
        }
        return text;
    }

    @Override
    public void setText(String text) {
        if (!ObjectUtils.sameObject(text, getText())) {
            field.getDocument().removeDocumentListener(this);
            field.setText(text == null ? ObjectUtils.EMPTY_STRING : text.trim());
            updateTooltip(getText());
            field.getDocument().addDocumentListener(this);
        }
    }

    @Override
    public void insertUpdate(DocumentEvent e) {
        warnMediators();
    }

    @Override
    public void removeUpdate(DocumentEvent e) {
        warnMediators();
    }

    @Override
    public void changedUpdate(DocumentEvent e) {
        // not managed
    }

}
