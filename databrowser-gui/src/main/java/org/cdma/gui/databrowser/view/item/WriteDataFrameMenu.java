/*
 * This file is part of databrowser-gui.
 * 
 * databrowser-gui is free software: you can redistribute it and/or modify it under the terms of the GNU General Public
 * License as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later
 * version.
 * 
 * databrowser-gui is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the
 * implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for more
 * details.
 * 
 * You should have received a copy of the GNU General Public License along with databrowser-gui. If not, see
 * <https://www.gnu.org/licenses/>.
 */
package org.cdma.gui.databrowser.view.item;

import java.awt.event.WindowEvent;
import java.awt.event.WindowListener;
import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.Map;

import javax.swing.JFrame;

import org.cdma.gui.databrowser.model.WriteDataItem;

import fr.soleil.comete.swing.NumberMatrixArea;

public class WriteDataFrameMenu extends WriteDataItem implements WindowListener {

    private static final long serialVersionUID = -4249558898573194839L;

    private static final String NUMBER_MATRIX = "NumberMatrix";

    private final Map<String, JFrame> writeViewFrameMap;
    private final Map<String, NumberMatrixArea> numberMatrixChartViewMap;

    public WriteDataFrameMenu() {
        super();
        writeViewFrameMap = new HashMap<>();
        numberMatrixChartViewMap = new HashMap<>();
    }

    public void clear() {
        if (writeViewFrameMap != null) {
            Collection<JFrame> toClean = new ArrayList<>(writeViewFrameMap.values());
            for (JFrame frame : toClean) {
                if (frame.isVisible()) {
                    frame.setVisible(false);
                }
                clean(frame);
            }
            toClean.clear();
            writeViewFrameMap.clear();
        }
        if (numberMatrixChartViewMap != null) {
            numberMatrixChartViewMap.clear();
        }
    }

    @Override
    public void showMatrixAreaView() {
        if (numberMatrixArea != null) {
            String title = NUMBER_MATRIX;
            JFrame frame = writeViewFrameMap.get(title);
            if (frame == null) {
                frame = new JFrame();
                frame.setSize(400, 400);
                frame.setLocationRelativeTo(this);
                frame.setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
                frame.setVisible(true);
                frame.setTitle(title);
                frame.setContentPane(numberMatrixArea);

                frame.addWindowListener(this);
                writeViewFrameMap.put(title, frame);
                numberMatrixChartViewMap.put(title, numberMatrixArea);
            }
            frame.toFront();
            frame.setVisible(true);
        }
    }

    @Override
    public void windowOpened(WindowEvent e) {
    }

    @Override
    public void windowClosing(WindowEvent e) {
    }

    protected void clean(JFrame frame) {
        String title = frame.getTitle();
        if (writeViewFrameMap != null) {
            writeViewFrameMap.remove(title);
        }
        if (numberMatrixChartViewMap != null) {
            NumberMatrixArea numberMatrixArea = numberMatrixChartViewMap.get(title);
            if (numberMatrixArea != null) {
                numberMatrixChartViewMap.remove(title);
            }
        }
    }

    @Override
    public void windowClosed(WindowEvent e) {
        Object source = e.getSource();
        if (source instanceof JFrame) {
            clean((JFrame) source);
        }
    }

    @Override
    public void windowIconified(WindowEvent e) {

    }

    @Override
    public void windowDeiconified(WindowEvent e) {

    }

    @Override
    public void windowActivated(WindowEvent e) {

    }

    @Override
    public void windowDeactivated(WindowEvent e) {

    }

    @Override
    protected void finalize() throws Throwable {
        clear();
        super.finalize();
    }

}