/*
 * This file is part of databrowser-gui.
 * 
 * databrowser-gui is free software: you can redistribute it and/or modify it under the terms of the GNU General Public
 * License as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later
 * version.
 * 
 * databrowser-gui is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the
 * implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for more
 * details.
 * 
 * You should have received a copy of the GNU General Public License along with databrowser-gui. If not, see
 * <https://www.gnu.org/licenses/>.
 */
package org.cdma.gui.databrowser;

import java.awt.Cursor;
import java.awt.Frame;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.HashMap;
import java.util.HashSet;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Set;
import java.util.concurrent.CountDownLatch;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.FutureTask;

import javax.swing.JButton;
import javax.swing.JComponent;
import javax.swing.JDialog;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.SwingWorker;
import javax.xml.bind.DatatypeConverter;

import org.cdma.gui.databrowser.exception.DataBrowserException;
import org.cdma.gui.databrowser.exception.LoadException;
import org.cdma.gui.databrowser.interfaces.AxisType;
import org.cdma.gui.databrowser.interfaces.DataFormat;
import org.cdma.gui.databrowser.interfaces.DataType;
import org.cdma.gui.databrowser.interfaces.IAbstractController;
import org.cdma.gui.databrowser.interfaces.IDataBrowserItemListener;
import org.cdma.gui.databrowser.interfaces.IDataSourceBrowser;
import org.cdma.gui.databrowser.interfaces.Item;
import org.cdma.gui.databrowser.model.DataItemViewer;
import org.cdma.gui.databrowser.util.DockingUtil;
import org.cdma.gui.databrowser.view.tree.TreeViewer;
import org.jdom2.Attribute;
import org.jdom2.Document;
import org.jdom2.Element;
import org.jdom2.JDOMException;
import org.jdom2.input.DOMBuilder;
import org.jdom2.input.SAXBuilder;
import org.jdom2.output.DOMOutputter;
import org.jdom2.output.Format;
import org.jdom2.output.XMLOutputter;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import fr.soleil.comete.definition.widget.IChartViewer;
import fr.soleil.comete.definition.widget.properties.ChartProperties;
import fr.soleil.comete.definition.widget.properties.ImageProperties;
import fr.soleil.comete.definition.widget.properties.PlotProperties;
import fr.soleil.comete.definition.widget.properties.xml.ChartPropertiesXmlManager;
import fr.soleil.comete.definition.widget.properties.xml.ImagePropertiesXmlManager;
import fr.soleil.comete.definition.widget.properties.xml.PlotPropertiesXmlManager;
import fr.soleil.comete.definition.widget.util.ITreeNode;
import fr.soleil.comete.swing.util.CometeUtils;
import fr.soleil.comete.swing.util.EDTManager;
import fr.soleil.data.exception.KeyCompatibilityException;
import fr.soleil.data.service.AbstractKey;
import fr.soleil.data.service.IKey;
import fr.soleil.lib.project.swing.text.HtmlTextPane;
import fr.soleil.lib.project.xmlhelpers.HtmlEscape;
import fr.soleil.lib.project.xmlhelpers.XMLUtils;
import fr.soleil.lib.project.xmlhelpers.exception.XMLWarning;

public class DataBrowserController implements IAbstractController {

    private static final Logger LOGGER = LoggerFactory.getLogger(DataBrowserController.class);

    private static TreeViewer treeViewer;
    private DataItemViewer dataItemViewer;
    private final Set<IDataBrowserItemListener> itemListeners;
    private IChartViewer chartViewer;
    private DockingUtil dockingUtil;

    private boolean isDevicePanel;

    private JComponent rootWindow;

    // <browser, sources' keys>
    protected Map<IDataSourceBrowser, List<IKey>> openedSourcesFromBrowser;

    // <source key, items' keys>
    protected final Map<IKey, List<IKey>> openedItemsFromSource;

    // <item key, sources' keys>
    protected final Map<IKey, List<IKey>> openedSourcesForItem;

    // <item key, item>
    protected final Map<IKey, Item> items;

//    private IKey currentItemKey;

    private DataBrowser dataBrowserPanel;

    private List<Item> openedNumericalSpectrumItems;
    private List<Item> openedNumericalMatrixItems;

    protected JDialog errorDialog;
    protected HtmlTextPane errorTextPane;
    protected JButton okButton;
    protected final Object errorLock;
    boolean showFullScalarLabel;

    public DataBrowserController(final DataBrowser dataBrowserPanel) {
        this();
        this.dataBrowserPanel = dataBrowserPanel;
        dockingUtil = new DockingUtil();
        List<IDataSourceBrowser> browsers = DataSourceManager.getBrowsers();
        for (IDataSourceBrowser browser : browsers) {
            openedSourcesFromBrowser.put(browser, new ArrayList<>());
        }
    }

    public DataBrowserController() {
        dockingUtil = new DockingUtil();
        itemListeners = new HashSet<>();
        openedSourcesFromBrowser = new HashMap<>();
        openedItemsFromSource = new HashMap<>();
        openedSourcesForItem = new HashMap<>();
        items = new HashMap<>();
        openedNumericalSpectrumItems = new ArrayList<>();
        openedNumericalMatrixItems = new ArrayList<>();
        errorLock = new Object();

    }

    public static IDataSourceBrowser getDataSourceBrowserForId(final String producterId) {
        return DataSourceManager.getDataSourceBrowserForId(producterId);
    }

    private void refreshOpenedItemsList() {
        openedNumericalSpectrumItems.clear();
        openedNumericalMatrixItems.clear();
        Collection<Item> values = items.values();
        DataType type = null;
        DataFormat format = null;
        for (Item item : values) {
            type = item.getType();
            format = item.getFormat();
            if ((format == DataFormat.NUMERICAL) || (format == DataFormat.BOOLEAN)) {
                if (type == DataType.SPECTRUM) {
                    openedNumericalSpectrumItems.add(item);
                } else if (type == DataType.IMAGE) {
                    openedNumericalMatrixItems.add(item);
                }
            }
        }
    }

    public List<Item> getOpenedNumericalSpectrumItems() {
        return openedNumericalSpectrumItems;
    }

    public List<Item> getOpenedNumericalMatrixItems() {
        return openedNumericalMatrixItems;
    }

    public List<Item> getOpenedNumericalMatrixSpectrumItems() {
        List<Item> numericalMatrixSpectrumItem = new ArrayList<>();
        numericalMatrixSpectrumItem.addAll(getOpenedNumericalSpectrumItems());
        numericalMatrixSpectrumItem.addAll(getOpenedNumericalMatrixItems());
        return numericalMatrixSpectrumItem;
    }

    public void setTreeViewer(final TreeViewer treeViewer) {
        if ((DataBrowserController.treeViewer == null) && (treeViewer != null)) {
            DataBrowserController.treeViewer = treeViewer;
        }
    }

    public void setDataItemViewer(final DataItemViewer dataItemViewer) {
        this.dataItemViewer = dataItemViewer;
    }

    public DataBrowser getDataBrowserPanel() {
        return dataBrowserPanel;
    }

    public boolean canOpenSource(final String path) {
        boolean result = false;

        if (path != null) {
            // look for a dataBrowser that can handle the file
            IDataSourceBrowser browser = DataSourceManager.getProducerFromString(path);
            result = (browser != null);
        }

        return result;
    }

    public void showErrorDialog(String message, String title) {
        if (errorDialog == null) {
            synchronized (errorLock) {
                if (errorDialog == null) {
                    errorDialog = new JDialog(CometeUtils.getFrameForComponent(dataBrowserPanel), title, true);
                    JPanel mainPanel = new JPanel(new GridBagLayout());
                    errorTextPane = new HtmlTextPane();
                    errorTextPane.setEditable(false);
                    JScrollPane errorScrollPane = new JScrollPane(errorTextPane);
                    okButton = new JButton("OK");
                    okButton.addActionListener(new ActionListener() {
                        @Override
                        public void actionPerformed(ActionEvent e) {
                            errorDialog.setVisible(false);
                        }
                    });
                    GridBagConstraints errorScrollPaneConstraints = new GridBagConstraints();
                    errorScrollPaneConstraints.fill = GridBagConstraints.BOTH;
                    errorScrollPaneConstraints.gridx = 0;
                    errorScrollPaneConstraints.gridy = 0;
                    errorScrollPaneConstraints.weightx = 1;
                    errorScrollPaneConstraints.weighty = 1;
                    mainPanel.add(errorScrollPane, errorScrollPaneConstraints);
                    GridBagConstraints okButtonConstraints = new GridBagConstraints();
                    okButtonConstraints.fill = GridBagConstraints.NONE;
                    okButtonConstraints.gridx = 0;
                    okButtonConstraints.gridy = 1;
                    okButtonConstraints.weightx = 0;
                    okButtonConstraints.weighty = 0;
                    okButtonConstraints.anchor = GridBagConstraints.EAST;
                    mainPanel.add(okButton, okButtonConstraints);
                    errorDialog.setContentPane(mainPanel);
                }
            }
        }
        errorDialog.setTitle(title);
        errorTextPane.setText(message);
        errorDialog.pack();
        errorDialog.setLocationRelativeTo(errorDialog.getOwner());
        okButton.grabFocus();
        errorDialog.setVisible(true);
    }

    protected void showErrors(final List<DataBrowserException> list) {
        if (!list.isEmpty()) {
            StringBuilder sb = new StringBuilder("<html><ul>");
            for (DataBrowserException dataBrowserException : list) {
                if (dataBrowserException instanceof LoadException) {
                    LoadException e = (LoadException) dataBrowserException;
                    String sourcePath = e.getSourcePath();
                    String message = e.getMessage();

                    sb.append("<li>");
                    sb.append(sourcePath + "<br/>");
                    sb.append("<font color=#0000ff>" + HtmlEscape.escape(message, true, false) + "</font>");
                    sb.append("</li>");
                    LOGGER.info("sourcePath : {}", message);
                } else {
                    String message = dataBrowserException.getMessage();

                    sb.append("<li>");
                    sb.append(HtmlEscape.escape(message, true, false));
                    sb.append("</li>");
                    LOGGER.info("showErrors : {}", message);
                }
            }
            sb.append("</ul></html>");
            showErrorDialog(sb.toString(), DataBrowser.MESSAGES.getString("Error.Title"));
//            JOptionPane.showMessageDialog(CometeUtils.getFrameForComponent(dataBrowserPanel), sb.toString(),
//                    DataBrowser.MESSAGES.getString("Error.Title"), JOptionPane.WARNING_MESSAGE);
        }
    }

    public void isDevicePanel(boolean isDevicePanel) {
        this.isDevicePanel = isDevicePanel;
    }

    public void loadSource(final LoadParameter param) {
        ArrayList<LoadParameter> paramList = new ArrayList<>(1);
        paramList.add(param);
        loadSources(paramList);
    }

    public void loadSources(final List<LoadParameter> paramList) {
        loadSources(paramList, null);
    }

    public void loadSources(final List<LoadParameter> paramList, final CountDownLatch latch) {

        SwingWorker<List<DataBrowserException>, LoadSourceStructure> swingWorker = new LoadSourceWorker(paramList,
                latch);

        swingWorker.execute();
    }

    protected synchronized void closeSource(final IDataSourceBrowser browser, final IKey sourceKey, boolean gc) {
        if ((browser != null) && (sourceKey != null)) {
            // remove the source
            List<IKey> sourceKeys = openedSourcesFromBrowser.get(browser);
            boolean sourceWasOpened = sourceKeys.remove(sourceKey);
            if (sourceWasOpened) {
                // remove the items from that source
                List<IKey> itemKeys = openedItemsFromSource.remove(sourceKey);
                if (itemKeys != null) {
                    for (IKey itemKey : itemKeys) {
                        // remove item only if last source
                        List<IKey> sources = openedSourcesForItem.get(itemKey);
                        sources.remove(sourceKey);
                        // if no more source has opened this item, remove it
                        if (sources.isEmpty()) {
                            openedSourcesForItem.remove(itemKey);
                            deselectItems(itemKey);
                            Item removedItem = items.remove(itemKey);
                            refreshOpenedItemsList();
                            fireItemClosed(removedItem);
                        }
                    }
                    itemKeys.clear();
                }
                treeViewer.sourceClosed(browser, sourceKey);
                browser.closeSource(sourceKey);
            }
            if (gc) {
                callGC();
            }
        }
    }

    public synchronized void closeSource(final IDataSourceBrowser browser, final IKey sourceKey) {
        closeSource(browser, sourceKey, true);
    }

    public void closeAllSources() {
        for (Entry<IDataSourceBrowser, List<IKey>> browserSourceKeys : openedSourcesFromBrowser.entrySet()) {
            // use a new list to avoid concurrent modifications
            List<IKey> keyList = new ArrayList<>(browserSourceKeys.getValue());
            for (IKey key : keyList) {
                closeSource(browserSourceKeys.getKey(), key, false);
            }
        }
        callGC();
    }

    public void removeSourceItems(final IKey sourceKey) {
        if (sourceKey != null) {
            List<IKey> itemKeys = openedItemsFromSource.get(sourceKey);
            if (itemKeys != null) {
                for (IKey itemKey : itemKeys) {
                    // remove item only if last source
                    List<IKey> sources = openedSourcesForItem.get(itemKey);
                    sources.remove(sourceKey);
                    // if no more source has opened this item, remove it
                    if (sources.isEmpty()) {
                        openedSourcesForItem.remove(itemKey);

                        deselectItems(itemKey);
                        Item removedItem = items.remove(itemKey);
                        refreshOpenedItemsList();
                        fireItemClosed(removedItem);
                    }
                }
                itemKeys.clear();
            }
            callGC();
        }
    }

    public AxisType getCurrentAxisForKey(final IKey key) {
        AxisType axistype = null;
        Item item = items.get(key);
        if (item != null) {
            axistype = item.getAxis();
        }
        return axistype;

    }

    public void openItem() {
        LOGGER.trace("openItem {}");
        treeViewer.openSelectedItem();
    }

    public void openItem(final DataType forcedType, final AxisType type) {
        LOGGER.trace("openItem ({},{})", forcedType, type);
        treeViewer.openSelectedItem(forcedType, type);
    }

    public boolean isDropEnable(DataType type) {
        boolean canImport = false;
        if (treeViewer != null) {
            canImport = treeViewer.isDropEnable(type);
        }
        return canImport;
    }

    @Override
    public void openItem(final IDataSourceBrowser browser, final IKey sourceKey, final IKey itemKey) {
        LOGGER.trace("openItem {}", itemKey);
        openItem(browser, sourceKey, itemKey, browser.getType(itemKey), null);
    }

    protected IKey findSourceKey(IKey key) {
        IKey sourceKey;
        Item xItem = items.get(key);
        sourceKey = xItem == null ? null : xItem.getSourceKey();
        return sourceKey;
    }

    @Override
    public void openItem(final IDataSourceBrowser browser, final IKey sourceKey, final IKey itemKey,
            DataType forcedType, AxisType forcedAxis) {
        LOGGER.trace("openItem {}", itemKey);

        // Display the waiting cursor
        if ((sourceKey != null) && (itemKey != null)) {
            IKey xScale = browser.getXScale(itemKey);
            IKey sliderScale = browser.getSliderScale(itemKey);

            final Frame frameForComponent = CometeUtils.getFrameForComponent(dataBrowserPanel);
            final Cursor cursor = (frameForComponent != null ? frameForComponent.getCursor() : null);

            EDTManager.INSTANCE.runInDrawingThread(() -> {
                if (frameForComponent != null) {
                    frameForComponent.setCursor(Cursor.getPredefinedCursor(Cursor.WAIT_CURSOR));
                }
            });

            try {
                // Check if the item is already opened
                if (isItemOpened(sourceKey, itemKey)) {
                    // The item is already open change only the axis
                    setItemAxis(itemKey, forcedAxis);
                } else {
                    // If the item is not open
                    List<IKey> itemKeys = openedItemsFromSource.get(sourceKey);
                    if (itemKeys == null) {
                        itemKeys = new ArrayList<>();
                        openedItemsFromSource.put(sourceKey, itemKeys);
                    }
                    itemKeys.add(itemKey);

                    // multi-sources management
                    List<IKey> sources = openedSourcesForItem.get(itemKey);
                    if (sources == null) {
                        sources = new ArrayList<>();
                        openedSourcesForItem.put(itemKey, sources);
                    }
                    sources.add(sourceKey);

                    DataFormat format = browser.getFormat(itemKey);
                    Item item = new Item(itemKey, sourceKey, browser, forcedType, format);
                    if (xScale != null) {
                        item.setXKey(xScale, findSourceKey(xScale));
                    }
                    if (forcedAxis != null) {
                        DataType currentType = browser.getType(itemKey);
                        if ((forcedAxis == AxisType.IMAGE) && (currentType != DataType.IMAGE)) {
                            // In this case force a spectrum to image before see JIRA EXPDATA-215
                            browser.setType(itemKey, DataType.IMAGE);
                        }
                        item.setAxis(forcedAxis);
                    }
                    items.put(itemKey, item);
                    refreshOpenedItemsList();
                    fireItemOpened(item);
                }

                // ensure item has been added
                // Set item selected in viewer
                if (items.get(itemKey) != null) {
                    selectItems(itemKey);
                }

                dataItemViewer.setValueConvertor(sliderScale);
            } finally {
                // Set the standard cursor
                EDTManager.INSTANCE.runInDrawingThread(() -> {
                    // cursor is null only if frame is null
                    if (cursor != null) {
                        frameForComponent.setCursor(cursor);
                    }
                });
            }
        }
    }

    public void closeItem(final IKey sourceKey, final IKey itemKey, boolean gc) {
        LOGGER.trace("closeItem {}", itemKey);
        if ((sourceKey != null) && (itemKey != null)) {
            List<IKey> itemKeys = openedItemsFromSource.get(sourceKey);
            if (itemKeys != null) {
                boolean itemWasOpened = itemKeys.remove(itemKey);
                if (itemWasOpened) {
                    // remove item only if last source
                    List<IKey> sources = openedSourcesForItem.get(itemKey);
                    sources.remove(sourceKey);
                    // if no more source has opened this item, remove it
                    if (sources.isEmpty()) {
                        openedSourcesForItem.remove(itemKey);
                        deselectItems(itemKey);
                        Item itemRemoved = items.remove(itemKey);
                        refreshOpenedItemsList();
                        fireItemClosed(itemRemoved);
                    }
                }
            }
        }
        if (gc) {
            callGC();
        }
    }

    public void closeItem(final IKey sourceKey, final IKey itemKey) {
        closeItem(sourceKey, itemKey, true);
    }

    public void callGC() {
        new SwingWorker<Void, Void>() {
            @Override
            protected Void doInBackground() throws Exception {
                // XXX It is generally not a good thing to call System.gc(), but it does seem to work here...
                // Note: Calling it twice is a solution found here:
                // https://stackoverflow.com/questions/13305813/system-gc-vs-gc-button-in-jvisualvm-jconsole
                System.gc();
                Thread.sleep(500);
                System.gc();
                return null;
            }
        }.execute();
    }

    public void closeItem(final IKey itemKey) {
        Item item = items.get(itemKey);
        if (item != null) {
            List<IKey> sources = openedSourcesForItem.get(itemKey);
            List<IKey> cpSources = new ArrayList<>(sources);
            if (cpSources != null) {
                for (IKey sourceKey : cpSources) {
                    closeItem(sourceKey, itemKey, false);
                }
                cpSources.clear();
            }
        }
        callGC();
    }

    //
    public void closeAllItemForSource(final IKey sourceKey) {
        // new list because closeItem removes from sources
        List<IKey> openItem = new ArrayList<>(openedSourcesForItem.get(sourceKey));
        for (IKey item : openItem) {
            closeItem(sourceKey, item, false);
        }
        callGC();
    }

    public void changeItemType(final IKey itemKey, final DataType type) {
        if ((itemKey != null) && (type != null)) {
            // If the item is open change type, close and open
            List<IKey> sources = openedSourcesForItem.get(itemKey);
            if ((sources != null) && !sources.isEmpty()) {
                List<IKey> copySources = new ArrayList<>();
                synchronized (sources) {
                    copySources.addAll(sources);
                    LOGGER.trace("openedSourcesForItem({}, {})", type, itemKey);
                }
                for (IKey source : copySources) {
                    Set<Entry<IDataSourceBrowser, List<IKey>>> entrySet = openedSourcesFromBrowser.entrySet();
                    for (Entry<IDataSourceBrowser, List<IKey>> entry : entrySet) {
                        List<IKey> sourceKeyList = entry.getValue();
                        IDataSourceBrowser browser = entry.getKey();
                        if (sourceKeyList.contains(source)) {
                            closeItem(source, itemKey, false);
                            browser.setType(itemKey, type);
                            openItem(browser, source, itemKey);
                        }
                    }
                }
            } else if ((treeViewer != null) && (treeViewer.getTree() != null)) {
                // Else change type
                IDataSourceBrowser browser = treeViewer.getTree().getSelectedBrowser();
                if (browser != null) {
                    browser.setType(itemKey, type);
                }
            }
        }
        callGC();
    }

    public boolean isItemOpened(final IKey sourceKey, final IKey itemKey) {
        boolean result = false;
        if ((sourceKey != null) && (itemKey != null)) {
            List<IKey> itemKeys = openedItemsFromSource.get(sourceKey);
            if (itemKeys != null) {
                result = itemKeys.contains(itemKey);
            }
        }
        return result;
    }

    public boolean isItemOpened(final IKey itemKey) {
        return items.containsKey(itemKey);
    }

    public void selectItems(final IKey... itemKeys) {
        if (itemKeys != null) {
            Collection<Item> itemList = new ArrayList<>(itemKeys.length);
            for (IKey itemKey : itemKeys) {
                if (itemKey != null) {
                    Item item = items.get(itemKey);
                    if (item != null) {
                        itemList.add(item);
                    }
                }
            }
            if (!itemList.isEmpty()) {
                for (Item item : itemList) {
                    fireItemSelected(item);
                }
                itemList.clear();
            }
        }
    }

    public void deselectItems(final IKey... itemKeys) {
        if (itemKeys != null) {
            Collection<Item> itemList = new ArrayList<>(itemKeys.length);
            for (IKey itemKey : itemKeys) {
                if (itemKey != null) {
                    Item item = items.get(itemKey);
                    if (item != null) {
                        itemList.add(item);
                    }
                }
            }
            if (!itemList.isEmpty()) {
                for (Item item : itemList) {
                    fireItemDeselected(item);
                }
                itemList.clear();
            }
        }
    }

    public void deselectAllItems() {
        deselectItems(items.keySet().toArray(new IKey[items.size()]));
    }

    public void setItemAxis(final IKey itemKey, final AxisType axis) {
        Item item = items.get(itemKey);
        if (item != null && axis != null) {
            item.setAxis(axis);
            fireItemAxisChanged(item);
        }
    }

    public void setItemScale(final Item item) {
        if (item != null) {
            Item newItem = items.get(item.getKey());
            if (newItem != null) {
                newItem.setXItem(item.getXItem());
                newItem.setYItem(item.getYItem());
                Item xItem = item.getXItem(), yItem = item.getYItem();
                if ((xItem != null) && !isItemOpened(xItem.getKey())) {
                    openItem(xItem.getBrowser(), xItem.getSourceKey(), xItem.getKey(),
                            xItem.getBrowser().getType(xItem.getKey()), AxisType.INVISIBLE);
                }
                if ((yItem != null) && !isItemOpened(yItem.getKey())) {
                    openItem(yItem.getBrowser(), yItem.getSourceKey(), yItem.getKey(),
                            yItem.getBrowser().getType(yItem.getKey()), AxisType.INVISIBLE);
                }
                fireItemAxisChanged(newItem);
            }
        }
    }

    public void setRootWindows(JComponent rootWindow) {
        if (rootWindow != null) {
            this.rootWindow = rootWindow;
        }
    }

    public void exportViewConfiguration(final String filename) {
        if (filename != null) {

            Element racine = new Element("DataBrowserConfig");

            Document document = new Document(racine);

            DOMBuilder builder = new DOMBuilder();

            try {
                ChartProperties chartProperties = dataItemViewer.getChartProperties();
                if (chartProperties != null) {
                    String chartPropXML = ChartPropertiesXmlManager.toXmlString(chartProperties);
                    org.w3c.dom.Element chartPropNode;
                    chartPropNode = (org.w3c.dom.Element) XMLUtils.getRootNodeFromFileContent(chartPropXML);
                    Element elemChartProp = builder.build(chartPropNode);
                    elemChartProp.detach();
                    racine.addContent(elemChartProp);
                }
            } catch (XMLWarning e) {
                LOGGER.error(String.format("Impossible to export view configuration %s", e.getMessage()), e);
            }

            Element elemSources = new Element("sources");
            racine.addContent(elemSources);

            // XXX no more global scale item
//            // image scales
//            Item xScaleItem = dataItemViewer.getImageXScaleItem();
//            Item yScaleItem = dataItemViewer.getImageYScaleItem();
//            IKey xScaleItemKey = (xScaleItem == null ? null : xScaleItem.getKey());
//            IKey yScaleItemKey = (yScaleItem == null ? null : yScaleItem.getKey());
//            Item xItem = dataItemViewer.getImageXScaleItem();
//            Item yItem = dataItemViewer.getImageYScaleItem();
//
//            AxisType axis = null;
//
//            for (Entry<IDataSourceBrowser, List<IKey>> entry : openedSourcesFromBrowser.entrySet()) {
//                List<IKey> sourcesList = entry.getValue();
//                for (IKey sourceKey : sourcesList) {
//                    Element elemSource = new Element("source");
//                    elemSources.addContent(elemSource);
//
//                    try {
//                        // previous way, text inserted as node content ->
//                        // xml escaped string
//                        // Element elemSourceKey = new Element("key");
//                        // elemSourceKey.setText(sourceKey.toXML());
//                        // elemSource.addContent(elemSourceKey);
//
//                        String sourceKeyXML = sourceKey.toXML();
//                        org.w3c.dom.Element sourceKeyNode = (org.w3c.dom.Element) XMLUtils
//                                .getRootNodeFromFileContent(sourceKeyXML);
//                        Element elemSourceKey = builder.build(sourceKeyNode);
//                        elemSourceKey.detach();
//                        elemSource.addContent(elemSourceKey);
//
//                        Element elemItems = new Element("items");
//                        elemSource.addContent(elemItems);
//
//                        for (IKey itemKey : openedItemsFromSource.get(sourceKey)) {
//                            Item theItem = items.get(itemKey);
//                            axis = theItem.getAxis();
//                            if (axis != null) {
//                                Element elemItem = new Element("item");
//                                elemItems.addContent(elemItem);
//
//                                elemItem.setAttribute("axis", axis.name());
//
//                                xItem = theItem.getXItem();
//                                yItem = theItem.getYItem();
//
//                                if (itemKey == xScaleItemKey) {
//                                    elemItem.setAttribute("imageXScale", "true");
//                                }
//                                if (itemKey == yScaleItemKey) {
//                                    elemItem.setAttribute("imageYScale", "true");
//                                }
//
//                                // set item key
//                                String itemKeyXML = itemKey.toXML();
//                                org.w3c.dom.Element itemKeyNode = (org.w3c.dom.Element) XMLUtils
//                                        .getRootNodeFromFileContent(itemKeyXML);
//                                Element elemItemKey = builder.build(itemKeyNode);
//                                elemItemKey.detach();
//                                elemItem.addContent(elemItemKey);
//
//                                // Set X key
//                                if (xItem != null) {
//                                    Element elemX = new Element("xscale");
//                                    itemKeyXML = xItem.getKey().toXML();
//                                    org.w3c.dom.Element xitemKeyNode = (org.w3c.dom.Element) XMLUtils
//                                            .getRootNodeFromFileContent(itemKeyXML);
//                                    Element xelemItemKey = builder.build(xitemKeyNode);
//                                    xelemItemKey.detach();
//                                    elemX.addContent(xelemItemKey);
//                                    elemItem.addContent(elemX);
//                                }
//
//                                // Set Y key
//                                if (yItem != null) {
//                                    Element elemY = new Element("yscale");
//                                    itemKeyXML = yItem.getKey().toXML();
//                                    org.w3c.dom.Element yitemKeyNode = (org.w3c.dom.Element) XMLUtils
//                                            .getRootNodeFromFileContent(itemKeyXML);
//                                    Element yelemItemKey = builder.build(yitemKeyNode);
//                                    yelemItemKey.detach();
//                                    elemY.addContent(yelemItemKey);
//                                    elemItem.addContent(elemY);
//                                }
//
//                                // set item properties (plot or image)
//                                switch (theItem.getAxis()) {
//                                    case Y1:
//                                    case Y2:
//                                    case Y1_COLUMN:
//                                    case Y1_ROW:
//                                    case Y2_COLUMN:
//                                    case Y2_ROW:
//                                        PlotProperties plotProperties = dataItemViewer
//                                                .getChartDataViewPlotProperties(itemKey);
//                                        if (plotProperties != null) {
//                                            String plotPropXML = PlotPropertiesXmlManager.toXmlString(plotProperties);
//                                            org.w3c.dom.Element plotPropNode = (org.w3c.dom.Element) XMLUtils
//                                                    .getRootNodeFromFileContent(plotPropXML);
//                                            Element elemPlotProp = builder.build(plotPropNode);
//                                            elemPlotProp.detach();
//                                            elemItem.addContent(elemPlotProp);
//                                        }
//                                        break;
//
//                                    case IMAGE:
//                                        ImageProperties imageProperties = dataItemViewer.getImageProperties(itemKey);
//                                        if (imageProperties != null) {
//                                            String imagePropXML = ImagePropertiesXmlManager
//                                                    .toXmlString(imageProperties);
//
//                                            org.w3c.dom.Element imagePropNode = (org.w3c.dom.Element) XMLUtils
//                                                    .getRootNodeFromFileContent(imagePropXML);
//                                            Element elemImageProp = builder.build(imagePropNode);
//                                            elemImageProp.detach();
//                                            elemItem.addContent(elemImageProp);
//                                        }
//                                        break;
//
//                                    default:
//                                        break;
//                                }
//                            }
//
//                        }
//
//                    } catch (XMLWarning e) {
//                        LOGGER.error(String.format("Impossible to export view configuration %s", e.getMessage()), e);
//                    }
//
//                } // end for (IKey sourceKey : sourcesList)
//            } // end for (Entry<IDataSourceBrowser, List<IKey>> entry : openedSourcesFromBrowser.entrySet())

            Element elemDocking = new Element("docking");
            racine.addContent(elemDocking);

            // For DataItemViewer
            Element elemLayout = new Element("dataitemlayout");
            elemDocking.addContent(elemLayout);
            byte[] dataItemDocking = dockingUtil.getLayout(DockingUtil.ITEM_VIEW_ID, dataItemViewer.getDockingArea());
            String valueDataItemLayout = byteArrayToString(dataItemDocking);
            elemLayout.setAttribute("value", valueDataItemLayout);

            // For RootWindow
            Element windowLayout = new Element("windowlayout");
            elemDocking.addContent(windowLayout);
            if (rootWindow != null) {
                byte[] rootLayout = dockingUtil.getLayout(null, rootWindow);
                String valueRootLayout = byteArrayToString(rootLayout);
                windowLayout.setAttribute("value", valueRootLayout);
            }

            FileOutputStream fileOutputStream = null;
            try {
                XMLOutputter sortie = new XMLOutputter(Format.getPrettyFormat());
                fileOutputStream = new FileOutputStream(new File(filename));
                sortie.output(document, fileOutputStream);
            } catch (IOException e) {
                LOGGER.error(String.format("Impossible to export view configuration %s", e.getMessage()), e);
            } finally {
                try {
                    fileOutputStream.close();
                } catch (IOException e) {
                    LOGGER.error(String.format("Impossible to close view configuration file %s because %s", filename,
                            e.getMessage()), e);
                }
            }
        }
    }

    /**
     * Converts a null-terminated byte array to java string
     */
    protected String byteArrayToString(byte[] array) {
        return DatatypeConverter.printBase64Binary(array);
    }

    /**
     * Returns this java string as a null-terminated byte array
     */
    protected byte[] stringToByteArray(String str) {
        byte[] byteArray = null;
        try {
            byteArray = DatatypeConverter.parseBase64Binary(str);
        } catch (Exception e) {
            LOGGER.error("Failed to convert \"" + str + "\" to byte array", e);
        }
        return byteArray;
    }

    public void importViewConfiguration(final String filename) {
        Runnable viewConfigRunnable = () -> {
            final Frame frameForComponent = CometeUtils.getFrameForComponent(dataBrowserPanel);
            final Cursor cursor = (frameForComponent != null ? frameForComponent.getCursor() : null);
            EDTManager.INSTANCE.runInDrawingThread(() -> {
                if (frameForComponent != null) {
                    frameForComponent.setCursor(Cursor.getPredefinedCursor(Cursor.WAIT_CURSOR));
                }
            });

            List<DataBrowserException> errors = new ArrayList<>();

            Document document = null;
            SAXBuilder sxb = new SAXBuilder();
            try {
                document = sxb.build(new File(filename));

                if (document != null) {
                    // list of sources to load
                    ArrayList<LoadParameter> paramList = new ArrayList<>();

                    Map<Element, IKey> sourcesTable = new HashMap<>();
                    Map<Element, IDataSourceBrowser> browsersTable = new HashMap<>();

                    Element root = document.getRootElement();

                    final DOMOutputter domOutputter = new DOMOutputter();

                    // load chart properties
                    Element elemChartProp = root.getChild("chartParams");

                    if (elemChartProp != null) {
                        org.w3c.dom.Element chartPropNode = domOutputter.output(elemChartProp);
                        ChartProperties chartProperties = ChartPropertiesXmlManager.loadChartProperties(chartPropNode);
                        dataItemViewer.setChartProperties(chartProperties);
                    }

                    /* Load docking view */
                    Element child = root.getChild("docking");
                    Element layoutElem = child.getChild("dataitemlayout");
                    String attributeValue = layoutElem.getAttributeValue("value");

                    Element layoutWindowElem = child.getChild("windowlayout");
                    String attributeWindowValue = layoutWindowElem.getAttributeValue("value");

                    // For DataItemViewer
                    byte[] decodeBuffer = stringToByteArray(attributeValue);
                    dockingUtil.setLayout(decodeBuffer, dataItemViewer.getDockingArea(), DockingUtil.ITEM_VIEW_ID);

                    // For WindowRoot
                    byte[] decodeWind = stringToByteArray(attributeWindowValue);
                    dockingUtil.setLayout(decodeWind, rootWindow, null);

                    /* Load docking view */

                    Element elemSources = root.getChild("sources");
                    if (elemSources != null) {// should not be null
                        List<Element> elemSourceList = elemSources.getChildren("source");
                        for (Element elemSource : elemSourceList) {
                            // previous way, text inserted as node content
                            // -> xml escaped string
                            // Element elemSourceKey =
                            // elemSource.getChild("key");
                            // try {
                            // IKey sourceKey =
                            // AbstractKey.recoverKeyFromXML(elemSourceKey.getValue());

                            Element elemSourceKey = elemSource.getChild("iKey");
                            try {
                                org.w3c.dom.Element sourceKeyNode = domOutputter.output(elemSourceKey);
                                IKey sourceKey = AbstractKey.recoverKeyFromXMLNode(sourceKeyNode);

                                IDataSourceBrowser browser = DataSourceManager.getProducerFromKey(sourceKey);
                                if (browser == null) {
                                    throw new LoadException(
                                            filename + " (source node #" + (1 + elemSourceList.indexOf(elemSource))
                                                    + ")",
                                            DataBrowser.MESSAGES.getString("Error.LoadSource.UnavailablePlugin") + " "
                                                    + sourceKey.getInformationKey());
                                }

                                sourcesTable.put(elemSource, sourceKey);
                                browsersTable.put(elemSource, browser);

                                // add source to load queue
                                paramList.add(new KeyParameter(browser, sourceKey));
                            } catch (KeyCompatibilityException e) {
                                LOGGER.error(String.format("Impossible to import view configuration %s because %s",
                                        filename, e.getMessage()), e);
                                errors.add(new LoadException(
                                        filename + " (source node #" + (1 + elemSourceList.indexOf(elemSource)) + ")",
                                        DataBrowser.MESSAGES.getString("Error.LoadConfig.XmlKeyRecovering")));

                            } catch (LoadException e) {
                                LOGGER.error(String.format("Impossible to load view configuration %s because %s",
                                        filename, e.getMessage()), e);
                                errors.add(e);
                            }
                        }
                    }

                    try {
                        // use this to wait for all sources to be loaded
                        // before trying to load items
                        CountDownLatch sourcesLatch = new CountDownLatch(paramList.size());

                        // load all valid sources
                        LOGGER.trace("Load sources {}", Arrays.toString(paramList.toArray()));
                        loadSources(paramList, sourcesLatch);
                        sourcesLatch.await();
                    } catch (InterruptedException e) {
                        LOGGER.error(String.format("InterruptedException %s", e.getMessage()), e);
                    }

                    // go for loading items of successfully opened sources

                    for (Map.Entry<Element, IKey> sourceEntry : sourcesTable.entrySet()) {
                        Element elemSource = sourceEntry.getKey();
                        final IKey sourceKey = sourceEntry.getValue();
                        final IDataSourceBrowser browser = browsersTable.get(elemSource);

                        // only consider items from successfully opened sources
                        if (browser.isSourceOpened(sourceKey)) {
                            Element elemItems = elemSource.getChild("items");
                            if (elemItems != null) {// should not be null
                                List<Element> elemItemList = elemItems.getChildren("item");
                                for (final Element elemItem : elemItemList) {
                                    Element elemItemKey = elemItem.getChild("iKey");

                                    try {
                                        // recover the xml key
                                        org.w3c.dom.Element itemKeyNode = domOutputter.output(elemItemKey);
                                        final IKey itemKey = AbstractKey.recoverKeyFromXMLNode(itemKeyNode);
                                        LOGGER.trace("Load item {}", browser.getDisplayName(itemKey));

                                        // get the axis
                                        String axisName = elemItem.getAttribute("axis").getValue();
                                        AxisType axis = AxisType.valueOf(axisName);

                                        boolean isImageXScale = false;
                                        Attribute xScaleAttribute = elemItem.getAttribute("imageXScale");
                                        if (xScaleAttribute != null) {
                                            isImageXScale = xScaleAttribute.getBooleanValue();
                                        }

                                        boolean isImageYScale = false;
                                        Attribute yScaleAttribute = elemItem.getAttribute("imageYScale");
                                        if (yScaleAttribute != null) {
                                            isImageYScale = yScaleAttribute.getBooleanValue();
                                        }

                                        // Load xScale
                                        IKey xKey = null;
                                        Element xElem = elemItem.getChild("xscale");
                                        if (xElem != null) {
                                            Element xKeyElem = xElem.getChild("iKey");
                                            org.w3c.dom.Element xitemKeyNode = domOutputter.output(xKeyElem);
                                            xKey = AbstractKey.recoverKeyFromXMLNode(xitemKeyNode);
                                        }

                                        // Load yScale
                                        IKey yKey = null;
                                        Element yElem = elemItem.getChild("yscale");
                                        if (yElem != null) {
                                            Element yKeyElem = yElem.getChild("yElem");
                                            org.w3c.dom.Element yitemKeyNode = domOutputter.output(yKeyElem);
                                            yKey = AbstractKey.recoverKeyFromXMLNode(yitemKeyNode);
                                        }

                                        // load plot properties
                                        PlotProperties plotProperties = null;
                                        Element elemPlotProp = elemItem.getChild("plotParams");
                                        if (elemPlotProp != null) {
                                            org.w3c.dom.Element plotPropNode = domOutputter.output(elemPlotProp);
                                            plotProperties = PlotPropertiesXmlManager.loadPlotProperties(plotPropNode);
                                        }

                                        // load image properties
                                        ImageProperties imageProperties = null;
                                        Element elemImageProp = elemItem.getChild("imageParams");
                                        if (elemImageProp != null) {
                                            org.w3c.dom.Element imagePropNode = domOutputter.output(elemImageProp);
                                            imageProperties = ImagePropertiesXmlManager
                                                    .loadImageProperties(imagePropNode);
                                        }

                                        final OpenItemStructure openStructure = new OpenItemStructure();
                                        openStructure.browser = browser;
                                        openStructure.sourceKey = sourceKey;
                                        openStructure.itemKey = itemKey;
                                        openStructure.axis = axis;
                                        openStructure.isImageXScale = isImageXScale;
                                        openStructure.isImageYScale = isImageYScale;
                                        openStructure.plotProperties = plotProperties;
                                        openStructure.imageProperties = imageProperties;
                                        openStructure.xKey = xKey;
                                        openStructure.yKey = yKey;

                                        EDTManager.INSTANCE.runInDrawingThread(() -> {
                                            // open the item in DataBrowser
                                            openItem(openStructure.browser, openStructure.sourceKey,
                                                    openStructure.itemKey);

                                            // set the axis
                                            setItemAxis(openStructure.itemKey, openStructure.axis);

                                            Item openedItem = items.get(openStructure.itemKey);
                                            // XXX no more global scale item
//                                            if (openStructure.isImageXScale) {
//                                                dataItemViewer.setImageXScaleItem(openedItem);
//                                            }
//                                            if (openStructure.isImageYScale) {
//                                                dataItemViewer.setImageYScaleItem(openedItem);
//                                            }

                                            // set the plot properties
                                            if (openStructure.plotProperties != null) {
                                                dataItemViewer.setChartDataViewPlotProperties(openStructure.itemKey,
                                                        openStructure.plotProperties, openedItem);
                                            }

                                            // set the image properties
                                            if (openStructure.imageProperties != null) {
                                                dataItemViewer.setImageProperties(openStructure.itemKey,
                                                        openStructure.imageProperties);
                                            }

                                            boolean changeScale = false;
                                            if (openStructure.xKey != null) {
                                                changeScale = true;
                                                IDataSourceBrowser dataSourceBrowserForId = getDataSourceBrowserForId(
                                                        openStructure.xKey.getSourceProduction());
                                                DataType dataType = dataSourceBrowserForId.getType(openStructure.xKey);
                                                DataFormat format = dataSourceBrowserForId
                                                        .getFormat(openStructure.xKey);
                                                openedItem.setXKey(openStructure.xKey,
                                                        findSourceKey(openStructure.xKey), dataSourceBrowserForId,
                                                        dataType, format);
                                            }

                                            if (openStructure.yKey != null) {
                                                changeScale = true;
                                                IDataSourceBrowser dataSourceBrowserForId = getDataSourceBrowserForId(
                                                        openStructure.yKey.getSourceProduction());
                                                DataType dataType = dataSourceBrowserForId.getType(openStructure.yKey);
                                                DataFormat format = dataSourceBrowserForId
                                                        .getFormat(openStructure.yKey);
                                                openedItem.setYKey(openStructure.yKey,
                                                        findSourceKey(openStructure.yKey), dataSourceBrowserForId,
                                                        dataType, format);
                                            }

                                            if (changeScale) {
                                                setItemScale(openedItem);
                                            }

                                        });

                                    } catch (KeyCompatibilityException e) {
                                        errors.add(new LoadException(filename,
                                                DataBrowser.MESSAGES.getString("Error.LoadConfig.XmlKeyRecovering")));
                                    } catch (Exception e) {
                                        LOGGER.error(
                                                String.format("Impossible to import view configuration %s because %s",
                                                        filename, e.getMessage()),
                                                e);
                                    }

                                }
                            }
                        }
                    }

                }
            } catch (JDOMException e1) {
                LOGGER.error(String.format("Impossible to import view configuration %s because %s", filename,
                        e1.getMessage()), e1);
                errors.add(new LoadException(filename,
                        DataBrowser.MESSAGES.getString("Error.LoadConfig.StructureProblem")));
            } catch (IOException e1) {
                LOGGER.error(String.format("Impossible to import view configuration %s because %s", filename,
                        e1.getMessage()), e1);
                errors.add(new LoadException(filename, DataBrowser.MESSAGES.getString("Error.LoadConfig.ReadProblem")));
            } catch (Exception e) {
                LOGGER.error(
                        String.format("Impossible to import view configuration %s due to unexpected error", filename),
                        e);
            } finally {
                EDTManager.INSTANCE.runInDrawingThread(() -> {
                    // cursor is null only if frame is null
                    if (cursor != null) {
                        frameForComponent.setCursor(cursor);
                    }
                });

                showErrors(errors);
            }
        };

        new Thread(new FutureTask<Void>(viewConfigRunnable, null)).start();
    }

    public Map<IDataSourceBrowser, List<IKey>> getOpenedSourcesFromBrowser() {
        return openedSourcesFromBrowser;
    }

    public DockingUtil getDockingUtil() {
        return dockingUtil;
    }

    public void setChartViewer(IChartViewer chartViewer) {
        this.chartViewer = chartViewer;
    }

    public IChartViewer getChartViewer() {
        return chartViewer;
    }

    public Map<IKey, List<IKey>> getOpenedItemsFromSource() {
        return openedItemsFromSource;
    }

    public Map<IKey, List<IKey>> getOpenedSourcesForItem() {
        return openedSourcesForItem;
    }

    public void xAttributForOpenedAttributes(IKey xKeyItem, IKey readKey) {
        Item item = items.get(xKeyItem);
        item.setAxis(AxisType.X);
//        xKeyItem = item.getKey();
//
//        
//        if ((xKeyItem != null) && (readKey != null)) {
//            currentItemKey = ChartViewerBox.createDualKey(xKeyItem, readKey);
//        } else {
//            currentItemKey = readKey;
//        }

        // chartBox.connectWidgetDual(chartViewer, xKeyItem, readKey);
//      chartViewerBox.connectWidget(chartViewer, currentKey);
    }

    public boolean isShowFullScalarLabel() {
        return showFullScalarLabel;
    }

    public void setShowFullScalarLabel(boolean showFullScalarLabel) {
        this.showFullScalarLabel = showFullScalarLabel;
    }

    // ------------------------------
    // listeners management
    // ------------------------------

    public void addDataBrowserItemListener(final IDataBrowserItemListener listener) {
        if (listener != null) {
            itemListeners.add(listener);
        }
    }

    public void removeDataBrowserItemListener(final IDataBrowserItemListener listener) {
        if (listener != null) {
            itemListeners.remove(listener);
        }
    }

    protected void fireItemOpened(final Item item) {
        DataBrowserItemEvent event = new DataBrowserItemEvent(this, item);
        for (IDataBrowserItemListener listener : itemListeners) {
            listener.onItemOpened(event);
        }
    }

    private void fireItemClosed(final Item item) {
        DataBrowserItemEvent event = new DataBrowserItemEvent(this, item);
        for (IDataBrowserItemListener listener : itemListeners) {
            listener.onItemClosed(event);
        }
    }

    private void fireItemSelected(final Item item) {
        DataBrowserItemEvent event = new DataBrowserItemEvent(this, item);
        for (IDataBrowserItemListener listener : itemListeners) {
            listener.onItemSelected(event);
        }
    }

    private void fireItemDeselected(final Item item) {
        DataBrowserItemEvent event = new DataBrowserItemEvent(this, item);
        for (IDataBrowserItemListener listener : itemListeners) {
            listener.onItemDeselected(event);
        }
    }

    private void fireItemAxisChanged(final Item item) {
        DataBrowserItemEvent event = new DataBrowserItemEvent(this, item);
        for (IDataBrowserItemListener listener : itemListeners) {
            listener.onItemAxisChanged(event);
        }
    }

    // ///////////// //
    // Inner classes //
    // ///////////// //

    public class OpenItemStructure {
        public IDataSourceBrowser browser;
        public IKey sourceKey;
        public IKey itemKey;
        public AxisType axis = AxisType.SCALAR;
        public boolean isImageXScale = false;
        public boolean isImageYScale = false;
        public PlotProperties plotProperties;
        public ImageProperties imageProperties;
        public IKey xKey;
        public IKey yKey;
    }

    private class LoadSourceStructure {
        public IDataSourceBrowser browser;
        public IKey key;
        public ITreeNode treeNode;
    }

    private class LoadSourceWorker extends SwingWorker<List<DataBrowserException>, LoadSourceStructure> {
        private final List<LoadParameter> paramList;
        private final CountDownLatch countDown;

        public LoadSourceWorker(final List<LoadParameter> paramList, final CountDownLatch countDownLatch) {
            this.paramList = paramList;
            countDown = countDownLatch;
        }

        @Override
        protected List<DataBrowserException> doInBackground() {
            List<DataBrowserException> log = new LinkedList<>();

            final Frame frameForComponent = CometeUtils.getFrameForComponent(dataBrowserPanel);
            final Cursor cursor = (frameForComponent != null ? frameForComponent.getCursor() : null);
            EDTManager.INSTANCE.runInDrawingThread(() -> {
                if (frameForComponent != null) {
                    frameForComponent.setCursor(Cursor.getPredefinedCursor(Cursor.WAIT_CURSOR));
                }
            });

            for (LoadParameter loadParameter : paramList) {

                // TODO not used for instant
                if (isCancelled()) {
                    break;
                }

                try {
                    IDataSourceBrowser browser = null;
                    String sourcePath = null;
                    IKey key = null;

                    if (loadParameter instanceof PathParameter) {
                        PathParameter param = (PathParameter) loadParameter;

                        sourcePath = param.getPath();
                        // may return null
                        browser = DataSourceManager.getProducerFromString(sourcePath);

                        // browser can be null for argument and drop
                        if (browser != null) {
                            key = browser.createKeyFromSource(sourcePath);
                        } else {
                            throw new LoadException(sourcePath,
                                    DataBrowser.MESSAGES.getString("Error.LoadSource.UnavailablePlugin"));
                        }
                    } else if (loadParameter instanceof KeyParameter) {
                        KeyParameter param = (KeyParameter) loadParameter;

                        key = param.getKey();
                        browser = param.getBrowser();
                        sourcePath = browser.getDisplayName(key);

                        if (!browser.canRead(key)) {
                            throw new LoadException(sourcePath,
                                    DataBrowser.MESSAGES.getString("Error.LoadSource.InvalidArgument"));
                        }
                    }

                    if ((browser != null) && (key != null)) {
                        // need to keep this part atomic (contains? ... add)
                        synchronized (DataBrowserController.this) {
                            if (!browser.isSourceOpened(key)) {
                                ITreeNode treeNode = null;

                                try {
                                    treeNode = browser.createNode(key);
                                } catch (Exception e) {
                                    throw new LoadException(sourcePath, e.getMessage());
                                }

                                // here, we can add it safely
                                if (!isDevicePanel) {
                                    List<IKey> sourceKeys = openedSourcesFromBrowser.get(browser);
                                    sourceKeys.add(key);
                                }
                                openedItemsFromSource.put(key, new ArrayList<>());
                                // let the seeker open in the last opened file directory
                                if (browser.getSeeker() != null) {
                                    browser.getSeeker().setDirectory(sourcePath);
                                }

                                // publish the new node
                                LoadSourceStructure loadStructure = new LoadSourceStructure();
                                loadStructure.browser = browser;
                                loadStructure.key = key;
                                loadStructure.treeNode = treeNode;
                                publish(loadStructure);
                                // XXX Don't consider opened source as an error
//                            } else {
//                                throw new LoadException(sourcePath,
//                                        DataBrowser.MESSAGES.getString("Error.LoadSource.SourceAlreadyOpened"));
                            }
                        }
                    }

                } catch (DataBrowserException e) {
                    if (!isDevicePanel) {
                        LOGGER.error(String.format("DataBrowser error %s", e.getMessage()), e);
                        log.add(e);
                    }
                } catch (Exception e) {
                    LOGGER.error(String.format("Unknow error %s", e.getMessage()), e);
                } finally {
                    if (countDown != null) {
                        countDown.countDown();
                    }
                }
            }

            EDTManager.INSTANCE.runInDrawingThread(() -> {
                // cursor is null only if frame is null
                if (cursor != null) {
                    frameForComponent.setCursor(cursor);
                }
            });

            return log;
        }

        @Override
        protected void process(final List<LoadSourceStructure> chunks) {
            if (!isDevicePanel) {
                for (LoadSourceStructure loadStructure : chunks) {
                    // add new valid nodes to the tree
                    treeViewer.sourceOpened(loadStructure.browser, loadStructure.key, loadStructure.treeNode);
                }
            }
        }

        @Override
        protected void done() {
            try {
                List<DataBrowserException> list = get();
                if (!isDevicePanel) {
                    showErrors(list);
                }
            } catch (InterruptedException e) {
                LOGGER.error(String.format("InterruptedException %s", e.getMessage()), e);
            } catch (ExecutionException e) {
                LOGGER.error(String.format("ExecutionException {}", e.getMessage()), e);
            } finally {
                callGC();
            }
        }

    }

}
