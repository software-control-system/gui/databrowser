/*
 * This file is part of databrowser-gui.
 * 
 * databrowser-gui is free software: you can redistribute it and/or modify it under the terms of the GNU General Public
 * License as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later
 * version.
 * 
 * databrowser-gui is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the
 * implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for more
 * details.
 * 
 * You should have received a copy of the GNU General Public License along with databrowser-gui. If not, see
 * <https://www.gnu.org/licenses/>.
 */
package org.cdma.gui.databrowser.model;

import java.awt.Component;
import java.awt.Point;
import java.awt.Rectangle;
import java.io.File;
import java.io.IOException;
import java.util.Arrays;

import javax.swing.SwingUtilities;

import org.cdma.gui.databrowser.DataBrowserController;
import org.cdma.gui.databrowser.interfaces.IPlayerPanelListener;
import org.cdma.gui.databrowser.interfaces.Item;
import org.cdma.gui.databrowser.view.item.PlayerPanel;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import fr.soleil.comete.definition.widget.IPlayer;
import fr.soleil.comete.swing.util.AnimationTool;
import fr.soleil.data.manager.SimpleRandomTaskManager;
import fr.soleil.data.service.IKey;
import fr.soleil.data.target.ITarget;

public abstract class AbstractItem<C extends Component & ITarget> extends Item implements IPlayerPanelListener {

    private static final Logger LOGGER = LoggerFactory.getLogger(AbstractItem.class);
    protected static final SimpleRandomTaskManager CONNECTION_SCHEDULER = new SimpleRandomTaskManager(LOGGER,
            "AbstractItem connection/diconnection");
    public static String currentPlayerVideoDirectory = null;

    protected IKey readKey;
    protected IKey writeKey;

    protected C viewer;

    protected int[] shape;
    protected int nbPlayers = 0;
    protected int[] origins = null;

    protected PlayerPanel playerPanel;

    protected boolean selected = false;

    public AbstractItem(final Item item, DataBrowserController controller) {
        super(item);

        // compute player part
        int dimension = 0;
        switch (getType()) {
            case SCALAR:
                dimension = 0;
                break;
            case SPECTRUM:
                dimension = 1;
                break;
            case IMAGE:
                dimension = 2;
                break;
            default:
                break;
        }

        int rank = browser.getRank(key);
        if (rank < dimension) {
            rank = dimension;
        }
        if (rank > 0) {
            nbPlayers = rank - dimension;

            int[] completeShape = browser.getShape(key);
            if (completeShape != null) {
                shape = new int[rank];
                Arrays.fill(shape, 1);
                int completeShapeIndex = 0;
                for (int i = 0; i < dimension; i++) {
                    completeShapeIndex = completeShape.length - (i + 1);
                    if ((completeShapeIndex > -1) && (completeShapeIndex < completeShape.length)) {
                        shape[shape.length - (i + 1)] = completeShape[completeShapeIndex];
                    }
                }

                if (nbPlayers > 0) {
                    playerPanel = new PlayerPanel(completeShape, nbPlayers, controller);
                    playerPanel.addPlayerPanelListener(this);

                    origins = new int[rank];
                    Arrays.fill(origins, 0);
                }
            }
        }
    }

    public IKey getReadKey() {
        return readKey;
    }

    public void computeKeys() {
        if (nbPlayers > 0) {
            readKey = browser.generateRegionKey(key, shape, origins);
        } else {
            readKey = key;
        }
        writeKey = browser.generateSettableKey(readKey);
    }

    public PlayerPanel getPlayerPanel() {
        return playerPanel;
    }

    public final void connect() {
        CONNECTION_SCHEDULER.submit(() -> {
            connectImmediately();
        });
    }

    protected final void connectImmediately() {
        computeKeys();
        doConnect();
    }

    protected abstract void doConnect();

    public final void disconnect() {
        CONNECTION_SCHEDULER.submit(() -> {
            disconnectImmediately();
        });
    }

    protected abstract void disconnectImmediately();

    public void setSelected(final boolean selected) {
        this.selected = selected;
    }

    public boolean isSelected() {
        return selected;
    }

    @Override
    public void selectedIndexChanged(final int[] newOrigins) {
        this.origins = newOrigins;

        // assume we are connected if player moves.
        reconnect();
    }

    public final void reconnect() {
        CONNECTION_SCHEDULER.submit(() -> {
            disconnectImmediately();
            connectImmediately();
        });
    }

    @Override
    public String generateAnimation(final IPlayer player, final String targetDirectory) {
        // parent is supposed to be the splitPane, with the player panel on top
        final Component component = ((Component) viewer).getParent();
        final Point point = new Point(0, 0);
        SwingUtilities.convertPointToScreen(point, component);

        new Thread(AbstractItem.this.getClass().getName() + ".generateAnimation") {
            @Override
            public void run() {
                try {
                    File file = AnimationTool.createAnimation(player, currentPlayerVideoDirectory,
                            new Rectangle(point.x, point.y, component.getWidth(), component.getHeight()));
                    // path is set when animation is finished
                    if (file != null) {
                        if (file.exists() && !file.isDirectory()) {
                            file = file.getParentFile();
                        }
                        currentPlayerVideoDirectory = file.getAbsolutePath();
                    }
                } catch (IOException e) {
                    LOGGER.error("Impossible to generate animation {} because {} ", currentPlayerVideoDirectory,
                            e.getMessage());
                    LOGGER.debug("Stack trace", e);
                }
            }
        }.start();

        return currentPlayerVideoDirectory;
    }

}
