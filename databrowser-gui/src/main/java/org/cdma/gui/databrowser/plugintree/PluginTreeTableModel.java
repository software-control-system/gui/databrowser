/*
 * This file is part of databrowser-gui.
 * 
 * databrowser-gui is free software: you can redistribute it and/or modify it under the terms of the GNU General Public
 * License as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later
 * version.
 * 
 * databrowser-gui is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the
 * implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for more
 * details.
 * 
 * You should have received a copy of the GNU General Public License along with databrowser-gui. If not, see
 * <https://www.gnu.org/licenses/>.
 */
package org.cdma.gui.databrowser.plugintree;

import org.cdma.gui.databrowser.DataBrowser;
import org.jdesktop.swingx.treetable.DefaultTreeTableModel;
import org.jdesktop.swingx.treetable.TreeTableNode;

public class PluginTreeTableModel extends DefaultTreeTableModel {

    public enum PluginTreeColumns {

        // Order of enum constants stands for columns' order
        FIRST(""), LABEL(DataBrowser.MESSAGES.getString("About.Plugins.Column.Label")),
        VERSION(DataBrowser.MESSAGES.getString("About.Plugins.Column.Version")),
        DESCRIPTION(DataBrowser.MESSAGES.getString("About.Plugins.Column.Description"));

        private String name;

        private PluginTreeColumns(final String columnName) {
            name = columnName;
        }

        public String getName() {
            return name;
        }

    }

    public PluginTreeTableModel(final TreeTableNode root) {
        super(root);
    }

    @Override
    public int getColumnCount() {
        return PluginTreeColumns.values().length;
    }

    @Override
    public String getColumnName(final int column) {
        return PluginTreeColumns.values()[column].getName();
    }

    public int getNodeCount(final Object node) {
        int count = 1; // the node itself
        int childCount = getChildCount(node);
        for (int i = 0; i < childCount; i++) {
            count += getNodeCount(getChild(node, i));
        }
        return count;
    }

}
