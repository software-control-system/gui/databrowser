/*
 * This file is part of databrowser-gui.
 * 
 * databrowser-gui is free software: you can redistribute it and/or modify it under the terms of the GNU General Public
 * License as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later
 * version.
 * 
 * databrowser-gui is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the
 * implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for more
 * details.
 * 
 * You should have received a copy of the GNU General Public License along with databrowser-gui. If not, see
 * <https://www.gnu.org/licenses/>.
 */
package org.cdma.gui.databrowser.model;

import java.awt.Component;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.swing.JMenuItem;
import javax.swing.JPopupMenu;
import javax.swing.SwingUtilities;

import org.cdma.gui.databrowser.interfaces.DataFormat;
import org.cdma.gui.databrowser.interfaces.IDataSourceBrowser;
import org.cdma.gui.databrowser.interfaces.Item;
import org.cdma.gui.databrowser.view.item.SelectDataNumberMatrix;

import fr.soleil.comete.box.matrixbox.BooleanMatrixBox;
import fr.soleil.comete.box.matrixbox.NumberMatrixBox;
import fr.soleil.comete.swing.Chart;
import fr.soleil.comete.swing.NumberMatrixArea;
import fr.soleil.comete.swing.chart.data.DataView;
import fr.soleil.comete.swing.chart.util.AbstractChartCustomMenu;
import fr.soleil.comete.swing.chart.view.component.ChartPopupMenu;
import fr.soleil.data.service.IKey;

public abstract class WriteDataItem extends AbstractChartCustomMenu {

    private static final long serialVersionUID = 3016475649188474467L;

    protected NumberMatrixArea numberMatrixArea;
    private final static NumberMatrixBox numberMatrixBox = new NumberMatrixBox();
    private final static BooleanMatrixBox booleanMatrixBox = new BooleanMatrixBox();
    private final Map<String, Item> itemMap = new HashMap<>();

    public WriteDataItem() {
        super("Write data", false, "Show in selection", "Select view...");
    }

    @Override
    public void manageDataViews(List<DataView> result, Chart chart) {
        if ((result != null) && (!result.isEmpty())) {
            DataView dataView = result.get(0);
            String id = dataView.getId();
            manageDataViews(id, chart);
        }
    }

    public void manageDataViews(String dataViewId, Chart chart) {
        if (numberMatrixArea == null) {
            numberMatrixArea = new NumberMatrixArea();
        }
        if (!numberMatrixArea.isVisible()) {
            numberMatrixArea.setVisible(true);
        }

        numberMatrixBox.disconnectWidgetFromAll(numberMatrixArea);

        if ((dataViewId != null) && (!dataViewId.isEmpty())) {
            Item item = itemMap.get(dataViewId);
            IKey key = item.getKey();
            IDataSourceBrowser browser = item.getBrowser();
            if (key != null) {
                DataFormat format = browser.getFormat(key);
                if (format == DataFormat.BOOLEAN) {
                    booleanMatrixBox.connectWidget(numberMatrixArea, key);
                } else {
                    numberMatrixBox.connectWidget(numberMatrixArea, key);
                }
            }
        }
        showMatrixAreaView();
    }

    public void addItem(final Item item) {
        if (item != null) {
            registerItem(item);
            // Add X Key
            Item xItem = item.getXItem();
            registerItem(xItem);
            // Add Y Key
            Item yItem = item.getYItem();
            registerItem(yItem);
        }
    }

    public void removeItem(final Item item) {
        if (item != null) {
            unregisterItem(item);
            // Add X Key
            Item xItem = item.getXItem();
            unregisterItem(xItem);
            // Add Y Key
            Item yItem = item.getYItem();
            unregisterItem(yItem);
        }
    }

    private void registerItem(Item item) {
        if (item != null) {
            IKey key = item.getKey();
            String dataViewId = key.getInformationKey();
            boolean settableSource = isSettableSource(item);
            if (settableSource) {
                itemMap.put(dataViewId, item);
            }
        }
    }

    private void unregisterItem(final Item item) {
        if (item != null) {
            IKey key = item.getKey();
            String dataViewId = key.getInformationKey();
            itemMap.remove(dataViewId);
        }
    }

    @Override
    public Component add(Component c) {
        Component comp = c;

        if (c instanceof JMenuItem) {
            JMenuItem origMenu = (JMenuItem) c;
            String dataViewId = origMenu.getName();
            Item item = itemMap.get(dataViewId);
            if (item != null) {
                comp = super.add(c);
            }
        } else {
            comp = super.add(c);
        }
        return comp;
    }

    public boolean isSettableSource(Item item) {
        IKey key = null;
        boolean settable = false;
        if (item != null) {
            key = item.getKey();
            IDataSourceBrowser browser = item.getBrowser();
            settable = browser.isSettable(key);
        }
        return settable;
    }

    @Override
    public void showDataViewDialog(Chart chart) {
        String result = showSelectDialog(chart, "Select data to write data", false);
        if ((result != null) && (!result.isEmpty())) {
            manageDataViews(result, chart);
        }
    }

    private String showSelectDialog(Chart chart, String dialogTitle, boolean all) {
        JPopupMenu componentPopupMenu = chart.getChartView().getComponentPopupMenu();
        List<DataView> clickableViews = null;
        if (componentPopupMenu instanceof ChartPopupMenu) {
            ChartPopupMenu chartMenu = (ChartPopupMenu) componentPopupMenu;
            clickableViews = chartMenu.getClickableViews();
        }
        SelectDataNumberMatrix dialog = null;
        List<String> settableViews = new ArrayList<>();
        if (clickableViews != null) {
            for (DataView dataview : clickableViews) {
                String id = dataview.getId();
                Item item = itemMap.get(id);
                boolean settableSource = isSettableSource(item);
                if (settableSource) {
                    settableViews.add(id);
                }
            }
        }

        if (dialog == null) {
            dialog = new SelectDataNumberMatrix(SwingUtilities.getWindowAncestor(this), dialogTitle, true, false);
        }
        dialog.updateLists(settableViews);
        return dialog.showOptionDialog(SwingUtilities.getWindowAncestor(this));
    }

    public abstract void showMatrixAreaView();
}
