/*
 * This file is part of databrowser-gui.
 * 
 * databrowser-gui is free software: you can redistribute it and/or modify it under the terms of the GNU General Public
 * License as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later
 * version.
 * 
 * databrowser-gui is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the
 * implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for more
 * details.
 * 
 * You should have received a copy of the GNU General Public License along with databrowser-gui. If not, see
 * <https://www.gnu.org/licenses/>.
 */
package org.cdma.gui.databrowser.util;

import java.io.File;

import javax.swing.JFileChooser;
import javax.swing.JOptionPane;
import javax.swing.filechooser.FileFilter;

import fr.soleil.lib.project.file.FileUtils;

public class SingleFileChooser extends JFileChooser {

    private static final long serialVersionUID = 2084264899293828441L;

    private boolean autoCompleteExtension = true;

    private String confirmTitle = "Confirm save";
    private String confirmMessage = "File \"%1s\" already exists.\nOverwrite it?";

    public SingleFileChooser() {
        setMultiSelectionEnabled(false);
    }

    public boolean isAutoCompleteExtension() {
        return autoCompleteExtension;
    }

    public void setAutoCompleteExtension(final boolean autoCompleteExtension) {
        this.autoCompleteExtension = autoCompleteExtension;
    }

    public void setConfirmTitle(final String confirmTitle) {
        this.confirmTitle = confirmTitle;
    }

    /**
     * Set the confirmation message. Sequence '%1s' will be replaced by the file
     * name.
     * 
     * @param confirmMessage
     */
    public void setConfirmMessage(final String confirmMessage) {
        this.confirmMessage = confirmMessage;
    }

    @Override
    public void approveSelection() {
        if (getDialogType() == SAVE_DIALOG) {
            File selectedFile = getSelectedFile();

            if (autoCompleteExtension) {
                FileFilter fileFilter = getFileFilter();
                if (fileFilter instanceof SoleilFileFilter) {
                    String extension = FileUtils.getExtension(selectedFile);
                    if (extension == null || extension.isEmpty()) {
                        selectedFile = new File(
                                selectedFile.getPath() + "." + ((SoleilFileFilter) fileFilter).getDefaultExtension());
                        setSelectedFile(selectedFile);
                    }
                }
            }

            if (selectedFile.exists()) {
                int result = JOptionPane.showConfirmDialog(this, String.format(confirmMessage, selectedFile.getName()),
                        confirmTitle, JOptionPane.YES_NO_OPTION);
                switch (result) {
                    case JOptionPane.YES_OPTION:
                        super.approveSelection();
                        break;
                    case JOptionPane.NO_OPTION:
                    case JOptionPane.CLOSED_OPTION:
                        break;
                }
                return;
            }
        }
        super.approveSelection();
    }

}
