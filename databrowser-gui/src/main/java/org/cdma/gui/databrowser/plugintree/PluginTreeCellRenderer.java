/*
 * This file is part of databrowser-gui.
 * 
 * databrowser-gui is free software: you can redistribute it and/or modify it under the terms of the GNU General Public
 * License as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later
 * version.
 * 
 * databrowser-gui is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the
 * implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for more
 * details.
 * 
 * You should have received a copy of the GNU General Public License along with databrowser-gui. If not, see
 * <https://www.gnu.org/licenses/>.
 */
package org.cdma.gui.databrowser.plugintree;

import java.awt.Component;

import javax.swing.JLabel;
import javax.swing.JTree;
import javax.swing.tree.DefaultTreeCellRenderer;

import org.cdma.gui.databrowser.DataBrowser;
import org.cdma.gui.databrowser.interfaces.IDataSourceBrowser;

public class PluginTreeCellRenderer extends DefaultTreeCellRenderer {

    private static final long serialVersionUID = -3290268482194387336L;

    @Override
    public Component getTreeCellRendererComponent(final JTree tree, final Object value, final boolean sel,
            final boolean expanded, final boolean leaf, final int row, final boolean hasTheFocus) {
        Component treeCellRendererComponent = super.getTreeCellRendererComponent(tree, value, sel, expanded, leaf, row,
                hasTheFocus);

        if (treeCellRendererComponent instanceof JLabel) {
            JLabel label = (JLabel) treeCellRendererComponent;
            if (value != null) {
                if (value instanceof SourceBrowserNode) {
                    SourceBrowserNode node = (SourceBrowserNode) value;
                    IDataSourceBrowser browser = (IDataSourceBrowser) node.getUserObject();
                    label.setText(browser.getName());
                    label.setIcon(DataBrowser.ICONS.getIcon("PluginTree.Node.Browser"));
                } else if (value instanceof BrowserPluginNode) {
                    BrowserPluginNode node = (BrowserPluginNode) value;
                    IDataSourceBrowser browser = (IDataSourceBrowser) node.getUserObject();
                    label.setText(browser.getId());
                    label.setIcon(DataBrowser.ICONS.getIcon("PluginTree.Node.Plugin"));
                }
            }
        }

        return treeCellRendererComponent;
    }
}
