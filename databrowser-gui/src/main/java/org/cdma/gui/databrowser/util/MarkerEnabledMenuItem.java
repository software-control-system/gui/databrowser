/*
 * This file is part of databrowser-gui.
 * 
 * databrowser-gui is free software: you can redistribute it and/or modify it under the terms of the GNU General Public
 * License as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later
 * version.
 * 
 * databrowser-gui is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the
 * implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for more
 * details.
 * 
 * You should have received a copy of the GNU General Public License along with databrowser-gui. If not, see
 * <https://www.gnu.org/licenses/>.
 */
package org.cdma.gui.databrowser.util;

import java.awt.Dimension;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JCheckBoxMenuItem;
import javax.swing.border.TitledBorder;

import org.cdma.gui.databrowser.DataBrowser;
import org.cdma.gui.databrowser.interfaces.IChartSpectrumManager;

import fr.soleil.comete.awt.util.ColorTool;
import fr.soleil.comete.awt.util.FontTool;
import fr.soleil.comete.definition.data.target.scalar.IBooleanComponent;
import fr.soleil.comete.definition.listener.IMouseListener;
import fr.soleil.comete.definition.widget.util.CometeColor;
import fr.soleil.comete.definition.widget.util.CometeFont;
import fr.soleil.comete.swing.util.EDTManager;
import fr.soleil.comete.swing.util.ErrorNotificationDelegate;
import fr.soleil.data.mediator.Mediator;
import fr.soleil.data.service.thread.IDrawingThreadManager;
import fr.soleil.data.target.TargetDelegate;
import fr.soleil.data.target.information.TargetInformation;

public class MarkerEnabledMenuItem extends JCheckBoxMenuItem
        implements IChartSpectrumManager, IBooleanComponent, ActionListener {

    private static final long serialVersionUID = 5508351223304001331L;

    private static final String TEXT = DataBrowser.MESSAGES.getString("Action.ViewConfig.Markers.Add.Text");
    private static final String TOOLTIP = String
            .format(DataBrowser.MESSAGES.getString("Action.ViewConfig.Markers.Add.Tooltip"), MAX_MARKER_COUNT);

    private final TargetDelegate targetDelegate;
    private final ErrorNotificationDelegate errorNotificationDelegate;

    public MarkerEnabledMenuItem() {
        super(TEXT);
        setToolTipText(TOOLTIP);
        targetDelegate = new TargetDelegate();
        errorNotificationDelegate = new ErrorNotificationDelegate(this);
        addActionListener(this);
    }

    @Override
    public void addMediator(Mediator<?> mediator) {
        targetDelegate.addMediator(mediator);
    }

    @Override
    public void removeMediator(Mediator<?> mediator) {
        targetDelegate.removeMediator(mediator);
    }

    @Override
    public void setCometeBackground(CometeColor color) {
        setBackground(ColorTool.getColor(color));
    }

    @Override
    public CometeColor getCometeBackground() {
        return ColorTool.getCometeColor(getBackground());
    }

    @Override
    public void setCometeForeground(CometeColor color) {
        setForeground(ColorTool.getColor(color));
    }

    @Override
    public CometeColor getCometeForeground() {
        return ColorTool.getCometeColor(getForeground());
    }

    @Override
    public void setCometeFont(CometeFont font) {
        setFont(FontTool.getFont(font));
    }

    @Override
    public CometeFont getCometeFont() {
        return FontTool.getCometeFont(getFont());
    }

    @Override
    public void setPreferredSize(int width, int height) {
        setPreferredSize(new Dimension(width, height));
    }

    @Override
    public boolean isEditingData() {
        return false;
    }

    @Override
    public void setTitledBorder(String title) {
        setBorder(new TitledBorder(title));
    }

    @Override
    public void addMouseListener(IMouseListener listener) {
        // not managed
    }

    @Override
    public void removeMouseListener(IMouseListener listener) {
        // not managed
    }

    @Override
    public void removeAllMouseListeners() {
        // not managed
    }

    @Override
    public void actionPerformed(ActionEvent e) {
        targetDelegate.warnMediators(new TargetInformation<>(this, Boolean.valueOf(isSelected())));
    }

    @Override
    public void notifyForError(String message, Throwable error) {
        errorNotificationDelegate.notifyForError(message, error);
    }

    @Override
    public IDrawingThreadManager getDrawingThreadManager() {
        return EDTManager.INSTANCE;
    }

    @Override
    public boolean shouldReceiveDataInDrawingThread() {
        return true;
    }
}